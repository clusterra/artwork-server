/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.commentary;

import artwork.commentary.domain.comment.Comment;
import artwork.commentary.domain.commentable.Commentable;
import artwork.common.rest.validation.ValidationConfig;
import artwork.iam.MongoConfig;
import artwork.likeable.LikeableConfig;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created on 11/01/2017.
 *
 * @author kepkap
 */
@Configuration
@ComponentScan
@EntityScan(basePackageClasses = {Commentable.class, Comment.class})
@Import({
        MongoConfig.class,
        ValidationConfig.class,
        LikeableConfig.class
})
@EnableMongoRepositories(basePackageClasses = {Commentable.class, Comment.class})
@EnableWebMvc
@EnableHypermediaSupport(type = EnableHypermediaSupport.HypermediaType.HAL)
public class CommentaryConfig {

}
