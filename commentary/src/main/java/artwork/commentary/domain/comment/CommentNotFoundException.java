/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.commentary.domain.comment;

import artwork.commentary.domain.commentable.RelateToId;

/**
 * Created by Denis Kuchugurov
 * on 05/03/16 11:20.
 */
public class CommentNotFoundException extends Exception {
    CommentNotFoundException(CommentId commentId) {
        super(String.format("comment not found by comment id '%s'", commentId));
    }

    CommentNotFoundException(RelateToId relateToId) {
        super(String.format("comment not found by related to id '%s'", relateToId));
    }
}
