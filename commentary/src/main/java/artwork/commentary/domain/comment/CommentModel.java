/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.commentary.domain.comment;

import artwork.commentary.domain.commentable.CommentableId;
import artwork.commentary.domain.commentable.CommentableOwnerId;
import artwork.commentary.domain.commentable.RelateToId;
import artwork.likeable.domain.Likeable;
import artwork.likeable.domain.LikeableModel;
import artwork.likeable.domain.dislike.Dislike;
import artwork.likeable.domain.dislike.Disliker;
import artwork.likeable.domain.dislike.DislikerId;
import artwork.likeable.domain.like.Like;
import artwork.likeable.domain.like.Liker;
import artwork.likeable.domain.like.LikerId;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.annotation.Version;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import static artwork.commentary.domain.comment.CommentDependencies.commentModelRepository;
import static artwork.commentary.domain.comment.CommentDependencies.eventPublisher;

/**
 * commentableId - is an entity that can be commented.
 * In case of hierarchical comments A comment can be commentable too with replies.
 * <p>
 * relateTo - is an entity a comment relates to.
 * parentId - used in n case of hierarchical comments holds a reference to 'parent' comment
 * <p>
 * Created by Denis Kuchugurov
 * on 24.02.2016.
 */
@Document(collection = "comment")
@CompoundIndexes({
        @CompoundIndex(name = "commentableId.createdDate", def = "{ 'commentableId.uuid' : 1, 'createdDate' : -1 }"),
        @CompoundIndex(name = "parentCommentId.createdDate", def = "{ 'parentCommentId.uuid' : 1, 'createdDate' : -1}"),
})
public class CommentModel implements Likeable, Comment {

    @CreatedDate
    @Indexed(direction = IndexDirection.DESCENDING)
    private Date createdDate;

    @LastModifiedDate
    private Date lastModifiedDate;

    @CreatedBy
    private String createdBy;

    @LastModifiedBy
    private String lastModifiedBy;

    @Version
    private Long version;

    @NotNull
    @Valid
    @Indexed
    private CommentableId commentableId;

    @NotEmpty
    private String commentableOwnerUuid;

    @Id
    @Valid
    private CommentId commentId;

    @Valid
    @Indexed
    private RelateToId relateToId;

    private int relateToCommentsCount;

    @Valid
    @Indexed
    private CommentId parentCommentId;

    private int childCommentsCount;

    @NotNull
    @Valid
    private CommentAuthor commentAuthor;

    @NotEmpty
    @Size(max = 4096)
    private String text;

    private boolean isAuthorial;

    private boolean isExpertial;

    private boolean isEdited;

    @NotNull
    @Valid
    private LikeableModel likeable;

    {
        Objects.requireNonNull(commentModelRepository, "check if Spring context is initialized properly");
        Objects.requireNonNull(eventPublisher, "check if Spring context is initialized properly");
    }

    @PersistenceConstructor
    CommentModel() {

    }

    public CommentModel(CommentableId commentableId, CommentableOwnerId commentableOwnerId, CommentId commentId, CommentAuthor commentAuthor, String text, boolean isAuthorial) {
        Objects.requireNonNull(commentableId);
        Objects.requireNonNull(commentId);
        Objects.requireNonNull(commentAuthor);

        this.commentableId = commentableId;
        this.commentableOwnerUuid = commentableOwnerId.getUuid();
        this.commentId = commentId;
        this.commentAuthor = commentAuthor;
        this.text = text;
        this.isAuthorial = isAuthorial;
        this.likeable = new LikeableModel(commentId.toLikeableId(), new ArrayList<>(10), new ArrayList<>(10));
    }

    private CommentModel(CommentableId commentableId, CommentableOwnerId commentableOwnerId, CommentId commentId, CommentAuthor commentAuthor, String text, boolean isAuthorial, CommentId parentCommentId) {
        this(commentableId, commentableOwnerId, commentId, commentAuthor, text, isAuthorial);
        Objects.requireNonNull(parentCommentId);
        this.parentCommentId = parentCommentId;
    }

    public CommentModel(CommentableId commentableId, CommentableOwnerId commentableOwnerId, CommentId commentId, CommentAuthor commentAuthor, String text, boolean isAuthorial, RelateToId relateToId) {
        this(commentableId, commentableOwnerId, commentId, commentAuthor, text, isAuthorial);
        Objects.requireNonNull(relateToId);
        this.relateToId = relateToId;
    }

    public CommentableId getCommentableId() {
        return commentableId;
    }

    public RelateToId getRelateToId() {
        return relateToId;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public boolean isAuthorial() {
        return isAuthorial;
    }

    @Override
    public CommentAuthor getCommentAuthor() {
        return commentAuthor;
    }

    @Override
    public int getLikesCount() {
        return likeable.getLikesCount();
    }

    @Override
    public int getDislikesCount() {
        return likeable.getDislikesCount();
    }

    @Override
    public boolean like(Liker liker) {
        boolean modified = likeable.like(liker);
        if (modified) {
            commentModelRepository.save(this);
            eventPublisher.publishEvent(new CommentLikedEvent(this, liker));
        }
        return modified;
    }

    @Override
    public boolean unlike(Liker liker) {
        boolean modified = likeable.unlike(liker);
        if (modified) {
            commentModelRepository.save(this);
            eventPublisher.publishEvent(new CommentUnlikedEvent(this, liker));
        }
        return modified;
    }

    @Override
    public boolean dislike(Disliker disliker) {
        boolean modified = likeable.dislike(disliker);
        if (modified) {
            commentModelRepository.save(this);
        }
        return modified;
    }

    @Override
    public boolean undislike(Disliker disliker) {
        boolean modified = likeable.undislike(disliker);
        if (modified) {
            commentModelRepository.save(this);
        }
        return modified;
    }

    @Override
    public boolean alreadyLiked(LikerId likerId) {
        return likeable.alreadyLiked(likerId);
    }

    @Override
    public boolean alreadyDisliked(DislikerId dislikerId) {
        return likeable.alreadyDisliked(dislikerId);
    }

    @Override
    public Page<Like> findLikes(Pageable pageable) {
        return likeable.findLikes(pageable);
    }

    @Override
    public Page<Dislike> findDislikes(Pageable pageable) {
        return likeable.findDislikes(pageable);
    }

    @Override
    public List<Liker> getLast10Likes() {
        return likeable.getLast10Likes();
    }

    @Override
    public List<Disliker> getLast10Dislikes() {
        return likeable.getLast10Dislikes();
    }

    @Override
    public void deleteText(CommentAuthor commentAuthor) throws IllegalCommentAuthorOperationException {
        updateText(commentAuthor, "deleted");
        CommentDependencies.eventPublisher.publishEvent(new CommentTextDeletedEvent(this));
    }

    @Override
    public int getScore() {
        return likeable.getLikesCount() - likeable.getDislikesCount();
    }

    @Override
    public void updateText(CommentAuthor commentAuthor, String text) throws IllegalCommentAuthorOperationException {
        Objects.requireNonNull(commentAuthor);
        if (!this.commentAuthor.getUuid().equals(commentAuthor.getUuid())) {
            throw new IllegalCommentAuthorOperationException(getCommentId());
        }
        if (this.text.equals(text)) {
            return;
        }
        this.text = text;
        this.isEdited = true;
        commentModelRepository.save(this);
        eventPublisher.publishEvent(new CommentTextUpdatedEvent(this));
    }

    @Override
    public boolean hasRelateTo() {
        return relateToId != null;
    }

    @Override
    public int getRelateToCommentsCount() {
        return relateToCommentsCount;
    }

    @Override
    public int getChildCommentsCount() {
        return childCommentsCount;
    }

    @Override
    public void incrementRelateToCommentCount() {
        relateToCommentsCount++;
        commentModelRepository.save(this);
    }

    @Override
    public void removeRelateTo() throws IllegalCommentAuthorOperationException {
        Objects.requireNonNull(relateToId, "relateToId is null");
        relateToId = null;
        relateToCommentsCount = 0;
        updateText(commentAuthor, text + " (removed)");
    }

    @Override
    public boolean isMine(CommentAuthorId commentAuthorId) {
        Objects.requireNonNull(commentAuthorId);
        return this.commentAuthor.getUuid().equals(commentAuthorId.getUuid());
    }

    @Override
    public Comment addComment(CommentAuthor commentAuthor, String text) {
        CommentModel commentModel = new CommentModel(getCommentableId(), getCommentableOwnerId(), CommentId.generateId(), commentAuthor, text, isAuthorial, getCommentId());
        commentModelRepository.save(commentModel);
        eventPublisher.publishEvent(new ChildCommentCreatedEvent(this, commentModel, commentAuthor));

        childCommentsCount++;
        commentModelRepository.save(this);

        return commentModel;
    }

    public CommentId getParentCommentId() {
        return parentCommentId;
    }


    @Override
    public Date getCreatedDate() {
        return createdDate;
    }

    @Override
    public String getCreatedBy() {
        return createdBy;
    }

    @Override
    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    @Override
    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    @Override
    public Long getVersion() {
        return version;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CommentModel that = (CommentModel) o;

        return commentId != null ? commentId.equals(that.commentId) : that.commentId == null;
    }

    @Override
    public int hashCode() {
        return commentId != null ? commentId.hashCode() : 0;
    }

    public CommentId getCommentId() {
        return commentId;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean isEdited() {
        return isEdited;
    }

    public CommentableOwnerId getCommentableOwnerId() {
        return new CommentableOwnerId(commentableOwnerUuid);
    }

}
