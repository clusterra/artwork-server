/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.commentary.domain.comment;

import artwork.commentary.domain.commentable.CommentableId;
import artwork.commentary.domain.commentable.CommentableOwnerId;
import artwork.commentary.domain.commentable.RelateToId;
import artwork.likeable.domain.Likeable;

import java.util.Date;

/**
 * Created on 11/01/2017.
 *
 * @author kepkap
 */
public interface Comment extends Likeable {

    boolean isMine(CommentAuthorId commentAuthorId);

    Comment addComment(CommentAuthor commentAuthor, String text);

    void updateText(CommentAuthor commentAuthor, String text) throws IllegalCommentAuthorOperationException;

    void deleteText(CommentAuthor commentAuthor) throws IllegalCommentAuthorOperationException;

    CommentableId getCommentableId();

    CommentableOwnerId getCommentableOwnerId();

    CommentId getCommentId();

    RelateToId getRelateToId();

    String getText();

    boolean isAuthorial();

    boolean isEdited();

    CommentAuthor getCommentAuthor();

    int getScore();

    boolean hasRelateTo();

    int getRelateToCommentsCount();

    int getChildCommentsCount();

    void incrementRelateToCommentCount();

    void removeRelateTo() throws IllegalCommentAuthorOperationException;

    CommentId getParentCommentId();

    Date getCreatedDate();

    String getCreatedBy();

    Date getLastModifiedDate();

    String getLastModifiedBy();

    Long getVersion();
}
