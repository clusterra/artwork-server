/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.commentary.domain.commentable;

import artwork.iam.user.model.UserId;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.Objects;

/**
 * Created by Denis Kuchugurov
 * on 25.02.2016.
 */
public class CommentableOwnerId {

    @NotEmpty
    private String uuid;

    public CommentableOwnerId() {
    }

    public CommentableOwnerId(String uuid) {
        Objects.requireNonNull(uuid);
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public String toString() {
        return uuid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CommentableOwnerId commenterId = (CommentableOwnerId) o;

        return uuid.equals(commenterId.uuid);
    }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }

    public RelateToOwnerId toRelateToOwnerId() {
        return new RelateToOwnerId(uuid);
    }

    public UserId toUserId() {
        return new UserId(uuid);
    }

    public static CommentableOwnerId fromUserId(UserId userId) {
        return new CommentableOwnerId(userId.getId());
    }
}
