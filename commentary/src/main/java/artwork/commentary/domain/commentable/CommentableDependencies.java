/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.commentary.domain.commentable;

import artwork.commentary.domain.comment.CommentModelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * Created by Denis Kuchugurov
 * on 13/03/16 23:54.
 */
@Component
class CommentableDependencies {


    static CommentableModelRepository commentableModelRepository;
    static CommentModelRepository commentModelRepository;
    static ApplicationEventPublisher eventPublisher;


    @Autowired
    public CommentableDependencies(CommentableModelRepository commentableModelRepository,
                                   CommentModelRepository commentModelRepository,
                                   ApplicationEventPublisher eventPublisher) {
        CommentableDependencies.commentableModelRepository = commentableModelRepository;
        CommentableDependencies.commentModelRepository = commentModelRepository;
        CommentableDependencies.eventPublisher = eventPublisher;
    }

    @Bean
    CommentableEventListener commentableEventListener() {
        return new CommentableEventListener();
    }

}
