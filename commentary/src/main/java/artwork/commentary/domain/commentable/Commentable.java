/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.commentary.domain.commentable;

import artwork.commentary.domain.comment.Comment;
import artwork.commentary.domain.comment.CommentAuthor;
import artwork.commentary.domain.comment.CommentId;
import artwork.commentary.domain.comment.CommentNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Date;


/**
 * Created on 11/01/2017.
 *
 * @author kepkap
 */
public interface Commentable {

    static Commentable findOne(CommentableId commentableId) throws CommentableNotFoundException {
        Commentable commentable = CommentableDependencies.commentableModelRepository.findOne(commentableId);
        if (commentable == null) {
            throw new CommentableNotFoundException(commentableId);
        }
        return commentable;
    }

    Page<Comment> findComments(Pageable pageable);

    Page<Comment> findComments(CommentId parentCommentId, Pageable pageable);

    Comment findOne(CommentId commentId) throws CommentNotFoundException;

    Comment findOne(RelateToId relateToId) throws CommentNotFoundException;

    Comment addComment(CommentAuthor commentAuthor, String text);

    Comment addComment(CommentAuthor commentAuthor, String text, RelateToId relateToId);

    CommentableId getCommentableId();

    RelateTo getRelateTo();

    CommentableOwnerId getCommentableOwnerId();

    void incrementCommentsCount();

    int getCommentsCount();

    Date getCreatedDate();

    String getCreatedBy();

    Date getLastModifiedDate();

    String getLastModifiedBy();

    Long getVersion();
}
