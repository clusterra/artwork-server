/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.commentary.domain.commentable;

import artwork.commentary.domain.comment.ChildCommentCreatedEvent;
import artwork.commentary.domain.comment.Comment;
import artwork.commentary.domain.comment.CommentCreatedEvent;
import artwork.commentary.domain.comment.CommentNotFoundException;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * Created by kepkap
 * on 10/09/16.
 */
@Component
class CommentableEventListener {

    @EventListener
    void incrementCommentsCount(ChildCommentCreatedEvent event) throws CommentableNotFoundException {

        Comment parent = event.getParent();

        Commentable commentable = Commentable.findOne(parent.getCommentableId());

        commentable.incrementCommentsCount();

    }


    @EventListener
    void incrementRelateToCommentCount(CommentCreatedEvent event) throws CommentableNotFoundException, CommentNotFoundException {
        Comment comment = event.getComment();

        CommentableId commentableId = comment.getCommentableId();

        Commentable commentable = Commentable.findOne(commentableId);

        if (commentable.getRelateTo() != null) {
            Commentable relateTo = Commentable.findOne(commentable.getRelateTo().getRelateToId().toCommentableId());
            Comment comment1 = relateTo.findOne(commentableId.toRelateToId());
            comment1.incrementRelateToCommentCount();
        }
    }

}
