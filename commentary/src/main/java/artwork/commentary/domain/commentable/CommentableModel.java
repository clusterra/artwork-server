/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.commentary.domain.commentable;

import artwork.commentary.domain.comment.Comment;
import artwork.commentary.domain.comment.CommentAuthor;
import artwork.commentary.domain.comment.CommentCreatedEvent;
import artwork.commentary.domain.comment.CommentId;
import artwork.commentary.domain.comment.CommentModel;
import artwork.commentary.domain.comment.CommentNotFoundException;
import artwork.commentary.domain.comment.CommentWithRelateToCreatedEvent;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.annotation.Version;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Objects;


/**
 * Created by kepkap
 * on 10/06/16.
 */
@Document(collection = "commentable")
public class CommentableModel implements Commentable {

    @CreatedDate
    private Date createdDate;

    @LastModifiedDate
    private Date lastModifiedDate;

    @CreatedBy
    private String createdBy;

    @LastModifiedBy
    private String lastModifiedBy;

    @Version
    private Long version;

    @Id
    private CommentableId commentableId;

    @NotNull
    @Valid
    private CommentableOwnerId commentableOwnerId;

    private int commentsCount;

    private RelateTo relateTo;


    {
        Objects.requireNonNull(CommentableDependencies.commentableModelRepository, "check if Spring context is initialized properly");
    }

    @PersistenceConstructor
    CommentableModel() {
    }

    public CommentableModel(CommentableId commentableId, CommentableOwnerId commentableOwnerId) {
        Objects.requireNonNull(commentableId);
        Objects.requireNonNull(commentableOwnerId);
        this.commentableId = commentableId;
        this.commentableOwnerId = commentableOwnerId;
        CommentableDependencies.commentableModelRepository.save(this);
        CommentableDependencies.eventPublisher.publishEvent(new CommentableCreatedEvent(this));
    }

    public CommentableModel(CommentableId commentableId, RelateTo relateTo, CommentableOwnerId commentableOwnerId) {
        Objects.requireNonNull(commentableId);
        Objects.requireNonNull(relateTo);
        Objects.requireNonNull(commentableOwnerId);
        this.commentableId = commentableId;
        this.relateTo = relateTo;
        this.commentableOwnerId = commentableOwnerId;
        CommentableDependencies.commentableModelRepository.save(this);
        CommentableDependencies.eventPublisher.publishEvent(new CommentableCreatedEvent(this));
    }

    @Override
    public CommentableId getCommentableId() {
        return commentableId;
    }

    public RelateTo getRelateTo() {
        return relateTo;
    }

    @Override
    public Comment addComment(CommentAuthor commentAuthor, String text) {
        Objects.requireNonNull(commentAuthor);
        boolean isAuthorial = commentableOwnerId.getUuid().equals(commentAuthor.getUuid());
        CommentModel commentModel = new CommentModel(getCommentableId(), getCommentableOwnerId(), CommentId.generateId(), commentAuthor, text, isAuthorial);
        CommentableDependencies.commentModelRepository.save(commentModel);
        CommentableDependencies.eventPublisher.publishEvent(new CommentCreatedEvent(this, commentModel));
        incrementCommentsCount();
        return commentModel;
    }

    @Override
    public Comment addComment(CommentAuthor commentAuthor, String text, RelateToId relateToId) {
        Objects.requireNonNull(commentAuthor);
        boolean isAuthorial = commentableOwnerId.getUuid().equals(commentAuthor.getUuid());
        CommentModel commentModel = new CommentModel(getCommentableId(), getCommentableOwnerId(), CommentId.generateId(), commentAuthor, text, isAuthorial, relateToId);
        CommentableDependencies.commentModelRepository.save(commentModel);
        CommentableDependencies.eventPublisher.publishEvent(new CommentWithRelateToCreatedEvent(commentModel, relateToId));
        incrementCommentsCount();
        return commentModel;
    }

    @Override
    public Page<Comment> findComments(Pageable pageable) {
        return CommentableDependencies.commentModelRepository.findByCommentableId(commentableId, pageable);
    }

    public Page<Comment> findComments(CommentId parentCommentId, Pageable pageable) {
        return CommentableDependencies.commentModelRepository.findByCommentableIdAndParentCommentId(commentableId, parentCommentId, pageable);
    }

    public Comment findOne(RelateToId relateToId) throws CommentNotFoundException {
        return CommentableDependencies.commentModelRepository.findByCommentableIdAndRelateToId(commentableId, relateToId);
    }

    public Comment findOne(CommentId commentId) throws CommentNotFoundException {
        return CommentableDependencies.commentModelRepository.findOne(commentId);
    }

    @Override
    public void incrementCommentsCount() {
        commentsCount++;
        CommentableDependencies.commentableModelRepository.save(this);
    }

    @Override
    public int getCommentsCount() {
        return commentsCount;
    }

    @Override
    public Date getCreatedDate() {
        return createdDate;
    }

    @Override
    public String getCreatedBy() {
        return createdBy;
    }

    @Override
    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    @Override
    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    @Override
    public Long getVersion() {
        return version;
    }

    @Override
    public CommentableOwnerId getCommentableOwnerId() {
        return commentableOwnerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CommentableModel that = (CommentableModel) o;

        return commentableId.equals(that.commentableId);
    }

    @Override
    public int hashCode() {
        return commentableId.hashCode();
    }

}
