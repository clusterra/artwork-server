/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.commentary.rest;

import artwork.commentary.domain.comment.CommentAuthor;
import artwork.commentary.domain.comment.CommentAuthorId;
import artwork.likeable.domain.dislike.Disliker;
import artwork.likeable.domain.like.Liker;
import artwork.likeable.domain.like.LikerId;
import artwork.web.security.tracking.CurrentUser;

/**
 * Created on 26/01/2017.
 *
 * @author kepkap
 */
class CurrentCommentUser {


    static Liker asLiker() {
        CurrentUser currentUser = CurrentUser.requireNotAnonymous();
        return new Liker(currentUser.getUuid(), currentUser.getDisplayName(), currentUser.getImageUrl(), currentUser.getProfileUrl());
    }

    static Disliker asDisliker() {
        CurrentUser currentUser = CurrentUser.requireNotAnonymous();
        return new Disliker(currentUser.getUuid(), currentUser.getDisplayName(), currentUser.getImageUrl(), currentUser.getProfileUrl());
    }

    static CommentAuthor asCommentAuthor() {
        CurrentUser currentUser = CurrentUser.requireNotAnonymous();
        return new CommentAuthor(currentUser.getUuid(), currentUser.getDisplayName(), currentUser.getImageUrl(), currentUser.getProfileUrl());
    }

    static CommentAuthorId asCommentAuthorId() {
        CurrentUser any = CurrentUser.requireAny();
        return new CommentAuthorId(any.getUuid());
    }

    static LikerId asLikerId() {
        CurrentUser any = CurrentUser.requireAny();
        return new LikerId(any.getUuid());
    }
}
