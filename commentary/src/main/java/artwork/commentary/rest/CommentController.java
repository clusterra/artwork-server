/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.commentary.rest;

import artwork.commentary.domain.comment.Comment;
import artwork.commentary.domain.comment.CommentId;
import artwork.commentary.domain.comment.CommentNotFoundException;
import artwork.commentary.domain.comment.IllegalCommentAuthorOperationException;
import artwork.commentary.domain.commentable.Commentable;
import artwork.commentary.domain.commentable.CommentableId;
import artwork.commentary.domain.commentable.CommentableNotFoundException;
import artwork.common.rest.validation.error.Error;
import artwork.common.rest.validation.error.ErrorsResponse;
import artwork.common.rest.validation.error.ModelError;
import artwork.likeable.domain.dislike.Dislike;
import artwork.likeable.domain.dislike.Disliker;
import artwork.likeable.domain.like.Like;
import artwork.likeable.domain.like.Liker;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mongodb.MongoException;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Denis Kuchugurov
 * on 14/03/16 22:38.
 */
@RestController
@RequestMapping(value = "/api/commentable/{commentableId}/comments", produces = MediaTypes.HAL_JSON_VALUE)
public class CommentController {
    private static final Logger log = LoggerFactory.getLogger(CommentController.class);

    private final CommentResourceAssembler commentResourceAssembler;

    public CommentController(CommentResourceAssembler commentResourceAssembler) {
        this.commentResourceAssembler = commentResourceAssembler;
    }


    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<CommentResource> create(@PathVariable("commentableId") CommentableId commentableId,
                                                  @Valid @RequestBody CommentRequest commentRequest) throws CommentableNotFoundException {


        Commentable commentable = Commentable.findOne(commentableId);
        Comment comment = commentable.addComment(CurrentCommentUser.asCommentAuthor(), commentRequest.text);
        return new ResponseEntity<>(commentResourceAssembler.toResource(comment), HttpStatus.CREATED);
    }

    private static class CommentRequest {

        @JsonProperty
        @NotEmpty
        @Size(max = 4096)
        public String text;

        CommentRequest() {
        }

        public CommentRequest(String text) {
            this.text = text;
        }
    }

    @RequestMapping(value = "/{commentId}", method = RequestMethod.DELETE)
    public ResponseEntity<CommentResource> delete(
            @PathVariable("commentableId") CommentableId commentableId,
            @PathVariable("commentId") CommentId commentId) throws IllegalCommentAuthorOperationException, CommentableNotFoundException, CommentNotFoundException {


        Commentable commentable = Commentable.findOne(commentableId);

        Comment comment = commentable.findOne(commentId);
        comment.deleteText(CurrentCommentUser.asCommentAuthor());
        return new ResponseEntity<>(commentResourceAssembler.toResource(comment), HttpStatus.OK);
    }

    @RequestMapping(value = "/{commentId}", method = RequestMethod.PUT)
    public ResponseEntity<CommentResource> update(
            @PathVariable("commentableId") CommentableId commentableId,
            @PathVariable("commentId") CommentId commentId,
            @RequestParam(required = false) String text,
            @RequestParam(required = false) Boolean like,
            @RequestParam(required = false) Boolean dislike) throws CommentableNotFoundException, CommentNotFoundException, IllegalCommentAuthorOperationException {

        Commentable commentable = Commentable.findOne(commentableId);

        Comment comment = commentable.findOne(commentId);

        if (like != null) {
            Liker liker = CurrentCommentUser.asLiker();
            if (like) {
                comment.like(liker);
            } else {
                comment.unlike(liker);
            }
        }

        if (dislike != null) {
            Disliker disliker = CurrentCommentUser.asDisliker();
            if (dislike) {
                comment.dislike(disliker);
            } else {
                comment.undislike(disliker);
            }
        }

        if (text != null) {
            comment.updateText(CurrentCommentUser.asCommentAuthor(), text);
        }

        return new ResponseEntity<>(commentResourceAssembler.toResource(comment), HttpStatus.OK);
    }

    @RequestMapping(value = "/{commentId}", method = RequestMethod.GET)
    public ResponseEntity<CommentResource> get(@PathVariable("commentableId") CommentableId commentableId,
                                               @PathVariable("commentId") CommentId commentId) throws CommentableNotFoundException, CommentNotFoundException {

        Commentable commentable = Commentable.findOne(commentableId);

        Comment comment = commentable.findOne(commentId);
        return new ResponseEntity<>(commentResourceAssembler.toResource(comment), HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<PagedResources<CommentResource>> getAll(@PathVariable("commentableId") CommentableId commentableId,
                                                                  @PageableDefault Pageable pageable,
                                                                  PagedResourcesAssembler<Comment> pagedResourcesAssembler) throws CommentableNotFoundException {

        Commentable commentable = Commentable.findOne(commentableId);
        Page<Comment> page = commentable.findComments(pageable);
        return new ResponseEntity<>(pagedResourcesAssembler.toResource(page, commentResourceAssembler), HttpStatus.OK);
    }

    @RequestMapping(value = "/{commentId}/liked", method = RequestMethod.GET)
    public ResponseEntity<List<CommentAuthorJson>> getAuthorsWhoLiked(
            @PathVariable("commentableId") CommentableId commentableId,
            @PathVariable("commentId") CommentId commentId,
            @PageableDefault Pageable pageable) throws CommentableNotFoundException, CommentNotFoundException {

        Commentable commentable = Commentable.findOne(commentableId);

        Comment comment = commentable.findOne(commentId);
        Page<Like> liked = comment.findLikes(pageable);
        return new ResponseEntity<>(liked.getContent().stream()
                .map(Like::getLiker)
                .map(liker -> new CommentAuthorJson(
                        liker.getUuid(),
                        liker.getDisplayName(),
                        liker.getImageUrl(),
                        liker.getProfileUrl()
                ))
                .collect(Collectors.toList()), HttpStatus.OK);
    }

    @RequestMapping(value = "/{commentId}/disliked", method = RequestMethod.GET)
    public ResponseEntity<List<Disliker>> getAuthorsWhoDisliked(
            @PathVariable("commentableId") CommentableId commentableId,
            @PathVariable("commentId") CommentId commentId,
            @PageableDefault Pageable pageable) throws CommentableNotFoundException, CommentNotFoundException {

        Commentable commentable = Commentable.findOne(commentableId);

        Comment comment = commentable.findOne(commentId);
        Page<Dislike> disliked = comment.findDislikes(pageable);
        return new ResponseEntity<>(disliked.getContent().stream().map(Dislike::getDisliker).collect(Collectors.toList()), HttpStatus.OK);
    }

    public static Object getExampleBody(Class target) {
        if (target == CommentRequest.class) {
            return new CommentRequest("Cool work!");
        }
        return null;
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorsResponse handle(CommentNotFoundException e) {
        log.trace(e.getMessage(), e);
        return new ErrorsResponse(new ModelError("comment_not_found", e.getMessage()));
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ResponseBody
    public ErrorsResponse handle(IllegalCommentAuthorOperationException e) {
        log.trace(e.getMessage(), e);
        return new ErrorsResponse(new ModelError("illegal_comment_owner_operation", e.getMessage()));
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorsResponse handle(CommentableNotFoundException e) {
        log.trace(e.getMessage(), e);
        return new ErrorsResponse(new ModelError("commentable_not_found", e.getMessage()));
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ErrorsResponse handle(MongoException e) {
        log.trace(e.getMessage(), e);
        return new ErrorsResponse(new Error("data_storage_error", e.getMessage(), Collections.emptyList()));
    }

}
