/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.commentary.rest;

import org.springframework.hateoas.ResourceSupport;

import java.util.Date;

/**
 * Created by Denis Kuchugurov
 * on 18.02.2016.
 */
class CommentResource extends ResourceSupport {

    public final String commentId;

    public final CommentAuthorJson author;

    public final String text;

    public final int likesCount;

    public final boolean alreadyLiked;

    public final Date createdDate;

    public final boolean isMine;

    public final boolean isEdited;

    public final boolean isAuthorial;

    public final int suggestionCommentsCount;

    public final boolean relatesToSuggestion;

    public CommentResource(String commentId, CommentAuthorJson author, String text, int likesCount, boolean alreadyLiked, Date createdDate, boolean isMine, boolean isEdited, boolean isAuthorial, int suggestionCommentsCount, boolean relatesToSuggestion) {
        this.commentId = commentId;
        this.author = author;
        this.text = text;
        this.likesCount = likesCount;
        this.alreadyLiked = alreadyLiked;
        this.createdDate = createdDate;
        this.isMine = isMine;
        this.isEdited = isEdited;
        this.isAuthorial = isAuthorial;
        this.suggestionCommentsCount = suggestionCommentsCount;
        this.relatesToSuggestion = relatesToSuggestion;
    }

}
