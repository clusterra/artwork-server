/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.commentary.rest;

import artwork.commentary.domain.commentable.Commentable;
import artwork.commentary.domain.commentable.CommentableId;
import artwork.commentary.domain.commentable.CommentableNotFoundException;
import artwork.common.rest.validation.error.Error;
import artwork.common.rest.validation.error.ErrorsResponse;
import artwork.common.rest.validation.error.ModelError;
import com.mongodb.MongoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;

/**
 * Created on 19/01/2017.
 *
 * @author kepkap
 */
@RestController
@RequestMapping(value = "/api/commentable/{commentableId}", produces = MediaTypes.HAL_JSON_VALUE)
public class CommentableController {

    private static final Logger log = LoggerFactory.getLogger(CommentableController.class);


    private final CommentableResourceAssembler assembler;

    public CommentableController(CommentableResourceAssembler assembler) {
        this.assembler = assembler;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<CommentableResource> get(@PathVariable("commentableId") CommentableId commentableId) throws CommentableNotFoundException {

        Commentable commentable = Commentable.findOne(commentableId);

        return new ResponseEntity<>(assembler.toResource(commentable), HttpStatus.OK);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorsResponse handle(CommentableNotFoundException e) {
        log.trace(e.getMessage(), e);
        return new ErrorsResponse(new ModelError("commentable_not_found", e.getMessage()));
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorsResponse handle(MongoException e) {
        log.trace(e.getMessage(), e);
        return new ErrorsResponse(new Error("data_storage_error", e.getMessage(), Collections.emptyList()));
    }

}
