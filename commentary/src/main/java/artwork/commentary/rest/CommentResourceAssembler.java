/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.commentary.rest;

import artwork.commentary.domain.comment.Comment;
import artwork.commentary.domain.comment.CommentAuthor;
import artwork.likeable.domain.like.LikerId;
import artwork.web.security.tracking.CurrentUser;
import org.springframework.data.domain.PageRequest;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by Denis Kuchugurov
 * on 18.02.2016.
 */
@Component
class CommentResourceAssembler extends ResourceAssemblerSupport<Comment, CommentResource> {

    public CommentResourceAssembler() {
        super(CommentController.class, CommentResource.class);
    }

    @Override
    public CommentResource toResource(Comment entity) {


        try {

            LikerId likerId = CurrentCommentUser.asLikerId();
            CommentAuthor commentAuthor = entity.getCommentAuthor();
            CommentResource resource = new CommentResource(
                    entity.getCommentId().getUuid(),
                    new CommentAuthorJson(
                            commentAuthor.getUuid(),
                            commentAuthor.getDisplayName(),
                            commentAuthor.getImageUrl(),
                            commentAuthor.getProfileUrl()
                    ),
                    entity.getText(),
                    entity.getLikesCount(),
                    entity.alreadyLiked(likerId),
                    entity.getCreatedDate(),
                    entity.isMine(CurrentCommentUser.asCommentAuthorId()),
                    entity.isEdited(),
                    entity.isAuthorial(),
                    entity.getRelateToCommentsCount(),
                    entity.hasRelateTo()
            );
            resource.add(linkTo(methodOn(CommentController.class).get(entity.getCommentableId(), entity.getCommentId())).withSelfRel());

            if (!CurrentUser.isAnonymous() && !entity.alreadyLiked(likerId)) {
                resource.add(linkTo(methodOn(CommentController.class).update(entity.getCommentableId(), entity.getCommentId(), null, true, null)).withRel("like"));
            }

            if (!CurrentUser.isAnonymous() && entity.alreadyLiked(likerId)) {
                resource.add(linkTo(methodOn(CommentController.class).update(entity.getCommentableId(), entity.getCommentId(), null, false, null)).withRel("unlike"));
            }

            resource.add(linkTo(methodOn(CommentController.class).update(entity.getCommentableId(), entity.getCommentId(), "", null, null)).withRel("update-text"));
            resource.add(linkTo(methodOn(CommentController.class).getAuthorsWhoLiked(entity.getCommentableId(), entity.getCommentId(), new PageRequest(0, 20))).withRel("who-liked"));
            resource.add(linkTo(methodOn(CommentController.class).getAuthorsWhoDisliked(entity.getCommentableId(), entity.getCommentId(), new PageRequest(0, 20))).withRel("who-disliked"));

            if (entity.hasRelateTo()) {
                resource.add(ControllerLinkBuilder.linkTo(methodOn(CommentableController.class).get(entity.getRelateToId().toCommentableId())).withRel("relate-to"));
            }
            resource.add(ControllerLinkBuilder.linkTo(methodOn(CommentableController.class).get(entity.getCommentableId())).withRel("commentable"));


            return resource;
        } catch (Exception e) {
            throw new IllegalStateException("should never happen...", e);
        }
    }
}
