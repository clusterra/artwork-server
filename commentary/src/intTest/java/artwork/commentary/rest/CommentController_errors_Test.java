/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.commentary.rest;

import artwork.commentary.TestCommentaryConfig;
import artwork.commentary.domain.commentable.Commentable;
import artwork.commentary.domain.commentable.CommentableId;
import artwork.commentary.domain.commentable.CommentableModel;
import artwork.commentary.domain.commentable.CommentableOwnerId;
import artwork.web.security.social.UserDetails;
import com.jayway.jsonpath.JsonPath;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static com.jayway.jsonpath.JsonPath.read;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Denis Kuchugurov
 * on 11.08.2016.
 */
@SpringBootTest(classes = {TestCommentaryConfig.class})
public class CommentController_errors_Test extends AbstractTestNGSpringContextTests {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    private Authentication authentication_1;
    private Authentication authentication_2;
    private AnonymousAuthenticationToken anonymousAuthentication;


    private String comments_href;


    @BeforeMethod
    public void setup() throws Exception {

        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_SOCIAL_USER");
        authentication_1 = new TestingAuthenticationToken(UserDetails.from("user_1"), "", authorities);
        authentication_2 = new TestingAuthenticationToken(UserDetails.from("user_2"), "", authorities);
        anonymousAuthentication = new AnonymousAuthenticationToken("TEST_ANONYMOUS_KEY", "anonymousUser", AuthorityUtils.createAuthorityList("ROLE_ANONYMOUS"));
        authentication_1.setAuthenticated(true);
        authentication_2.setAuthenticated(true);
        anonymousAuthentication.setAuthenticated(true);


        Commentable commentable = new CommentableModel(CommentableId.generateId(), new CommentableOwnerId("commentable_controller_test"));

        MvcResult commentableResult = mvc.perform(get("/api/commentable/{commentableId}", commentable.getCommentableId().getUuid())
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("commentableId").exists())
                .andExpect(jsonPath("commentableId").isString())
                .andExpect(jsonPath("commentsCount", equalTo(0)))
                .andExpect(jsonPath("relateToId", nullValue()))
                .andReturn();

        String commentableBody = commentableResult.getResponse().getContentAsString();

        comments_href = read(commentableBody, "_links.comments.href");

    }

    @Test
    public void test_text_blank() throws Exception {
        String jsonWithBlankText = "{\"text\": \"\"}";

        MvcResult result = this.mvc.perform(post(comments_href)
                .with(authentication(authentication_1))
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonWithBlankText))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", notNullValue()))
                .andExpect(jsonPath("$.errors[0].property", equalTo("text")))
                .andExpect(jsonPath("$.errors[0].code").value("NotEmpty"))
                .andExpect(jsonPath("$.errors[0].message").value(containsString("may not be empty")))
                .andExpect(jsonPath("$.message").value(containsString("validation failed with 1 error(s)")))

                .andExpect(jsonPath("exampleBody", notNullValue()))
                .andExpect(jsonPath("message", notNullValue()))
                .andReturn();

    }

    @Test
    public void test_text_too_long() throws Exception {
        String jsonWithBlankText = "{\"text\": \""+ StringUtils.repeat('A', 4097) +"\"}";

        MvcResult result = this.mvc.perform(post(comments_href)
                .with(authentication(authentication_1))
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonWithBlankText))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", notNullValue()))
                .andExpect(jsonPath("$.errors[0].property", equalTo("text")))
                .andExpect(jsonPath("$.errors[0].code").value("Size"))

                .andExpect(jsonPath("exampleBody", notNullValue()))
                .andExpect(jsonPath("message", notNullValue()))
                .andReturn();

    }

    @Test
    public void test_post_comment_wrong_commentable_id() throws Exception {
        String jsonWithBlankText = "{\"text\": \"valid\"}";

        MvcResult result = this.mvc.perform(post("/api/commentable/{id}/comments", "wrong_id_123")
                .with(authentication(authentication_1))
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonWithBlankText))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", notNullValue()))
                .andExpect(jsonPath("$.errors[0].code").value("commentable_not_found"))
                .andExpect(jsonPath("$.errors[0].property").doesNotExist())
                .andExpect(jsonPath("$.errors[0].message").value(containsString("commentable not found by")))
                .andExpect(jsonPath("$.message").value(containsString("validation failed with 1 error(s)")))


                .andReturn();
    }

    @Test
    public void test_get_wrong_commentable_id() throws Exception {

        MvcResult result = this.mvc.perform(get("/api/commentable/{id}", "wrong_id_123")
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", notNullValue()))
                .andExpect(jsonPath("$.errors[0].code").value("commentable_not_found"))
                .andExpect(jsonPath("$.errors[0].property").doesNotExist())
                .andExpect(jsonPath("$.errors[0].message").value(containsString("commentable not found by")))
                .andReturn();
    }

    @Test
    public void test_illegal_comment_owner_operation_1() throws Exception {

        String testCommentText = "some test text";

        MvcResult commentResult = mvc.perform(
                post(comments_href)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"text\":\"" + testCommentText + "\"}")
                        .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("commentId").exists())
                .andExpect(jsonPath("commentId").isString())
                .andReturn();

        String commentBody = commentResult.getResponse().getContentAsString();

        String comment_self_href = JsonPath.read(commentBody, "_links.self.href");

        mvc.perform(get(comment_self_href)
                .with(authentication(authentication_2)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("text", equalTo(testCommentText)))
                .andReturn();


        String newText = "new text";

        mvc.perform(put(comment_self_href).param("text", newText)
                .with(authentication(authentication_2)))
                .andDo(print())
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.errors", notNullValue()))
                .andExpect(jsonPath("$.errors[0].code").value("illegal_comment_owner_operation"))
                .andExpect(jsonPath("$.errors[0].property").doesNotExist())
                .andExpect(jsonPath("$.errors[0].message").value(containsString("operation is not allowed on not owning entity")))
                .andReturn();

        mvc.perform(put(comment_self_href).param("text", newText)
                .with(authentication(anonymousAuthentication)))
                .andDo(print())
                .andExpect(status().isUnauthorized())
                .andReturn();

    }
}
