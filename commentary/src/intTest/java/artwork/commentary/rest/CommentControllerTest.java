/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.commentary.rest;

import artwork.commentary.TestCommentaryConfig;
import artwork.commentary.domain.commentable.Commentable;
import artwork.commentary.domain.commentable.CommentableId;
import artwork.commentary.domain.commentable.CommentableModel;
import artwork.commentary.domain.commentable.CommentableOwnerId;
import artwork.web.security.social.UserDetails;
import com.jayway.jsonpath.JsonPath;
import org.hamcrest.Matchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.nullValue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/**
 * Created by Denis Kuchugurov
 * on 15.03.2016.
 */
@SpringBootTest(classes = TestCommentaryConfig.class)
public class CommentControllerTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private WebApplicationContext context;
    private MockMvc mvc;

    private Authentication authentication_1;

    private Authentication authentication_2;
    private AnonymousAuthenticationToken anonymousAuthentication;

    private String commentable_self_href;

    private String comments_href;
    private String comment_self_href;
    private String comment_like_href;
    private String comment_liked_href;

    private String testCommentText = "test_comment";


    @BeforeMethod
    public void before() throws Exception {

        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_SOCIAL_USER");
        authentication_1 = new TestingAuthenticationToken(UserDetails.from("user_1"), "", authorities);
        authentication_2 = new TestingAuthenticationToken(UserDetails.from("user_2"), "", authorities);
        anonymousAuthentication = new AnonymousAuthenticationToken("TEST_ANONYMOUS_KEY", "anonymousUser", AuthorityUtils.createAuthorityList("ROLE_ANONYMOUS"));
        authentication_1.setAuthenticated(true);
        authentication_2.setAuthenticated(true);
        anonymousAuthentication.setAuthenticated(true);

        Commentable commentable = new CommentableModel(CommentableId.generateId(), new CommentableOwnerId("commentable_controller_test"));

        MvcResult commentableResult = mvc.perform(get("/api/commentable/{commentableId}", commentable.getCommentableId().getUuid())
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("commentableId").exists())
                .andExpect(jsonPath("commentableId").isString())
                .andExpect(jsonPath("commentsCount", equalTo(0)))
                .andExpect(jsonPath("relateToId", nullValue()))
                .andReturn();

        String commentableBody = commentableResult.getResponse().getContentAsString();
        comments_href = JsonPath.read(commentableBody, "_links.comments.href");
        commentable_self_href = JsonPath.read(commentableBody, "_links.self.href");


        MvcResult commentResult = mvc.perform(
                post(comments_href)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"text\":\"" + testCommentText + "\"}")
                        .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("commentId").exists())
                .andExpect(jsonPath("commentId").isString())
                .andReturn();

        String commentBody = commentResult.getResponse().getContentAsString();

        comment_self_href = JsonPath.read(commentBody, "_links.self.href");
        comment_like_href = JsonPath.read(commentBody, "_links.like.href");
        comment_liked_href = JsonPath.read(commentBody, "_links.who-liked.href");

        //WA for templated uri from server i.e.
        //http://localhost/commentable/fNPWDz6FSouP0zPq3OtgnQ/comments/UlyISabzS-SxAHAFKyaBsw?like=true{&text,dislike}
        comment_like_href = comment_like_href.substring(0, comment_like_href.indexOf('{'));
    }

    @Test
    public void test_create_comment() throws Exception {
        String commentJson = "{\"text\": \"" + testCommentText + "\"}";


        this.mvc.perform(post(comments_href)
                .with(authentication(authentication_1))
                .contentType(MediaType.APPLICATION_JSON)
                .content(commentJson))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("text", equalTo(testCommentText)));
    }

    @Test
    public void test_like() throws Exception {

        mvc.perform(get(comment_self_href)
                .with(authentication(authentication_2)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("likesCount", equalTo(0)))
                .andReturn();


        mvc.perform(put(comment_like_href)
                .with(authentication(authentication_2)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("likesCount", equalTo(1)))
                .andReturn();

        mvc.perform(get(comment_liked_href)
                .with(authentication(authentication_2)))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void test_update_text() throws Exception {

        mvc.perform(get(comment_self_href)
                .with(authentication(authentication_2)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("text", equalTo(testCommentText)))
                .andExpect(jsonPath("isEdited", equalTo(false)))
                .andReturn();


        String newText = "new text";

        mvc.perform(put(comment_self_href).param("text", newText)
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("text", equalTo(newText)))
                .andExpect(jsonPath("isEdited", equalTo(true)))
                .andReturn();

    }

    @Test
    public void test_get_work_comments() throws Exception {

        mvc.perform(get(comments_href)
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isOk()).andReturn();

    }

    @Test
    public void test_add_comment_then_check_comments_count() throws Exception {

        mvc.perform(get(comments_href)
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isOk()).andReturn();

        String commentableJson = mvc.perform(get(commentable_self_href)
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();


        String commentableJsonAnonymous = mvc.perform(get(commentable_self_href)
                .with(authentication(anonymousAuthentication)))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();


        Integer commentsCount = JsonPath.read(commentableJsonAnonymous, "commentsCount");
        assertThat(commentsCount).isGreaterThanOrEqualTo(1);
    }

    @Test
    public void test_pageable() throws Exception {

        mvc.perform(post(comments_href)
                .content("{\"text\":\"some_text\"}")
                .contentType(MediaType.APPLICATION_JSON)
                .with(authentication(authentication_1)))
                .andExpect(status().isCreated());

        mvc.perform(post(comments_href)
                .content("{\"text\":\"some_text\"}")
                .contentType(MediaType.APPLICATION_JSON)
                .with(authentication(authentication_1)))
                .andExpect(status().isCreated());

        mvc.perform(post(comments_href)
                .content("{\"text\":\"some_text\"}")
                .contentType(MediaType.APPLICATION_JSON)
                .with(authentication(authentication_1)))
                .andExpect(status().isCreated());

        String content = mvc.perform(
                get(comments_href.replace("size=20", "size=2"))
                        .param("size", "2")
                        .with(authentication(authentication_1)))

                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("_links.next.href").exists())
                .andExpect(jsonPath("page.size").isNumber())
                .andExpect(jsonPath("page.size", Matchers.equalTo(2)))
                .andReturn().getResponse().getContentAsString();

        String suggestion_self_href = JsonPath.read(content, "_links.self.href");
        assertThat(suggestion_self_href).isNotEmpty();

        String next_href = JsonPath.read(content, "_links.next.href");


        content = mvc.perform(
                get(next_href)
                        .with(authentication(authentication_1)))

                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("_links.next.href").doesNotExist())
                .andExpect(jsonPath("page.size").isNumber())
                .andExpect(jsonPath("page.size", Matchers.equalTo(2)))
                .andReturn().getResponse().getContentAsString();


    }

}