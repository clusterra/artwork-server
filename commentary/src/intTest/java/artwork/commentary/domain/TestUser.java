/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.commentary.domain;

import artwork.commentary.domain.comment.CommentAuthor;
import artwork.likeable.domain.dislike.Disliker;
import artwork.likeable.domain.like.Liker;

/**
 * Created on 26/01/2017.
 *
 * @author kepkap
 */
public class TestUser {


    private String uuid;

    TestUser(String uuid) {
        this.uuid = uuid;
    }

    public static TestUser create(String uuid) {
        return new TestUser(uuid);
    }

    public Liker toLiker() {
        return new Liker(uuid, getDisplayName(), getImageUrl(), getProfileUrl());
    }

    public Disliker toDisliker() {
        return new Disliker(uuid, getDisplayName(), getImageUrl(), getProfileUrl());
    }

    public CommentAuthor toAuthor() {
        return new CommentAuthor(uuid, getDisplayName(), getImageUrl(), getProfileUrl());
    }

    public String getUuid() {
        return uuid;
    }

    public String getDisplayName() {
        return "display_name";
    }

    public String getImageUrl() {
        return "http://localhost/image_url";
    }

    public String getProfileUrl() {
        return "http://localhost/profile_url";
    }
}
