/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.commentary.domain.comment;

import artwork.commentary.TestCommentaryConfig;
import artwork.commentary.domain.IdGenerator;
import artwork.commentary.domain.TestUser;
import artwork.commentary.domain.commentable.Commentable;
import artwork.commentary.domain.commentable.CommentableId;
import artwork.commentary.domain.commentable.CommentableModel;
import artwork.commentary.domain.commentable.CommentableOwnerId;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created on 11/01/2017.
 *
 * @author kepkap
 */
@SpringBootTest(classes = TestCommentaryConfig.class)
public class AuthorshipAwareTest extends AbstractTestNGSpringContextTests {


    private Comment comment;

    private CommentAuthor commentableUser = TestUser.create("test_comment").toAuthor();
    private CommentAuthor notCommentableUser = TestUser.create("not_test_comment").toAuthor();

    private Commentable commentable;


    @BeforeMethod
    public void before() throws Exception {
        SecurityContextHolder.getContext().setAuthentication(new TestingAuthenticationToken(commentableUser.getUuid(), ""));

        commentable = new CommentableModel(new CommentableId(IdGenerator.generateId()), new CommentableOwnerId(AuthorshipAwareTest.class.getCanonicalName()));
        comment = commentable.addComment(commentableUser, "comment 0");
    }

    @Test
    public void test_owner() {
        assertThat(comment.isMine(commentableUser.getCommentAuthorId())).isTrue();
        assertThat(comment.isMine(notCommentableUser.getCommentAuthorId())).isFalse();
    }
}