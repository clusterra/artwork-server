/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.commentary.domain.comment;

import artwork.commentary.TestCommentaryConfig;
import artwork.commentary.domain.IdGenerator;
import artwork.commentary.domain.TestUser;
import artwork.commentary.domain.commentable.Commentable;
import artwork.commentary.domain.commentable.CommentableId;
import artwork.commentary.domain.commentable.CommentableModel;
import artwork.commentary.domain.commentable.CommentableOwnerId;
import artwork.commentary.domain.commentable.RelateTo;
import artwork.commentary.domain.commentable.RelateToId;
import artwork.commentary.domain.commentable.RelateToOwnerId;
import artwork.likeable.domain.dislike.Dislike;
import artwork.likeable.domain.dislike.Disliker;
import artwork.likeable.domain.like.Like;
import artwork.likeable.domain.like.Liker;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.assertj.core.api.Assertions.assertThat;
import static org.testng.Assert.fail;

/**
 * Created by Denis Kuchugurov
 * on 15/03/16 00:31.
 */
@SpringBootTest(classes = TestCommentaryConfig.class)
public class CommentTest extends AbstractTestNGSpringContextTests {

    private Comment comment;

    private TestUser commenter0 = TestUser.create("author_0");

    private CommentableId commentableId;

    private Commentable commentable;


    @BeforeMethod
    public void before() throws Exception {
        SecurityContextHolder.getContext().setAuthentication(new TestingAuthenticationToken(commenter0.getUuid(), ""));

        commentableId = new CommentableId(IdGenerator.generateId());
        commentable = new CommentableModel(commentableId, new CommentableOwnerId(commenter0.getUuid()));
        comment = commentable.addComment(commenter0.toAuthor(), "comment 0");
    }

    @Test
    public void test_comment_auditing_values_are_set() {
        assertThat(comment.getCreatedDate()).isNotNull();
        assertThat(comment.getCreatedBy()).isNotNull();
        assertThat(comment.getLastModifiedDate()).isNotNull();
        assertThat(comment.getLastModifiedBy()).isNotNull();
    }

    @Test
    public void test_commentable_find_comments() throws Exception {
        Page<Comment> all = commentable.findComments(new PageRequest(0, 10));
        assertThat(all.getContent()).isNotEmpty();
    }

    @Test
    public void test_comment_delete_text() throws Exception {
        comment.deleteText(commenter0.toAuthor());
        comment = commentable.findOne(comment.getCommentId());
        assertThat(comment.getText()).isEqualToIgnoringWhitespace("deleted");
    }

    @Test
    public void test_like() throws Exception {
        CommentId commentId = comment.getCommentId();
        assertThat(commentable.findOne(commentId).getLikesCount()).isEqualTo(0);
        assertThat(commentable.findOne(commentId).getDislikesCount()).isEqualTo(0);

        Liker commenter1 = TestUser.create("author_1").toLiker();
        Liker commenter2 = TestUser.create("author_2").toLiker();
        Liker commenter3 = TestUser.create("author_3").toLiker();


        comment.like(commenter1);
        assertThat(commentable.findOne(commentId).getLikesCount()).isEqualTo(1);

        comment.like(commenter1);
        assertThat(commentable.findOne(commentId).getLikesCount()).isEqualTo(1);

        comment.like(commenter2);
        comment.like(commenter3);
        assertThat(commentable.findOne(commentId).getLikesCount()).isEqualTo(3);

        comment.unlike(commenter3);
        assertThat(commentable.findOne(commentId).getLikesCount()).isEqualTo(2);

        comment.dislike(Disliker.from(commenter1));
        assertThat(commentable.findOne(commentId).getLikesCount()).isEqualTo(1);
        assertThat(commentable.findOne(commentId).getDislikesCount()).isEqualTo(1);
        assertThat(commentable.findOne(commentId).getLast10Likes().stream().map(Liker::getUuid).collect(Collectors.toList())).containsOnly(commenter2.getUuid());
    }

    @Test
    public void test_like_10_last() throws Exception {
        CommentId commentId = comment.getCommentId();
        assertThat(commentable.findOne(commentId).getLikesCount()).isEqualTo(0);
        assertThat(commentable.findOne(commentId).getDislikesCount()).isEqualTo(0);

        Liker commenter1 = TestUser.create("author_1").toLiker();
        Liker commenter2 = TestUser.create("author_2").toLiker();
        Liker commenter3 = TestUser.create("author_3").toLiker();
        Liker commenter4 = TestUser.create("author_4").toLiker();
        Liker commenter5 = TestUser.create("author_5").toLiker();
        Liker commenter6 = TestUser.create("author_6").toLiker();
        Liker commenter7 = TestUser.create("author_7").toLiker();
        Liker commenter8 = TestUser.create("author_8").toLiker();
        Liker commenter9 = TestUser.create("author_9").toLiker();
        Liker commenter10 = TestUser.create("author_10").toLiker();
        Liker commenter11 = TestUser.create("author_11").toLiker();


        comment.like(commenter1);
        comment.like(commenter2);
        comment.like(commenter3);
        comment.like(commenter4);
        comment.like(commenter5);
        comment.like(commenter6);
        comment.like(commenter7);
        comment.like(commenter8);
        comment.like(commenter9);
        comment.like(commenter10);
        assertThat(commentable.findOne(commentId).getLast10Likes().stream().map(Liker::getUuid).collect(Collectors.toList())).containsExactly(
                commenter10.getUuid(),
                commenter9.getUuid(),
                commenter8.getUuid(),
                commenter7.getUuid(),
                commenter6.getUuid(),
                commenter5.getUuid(),
                commenter4.getUuid(),
                commenter3.getUuid(),
                commenter2.getUuid(),
                commenter1.getUuid()
        );

        comment.like(commenter11);


        assertThat(commentable.findOne(commentId).getLast10Likes().stream().map(Liker::getUuid).collect(Collectors.toList())).containsExactly(
                commenter11.getUuid(),
                commenter10.getUuid(),
                commenter9.getUuid(),
                commenter8.getUuid(),
                commenter7.getUuid(),
                commenter6.getUuid(),
                commenter5.getUuid(),
                commenter4.getUuid(),
                commenter3.getUuid(),
                commenter2.getUuid()
        );
    }

    @Test
    public void test_dislike() throws Exception {
        CommentId commentId = comment.getCommentId();

        assertThat(commentable.findOne(commentId).getLikesCount()).isEqualTo(0);
        assertThat(commentable.findOne(commentId).getDislikesCount()).isEqualTo(0);

        Disliker commenter1 = TestUser.create("author_1").toDisliker();
        Disliker commenter2 = TestUser.create("author_2").toDisliker();
        Disliker commenter3 = TestUser.create("author_3").toDisliker();


        comment.dislike(commenter0.toDisliker());
        assertThat(commentable.findOne(commentId).getDislikesCount()).isEqualTo(1);


        comment.dislike(commenter1);
        assertThat(commentable.findOne(commentId).getDislikesCount()).isEqualTo(2);

        comment.dislike(commenter1);
        assertThat(commentable.findOne(commentId).getDislikesCount()).isEqualTo(2);

        comment.dislike(commenter2);
        comment.dislike(commenter3);
        assertThat(commentable.findOne(commentId).getDislikesCount()).isEqualTo(4);

        comment.undislike(commenter3);
        assertThat(commentable.findOne(commentId).getDislikesCount()).isEqualTo(3);


        comment.like(Liker.from(commenter1));
        assertThat(commentable.findOne(commentId).getLikesCount()).isEqualTo(1);
        assertThat(commentable.findOne(commentId).getDislikesCount()).isEqualTo(2);

        assertThat(commentable.findOne(commentId).getLast10Dislikes().stream().map(Disliker::getUuid).collect(Collectors.toList())).containsOnly(
                commenter0.getUuid(),
                commenter2.getUuid());

    }

    @Test
    public void test_dislike_10_last() throws Exception {
        CommentId commentId = comment.getCommentId();
        assertThat(commentable.findOne(commentId).getLikesCount()).isEqualTo(0);
        assertThat(commentable.findOne(commentId).getDislikesCount()).isEqualTo(0);

        Disliker commenter1 = TestUser.create("author_1").toDisliker();
        Disliker commenter2 = TestUser.create("author_2").toDisliker();
        Disliker commenter3 = TestUser.create("author_3").toDisliker();
        Disliker commenter4 = TestUser.create("author_4").toDisliker();
        Disliker commenter5 = TestUser.create("author_5").toDisliker();
        Disliker commenter6 = TestUser.create("author_6").toDisliker();
        Disliker commenter7 = TestUser.create("author_7").toDisliker();
        Disliker commenter8 = TestUser.create("author_8").toDisliker();
        Disliker commenter9 = TestUser.create("author_9").toDisliker();
        Disliker commenter10 = TestUser.create("author_10").toDisliker();
        Disliker commenter11 = TestUser.create("author_11").toDisliker();


        comment.dislike(commenter1);
        comment.dislike(commenter2);
        comment.dislike(commenter3);
        comment.dislike(commenter4);
        comment.dislike(commenter5);
        comment.dislike(commenter6);
        comment.dislike(commenter7);
        comment.dislike(commenter8);
        comment.dislike(commenter9);
        comment.dislike(commenter10);
        assertThat(commentable.findOne(commentId).getLast10Dislikes().stream().map(Disliker::getUuid).collect(Collectors.toList())).containsExactly(
                commenter10.getUuid(),
                commenter9.getUuid(),
                commenter8.getUuid(),
                commenter7.getUuid(),
                commenter6.getUuid(),
                commenter5.getUuid(),
                commenter4.getUuid(),
                commenter3.getUuid(),
                commenter2.getUuid(),
                commenter1.getUuid()
        );

        comment.dislike(commenter11);


        assertThat(commentable.findOne(commentId).getLast10Dislikes().stream().map(Disliker::getUuid).collect(Collectors.toList())).containsExactly(
                commenter11.getUuid(),
                commenter10.getUuid(),
                commenter9.getUuid(),
                commenter8.getUuid(),
                commenter7.getUuid(),
                commenter6.getUuid(),
                commenter5.getUuid(),
                commenter4.getUuid(),
                commenter3.getUuid(),
                commenter2.getUuid()
        );
    }

    @Test
    public void test_score() throws Exception {
        CommentId commentId = comment.getCommentId();

        assertThat(commentable.findOne(commentId).getScore()).isEqualTo(0);

        Disliker commenter1 = TestUser.create("author_1").toDisliker();
        Disliker commenter2 = TestUser.create("author_2").toDisliker();
        Disliker commenter3 = TestUser.create("author_3").toDisliker();


        comment.like(Liker.from(commenter1));
        assertThat(commentable.findOne(commentId).getScore()).isEqualTo(1);
        comment.like(Liker.from(commenter2));
        assertThat(commentable.findOne(commentId).getScore()).isEqualTo(2);
        comment.dislike(commenter3);
        assertThat(commentable.findOne(commentId).getScore()).isEqualTo(1);
        comment.dislike(commenter2);
        assertThat(commentable.findOne(commentId).getScore()).isEqualTo(-1);
        comment.dislike(commenter1);
        assertThat(commentable.findOne(commentId).getScore()).isEqualTo(-3);

    }

    @Test
    public void test_comment_get_liked() throws Exception {
        Liker commenter1 = TestUser.create("author_1").toLiker();
        Liker commenter2 = TestUser.create("author_2").toLiker();
        Liker commenter3 = TestUser.create("author_3").toLiker();


        CommentId commentId = comment.getCommentId();
        comment.like(commenter1);
        comment.like(commenter2);
        comment.like(commenter3);

        List<Like> content = comment.findLikes(new PageRequest(0, 100)).getContent();
        assertThat(content).isNotEmpty();
        List<Liker> liked = content.stream().map(Like::getLiker).collect(Collectors.toList());

        assertThat(liked.stream().map(Liker::getUuid).collect(Collectors.toList())).contains(
                commenter1.getUuid(),
                commenter2.getUuid(),
                commenter3.getUuid());
    }

    @Test
    public void test_comment_get_liked_pageable() throws Exception {
        Liker commenter1 = TestUser.create("author_1").toLiker();
        Liker commenter2 = TestUser.create("author_2").toLiker();
        Liker commenter3 = TestUser.create("author_3").toLiker();


        CommentId commentId = comment.getCommentId();
        comment.like(commenter1);
        comment.like(commenter2);
        comment.like(commenter3);

        Page<Like> liked = comment.findLikes(new PageRequest(0, 2));

        assertThat(liked).isNotNull();
        assertThat(liked.hasNext()).isTrue();
        assertThat(liked.getContent().stream().map(Like::getLiker).map(Liker::getUuid).collect(Collectors.toList())).containsExactly(
                commenter1.getUuid(),
                commenter2.getUuid());

        liked = comment.findLikes(liked.nextPageable());
        assertThat(liked).isNotNull();
        assertThat(liked.hasNext()).isFalse();
        assertThat(liked.getContent().stream().map(Like::getLiker).map(Liker::getUuid).collect(Collectors.toList())).containsExactly(
                commenter3.getUuid());

    }

    @Test
    public void test_comment_get_disliked_pageable() throws Exception {
        Disliker commenter1 = TestUser.create("author_1").toDisliker();
        Disliker commenter2 = TestUser.create("author_2").toDisliker();
        Disliker commenter3 = TestUser.create("author_3").toDisliker();


        CommentId commentId = comment.getCommentId();
        assertThat(comment.dislike(commenter1)).isTrue();
        assertThat(comment.dislike(commenter2)).isTrue();
        assertThat(comment.dislike(commenter3)).isTrue();

        Page<Dislike> disliked = comment.findDislikes(new PageRequest(0, 2));

        assertThat(disliked).isNotNull();
        assertThat(disliked.hasNext()).isTrue();
        assertThat(disliked.getContent().stream().map(Dislike::getDisliker).map(Disliker::getUuid).collect(Collectors.toList())).containsExactly(
                commenter1.getUuid(),
                commenter2.getUuid());

        disliked = comment.findDislikes(disliked.nextPageable());
        assertThat(disliked).isNotNull();
        assertThat(disliked.hasNext()).isFalse();
        assertThat(disliked.getContent().stream().map(Dislike::getDisliker).map(Disliker::getUuid).collect(Collectors.toList())).containsExactly(
                commenter3.getUuid());
    }

    @Test
    public void test_comment_update_text_validation_max() throws Exception {
        try {
            comment.updateText(commenter0.toAuthor(), StringUtils.repeat('S', 4097));
            fail();
        } catch (ConstraintViolationException e) {
            assertThat(e.getConstraintViolations()).hasSize(1);
        }
    }

    @Test
    public void test_comment_create_text_validation_not_empty() throws Exception {
        StringBuilder builder = new StringBuilder("string that will exceed length 500 ");

        while (builder.length() < 800) {
            builder.append("more string ");
        }

        try {
            commentable.addComment(commenter0.toAuthor(), "");
            fail();
        } catch (ConstraintViolationException e) {
            assertThat(e.getConstraintViolations()).hasSize(1);
        }
    }

    @Test
    public void test_comment_create_author_validation_null() throws Exception {

        try {
            commentable.addComment(TestUser.create("").toAuthor(), "more string ");
            fail();
        } catch (ConstraintViolationException e) {
            assertThat(e.getConstraintViolations()).hasSize(1);
        }
    }

    @Test
    public void test_comment_hierarchy() throws Exception {
        Comment rootComment = commentable.addComment(commenter0.toAuthor(), "root comment");

        assertThat(Commentable.findOne(commentableId).getCommentsCount()).isEqualTo(2);

        assertThat(rootComment.getChildCommentsCount()).isEqualTo(0);

        Comment comment1 = rootComment.addComment(commenter0.toAuthor(), "comment 1");
        Comment comment2 = rootComment.addComment(commenter0.toAuthor(), "comment 2");
        Comment comment3 = rootComment.addComment(commenter0.toAuthor(), "comment 3");

        assertThat(commentable.findOne(rootComment.getCommentId()).getChildCommentsCount()).isEqualTo(3);

        Page<Comment> childComments = commentable.findComments(rootComment.getCommentId(), new PageRequest(0, 100));
        assertThat(childComments.getContent()).containsOnly(comment1, comment2, comment3);


        assertThat(Commentable.findOne(commentable.getCommentableId()).getCommentsCount()).isEqualTo(5);
    }

    @Test
    public void test_commentable_comments_count() throws Exception {
        assertThat(Commentable.findOne(commentable.getCommentableId()).getCommentsCount()).isEqualTo(1);
        Comment comment1 = commentable.addComment(commenter0.toAuthor(), "comment 1");
        Comment comment2 = commentable.addComment(commenter0.toAuthor(), "comment 2");
        Comment comment3 = commentable.addComment(commenter0.toAuthor(), "comment 3");

        Commentable commentable = Commentable.findOne(this.commentable.getCommentableId());
        assertThat(commentable.getCommentsCount()).isEqualTo(4);

        assertThat(commentable.findComments(new PageRequest(0, 100)).getContent()).contains(comment1, comment2, comment3);
        commentable.addComment(commenter0.toAuthor(), "comment 4");
        assertThat(Commentable.findOne(this.commentable.getCommentableId()).getCommentsCount()).isEqualTo(5);
    }

    @Test
    public void test_commentable_comments_count_with_relate_to() throws Exception {

        CommentAuthor commentAuthor2 = TestUser.create(randomAlphabetic(10)).toAuthor();
        RelateToId relateToId = commentable.getCommentableId().toRelateToId();
        RelateToOwnerId relateToOwnerId = commentable.getCommentableOwnerId().toRelateToOwnerId();

        Commentable suggestion = new CommentableModel(new CommentableId(IdGenerator.generateId()), new RelateTo(relateToId, relateToOwnerId), new CommentableOwnerId(commentAuthor2.getUuid()));

        Comment relateToComment = commentable.addComment(this.commenter0.toAuthor(), "comment 1", suggestion.getCommentableId().toRelateToId());

        assertThat(commentable.findOne(relateToComment.getCommentId()).getRelateToCommentsCount()).isEqualTo(0);

        suggestion.addComment(commentAuthor2, "comment 1");
        suggestion.addComment(commentAuthor2, "comment 2");

        assertThat(commentable.findOne(relateToComment.getCommentId()).getRelateToCommentsCount()).isEqualTo(2);

    }

}