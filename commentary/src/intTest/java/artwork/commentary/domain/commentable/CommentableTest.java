/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.commentary.domain.commentable;

import artwork.commentary.TestCommentaryConfig;
import artwork.commentary.domain.IdGenerator;
import artwork.commentary.domain.TestUser;
import artwork.commentary.domain.comment.Comment;
import artwork.commentary.domain.comment.CommentAuthor;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.validation.ConstraintViolationException;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

/**
 * Created by Denis Kuchugurov
 * on 15/03/16 00:31.
 */
@SpringBootTest(classes = TestCommentaryConfig.class)
public class CommentableTest extends AbstractTestNGSpringContextTests {

    private CommentAuthor commentAuthor = TestUser.create("test_author").toAuthor();

    private Commentable commentable;

    @BeforeMethod
    public void before() throws Exception {
        SecurityContextHolder.getContext().setAuthentication(new TestingAuthenticationToken(commentAuthor.getUuid(), ""));

        commentable = new CommentableModel(new CommentableId(IdGenerator.generateId()), new CommentableOwnerId(commentAuthor.getUuid()));
    }

    @Test
    public void test_auditing_values_are_set() throws Exception {
        assertThat(commentable.getCreatedDate()).isNotNull();
        Date date = commentable.getLastModifiedDate();
        assertThat(date).isNotNull();

        commentable.incrementCommentsCount();

        assertThat(date).isBefore(commentable.getLastModifiedDate());
    }

    @Test
    public void test_auditing_version_gets_incremented() throws Exception {
        assertThat(commentable.getVersion()).isEqualTo(0);
        commentable.incrementCommentsCount();
        assertThat(commentable.getVersion()).isEqualTo(1);
        commentable.incrementCommentsCount();
        assertThat(commentable.getVersion()).isEqualTo(2);
    }

    @Test
    public void test_auditing_values_are_set_2() throws Exception {
        Commentable commentable = Commentable.findOne(this.commentable.getCommentableId());

        assertThat(commentable.getCreatedDate()).isNotNull();
        assertThat(commentable.getCreatedBy()).isEqualTo(commentAuthor.getUuid());
        assertThat(commentable.getLastModifiedDate()).isNotNull();
        assertThat(commentable.getLastModifiedBy()).isEqualTo(commentAuthor.getUuid());

        SecurityContextHolder.getContext().setAuthentication(new TestingAuthenticationToken("test", ""));

        commentable.incrementCommentsCount();

        commentable = Commentable.findOne(this.commentable.getCommentableId());
        assertThat(commentable.getCreatedBy()).isEqualTo(commentAuthor.getUuid());
        assertThat(commentable.getLastModifiedBy()).isEqualTo("test");
        assertThat(commentable.getLastModifiedDate()).isAfter(commentable.getCreatedDate());
    }

    @Test
    public void test_commentable_find_comments() throws Exception {
        Page<Comment> all = commentable.findComments(new PageRequest(0, 100));
        assertThat(all.getContent()).isEmpty();
    }

    @Test
    public void test_commentable_validation_not_empty_author_id() throws Exception {
        try {
            new CommentableModel(new CommentableId(IdGenerator.generateId()), new CommentableOwnerId(""));
            failBecauseExceptionWasNotThrown(ConstraintViolationException.class);
        } catch (ConstraintViolationException e) {
            assertThat(e.getConstraintViolations()).hasSize(1);
            assertThat(e.getConstraintViolations().iterator().next().getMessage()).contains("may not be empty");
        }

    }
}