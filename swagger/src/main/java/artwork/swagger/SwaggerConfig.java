/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.web.PagedResourcesAssembler;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Created by Denis Kuchugurov
 * on 11.02.2016.
 */
@EnableSwagger2
@Configuration
public class SwaggerConfig {

    @Bean
    public Docket management() {
        return new Docket(DocumentationType.SWAGGER_2)
            .ignoredParameterTypes(PagedResourcesAssembler.class)
            .apiInfo(apiInfo())
            .select()
            .apis(RequestHandlerSelectors.any())
            .paths(PathSelectors.ant("/management/**"))
            .build()
            .groupName("management");
    }

    @Bean
    public Docket login() {
        return new Docket(DocumentationType.SWAGGER_2)
            .ignoredParameterTypes(PagedResourcesAssembler.class)
            .apiInfo(apiInfo())
            .select()
            .apis(RequestHandlerSelectors.any())
            .paths(PathSelectors.ant("/api/login/**"))
            .build()
            .groupName("login");
    }

    @Bean
    public Docket works() {
        return new Docket(DocumentationType.SWAGGER_2)
            .ignoredParameterTypes(PagedResourcesAssembler.class)
            .apiInfo(apiInfo())
            .select()
            .apis(RequestHandlerSelectors.any())
            .paths(PathSelectors.ant("/api/works/**"))
            .build()
            .groupName("works");
    }

    @Bean
    public Docket suggestions() {
        return new Docket(DocumentationType.SWAGGER_2)
            .ignoredParameterTypes(PagedResourcesAssembler.class)
            .apiInfo(apiInfo())
            .select()
            .apis(RequestHandlerSelectors.any())
            .paths(PathSelectors.ant("/api/works/**/suggestions/**"))
            .build()
            .groupName("suggestions");
    }

    @Bean
    public Docket suggestionGroups() {
        return new Docket(DocumentationType.SWAGGER_2)
            .ignoredParameterTypes(PagedResourcesAssembler.class)
            .apiInfo(apiInfo())
            .select()
            .apis(RequestHandlerSelectors.any())
            .paths(PathSelectors.ant("/api/works/**/suggestion-groups/**"))
            .build()
            .groupName("suggestion-groups");
    }

    @Bean
    public Docket comments() {
        return new Docket(DocumentationType.SWAGGER_2)
            .ignoredParameterTypes(PagedResourcesAssembler.class)
            .apiInfo(apiInfo())
            .select()
            .apis(RequestHandlerSelectors.any())
            .paths(PathSelectors.ant("/api/commentable/**"))
            .build()
            .groupName("commentable");
    }

    @Bean
    public Docket notifications() {
        return new Docket(DocumentationType.SWAGGER_2)
            .ignoredParameterTypes(PagedResourcesAssembler.class)
            .apiInfo(apiInfo())
            .select()
            .apis(RequestHandlerSelectors.any())
            .paths(PathSelectors.ant("/api/notifications/**"))
            .build()
            .groupName("notifications");
    }

    @Bean
    public Docket statistics() {
        return new Docket(DocumentationType.SWAGGER_2)
            .ignoredParameterTypes(PagedResourcesAssembler.class)
            .apiInfo(apiInfo())
            .select()
            .apis(RequestHandlerSelectors.any())
            .paths(PathSelectors.ant("/api/users/**"))
            .build()
            .groupName("users");
    }

    @Bean
    public Docket emails() {
        return new Docket(DocumentationType.SWAGGER_2)
            .ignoredParameterTypes(PagedResourcesAssembler.class)
            .apiInfo(apiInfo())
            .select()
            .apis(RequestHandlerSelectors.any())
            .paths(PathSelectors.ant("/api/emails/**"))
            .build()
            .groupName("emails");
    }

    @Bean
    public Docket subscription() {
        return new Docket(DocumentationType.SWAGGER_2)
            .ignoredParameterTypes(PagedResourcesAssembler.class)
            .apiInfo(apiInfo())
            .select()
            .apis(RequestHandlerSelectors.any())
            .paths(PathSelectors.ant("/api/subscription/**"))
            .build()
            .groupName("subscription");
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
            .title("ARTWORK.PRO")
            .description("API Documentation")
            .version("1.0")
            .license("Apache 2.0")
            .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0")
            .termsOfServiceUrl("urn:tos")
            .build();
    }
}
