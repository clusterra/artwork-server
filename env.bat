set AW_PROFILE=dev


set AW_MAIL_HOST=localhost
set AW_MAIL_PORT=465
set AW_MAIL_USERNAME=user
set AW_MAIL_PASSWORD=password
set AW_MAIL_FROM=no-reply@artwork.pro

set AW_APPLICATION_URL=https://artwork.pro

set AW_AUTH_FACEBOOK_APP_KEY=
set AW_AUTH_FACEBOOK_APP_SECRET=

set AW_AUTH_VKONTAKTE_CLIENT_ID=
set AW_AUTH_VKONTAKTE_CLIENT_SECRET=

set AW_MONGODB_URI=

set AW_REDIS_HOST=localhost
set AW_REDIS_PASSWORD=
set AW_REDIS_PORT=6379


set | findstr AW_ | sort
set | findstr JAVA_ | sort