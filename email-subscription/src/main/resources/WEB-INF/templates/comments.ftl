<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width">
    <title>Artwork - платформа для дизайнеров</title><!--[if mso 12]>
    <style type="text/css">
        .flexibleContainer {
            display: block !important;
            width: 100% !important;
        }
    </style>
    <![endif]--><!--[if mso 14]>
    <style type="text/css">
        .flexibleContainer {
            display: block !important;
            width: 100% !important;
        }
    </style>
    <![endif]--></head>
<body style="-ms-text-size-adjust:100%;-webkit-font-smoothing:antialiased;-webkit-text-size-adjust:none;background:#efefef;font-family:'Avenir Next',Helvetica,Helvetica,Arial,sans-serif;font-size:100%;height:100%!important;line-height:1.65;margin:0;padding:0;width:100%!important">
<style type="text/css">@media only screen and (max-width: 480px) {
    body {
        width: 100% !important;
        min-width: 100% !important
    }
}</style>
<table class="body-wrap"
       style="-ms-text-size-adjust:100%;-webkit-font-smoothing:antialiased;-webkit-text-size-adjust:none;background:#efefef;border-collapse:collapse;height:100%;margin-top:30px;mso-table-lspace:0;mso-table-rspace:0;width:100%!important">
    <tbody>
    <tr>
        <td class="container"
            style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;clear:both!important;display:block!important;margin:0 auto!important;max-width:580px!important;mso-table-lspace:0;mso-table-rspace:0">
            <table style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;border-collapse:collapse;border-radius:5px;mso-table-lspace:0;mso-table-rspace:0;width:100%!important">
                <tbody>
                <tr>
                    <td class="content"
                        style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background:#fff;mso-table-lspace:0;mso-table-rspace:0;padding:10px 35px 20px 35px">
                        <table class="logo"
                               style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;border-collapse:collapse;border-radius:5px;margin:20px 0 0 0;mso-table-lspace:0;mso-table-rspace:0;width:100%!important">
                            <tbody>
                            <tr>
                                <td style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;mso-table-lspace:0;mso-table-rspace:0">
                                    <img src="https://ucarecdn.com/7a2991cf-9163-499e-a58c-3e3b5d1a3964/logo.png"
                                         style="-ms-interpolation-mode:bicubic;border:0;display:block;margin:0 auto;outline:0;text-decoration:none;width:100px">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <h2 style="font-size:28px;font-weight:500;line-height:1.25;margin:35px 0 25px 0;margin-bottom:20px;margin-top:20px;padding:0;text-align:center">
                            Привет, ${workOwnerName}!</h2>
                        <p class="centered"
                           style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;font-size:16px;font-weight:400;margin:1em 0;margin-bottom:20px;text-align:center">
                            <a href="artwork.pro" target="_blank"
                               style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#13A0DF;text-decoration:none"><span
                                    class="user"
                                    style="background:url(${commentAuthorImageUrl});background-size:cover;border-radius:50%;display:block;height:75px;margin:10px auto;width:75px"></span>
                                <strong style="display:block">${commentAuthorName}</strong> </a>оставил комментарий к вашей
                            работе.
                            <sub
                                    style="color:#92a4b2;display:block;font-size:12px;font-weight:500;letter-spacing:0;margin-top:5px">${commentText}</sub>

                        </p>
                        <table style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;border-collapse:collapse;border-radius:5px;mso-table-lspace:0;mso-table-rspace:0;width:100%!important">
                            <tbody>
                            <tr>
                                <td align="center"
                                    style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;mso-table-lspace:0;mso-table-rspace:0">
                                    <p style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;font-size:16px;font-weight:400;margin:1em 0;margin-bottom:20px">
                                        <a href="${commentUrl}" class="button"
                                           style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background:#13A0DF;border:solid #13A0DF;border-radius:4px;border-width:10px 20px 8px;color:#fff;display:inline-block;font-weight:500;text-decoration:none">Посмотреть</a>
                                    </p></td>
                            </tr>
                            </tbody>
                        </table>
                        <p style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;font-size:16px;font-weight:400;margin:1em 0;margin-bottom:20px">
                            <em>– Команда Artwork</em></p></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td class="container"
            style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;clear:both!important;display:block!important;margin:0 auto!important;max-width:580px!important;mso-table-lspace:0;mso-table-rspace:0">
            <table style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;border-collapse:collapse;border-radius:5px;mso-table-lspace:0;mso-table-rspace:0;width:100%!important">
                <tbody>
                <tr>
                    <td class="content footer" align="center"
                        style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background:0 0;mso-table-lspace:0;mso-table-rspace:0;padding:10px 35px 20px 35px">
                        <p style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#888;font-size:14px;font-weight:400;margin:1em 0;margin-bottom:0;text-align:center">
                            Отправлено <a href="artwork.pro" target="_blank"
                                          style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#888;font-weight:700;text-decoration:none">Artwork</a>,
                            198206 Петергофское шоссе 57, Санкт-Петербург</p>
                        <p style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#888;font-size:14px;font-weight:400;margin:1em 0;margin-bottom:0;text-align:center">
                            <a href="mailto:"
                               style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#888;font-weight:700;text-decoration:none">info@artwork.pro</a>
                            | <a href="#"
                                 style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#888;font-weight:700;text-decoration:none">Отписаться</a>
                        </p></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>