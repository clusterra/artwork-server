<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width"/>
    <title>${msg("message.email.user-signed-up.subject")}</title>
    <style type="text/css">
        body, #bodyTable, #bodyCell {
            height: 100% !important;
            margin: 0;
            padding: 0;
            width: 100% !important;
        }

        table {
            border-collapse: collapse;
        }

        img, a img {
            border: 0;
            outline: none;
            text-decoration: none;
        }

        h1, h2, h3, h4, h5, h6 {
            margin: 0;
            padding: 0;
        }

        p {
            margin: 1em 0;
        }

        .ReadMsgBody {
            width: 100%;
        }

        .ExternalClass {
            width: 100%;
        }

        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
            line-height: 100%;
        }

        table, td {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        #outlook a {
            padding: 0;
        }

        img {
            -ms-interpolation-mode: bicubic;
        }

        body, table, td, p, a, li, blockquote {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            font-size: 100%;
            font-family: 'Avenir Next', "Helvetica", Helvetica, Arial, sans-serif;
            line-height: 1.65;
        }

        img {
            margin: 0 auto;
            display: block;
        }

        body,
        .body-wrap {
            width: 100% !important;
            height: 100%;
            background: #efefef;
            -webkit-font-smoothing: antialiased;
            -webkit-text-size-adjust: none;
        }

        table.body-wrap {
            margin-top: 30px;
        }

        a {
            color: #13A0DF;
            text-decoration: none;
        }

        .text-center {
            text-align: center;
        }

        .text-right {
            text-align: right;
        }

        .text-left {
            text-align: left;
        }

        .button {
            display: inline-block;
            color: white;
            background: #13A0DF;
            border: solid #13A0DF;
            border-width: 10px 20px 8px;
            font-weight: 500;
            border-radius: 4px;
        }

        h1, h2, h3, h4, h5, h6 {
            margin-bottom: 20px;
            line-height: 1.25;
        }

        h1 {
            font-size: 32px;
        }

        h2 {
            font-size: 28px;
            font-weight: 500;
            margin-top: 20px;
        }

        h3 {
            font-size: 24px;
        }

        h4 {
            font-size: 20px;
        }

        h5 {
            font-size: 16px;
        }

        p, ul, ol {
            font-size: 16px;
            font-weight: normal;
            margin-bottom: 20px;
        }

        .container {
            display: block !important;
            clear: both !important;
            margin: 0 auto !important;
            max-width: 580px !important;
        }

        .pad {
            margin: 30px auto 0 auto !important;
        }

        .container table {
            width: 100% !important;
            border-collapse: collapse;
            border-radius: 5px;
        }

        .container .masthead {
            color: white;
            border-radius: 5px 5px 0 0;
            width: 580px;
        }

        .container .masthead h1 {
            text-transform: uppercase;
        }

        .container .content {
            background: white;
            padding: 10px 35px 20px 35px;
        }

        .container .content.footer {
            background: none;
        }

        .container .content.footer p {
            margin-bottom: 0;
            color: #888;
            text-align: center;
            font-size: 14px;
        }

        .container .content.footer a {
            color: #888;
            text-decoration: none;
            font-weight: bold;
        }

        .logo {
            margin: 20px 0 0 0;
        }

        .logo img {
            width: 100px;
            margin: 0 auto;
        }

        td.masthead img {
            border-radius: 5px 5px 0 0;
            width: 100%;
        }

        @media only screen and (max-width: 480px) {
            body {
                width: 100% !important;
                min-width: 100% !important;
            }
        }
    </style>
    <!--
           Outlook Conditional CSS

           These two style blocks target Outlook 2007 & 2010 specifically, forcing
           columns into a single vertical stack as on mobile clients. This is
           primarily done to avoid the 'page break bug' and is optional.

           More information here:
           http://templates.mailchimp.com/development/css/outlook-conditional-css
       -->
    <!--[if mso 12]>
    <style type="text/css">
        .flexibleContainer {
            display: block !important;
            width: 100% !important;
        }
    </style>
    <![endif]-->
    <!--[if mso 14]>
    <style type="text/css">
        .flexibleContainer {
            display: block !important;
            width: 100% !important;
        }
    </style>
    <![endif]-->
</head>
<body>
<table class="body-wrap">
    <tr>
        <td class="container">
            <!-- Message start -->
            <table>
                <tr>
                    <td align="center" class="masthead">
                        <img src="https://ucarecdn.com/7a2991cf-9163-499e-a58c-3e3b5d1a3964/logo.png"
                             style="-ms-interpolation-mode:bicubic;border:0;display:block;margin:0 auto;outline:0;text-decoration:none;width:100px">
                    </td>
                </tr>
                <tr>
                    <td class="content">
                        <h2>${userDisplayName}</h2>
                        <p>Поздравляем с успешной регистрацией в сообществе <a href="${applicationUrl}"
                                                                               target="_blank"><strong>Artwork</strong></a>!
                        </p>
                    </td>
                </tr>
            </table>

        </td>
    </tr>
    <tr>
        <td class="container"
            style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;clear:both!important;display:block!important;margin:0 auto!important;max-width:580px!important;mso-table-lspace:0;mso-table-rspace:0">
            <table style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;border-collapse:collapse;border-radius:5px;mso-table-lspace:0;mso-table-rspace:0;width:100%!important">
                <tbody>
                <tr>
                    <td class="content footer" align="center"
                        style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background:0 0;mso-table-lspace:0;mso-table-rspace:0;padding:10px 35px 20px 35px">
                        <p style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#888;font-size:14px;font-weight:400;margin:1em 0;margin-bottom:0;text-align:center">
                            Отправлено <a href="artwork.pro" target="_blank"
                                          style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#888;font-weight:700;text-decoration:none">Artwork</a>,
                            198206 Петергофское шоссе 57, Санкт-Петербург</p>
                        <p style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#888;font-size:14px;font-weight:400;margin:1em 0;margin-bottom:0;text-align:center">
                            <a href="mailto:"
                               style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#888;font-weight:700;text-decoration:none">info@artwork.pro</a>
                            | <a href="#"
                                 style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#888;font-weight:700;text-decoration:none">Отписаться</a>
                        </p></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>
</body>
</html>