/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.email.subscription.rest;

import artwork.common.rest.validation.error.ErrorsResponse;
import artwork.common.rest.validation.error.ModelError;
import artwork.email.subscription.domain.Subscription;
import artwork.email.subscription.domain.SubscriptionNotFoundException;
import artwork.email.subscription.domain.SubscriptionTokenNotValidException;
import artwork.iam.user.model.UserId;
import artwork.iam.user.model.UserNotFoundException;
import artwork.web.security.tracking.CurrentUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Egor Petushkov
 * on 03.08.17.
 */
@RestController
@RequestMapping(value = "/api/subscription", produces = MediaTypes.HAL_JSON_VALUE)
public class SubscriptionController {

    private static final Logger log = LoggerFactory.getLogger(SubscriptionController.class);

    private final SubscriptionResourceAssembler resourceAssembler;

    public SubscriptionController(SubscriptionResourceAssembler resourceAssembler) {
        this.resourceAssembler = resourceAssembler;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<SubscriptionResource> get() throws UserNotFoundException {
        CurrentUser currentUser = CurrentUser.requireNotAnonymous();
        Subscription subscription = Subscription.findOrCreateBy(new UserId(currentUser.getUuid()));
        return new ResponseEntity<>(resourceAssembler.toResource(subscription), HttpStatus.OK);
    }

    @RequestMapping(value = "/cancel", method = RequestMethod.PUT)
    public ResponseEntity<SubscriptionResource> unsubscribe(@RequestParam String token) throws SubscriptionTokenNotValidException {
        Subscription subscription = Subscription.findByToken(token);
        subscription.unSubscribe();
        return new ResponseEntity<>(resourceAssembler.toResource(subscription), HttpStatus.ACCEPTED);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorsResponse handle(SubscriptionTokenNotValidException e) {
        log.trace(e.getMessage(), e);
        Map<String, Object> args = new HashMap<>();
        args.put("token", e.getToken());
        return new ErrorsResponse(new ModelError("subscription_token_not_valid", e.getMessage(), args));
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorsResponse handle(SubscriptionNotFoundException e) {
        log.trace(e.getMessage(), e);
        Map<String, Object> args = new HashMap<>();
        args.put("subscriptionId", e.getSubscriptionId().getUuid());
        return new ErrorsResponse(new ModelError("subscription_not_found", e.getMessage(), args));
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorsResponse handle(UserNotFoundException e) {
        log.trace(e.getMessage(), e);
        Map<String, Object> args = new HashMap<>();
        args.put("userId", e.getUserId().getId());
        return new ErrorsResponse(new ModelError("user_not_found", e.getMessage(), args));
    }

}
