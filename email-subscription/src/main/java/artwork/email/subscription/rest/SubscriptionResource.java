/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.email.subscription.rest;

import org.springframework.hateoas.ResourceSupport;

/**
 * Created by Egor Petushkov
 * on 03.08.17.
 */
class SubscriptionResource extends ResourceSupport {
    public final String name;
    public final String email;
    public final boolean subscribed;

    public SubscriptionResource(String name, String email, boolean subscribed) {
        this.name = name;
        this.email = email;
        this.subscribed = subscribed;
    }
}
