/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.email.subscription.domain;

import artwork.iam.user.model.UserId;
import org.apache.commons.lang3.Validate;

import java.io.Serializable;

/**
 * Created by Egor Petushkov
 * on 03.08.17.
 */
public class SubscriptionId implements Serializable {

    private final String uuid;

    public SubscriptionId(String uuid) {
        Validate.notEmpty(uuid, "uuid is empty");
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SubscriptionId that = (SubscriptionId) o;

        return uuid.equals(that.uuid);

    }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }

    @Override
    public String toString() {
        return uuid;
    }

    public static SubscriptionId from(UserId userId) {
        return new SubscriptionId(userId.getId());
    }
}
