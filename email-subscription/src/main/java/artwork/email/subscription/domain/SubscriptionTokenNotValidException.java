/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.email.subscription.domain;

/**
 * Created by Egor Petushkov
 * on 04.08.17.
 */
public class SubscriptionTokenNotValidException extends Exception {

    private String token;

    SubscriptionTokenNotValidException(String token) {
        super(String.format("subscription token %s is not valid", token));
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
