/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.email.subscription.domain;

import artwork.iam.user.model.User;
import artwork.iam.user.model.UserId;
import artwork.iam.user.model.UserNotFoundException;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;
import java.util.UUID;

import static artwork.email.subscription.domain.SubscriptionDependencies.eventPublisher;
import static artwork.email.subscription.domain.SubscriptionDependencies.subscriptionRepository;

/**
 * Created by Egor Petushkov
 * on 03.08.17.
 */
@Document(collection = "subscription")
public class Subscription {

    @Id
    private SubscriptionId subscriptionId;

    @NotEmpty
    private String name;

    @Email
    private String email;

    private boolean subscribed;

    private String token;

    {
        Objects.requireNonNull(subscriptionRepository, "check if Spring context is initialized properly");
        Objects.requireNonNull(eventPublisher, "check if Spring context is initialized properly");
    }

    public Subscription() {
    }

    private Subscription(SubscriptionId subscriptionId, String name, String email) {
        this.subscriptionId = subscriptionId;
        this.name = name;
        this.email = email;
        subscribed = true;
        updateToken();
    }

    public SubscriptionId getSubscriptionId() {
        return subscriptionId;
    }

    public String getName() {
        return name;
    }

    public boolean hasEmail() {
        return email != null;
    }

    public String getEmail() {
        return email;
    }

    public boolean isSubscribed() {
        return subscribed;
    }

    public boolean canSendTo() {
        return isSubscribed() && hasEmail();
    }

    public String getToken() {
        return token;
    }

    private void updateToken() {
        token = UUID.randomUUID().toString();
    }

    public void updateEmail(String email) {
        this.email = email;
        subscriptionRepository.save(this);
    }

    public void subscribe() {
        this.subscribed = true;
        updateToken();
        subscriptionRepository.save(this);
    }

    public void unSubscribe() {
        this.subscribed = false;
        subscriptionRepository.save(this);
    }

    public static Subscription create(SubscriptionId subscriptionId, String name, String email) {
        Subscription subscription = new Subscription(subscriptionId, name, email);
        return subscriptionRepository.save(subscription);
    }

    public static Subscription findById(SubscriptionId id) throws SubscriptionNotFoundException {
        Subscription subscription = subscriptionRepository.findOne(id);
        if (subscription == null) {
            throw new SubscriptionNotFoundException(id);
        }

        return subscription;
    }

    public static Subscription findByToken(String token) throws SubscriptionTokenNotValidException {
        Subscription subscription = subscriptionRepository.findByToken(token);
        if (subscription == null) {
            throw new SubscriptionTokenNotValidException(token);
        }

        return subscription;
    }

    public void delete() {
        subscriptionRepository.delete(this);
    }

    public static Subscription findOrCreateBy(UserId userId) throws UserNotFoundException {

        Subscription existing = subscriptionRepository.findOne(SubscriptionId.from(userId));

        if (existing != null) {
            return existing;
        }

        User user = User.findBy(userId);
        return create(SubscriptionId.from(user.getUserId()), user.getDisplayName(), user.getEmail());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Subscription that = (Subscription) o;

        return subscriptionId != null ? subscriptionId.equals(that.subscriptionId) : that.subscriptionId == null;
    }

    @Override
    public int hashCode() {
        return subscriptionId != null ? subscriptionId.hashCode() : 0;
    }
}
