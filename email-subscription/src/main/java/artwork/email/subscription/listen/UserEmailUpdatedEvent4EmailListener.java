/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.email.subscription.listen;

import artwork.email.subscription.domain.Subscription;
import artwork.email.subscription.domain.SubscriptionId;
import artwork.email.subscription.domain.SubscriptionNotFoundException;
import artwork.iam.user.model.UserEmailUpdatedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * Created by Egor Petushkov
 * on 03.08.17.
 */
@Component
public class UserEmailUpdatedEvent4EmailListener {
    
    @Async
    @EventListener
    void handle(UserEmailUpdatedEvent event) throws SubscriptionNotFoundException {
        Subscription subscription = Subscription.findById(new SubscriptionId(event.getUser().getUuid()));
        subscription.updateEmail(event.getUser().getEmail());
    }
}
