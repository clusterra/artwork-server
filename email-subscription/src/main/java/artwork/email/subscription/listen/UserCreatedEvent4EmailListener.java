/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.email.subscription.listen;

import artwork.email.subscription.domain.Subscription;
import artwork.email.subscription.domain.SubscriptionId;
import artwork.freemarker.renderer.FreemarkerTemplateRenderer;
import artwork.iam.user.model.User;
import artwork.iam.user.model.UserCreatedEvent;
import artwork.mail.sender.EmailSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.event.EventListener;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by Denis Kuchugurov
 * on 19/10/14 23:42.
 */
@Component
public class UserCreatedEvent4EmailListener {

    private static final Logger log = LoggerFactory.getLogger(UserCreatedEvent4EmailListener.class);

    private final EmailSender emailSender;

    private final MessageSource messageSource;

    private final FreemarkerTemplateRenderer freemarkerTemplateRenderer;

    @Value("${artwork.application.url}")
    private String applicationUrl;

    public UserCreatedEvent4EmailListener(EmailSender emailSender, MessageSource messageSource, FreemarkerTemplateRenderer freemarkerTemplateRenderer) {
        this.emailSender = emailSender;
        this.messageSource = messageSource;
        this.freemarkerTemplateRenderer = freemarkerTemplateRenderer;
    }

    @EventListener
    public void handle(UserCreatedEvent event) {
        User user = event.getUser();
        Subscription subscription = Subscription.create(
            SubscriptionId.from(user.getUserId()),
            user.getDisplayName(),
            user.getEmail()
        );

        if (!subscription.canSendTo()) {
            return;
        }

        Map<String, Object> variables = new HashMap<>();
        variables.put("userDisplayName", subscription.getName());
        variables.put("applicationUrl", applicationUrl);

        String messageSubject = messageSource.getMessage("message.email.user-signed-up.subject", null, LocaleContextHolder.getLocale());
        String messageBody = freemarkerTemplateRenderer.render("welcome.ftl", variables);

        emailSender.send(subscription.getEmail(), messageSubject, messageBody);
    }
}
