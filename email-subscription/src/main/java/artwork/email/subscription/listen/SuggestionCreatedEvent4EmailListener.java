/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.email.subscription.listen;

import artwork.commentary.domain.comment.CommentNotFoundException;
import artwork.commentary.domain.commentable.CommentableNotFoundException;
import artwork.core.domain.suggestion.Suggestion;
import artwork.core.domain.suggestion.SuggestionCreatedEvent;
import artwork.email.subscription.domain.Subscription;
import artwork.email.subscription.domain.SubscriptionId;
import artwork.email.subscription.domain.SubscriptionNotFoundException;
import artwork.freemarker.renderer.FreemarkerTemplateRenderer;
import artwork.mail.sender.EmailSender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.event.EventListener;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class SuggestionCreatedEvent4EmailListener {


    private final EmailSender emailSender;

    private final MessageSource messageSource;

    private final FreemarkerTemplateRenderer freemarkerTemplateRenderer;

    @Value("${artwork.application.url}")
    private String applicationUrl;

    public SuggestionCreatedEvent4EmailListener(
        EmailSender emailSender,
        MessageSource messageSource,
        FreemarkerTemplateRenderer freemarkerTemplateRenderer) {
        this.emailSender = emailSender;
        this.messageSource = messageSource;
        this.freemarkerTemplateRenderer = freemarkerTemplateRenderer;
    }


    @Async
    @EventListener
    void handle(SuggestionCreatedEvent event) throws CommentNotFoundException, CommentableNotFoundException, SubscriptionNotFoundException {
        Suggestion suggestion = event.getSuggestion();
        Subscription subscription = Subscription.findById(new SubscriptionId(suggestion.getWorkOwner().getUuid()));

        if (!subscription.canSendTo()
            || suggestion.getWorkOwner().getUuid().equals(suggestion.getSuggestionOwner().getUuid())) {
            return;
        }
        
        String suggestionUrl = String.format("%s/works/%s/suggestions?author=%s", applicationUrl, suggestion.getWorkId().getUuid(), suggestion.getSuggestionOwner().getUuid());
        Map<String, Object> variables = new HashMap<>();
        variables.put("suggestionUrl", suggestionUrl);
        variables.put("workOwnerName", subscription.getName());
        variables.put("suggestionText", suggestion.getDescription());
        variables.put("suggestionAuthorName", suggestion.getSuggestionOwner().getDisplayName());
        variables.put("suggestionAuthorImageUrl", suggestion.getSuggestionOwner().getImageUrl());

        String messageSubject = messageSource.getMessage("message.email.suggestion.subject", null, "Artwork - the platform for designers", LocaleContextHolder.getLocale());
        String messageBody = freemarkerTemplateRenderer.render("suggestion.ftl", variables);


        emailSender.send(subscription.getEmail(), messageSubject, messageBody);
    }

}
