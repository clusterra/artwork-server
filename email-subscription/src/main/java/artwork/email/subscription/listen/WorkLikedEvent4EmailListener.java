/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.email.subscription.listen;

import artwork.commentary.domain.comment.CommentNotFoundException;
import artwork.commentary.domain.commentable.CommentableNotFoundException;
import artwork.core.domain.work.Work;
import artwork.core.domain.work.WorkLikedEvent;
import artwork.email.subscription.domain.Subscription;
import artwork.email.subscription.domain.SubscriptionId;
import artwork.email.subscription.domain.SubscriptionNotFoundException;
import artwork.freemarker.renderer.FreemarkerTemplateRenderer;
import artwork.mail.sender.EmailSender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.event.EventListener;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class WorkLikedEvent4EmailListener {


    private final EmailSender emailSender;

    private final MessageSource messageSource;

    private final FreemarkerTemplateRenderer freemarkerTemplateRenderer;

    @Value("${artwork.application.url}")
    private String applicationUrl;

    public WorkLikedEvent4EmailListener(
        EmailSender emailSender,
        MessageSource messageSource,
        FreemarkerTemplateRenderer freemarkerTemplateRenderer) {
        this.emailSender = emailSender;
        this.messageSource = messageSource;
        this.freemarkerTemplateRenderer = freemarkerTemplateRenderer;
    }


    @Async
    @EventListener
    void handle(WorkLikedEvent event) throws CommentNotFoundException, CommentableNotFoundException, SubscriptionNotFoundException {
        Work work = event.getWork();
        Subscription subscription = Subscription.findById(new SubscriptionId(work.getWorkOwner().getUuid()));

        if (!subscription.canSendTo()
            || work.getWorkOwner().getWorkOwnerId().getUuid().equals(event.getLiker().getUuid())) {
            return;
        }

        String workUrl = String.format("%s/works/%s", applicationUrl, work.getWorkId().getUuid());
        Map<String, Object> variables = new HashMap<>();
        variables.put("workUrl", workUrl);
        variables.put("workOwnerName", subscription.getName());
        variables.put("likerName", event.getLiker().getDisplayName());
        variables.put("likerImageUrl", event.getLiker().getImageUrl());

        String messageSubject = messageSource.getMessage("message.email.like.subject", null, "Artwork - the platform for designers", LocaleContextHolder.getLocale());
        String messageBody = freemarkerTemplateRenderer.render("like.ftl", variables);

        emailSender.send(subscription.getEmail(), messageSubject, messageBody);
    }

}
