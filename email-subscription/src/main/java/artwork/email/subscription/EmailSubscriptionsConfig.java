/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.email.subscription;

import artwork.common.rest.validation.ValidationConfig;
import artwork.email.subscription.domain.Subscription;
import artwork.freemarker.FreemarkerConfig;
import artwork.iam.MongoConfig;
import artwork.mail.EmailSenderConfig;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created by kepkap
 * on 18.08.2016.
 */
@Configuration
@Import({
        MongoConfig.class,
        ValidationConfig.class,
        FreemarkerConfig.class,
        EmailSenderConfig.class
})
@EnableAsync
@ComponentScan
@EntityScan(basePackageClasses = {Subscription.class})
@EnableMongoRepositories(basePackageClasses = {Subscription.class})
@EnableWebMvc
@EnableHypermediaSupport(type = EnableHypermediaSupport.HypermediaType.HAL)
public class EmailSubscriptionsConfig {

    @Bean
    public MessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.addBasenames("WEB-INF.i18n.messages4email");
        return messageSource;
    }

}
