/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.email.subscription.listen;

import artwork.commentary.domain.comment.CommentAuthor;
import artwork.commentary.domain.commentable.Commentable;
import artwork.core.domain.images.ImageMeta;
import artwork.core.domain.suggestion.Point;
import artwork.core.domain.suggestion.SuggestionOwner;
import artwork.core.domain.work.Work;
import artwork.core.domain.work.WorkId;
import artwork.core.domain.work.WorkModel;
import artwork.core.domain.work.WorkOwner;
import artwork.email.subscription.TestEmailSubscriptionsConfig;
import artwork.iam.user.model.User;
import artwork.iam.user.model.UserId;
import artwork.likeable.domain.like.Liker;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

@SpringBootTest(classes = TestEmailSubscriptionsConfig.class)
public class SendEmailsManualTest extends AbstractTestNGSpringContextTests {


    @BeforeMethod
    public void before() throws Exception {
        String email = randomAlphabetic(10) + "@mailinator.com";

        User owner = User.create(UserId.generateId(), "Arnold Schwarzenegger", "http://localhost", "https://randomuser.me/api/portraits/thumb/lego/9.jpg", email);
        User commenter = User.create(UserId.generateId(), "Silvester Stallone", "http://localhost", "https://randomuser.me/api/portraits/thumb/lego/5.jpg", email);


        Work work = new WorkModel(WorkId.generateId(),
            new WorkOwner(
                owner.getUserId().getId(),
                owner.getDisplayName(),
                owner.getImageUrl(),
                owner.getProfileUrl()),
            new ImageMeta(randomAlphabetic(5), "image/png"), "description");

        Commentable commentable = Commentable.findOne(work.getWorkId().toCommentableId());

        commentable.addComment(new CommentAuthor(
            commenter.getUserId().getId(),
            commenter.getDisplayName(),
            commenter.getImageUrl(),
            commenter.getProfileUrl()), "comment text bla bla");

        work.addSuggestion("suggestion text bla bla",
            new SuggestionOwner(
                commenter.getUserId().getId(),
                commenter.getDisplayName(),
                commenter.getImageUrl(),
                commenter.getProfileUrl()),
            new Point(10, 10));

        Work.findOne(work.getWorkId()).like(new Liker(
            commenter.getUserId().getId(),
            commenter.getDisplayName(),
            commenter.getImageUrl(),
            commenter.getProfileUrl())
        );

    }

    @Test(enabled = false)
    public void test_manually_check_emails_must_be_sent() throws Exception {


        Thread.sleep(40000);
    }

}