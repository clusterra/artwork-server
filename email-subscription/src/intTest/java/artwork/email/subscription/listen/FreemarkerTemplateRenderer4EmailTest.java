/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.email.subscription.listen;

import artwork.email.subscription.TestEmailSubscriptionsConfig;
import artwork.freemarker.renderer.FreemarkerTemplateRenderer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Denis Kuchugurov
 * on 08/04/15 23:17.
 */
@SpringBootTest(classes = TestEmailSubscriptionsConfig.class)
public class FreemarkerTemplateRenderer4EmailTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private FreemarkerTemplateRenderer freemarkerTemplateRenderer;

    private Map<String, Object> variables = new HashMap<>();

    @BeforeMethod
    public void before() {
        variables.put("applicationUrl", "localhost");
        variables.put("userDisplayName", "Bruce, Willis");
    }

    @Test
    public void test_welcome_template() throws Exception {
        String render = freemarkerTemplateRenderer.render("welcome.ftl", variables);
        System.out.println(render);
    }

    @Test
    public void test_suggestion_template() throws Exception {
        variables.put("suggestionAuthorImageUrl", "");
        variables.put("suggestionAuthorName", "");
        variables.put("suggestionText", "");
        variables.put("suggestionUrl", "");
        String render = freemarkerTemplateRenderer.render("suggestion.ftl", variables);
        System.out.println(render);
    }

    @Test
    public void test_like_template() throws Exception {
        variables.put("likerName", "");
        variables.put("likerImageUrl", "");
        variables.put("workUrl", "");
        String render = freemarkerTemplateRenderer.render("like.ftl", variables);
        System.out.println(render);
    }

    @Test
    public void test_comments_template() throws Exception {
        variables.put("workOwnerName", "");
        variables.put("commentAuthorName", "");
        variables.put("commentAuthorImageUrl", "");
        variables.put("commentUrl", "");
        variables.put("commentText", "");
        String render = freemarkerTemplateRenderer.render("comments.ftl", variables);
        System.out.println(render);
    }

}