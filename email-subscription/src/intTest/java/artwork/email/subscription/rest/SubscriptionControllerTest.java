/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.email.subscription.rest;

import artwork.commentary.domain.IdGenerator;
import artwork.email.subscription.TestEmailSubscriptionsConfig;
import artwork.email.subscription.domain.Subscription;
import artwork.email.subscription.domain.SubscriptionId;
import artwork.iam.user.model.User;
import artwork.iam.user.model.UserId;
import artwork.web.security.social.UserDetails;
import org.hamcrest.Matchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Egor Petushkov
 * on 14.08.17.
 */
@SpringBootTest(classes = TestEmailSubscriptionsConfig.class)
public class SubscriptionControllerTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    private Authentication authentication;

    private User user;

    @BeforeMethod
    public void before() throws Exception {

        mvc = MockMvcBuilders
            .webAppContextSetup(context)
            .alwaysDo(print())
            .apply(springSecurity())
            .build();

        user = User.create(
            new UserId(IdGenerator.generateId()),
            randomAlphabetic(10),
            "http://localhost",
            "http://localhost",
            randomAlphabetic(10) + "@" + randomAlphabetic(10) + ".su");
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_SOCIAL_USER", "ROLE_USER");
        authentication = new TestingAuthenticationToken(UserDetails.from(user.getUuid()), "", authorities);
        authentication.setAuthenticated(true);
    }

    @Test
    public void test_get() throws Exception {
        mvc.perform(get("/api/subscription")
            .with(authentication(authentication)))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(jsonPath("email", Matchers.equalTo(user.getEmail())))
            .andExpect(jsonPath("name", Matchers.equalTo(user.getDisplayName())))
            .andExpect(jsonPath("subscribed", Matchers.equalTo(true)));
    }

    @Test
    public void test_un_subscribe_not_valid() throws Exception {
        String fakeToken = randomAlphabetic(10);
        mvc.perform(put("/api/subscription/cancel?token={token}", fakeToken))
            .andDo(print())
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.errors", notNullValue()))
            .andExpect(jsonPath("$.errors[0].token").value(fakeToken))
            .andExpect(jsonPath("$.errors[0].code").value("subscription_token_not_valid"))
            .andExpect(jsonPath("$.errors[0].message").value(containsString(String.format("subscription token %s is not valid", fakeToken))))
            .andExpect(jsonPath("$.message").value(containsString("validation failed with 1 error(s)")));
    }

    @Test(enabled = false)
    public void test_subscription_not_found_never_happens() throws Exception {

        SubscriptionId subscriptionId = SubscriptionId.from(user.getUserId());

        Subscription.findById(subscriptionId).delete();

        mvc.perform(get("/api/subscription")
            .with(authentication(authentication)))
            .andDo(print())
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.errors", notNullValue()))
            .andExpect(jsonPath("$.errors[0].code").value("subscription_not_found"))
            .andExpect(jsonPath("$.message").value(containsString("validation failed with 1 error(s)")));
    }

    @Test
    public void test_subscription_created_if_not_exists() throws Exception {
        mvc.perform(get("/api/subscription")
            .with(authentication(authentication)))
            .andDo(print())
            .andExpect(status().isOk());

        SubscriptionId subscriptionId = SubscriptionId.from(user.getUserId());

        Subscription.findById(subscriptionId).delete();

        mvc.perform(get("/api/subscription")
            .with(authentication(authentication)))
            .andDo(print())
            .andExpect(status().isOk());
    }

    @Test
    public void test_un_subscribe() throws Exception {
        Subscription subscription = Subscription.findById(new SubscriptionId(user.getUuid()));
        mvc.perform(put("/api/subscription/cancel?token={token}", subscription.getToken()))
            .andDo(print())
            .andExpect(status().isAccepted())
            .andExpect(jsonPath("subscribed", Matchers.equalTo(false)));
    }
}
