/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.email.subscription.domain;

import artwork.commentary.domain.IdGenerator;
import artwork.email.subscription.TestEmailSubscriptionsConfig;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;

/**
 * Created by Egor Petushkov
 * on 04.08.17.
 */
@SpringBootTest(classes = TestEmailSubscriptionsConfig.class)
public class SubscriptionTest extends AbstractTestNGSpringContextTests {

    private String randomEmail() {
        return randomAlphanumeric(10) + "@" + randomAlphanumeric(10) + ".su";
    }

    @Test
    public void create_with_email() throws Exception {
        String name = randomAlphanumeric(10);
        String email = randomEmail();
        Subscription subs = Subscription.create(new SubscriptionId(IdGenerator.generateId()), name, email);
        subs = Subscription.findById(subs.getSubscriptionId());

        Assert.assertTrue(subs.hasEmail());
        Assert.assertTrue(subs.isSubscribed());
        Assert.assertTrue(subs.canSendTo());
        Assert.assertEquals(subs.getName(), name);
        Assert.assertEquals(subs.getEmail(), email);
        Assert.assertNotNull(subs.getToken());

        Subscription tokSubs = Subscription.findByToken(subs.getToken());
        Assert.assertEquals(tokSubs.getSubscriptionId(), subs.getSubscriptionId());
    }

    @Test
    public void create_without_email() {
        String name = randomAlphanumeric(10);
        Subscription subs = Subscription.create(new SubscriptionId(IdGenerator.generateId()), name, null);
        Assert.assertFalse(subs.hasEmail());
        Assert.assertTrue(subs.isSubscribed());
        Assert.assertFalse(subs.canSendTo());
        Assert.assertEquals(subs.getName(), name);
        Assert.assertNull(subs.getEmail());
        Assert.assertNotNull(subs.getToken());
    }

    @Test
    public void set_email() throws Exception {
        Subscription subs = Subscription.create(
            new SubscriptionId(IdGenerator.generateId()),
            randomAlphanumeric(10),
            null
        );
        String email = randomEmail();
        subs.updateEmail(email);
        subs = Subscription.findById(subs.getSubscriptionId());
        Assert.assertEquals(subs.getEmail(), email);
        Assert.assertTrue(subs.canSendTo());
    }


    @Test
    public void cancel_subscription() throws Exception {
        String name = randomAlphanumeric(10);
        String email = randomEmail();
        Subscription subs = Subscription.create(new SubscriptionId(IdGenerator.generateId()), name, email);
        subs.unSubscribe();
        subs = Subscription.findById(subs.getSubscriptionId());
        Assert.assertFalse(subs.canSendTo());
    }

    @Test(expectedExceptions = SubscriptionNotFoundException.class)
    public void test_delete_subscription() throws Exception {
        String name = randomAlphanumeric(10);
        String email = randomEmail();
        SubscriptionId subscriptionId = new SubscriptionId(IdGenerator.generateId());
        Subscription subscription = Subscription.create(subscriptionId, name, email);

        subscription.delete();

        Subscription.findById(subscriptionId);
    }

}
