/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.email.subscription.domain;

import artwork.commentary.domain.IdGenerator;
import artwork.email.subscription.TestEmailSubscriptionsConfig;
import artwork.iam.user.model.User;
import artwork.iam.user.model.UserId;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

/**
 * Created by Egor Petushkov
 * on 04.08.17.
 */
@SpringBootTest(classes = TestEmailSubscriptionsConfig.class)
public class Subscription_recreated_Test extends AbstractTestNGSpringContextTests {


    private User user;

    @BeforeMethod
    public void before() throws Exception {
        user = User.create(
            new UserId(IdGenerator.generateId()),
            randomAlphabetic(10),
            "http://localhost",
            "http://localhost",
            randomAlphabetic(10) + "@" + randomAlphabetic(10) + ".su");

    }

    @Test
    public void created_if_not_found() throws Exception {

        SubscriptionId subscriptionId = SubscriptionId.from(user.getUserId());

        Subscription subscription = Subscription.findById(subscriptionId);
        assertThat(subscription).isNotNull();

        subscription.delete();

        try {
            Subscription.findById(subscriptionId);
            fail("expecting not found");
        } catch (SubscriptionNotFoundException e) {
        }


        Subscription resurrected = Subscription.findOrCreateBy(user.getUserId());
        assertThat(resurrected).isNotNull();
        assertThat(resurrected).isEqualTo(subscription);
    }

}
