FROM anapsix/alpine-java

ARG jar_name=artwork-server-1.0.0.RELEASE.jar

ADD ./server/build/libs/${jar_name} .
RUN mkdir log

RUN java -version

EXPOSE 8080
ENTRYPOINT java $JAVA_OPTS -jar artwork-server-1.0.0.RELEASE.jar