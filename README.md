# Artwork.pro - a collaborative service to share design ideas

# 1. Specs

* [`references`](docs/spec/references.md)
* [`works`](docs/spec/works.md)
* [`comments`](docs/spec/comments.md)
* [`errors`](docs/spec/errors.md)

# 2. Deployment

* [`docker`](docs/aws/docker.md)
* [`redis`](docs/aws/redis.md)
* [`mongodb`](docs/aws/mongodb.md)
* [`env.sh`](env.sh) / [`env.bat`](env.bat)

## 3. Gradle tasks

Command | For 
--- | --- 
``` ./gradlew intTest ``` | execute integration tests. 
``` ./gradlew dependencyReport ``` | generates report into build/reports folder


## 4. API Documentation

```
    HTML: `https://localhost:8080/swagger-ui.html`     
    REST: `https://localhost:8080/v2/api-docs`
```
# Building uploadcare-java
```
git submodule init
git submodule update
cd uploadcare-java
mvn clean compile
mvn dependency:copy-dependencies -DoutputDirectory=target/libs
```

[![Apache License](https://img.shields.io/badge/Licence-Apache%202.0-blue.svg?style=flat-square)](http://www.apache.org/licenses/LICENSE-2.0)    

(C) Copyright 2017 [Artwork Team][]


[Artwork Team]:https://artwork.pro
[aws-docker-run]: http://www.ybrikman.com/writing/2015/11/11/running-docker-aws-ground-up/