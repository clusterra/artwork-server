/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.web.security.social.connection;

import artwork.web.security.SocialConfig;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.context.PropertyPlaceholderAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.social.ApiBinding;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionData;
import org.springframework.social.connect.ConnectionFactory;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.DuplicateConnectionException;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.vkontakte.api.VKontakte;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Denis Kuchugurov
 * on 29.02.2016.
 */
@SpringBootTest(classes = {PropertyPlaceholderAutoConfiguration.class, SocialConfig.class})
public class UsersConnectionRepositoryTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private UsersConnectionRepository usersConnectionRepository;

    @Autowired
    private ConnectionFactoryLocator connectionFactoryLocator;

    private ConnectionRepository connectionRepository;

    private String providerId = "vkontakte";
    private String userId;
    private String providerUserId;
    private ConnectionData connectionData;

    @BeforeMethod
    public void before() {
        userId = "test_" + RandomStringUtils.randomAlphabetic(20);
        providerUserId = "providerUserId" + userId;

        connectionData = new ConnectionData(providerId, providerUserId, "displayName", "http://profileUrl", "http://imageUrl", "ACCESS_TOKEN", "SECRET", "REFRESH_TOKEN", LocalDateTime.now().plusMonths(1).toInstant(ZoneOffset.UTC).toEpochMilli());

        connectionRepository = usersConnectionRepository.createConnectionRepository(userId);
        ConnectionFactory<?> connectionFactory = connectionFactoryLocator.getConnectionFactory(providerId);
        Connection<?> connection = connectionFactory.createConnection(connectionData);
        connectionRepository.addConnection(connection);
    }


    @Test
    public void test_find_connections_to_users() throws Exception {

        MultiValueMap<String, String> providerUserIds = new LinkedMultiValueMap<>();
        providerUserIds.add(providerId, providerUserId);
        MultiValueMap<String, Connection<?>> connections = connectionRepository.findConnectionsToUsers(providerUserIds);

        assertThat(connections).isNotEmpty();
    }

    @Test
    public void test_find_primary_connection() throws Exception {

        Connection<?> connections = connectionRepository.findPrimaryConnection(VKontakte.class);

        assertThat(connections).isNotNull();
    }

    @Test
    public void test_remove_connection() throws Exception {
        List<Connection<?>> connections = connectionRepository.findConnections(providerId);

        assertThat(connections).isNotEmpty();

        Connection<?> connection = connections.get(0);

        assertThat(connections).contains(connection);

        connectionRepository.removeConnection(connection.getKey());

        connections = connectionRepository.findConnections(providerId);
        assertThat(connections).doesNotContain(connection);
    }

    @Test
    public void test_remove_connections() throws Exception {
        List<Connection<?>> connections = connectionRepository.findConnections(providerId);

        assertThat(connections).isNotEmpty();

        Connection<?> connection = connections.get(0);

        assertThat(connections).contains(connection);

        connectionRepository.removeConnections(providerId);

        connections = connectionRepository.findConnections(providerId);
        assertThat(connections).doesNotContain(connection);
    }

    @Test(expectedExceptions = DuplicateConnectionException.class)
    public void test_create_connection_twice() {
        ConnectionFactory<?> connectionFactory = connectionFactoryLocator.getConnectionFactory(providerId);
        Connection<?> connection = connectionFactory.createConnection(connectionData);
        connectionRepository.addConnection(connection);
        connection = connectionFactory.createConnection(connectionData);
        connectionRepository.addConnection(connection);
    }

    @Test
    public void test_find_user_ids_with_connection() throws Exception {
        MultiValueMap<String, Connection<?>> connections = connectionRepository.findAllConnections();

        List<Connection<?>> list = connections.get(providerId);
        assertThat(list).isNotEmpty();
        assertThat(list.get(0).createData()).isEqualToComparingOnlyGivenFields(connectionData, "providerId", "providerUserId", "displayName", "expireTime");

        List<String> userIdsWithConnection = usersConnectionRepository.findUserIdsWithConnection(list.get(0));
        assertThat(userIdsWithConnection).contains(userId);
    }

    @Test
    public void test_find_user_ids_connected_to() throws Exception {
        Set<String> connectedTo = usersConnectionRepository.findUserIdsConnectedTo(providerId, Collections.singleton(providerUserId));
        assertThat(connectedTo).contains(userId);
    }
}