/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.web.security.auth;

import artwork.web.security.WebSecurityConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by Denis Kuchugurov
 * on 06/10/16.
 */
@SpringBootTest(classes = {WebSecurityConfig.class})
public class LoginController_current_Test extends AbstractTestNGSpringContextTests {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    private String xAuthToken;

    @BeforeMethod
    public void setup() throws Exception {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

        xAuthToken = mvc.perform(post("/api/login")
                .content("{\"username\":\"system\", \"password\":\"Password10\"}")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .with(csrf())
        ).andReturn().getResponse().getHeader("x-auth-token");

        assertThat(xAuthToken).isNotEmpty();
    }

//    @Test
    public void test_current_authenticated() throws Exception {

        MvcResult result = this.mvc
                .perform(get("/api/login").header("x-auth-token", xAuthToken))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(header().string("x-auth-token", nullValue()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("username", equalTo("system")))
                .andExpect(jsonPath("authorities", equalTo("ROLE_USER")))
                .andReturn();
    }

//    @Test
    public void test_current_unauthenticated() throws Exception {


        MvcResult result = this.mvc
                .perform(get("/api/login")
                        .with(csrf())
                )
                .andDo(print())
                .andExpect(status().isUnauthorized())
                .andReturn();

        String header = result.getResponse().getHeader("x-auth-token");
        assertThat(header).isNullOrEmpty();


    }
}