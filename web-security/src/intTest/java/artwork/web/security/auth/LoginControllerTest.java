/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.web.security.auth;

import artwork.web.security.TestWebSecurityConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.cookie;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Denis Kuchugurov
 * on 06/10/16.
 */
@SpringBootTest(classes = {TestWebSecurityConfig.class})
public class LoginControllerTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    @Value("${management.user.name}")
    private String user;

    @Value("${management.user.password}")
    private String password;

    @BeforeMethod
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    //    @Test
    public void test_access_resource_then_login() throws Exception {

        /*this.mvc
                .perform(
                        get("/rest/hello")
                )
                .andDo(print())
                .andExpect(status().isUnauthorized());*/

        MvcResult result = this.mvc
                .perform(post("/auth/vkontakte").param("scope", "offline")
                        .with(csrf())
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("username", equalTo("system")))
                .andReturn();

        String header = result.getResponse().getHeader("x-auth-token");
        assertThat(header).isNotEmpty();

        result = this.mvc
                .perform(
                        get("/rest/hello").header("x-auth-token", header)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

    }

    @Test
    public void test_access_resource_then_login_then_logout() throws Exception {

        this.mvc
                .perform(
                        get("/api/hello")
                )
                .andDo(print())
                .andExpect(status().isUnauthorized());

        MvcResult result = this.mvc
                .perform(get("/api/hello")
//                        .with(user("user").password("password").roles("SOCIAL_USER")
                                .with(httpBasic(user, password))
                                .with(csrf())
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(cookie().exists("SESSION"))
                .andExpect(jsonPath("message", containsString(user)))
                .andReturn();


        result = this.mvc
                .perform(
                        delete("/api/login")
                                .with(csrf())
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(cookie().value("SESSION", nullValue()))
                .andReturn();

        this.mvc
                .perform(
                        get("/api/hello")
                )
                .andDo(print())
                .andExpect(status().isUnauthorized());

    }

    @Test
    public void test_logout() throws Exception {


        MvcResult result = this.mvc
                .perform(delete("/api/login")
                        .with(csrf())
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(cookie().value("SESSION", nullValue()))
                .andReturn();
    }
}