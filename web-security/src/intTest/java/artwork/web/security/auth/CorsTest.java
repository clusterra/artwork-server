/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.web.security.auth;

import artwork.web.security.TestWebSecurityConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.nullValue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by Denis Kuchugurov
 * on 06/10/16.
 */
@SpringBootTest(classes = {TestWebSecurityConfig.class})
public class CorsTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    @Value("${management.user.name}")
    private String user;

    @Value("${management.user.password}")
    private String password;

    @BeforeMethod
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void test_cors_headers_on_login() throws Exception {

        this.mvc
                .perform(
                        get("/api/hello")
                                .header("Origin", "https://www.artwork.pro")

                )
                .andDo(print())
                .andExpect(header().string("Access-Control-Allow-Origin", "https://www.artwork.pro"))
                .andExpect(status().isUnauthorized());

        MvcResult result = this.mvc
                .perform(get("/api/hello")
                        .header("Origin", "https://www.artwork.pro")
                        .with(httpBasic(user, password))
                        .with(csrf())
                )
                .andDo(print())
                .andExpect(header().string("Access-Control-Allow-Origin", "https://www.artwork.pro"))
                .andExpect(status().isOk())
                .andExpect(cookie().exists("SESSION"))
                .andExpect(jsonPath("message", containsString(user)))
                .andReturn();

        result = this.mvc
                .perform(get("/api/hello")
                        .header("Origin", "https://www.artwork.pro")
                        .with(httpBasic(user, password))
                        .with(csrf())
                )
                .andDo(print())
                .andExpect(header().string("Access-Control-Allow-Origin", "https://www.artwork.pro"))
                .andExpect(status().isOk())
                .andExpect(cookie().exists("SESSION"))
                .andExpect(jsonPath("message", containsString(user)))
                .andReturn();


        result = this.mvc
                .perform(
                        delete("/api/login")
                                .with(csrf())
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(cookie().value("SESSION", nullValue()))
                .andReturn();

        this.mvc
                .perform(
                        get("/api/hello")
                )
                .andDo(print())
                .andExpect(status().isUnauthorized());

    }

    @Test
    public void test_cors_wrong_headers_on_login() throws Exception {

        this.mvc
                .perform(
                        get("/api/hello")
                                .header("Origin", "*.blah")

                )
                .andDo(print())
                .andExpect(header().doesNotExist("Access-Control-Allow-Origin"))
                .andExpect(status().isForbidden());

    }

}