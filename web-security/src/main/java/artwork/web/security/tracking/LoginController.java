/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.web.security.tracking;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Denis Kuchugurov
 * on 20/10/16.
 */
@RestController
@RequestMapping(value = "/api/login", produces = MediaTypes.HAL_JSON_VALUE)
public class LoginController {

    private final static Logger logger = LoggerFactory.getLogger(LoginController.class);


    @RequestMapping(value = "/facebook", method = RequestMethod.POST)
    public void facebook() {
        logger.info("login via facebook ...");
    }

    @RequestMapping(value = "/vkontakte", method = RequestMethod.POST)
    public void vkontakte() {
        logger.info("login via vkontakte ...");
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<CurrentUser> current() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return new ResponseEntity<>(CurrentUser.requireAny(), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public ResponseEntity<CurrentUser> logout() {
        return current();
    }

    static class Credentials {

        @JsonProperty
        String username;

        @JsonProperty
        String password;
    }


}
