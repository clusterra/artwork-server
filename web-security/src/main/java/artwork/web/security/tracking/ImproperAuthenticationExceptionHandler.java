/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.web.security.tracking;

import artwork.common.rest.validation.error.Error;
import artwork.common.rest.validation.error.ErrorsResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Collections;

/**
 * Created by Denis Kuchugurov
 * on 18.02.2016.
 */
@ControllerAdvice
public class ImproperAuthenticationExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(ImproperAuthenticationExceptionHandler.class);

    @ExceptionHandler
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ResponseBody
    public ErrorsResponse handle(ImproperAuthenticationException e) {
        log.trace(e.getMessage(), e);
        return new ErrorsResponse(new Error("unauthorized", e.getMessage(), Collections.emptyList()));
    }


}
