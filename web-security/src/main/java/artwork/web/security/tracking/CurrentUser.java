/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.web.security.tracking;

import artwork.web.security.domain.UserProfile;
import artwork.web.security.social.ExtendedSocialUserDetails;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.social.security.SocialAuthenticationToken;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created by Denis Kuchugurov
 * on 02.08.2016.
 */
public class CurrentUser implements UserProfile {

    @JsonProperty
    private final String uuid;

    @JsonProperty
    private final String displayName;

    @JsonProperty
    private final String imageUrl;

    @JsonProperty
    private final String profileUrl;

    private final Map<String, Object> data;

    private CurrentUser(String uuid, String displayName, String imageUrl, String profileUrl, Map<String, Object> data) {
        this.uuid = uuid;
        this.displayName = displayName;
        this.imageUrl = imageUrl;
        this.profileUrl = profileUrl;
        this.data = data;
    }

    @JsonAnyGetter
    public Map<String, Object> getData() {
        return data;
    }

    public String getUuid() {
        return uuid;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    @Override
    public String toString() {
        return "CurrentUser{" +
                "uuid='" + uuid + '\'' +
                '}';
    }

    public static CurrentUser requireNotAnonymous(Authentication authentication) {
        Objects.requireNonNull(authentication, "authentication is null");

        Map<String, Object> data = new HashMap<>();

        if (authentication instanceof SocialAuthenticationToken) {
            SocialAuthenticationToken token = (SocialAuthenticationToken) authentication;
            data.put("providerId", token.getProviderId());
            data.putAll(token.getProviderAccountData());
        }

        data.put("authorities", authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.joining(",")));

        Object principal = authentication.getPrincipal();

        if (principal instanceof ExtendedSocialUserDetails) {
            ExtendedSocialUserDetails details = (ExtendedSocialUserDetails) principal;
            return new CurrentUser(details.getUserId(), details.getDisplayName(), details.getImageUrl(), details.getProfileUrl(), data);
        }

        if (principal instanceof User) {
            User user = (User) principal;
            return new CurrentUser(user.getUsername(), user.getUsername(), null, null, null);
        }

        throw new ImproperAuthenticationException(String.format("current authentication principal must be '%s', but got '%s'", ExtendedSocialUserDetails.class.getSimpleName(), principal));
    }


    public static CurrentUser requireNotAnonymous() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return requireNotAnonymous(authentication);
    }

    public static CurrentUser requireAny() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return requireAny(authentication);
    }

    public static CurrentUser requireAny(Principal principal) {
        Authentication authentication = (Authentication) principal;
        if (authentication == null) {
            throw new ImproperAuthenticationException("current authentication is not set, no user signed in");
        }

        Object authenticationPrincipal = authentication.getPrincipal();

        if (authenticationPrincipal instanceof String) {
            return new CurrentUser((String) authenticationPrincipal, null, null, null, null);
        }

        if (authenticationPrincipal instanceof User) {
            User user = (User) authenticationPrincipal;
            return new CurrentUser(user.getUsername(), user.getUsername(), null, null, null);
        }

        if (authenticationPrincipal instanceof ExtendedSocialUserDetails) {
            return requireNotAnonymous(authentication);
        }

        throw new ImproperAuthenticationException(String.format("current authentication principal must be String or '%s', but got '%s'", ExtendedSocialUserDetails.class.getSimpleName(), authenticationPrincipal));
    }

    public static boolean isAnonymous() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication != null && authentication instanceof AnonymousAuthenticationToken;
    }

}
