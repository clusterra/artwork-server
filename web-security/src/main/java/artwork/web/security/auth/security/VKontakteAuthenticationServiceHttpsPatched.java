/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.web.security.auth.security;

import artwork.web.security.auth.connect.VKontakteConnectionFactoryHttpsPatched;
import org.springframework.social.security.provider.OAuth2AuthenticationService;
import org.springframework.social.vkontakte.api.VKontakte;

public class VKontakteAuthenticationServiceHttpsPatched extends OAuth2AuthenticationService<VKontakte> {

    public VKontakteAuthenticationServiceHttpsPatched(String apiKey, String appSecret) {
        super(new VKontakteConnectionFactoryHttpsPatched(apiKey, appSecret));
    }
}
