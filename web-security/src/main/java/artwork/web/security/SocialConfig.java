/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.web.security;


import artwork.iam.MongoConfig;
import artwork.web.security.auth.security.VKontakteAuthenticationServiceHttpsPatched;
import artwork.web.security.social.SocialUserDetailServiceImpl;
import artwork.web.security.social.connection.ConnectionConverter;
import artwork.web.security.social.connection.ConnectionModel;
import artwork.web.security.social.connection.ImplicitConnectionSignUp;
import artwork.web.security.social.connection.UsersConnectionRepositoryImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.social.UserIdSource;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.ConnectionSignUp;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.web.ReconnectFilter;
import org.springframework.social.facebook.security.FacebookAuthenticationService;
import org.springframework.social.security.AuthenticationNameUserIdSource;
import org.springframework.social.security.SocialAuthenticationServiceRegistry;
import org.springframework.social.security.SocialUserDetailsService;

@Configuration
@EntityScan(basePackageClasses = ConnectionModel.class)
@Import(MongoConfig.class)
public class SocialConfig {

    @Bean
    public TextEncryptor textEncryptor() {
        return Encryptors.noOpText();
    }

    @Bean
    @Scope(value = "request", proxyMode = ScopedProxyMode.INTERFACES)
    public ConnectionRepository connectionRepository(UsersConnectionRepository usersConnectionRepository, UserIdSource userIdSource) {
        return usersConnectionRepository.createConnectionRepository(userIdSource.getUserId());
    }

    @Bean
    public ConnectionFactoryLocator connectionFactoryLocator(VKontakteAuthenticationServiceHttpsPatched vKontakteAuthenticationService,
                                                             FacebookAuthenticationService facebookAuthenticationService) {
        SocialAuthenticationServiceRegistry registry = new SocialAuthenticationServiceRegistry();
        registry.addAuthenticationService(vKontakteAuthenticationService);
        registry.addAuthenticationService(facebookAuthenticationService);
        return registry;
    }

    @Bean
    VKontakteAuthenticationServiceHttpsPatched vKontakteAuthenticationService(@Value("${vkontakte.clientId}") String clientId,
                                                                              @Value("${vkontakte.scope}") String scope,
                                                                              @Value("${vkontakte.clientSecret}") String clientSecret) {
        VKontakteAuthenticationServiceHttpsPatched service = new VKontakteAuthenticationServiceHttpsPatched(clientId, clientSecret);
        service.setDefaultScope(scope);
        return service;
    }

    @Bean
    FacebookAuthenticationService facebookAuthenticationService(@Value("${facebook.appKey}") String apiKey,
                                                                @Value("${facebook.scope}") String scope,
                                                                @Value("${facebook.appSecret}") String appSecret) {
        FacebookAuthenticationService service = new FacebookAuthenticationService(apiKey, appSecret);
        service.setDefaultScope(scope);
        return service;
    }

    @Bean
    public UserIdSource userIdSource() {
        return new AuthenticationNameUserIdSource();
    }

    @Bean
    public ReconnectFilter apiExceptionHandler(UsersConnectionRepository usersConnectionRepository, UserIdSource userIdSource) {
        return new ReconnectFilter(usersConnectionRepository, userIdSource);
    }

    @Bean
    ConnectionConverter mongoConnectionConverter(ConnectionFactoryLocator connectionFactoryLocator, TextEncryptor textEncryptor) {
        return new ConnectionConverter(connectionFactoryLocator, textEncryptor);
    }

    @Bean
    UsersConnectionRepositoryImpl usersConnectionRepository(MongoOperations mongoOperations, ConnectionSignUp connectionSignUp, ConnectionConverter connectionConverter, ConnectionFactoryLocator connectionFactoryLocator) {
        return new UsersConnectionRepositoryImpl(mongoOperations, connectionSignUp, connectionConverter, connectionFactoryLocator);
    }

    @Bean
    ConnectionSignUp connectionSignUp() {
        return new ImplicitConnectionSignUp();
    }

    @Bean
    SocialUserDetailsService socialUserDetailsService() {
        return new SocialUserDetailServiceImpl();
    }
}
