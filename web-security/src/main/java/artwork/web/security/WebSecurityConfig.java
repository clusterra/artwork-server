/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.web.security;


import artwork.web.security.tracking.ImproperAuthenticationExceptionHandler;
import artwork.web.security.tracking.LoginController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.security.Http401AuthenticationEntryPoint;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.ForwardAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.context.SecurityContextPersistenceFilter;
import org.springframework.security.web.savedrequest.NullRequestCache;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.session.web.http.SessionRepositoryFilter;
import org.springframework.social.UserIdSource;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.security.SocialAuthenticationFilter;
import org.springframework.social.security.SocialAuthenticationProvider;
import org.springframework.social.security.SocialAuthenticationServiceLocator;
import org.springframework.social.security.SocialUserDetailsService;
import org.springframework.util.StringUtils;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.annotation.PostConstruct;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Denis Kuchugurov
 */
@Configuration
@EnableWebMvc
@ComponentScan
@EnableWebSecurity
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private static final Logger log = LoggerFactory.getLogger(WebSecurityConfig.class);


    @Autowired
    private SessionRepositoryFilter sessionRepositoryFilter;

    @Autowired
    private UserIdSource userIdSource;

    @Autowired
    private UsersConnectionRepository usersConnectionRepository;

    @Autowired
    private SocialAuthenticationServiceLocator authServiceLocator;

    @Value("${artwork.auth.success.url}")
    private String authSuccessUrl;

    @Value("${artwork.allowed.origins}")
    private String allowedOrigins;

    @PostConstruct
    public void init() throws URISyntaxException {
        URI check = new URI(authSuccessUrl);
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
            .ignoring()
            .antMatchers("/**/*.css", "/**/*.png", "/**/*.gif", "/**/*.jpg", "/**/*.js")
            .antMatchers("/static/**", "/resources/**", "/favicon.ico")
        ;

        web
            .ignoring()
            .antMatchers("/swagger-ui.html")
            .antMatchers("/swagger-resources")
        ;

    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {


        http.cors()
            .and()
            .csrf()
            .disable()
            .exceptionHandling()
            .authenticationEntryPoint(new Http401AuthenticationEntryPoint("header value"))
            .and()
            .logout()
            .invalidateHttpSession(true)
            .clearAuthentication(true)
            .logoutRequestMatcher(new AntPathRequestMatcher("/api/login", HttpMethod.DELETE.name()))
            .logoutSuccessHandler(logoutSuccessHandler())
            .and()
            .rememberMe()
            .and()
            .requestCache()
            .requestCache(new NullRequestCache())
            .and()
            .addFilterBefore(socialAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class)
            .addFilterBefore(basicAuthenticationFilter(), SocialAuthenticationFilter.class)
            .addFilterBefore(sessionRepositoryFilter, SecurityContextPersistenceFilter.class)

        ;

        http.authorizeRequests()
            .antMatchers("/management").hasRole("ACTUATOR")
            .antMatchers("/admin/invites").hasRole("ACTUATOR")
        ;

        http.authorizeRequests()
            .antMatchers("/v2/api-docs").permitAll()
            .antMatchers("/swagger-resources/**").permitAll()
        ;


        http.authorizeRequests()

            .antMatchers(HttpMethod.GET, "/api/commentable/**").permitAll()
            .antMatchers(HttpMethod.GET, "/api/works/**").permitAll()
            .antMatchers(HttpMethod.GET, "/api/users/**/statistics").permitAll()
            .antMatchers(HttpMethod.GET, "/api/invites").permitAll()
            .antMatchers(HttpMethod.PUT, "/api/subscription/cancel").permitAll()
            .antMatchers("/api/**").hasRole("SOCIAL_USER")
        ;

        http.authorizeRequests()
            .antMatchers("/login-ui").permitAll()
            .antMatchers("/sse-ui").permitAll()
        ;

        //Attention: this block should go last
        http.authorizeRequests()
            .antMatchers(HttpMethod.GET, "/api/login/**").permitAll()
            .antMatchers(HttpMethod.POST, "/api/login/**").permitAll()
            .anyRequest().authenticated()
        ;
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        final CorsConfiguration configuration = new CorsConfiguration();

        List<String> origins = Arrays.asList(allowedOrigins.split(","));

        if (origins.isEmpty()) {
            throw new IllegalStateException(String.format("artwork.allowed.origins configuration property is not set properly: %s", allowedOrigins));
        }

        configuration.setAllowedOrigins(origins);

        configuration.setAllowedMethods(Arrays.asList("HEAD", "GET", "POST", "PUT", "DELETE", "PATCH"));
        // setAllowCredentials(true) is important, otherwise:
        // The value of the 'Access-Control-Allow-Origin' header in the response must not be the wildcard '*' when the request's credentials mode is 'include'.
        configuration.setAllowCredentials(true);
        // setAllowedHeaders is important! Without it, OPTIONS preflight request
        // will fail with 403 Invalid CORS request
        configuration.setAllowedHeaders(Arrays.asList("Authorization", "Cache-Control", "Content-Type"));
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    /**
     * if only https://jira.spring.io/browse/SPR-14755 was resolved we could use
     * {@link ForwardAuthenticationSuccessHandler} here and have the request forwarded directly to
     * {@link LoginController#logout()}
     * <p>
     * instead we do manual json writing.
     *
     * @return login success handler
     */
    private LogoutSuccessHandler logoutSuccessHandler() {
        return (request, response, authentication) -> {
            response.setStatus(HttpServletResponse.SC_OK);
            Cookie cookie = new Cookie("SESSION", null);
            String cookiePath = request.getContextPath() + "/";
            if (!StringUtils.hasLength(cookiePath)) {
                cookiePath = "/";
            }
            cookie.setPath(cookiePath);
            cookie.setMaxAge(0);
            response.addCookie(cookie);
        };
    }


    /**
     * if only https://jira.spring.io/browse/SPR-14755 was resolved we could use
     * {@link ForwardAuthenticationSuccessHandler} here and have the request forwarded directly to
     * {@link LoginController#current()}
     * <p>
     * instead we do manual json writing.
     *
     * @return login success handler
     */
    private AuthenticationSuccessHandler successHandler2() {
        return (request, response, authentication) -> {
            response.setContentType("text/html");
            PrintWriter pw = response.getWriter();
            pw.print("<!DOCTYPE html><html><head><meta http-equiv=\"refresh\" content=\"0; url=/\"></head><body></body></html>");
            pw.flush();
        };
    }

    private AuthenticationSuccessHandler successHandler() {
        return new SimpleUrlAuthenticationSuccessHandler(authSuccessUrl);
    }

    private AuthenticationFailureHandler failureHandler() {
        return (httpServletRequest, httpServletResponse, e) -> {
            httpServletResponse.getWriter().append("Authentication failure");
            httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        };
    }

    private SocialAuthenticationFilter socialAuthenticationFilter() throws Exception {
        SocialAuthenticationFilter filter = new SocialAuthenticationFilter(authenticationManager(), userIdSource, usersConnectionRepository, authServiceLocator);
        filter.setAuthenticationSuccessHandler(successHandler());
        filter.setFilterProcessesUrl("/api/login");
        return filter;
    }

    private BasicAuthenticationFilter basicAuthenticationFilter() throws Exception {
        return new BasicAuthenticationFilter(authenticationManager());
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth,
                                SocialAuthenticationProvider socialAuthenticationProvider,
                                @Value("${management.user.name}") String userName,
                                @Value("${management.user.password}") String userPassword) throws Exception {
        auth
            .inMemoryAuthentication()
            .withUser(userName).password(userPassword).roles("USER", "SOCIAL_USER", "ACTUATOR");
        auth.authenticationProvider(socialAuthenticationProvider);
    }

    @Bean
    SocialAuthenticationProvider socialAuthenticationProvider(UsersConnectionRepository usersConnectionRepository,
                                                              SocialUserDetailsService socialUserDetailsService) {
        return new SocialAuthenticationProvider(usersConnectionRepository, socialUserDetailsService);
    }

    @Bean
    ImproperAuthenticationExceptionHandler improperAuthenticationExceptionHandler() {
        return new ImproperAuthenticationExceptionHandler();
    }


}
