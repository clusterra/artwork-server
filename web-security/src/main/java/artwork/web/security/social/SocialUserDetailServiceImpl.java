/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.web.security.social;

import artwork.iam.user.model.UserId;
import artwork.iam.user.model.User;
import artwork.iam.user.model.UserNotFoundException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.social.security.SocialUserDetails;
import org.springframework.social.security.SocialUserDetailsService;

import java.util.List;

/**
 * Created by Denis Kuchugurov
 * on 03/01/16 23:16.
 */
public class SocialUserDetailServiceImpl implements SocialUserDetailsService {


    @Override
    public SocialUserDetails loadUserByUserId(String userId) throws UsernameNotFoundException {

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_SOCIAL_USER");
        try {
            User user = User.findBy(new UserId(userId));
            return new ExtendedSocialUserDetailsImpl(user.getUserId().getId(), authorities, user.getDisplayName(), user.getProfileUrl(), user.getImageUrl());
        } catch (UserNotFoundException e) {
            throw new UsernameNotFoundException(userId, e);
        }
    }
}
