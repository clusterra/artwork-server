/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.web.security.social;

import org.springframework.social.security.SocialUserDetails;

/**
 * Created on 02/01/2017.
 *
 * @author kepkap
 */
public interface ExtendedSocialUserDetails extends SocialUserDetails {

    String getDisplayName();

    String getProfileUrl();

    String getImageUrl();

}
