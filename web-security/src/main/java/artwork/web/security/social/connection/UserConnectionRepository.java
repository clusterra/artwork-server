/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.web.security.social.connection;

import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.ConnectionKey;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.DuplicateConnectionException;
import org.springframework.social.connect.NoSuchConnectionException;
import org.springframework.social.connect.NotConnectedException;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;
import static org.springframework.data.mongodb.core.query.Update.update;

/**
 * Created by Denis Kuchugurov
 * on 26.02.2016.
 */
public class UserConnectionRepository implements ConnectionRepository {

    private final String userId;
    private final MongoOperations mongoOperations;
    private final ConnectionFactoryLocator connectionFactoryLocator;
    private final ConnectionConverter connectionConverter;

    public UserConnectionRepository(String userId, MongoOperations mongoOperations, ConnectionFactoryLocator connectionFactoryLocator, ConnectionConverter connectionConverter) {
        this.userId = userId;
        this.mongoOperations = mongoOperations;
        this.connectionFactoryLocator = connectionFactoryLocator;
        this.connectionConverter = connectionConverter;
    }

    @Override
    public MultiValueMap<String, Connection<?>> findAllConnections() {
        Query query = query(where("userId").is(userId)).with(sortByProviderId().and(sortByCreated()));
        List<Connection<?>> results = findConnections(query);
        MultiValueMap<String, Connection<?>> connections = new LinkedMultiValueMap<>();
        for (String registeredProviderId : connectionFactoryLocator.registeredProviderIds()) {
            connections.put(registeredProviderId, Collections.unmodifiableList(Collections.emptyList()));
        }
        for (Connection<?> connection : results) {
            final String providerId = connection.getKey().getProviderId();
            if (connections.get(providerId).isEmpty()) {
                connections.put(providerId, new LinkedList<Connection<?>>());
            }
            connections.add(providerId, connection);
        }
        return connections;
    }

    @Override
    public List<Connection<?>> findConnections(final String providerId) {
        Query query = query(where("userId").is(userId).and("providerId").is(providerId)).with(sortByCreated());
        return Collections.unmodifiableList(findConnections(query));
    }

    @Override
    @SuppressWarnings("unchecked")
    public <A> List<Connection<A>> findConnections(final Class<A> apiType) {
        final List<?> connections = findConnections(getProviderId(apiType));
        return (List<Connection<A>>) connections;
    }

    @Override
    public MultiValueMap<String, Connection<?>> findConnectionsToUsers(MultiValueMap<String, String> providerUserIds) {
        if (providerUserIds == null || providerUserIds.isEmpty()) {
            throw new IllegalArgumentException("providerUserIds must be defined");
        }

        final List<Criteria> filters = new ArrayList<>(providerUserIds.size());
        for (Map.Entry<String, List<String>> entry : providerUserIds.entrySet()) {
            final String providerId = entry.getKey();
            filters.add(where("providerId").is(providerId).and("providerUserId").in(entry.getValue()));
        }

        Criteria criteria = where("userId").is(userId);
        criteria.orOperator(filters.toArray(new Criteria[filters.size()]));

        Query query = new Query(criteria).with(sortByProviderId().and(sortByCreated()));
        List<ConnectionModel> mongoConnections = mongoOperations.find(query, ConnectionModel.class);
        List<Connection<?>> results = mongoConnections.stream().map(connectionConverter::convert).collect(Collectors.toList());

        MultiValueMap<String, Connection<?>> connectionsForUsers = new LinkedMultiValueMap<>();
        for (Connection<?> connection : results) {
            String providerId = connection.getKey().getProviderId();
            String providerUserId = connection.getKey().getProviderUserId();
            List<String> userIds = providerUserIds.get(providerId);
            List<Connection<?>> connections = connectionsForUsers.get(providerId);
            if (connections == null) {
                connections = new ArrayList<>(userIds.size());
                for (int i = 0; i < userIds.size(); i++) {
                    connections.add(null);
                }
                connectionsForUsers.put(providerId, connections);
            }
            int connectionIndex = userIds.indexOf(providerUserId);
            connections.set(connectionIndex, connection);
        }
        return connectionsForUsers;
    }

    @Override
    public Connection<?> getConnection(final ConnectionKey connectionKey) {
        final Query query = query(where("userId").is(userId).and("providerId").is(connectionKey.getProviderId()).and("providerUserId").is(connectionKey.getProviderUserId()));
        final Connection<?> connection = findOneConnection(query);
        if (connection == null) {
            throw new NoSuchConnectionException(connectionKey);
        } else {
            return connection;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public <A> Connection<A> getConnection(Class<A> apiType, String providerUserId) {
        String providerId = getProviderId(apiType);
        return (Connection<A>) getConnection(new ConnectionKey(providerId, providerUserId));
    }

    @Override
    @SuppressWarnings("unchecked")
    public <A> Connection<A> getPrimaryConnection(Class<A> apiType) {
        String providerId = getProviderId(apiType);
        Connection<A> connection = (Connection<A>) findPrimaryConnection(providerId);
        if (connection == null) {
            throw new NotConnectedException(providerId);
        } else {
            return connection;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public <A> Connection<A> findPrimaryConnection(final Class<A> apiType) {
        final String providerId = getProviderId(apiType);
        return (Connection<A>) findPrimaryConnection(providerId);
    }

    @Override
    public void addConnection(Connection<?> connection) {
        try {
            ConnectionModel mongoConnection = connectionConverter.convert(userId, connection);
            mongoOperations.insert(mongoConnection);
        } catch (DuplicateKeyException ex) {
            throw new DuplicateConnectionException(connection.getKey());
        }
    }

    @Override
    public void updateConnection(final Connection<?> connection) {
        final ConnectionModel mongoConnection = connectionConverter.convert(userId, connection);
        final Query query = query(where("userId").is(userId).and("providerId").is(mongoConnection.getProviderId()).and("providerUserId").is(mongoConnection.getProviderUserId()));
        final Update update =
                update("displayName", mongoConnection.getDisplayName())
                        .set("profileUrl", mongoConnection.getProfileUrl())
                        .set("imageUrl", mongoConnection.getImageUrl())
                        .set("accessToken", mongoConnection.getAccessToken())
                        .set("secret", mongoConnection.getSecret())
                        .set("refreshToken", mongoConnection.getRefreshToken())
                        .set("expireTime", mongoConnection.getExpireTime());
        mongoOperations.updateFirst(query, update, ConnectionModel.class);
    }

    @Override
    public void removeConnections(final String providerId) {
        final Query query = query(where("userId").is(userId).and("providerId").is(providerId));
        mongoOperations.remove(query, ConnectionModel.class);
    }

    @Override
    public void removeConnection(final ConnectionKey connectionKey) {
        final Query query = query(where("userId").is(userId).and("providerId").is(connectionKey.getProviderId()).and("providerUserId").is(connectionKey.getProviderUserId()));
        mongoOperations.remove(query, ConnectionModel.class);
    }

    private Connection<?> findPrimaryConnection(String providerId) {
        final Query query = query(where("userId").is(userId).and("providerId").is(providerId)).with(sortByCreated());
        return findOneConnection(query);
    }

    private List<Connection<?>> findConnections(Query query) {
        List<ConnectionModel> mongoConnections = mongoOperations.find(query, ConnectionModel.class);
        return mongoConnections.stream().map(connectionConverter::convert).collect(Collectors.toList());
    }

    private Connection<?> findOneConnection(Query query) {
        return connectionConverter.convert(mongoOperations.findOne(query, ConnectionModel.class));
    }

    private <A> String getProviderId(Class<A> apiType) {
        return connectionFactoryLocator.getConnectionFactory(apiType).getProviderId();
    }

    private Sort sortByProviderId() {
        return new Sort(Sort.Direction.ASC, "providerId");
    }

    private Sort sortByCreated() {
        return new Sort(Sort.Direction.DESC, "created");
    }
}
