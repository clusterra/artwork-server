/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.web.security.social.connection;

import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionData;
import org.springframework.social.connect.ConnectionFactory;
import org.springframework.social.connect.ConnectionFactoryLocator;
/**
 * Created by Denis Kuchugurov
 * on 26.02.2016.
 */

/**
 * Converts data between spring social connections {@link Connection} and
 * MongoDB documents {@link ConnectionModel}.
 *
 * @author mendlik
 */
public class ConnectionConverter {

    private final ConnectionFactoryLocator connectionFactoryLocator;

    private final TextEncryptor textEncryptor;

    public ConnectionConverter(ConnectionFactoryLocator connectionFactoryLocator, TextEncryptor textEncryptor) {
        this.connectionFactoryLocator = connectionFactoryLocator;
        this.textEncryptor = textEncryptor;
    }

    Connection<?> convert(ConnectionModel connectionModel) {
        if (connectionModel == null)
            return null;

        ConnectionData connectionData = fillConnectionData(connectionModel);
        ConnectionFactory<?> connectionFactory = connectionFactoryLocator.getConnectionFactory(connectionData.getProviderId());
        return connectionFactory.createConnection(connectionData);
    }

    ConnectionModel convert(String userId, Connection<?> connection) {
        ConnectionData data = connection.createData();
        return new ConnectionModel(
                userId,
                data.getProviderId(),
                data.getProviderUserId(),
                data.getDisplayName(),
                data.getProfileUrl(),
                data.getImageUrl(),
                encrypt(data.getAccessToken()),
                encrypt(data.getSecret()),
                encrypt(data.getRefreshToken()),
                data.getExpireTime());
    }

    private ConnectionData fillConnectionData(ConnectionModel connectionModel) {
        return new ConnectionData(connectionModel.getProviderId(),
                connectionModel.getProviderUserId(),
                connectionModel.getDisplayName(),
                connectionModel.getProfileUrl(),
                connectionModel.getImageUrl(),
                decrypt(connectionModel.getAccessToken()),
                decrypt(connectionModel.getSecret()),
                decrypt(connectionModel.getRefreshToken()),
                connectionModel.getExpireTime());
    }

    private String decrypt(String encryptedText) {
        return encryptedText != null ? textEncryptor.decrypt(encryptedText) : null;
    }

    private String encrypt(String text) {
        return text != null ? textEncryptor.encrypt(text) : null;
    }
}
