/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.web.security.social.connection;

import artwork.iam.user.model.UserId;
import artwork.iam.user.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionSignUp;

/**
 * Created by Denis Kuchugurov
 * on 08/01/16 09:29.
 */
public class ImplicitConnectionSignUp implements ConnectionSignUp {

    private static final Logger logger = LoggerFactory.getLogger(ImplicitConnectionSignUp.class);

    @Override
    public String execute(Connection<?> connection) {
        logger.debug("creating local user account, after implicit sign-up from social profile {}", connection.getProfileUrl());
        String email = connection.fetchUserProfile().getEmail();
        User user = User.create(UserId.generateId(), connection.getDisplayName(), connection.getProfileUrl(), connection.getImageUrl(), email);
        return user.getUserId().getId();
    }
}
