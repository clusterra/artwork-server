/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.web.security.social;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * Created by Denis Kuchugurov
 * on 09.03.2016.
 */
class ExtendedSocialUserDetailsImpl implements ExtendedSocialUserDetails {

    private final String username;
    private final Collection<? extends GrantedAuthority> authorities;
    private final String displayName;
    private final String profileUrl;
    private final String imageUrl;


    ExtendedSocialUserDetailsImpl(String username, Collection<? extends GrantedAuthority> authorities, String displayName, String profileUrl, String imageUrl) {
        this.username = username;
        this.authorities = authorities;
        this.displayName = displayName;
        this.profileUrl = profileUrl;
        this.imageUrl = imageUrl;
    }

    @Override
    public String getUserId() {
        return getUsername();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return "[SECURED]";
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }

    @Override
    public String getProfileUrl() {
        return profileUrl;
    }

    @Override
    public String getImageUrl() {
        return imageUrl;
    }
}
