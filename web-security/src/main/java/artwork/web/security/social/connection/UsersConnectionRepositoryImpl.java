/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.web.security.social.connection;

/**
 * Created by Denis Kuchugurov
 * on 26.02.2016.
 */

import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.ConnectionKey;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.ConnectionSignUp;
import org.springframework.social.connect.UsersConnectionRepository;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

public class UsersConnectionRepositoryImpl implements UsersConnectionRepository {

    private final MongoOperations mongoOperations;

    private final ConnectionFactoryLocator connectionFactoryLocator;

    private final ConnectionConverter connectionConverter;

    private final ConnectionSignUp connectionSignUp;

    public UsersConnectionRepositoryImpl(MongoOperations mongoOperations,
                                         ConnectionSignUp connectionSignUp,
                                         ConnectionConverter connectionConverter,
                                         ConnectionFactoryLocator connectionFactoryLocator) {
        this.mongoOperations = mongoOperations;
        this.connectionSignUp = connectionSignUp;
        this.connectionConverter = connectionConverter;
        this.connectionFactoryLocator = connectionFactoryLocator;
    }


    @Override
    public List<String> findUserIdsWithConnection(Connection<?> connection) {
        ConnectionKey key = connection.getKey();
        Query query = query(where("providerId").is(key.getProviderId()).and("providerUserId").is(key.getProviderUserId()));
        query.fields().include("userId");
        List<ConnectionModel> mongoConnections = mongoOperations.find(query, ConnectionModel.class);
        List<String> localUserIds = mongoConnections.stream().map(ConnectionModel::getUserId).collect(Collectors.toList());
        if (localUserIds.isEmpty() && connectionSignUp != null) {
            String newUserId = connectionSignUp.execute(connection);
            if (newUserId != null) {
                createConnectionRepository(newUserId).addConnection(connection);
                return Collections.singletonList(newUserId);
            }
        }
        return localUserIds;
    }

    @Override
    public Set<String> findUserIdsConnectedTo(String providerId, Set<String> providerUserIds) {
        Query query = query(where("providerId").is(providerId).and("providerUserId").in(providerUserIds));
        query.fields().include("userId");
        List<ConnectionModel> connections = mongoOperations.find(query, ConnectionModel.class);
        return connections.stream().map(ConnectionModel::getUserId).collect(Collectors.toSet());
    }

    @Override
    public ConnectionRepository createConnectionRepository(String userId) {
        Objects.requireNonNull(userId, "userId must not be null");
        return new UserConnectionRepository(userId, mongoOperations, connectionFactoryLocator, connectionConverter);
    }
}
