/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.common.rest.validation;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Created by Egor Petushkov
 * on 11.01.17.
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class TestBody {
    @Min(0)
    @Max(100)
    @JsonProperty
    int age;

    @NotBlank
    @JsonProperty
    String name;

    @NotNull
    @Valid
    @JsonProperty
    TestInnerBody innerBody;

    public TestInnerBody getInnerBody() {
        return innerBody;
    }

    public void setInnerBody(TestInnerBody innerBody) {
        this.innerBody = innerBody;
    }

    TestBody(){}

    public TestBody(int age, String name, TestInnerBody innerBody) {
        this.age = age;
        this.name = name;
        this.innerBody = innerBody;
    }
}
