/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.common.rest.validation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Egor Petushkov
 * on 11.01.17.
 */
@SpringBootTest(classes = ValidationConfig.class)
public class CommonExceptionHandlerTest extends AbstractTestNGSpringContextTests {
    private static final String APPLICATION_HAL_JSON_CHARSET_UTF_8 = "application/hal+json;charset=UTF-8";

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    @BeforeMethod
    public void setup() throws Exception {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .build();
    }

    @Test
    public void showError() throws Exception {
        String content = mvc.perform(
                put("/test")
                        .contentType(APPLICATION_HAL_JSON_CHARSET_UTF_8)
                        .content(
                                o2j(
                                        new TestBody(200, "", new TestInnerBody("", null))
                                )
                        )
        )
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();
        System.out.println(content);
    }

    private String o2j(Object o) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(o);
    }
}
