/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.common.rest.validation;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Created by Egor Petushkov
 * on 11.01.17.
 */
@RestController
@RequestMapping(value = "/test")
public class TestController {

    @RequestMapping(value = "", method = RequestMethod.PUT)
    public ResponseEntity<?> get(@Valid @RequestBody TestBody body){
        return new ResponseEntity<>(body, HttpStatus.OK);
    }

    public static Object getExampleBody(Class target){
        if(target == TestBody.class){
            return new TestBody(50, "Abc", new TestInnerBody("a", "b"));
        }
        return null;
    }
}
