/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.common.rest.validation;

import artwork.common.rest.validation.error.ErrorsResponse;
import artwork.common.rest.validation.error.MalformedJsonError;
import artwork.common.rest.validation.error.ValidationError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Denis Kuchugurov
 * on 11.08.2016.
 */
@ControllerAdvice
class CommonExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(CommonExceptionHandler.class);

    private Object getExampleBody(Class controller, Class target) {
        try {
            return controller.getMethod("getExampleBody", Class.class).invoke(null, target);
        } catch (Exception e) {
            log.warn(
                    String.format(
                            "Cannot get example body in %s for %s",
                            controller.getCanonicalName(),
                            target.getCanonicalName()
                    ),
                    e
            );
            return null;
        }
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException exception, HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.debug("", exception);

        List<ValidationError> errors = new ArrayList<>();

        BindingResult bindingResult = exception.getBindingResult();
        for (FieldError error : bindingResult.getFieldErrors()) {
            LinkedList<Object> arguments = new LinkedList<>(Arrays.asList(error.getArguments()));
            arguments.pollFirst();
            errors.add(new ValidationError(error.getField(), error.getCode(), error.getDefaultMessage(), arguments));
        }

        return super.handleExceptionInternal(
                exception,
                new ErrorsResponse(
                        errors.stream().sorted().collect(Collectors.toList()),
                        getExampleBody(
                                exception.getParameter().getMethod().getDeclaringClass(),
                                exception.getBindingResult().getTarget().getClass()
                        )
                ),
                headers,
                status,
                request
        );
    }

    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
                                                                  HttpHeaders headers, HttpStatus status,
                                                                  WebRequest request) {
        return super.handleExceptionInternal(
                ex,
                new MalformedJsonError(ex.getMessage()),
                headers,
                status,
                request
        );
    }
}
