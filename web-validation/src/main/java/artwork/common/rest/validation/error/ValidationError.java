/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.common.rest.validation.error;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by Denis Kuchugurov
 * on 01/12/2016.
 */
public class ValidationError extends Error {

    @JsonProperty
    final String property;

    public ValidationError(String property, String code, String message) {
        this(property, code, message, Collections.emptyList());
    }

    public ValidationError(String property, String code, String message, List<Object> arguments) {
        super(code, message, arguments);
        this.property = property;
    }

    public static ValidationError notFound(String property, String message) {
        return new ValidationError(property, "notFound", message);
    }

    public static ValidationError limit(String property, Number limit, Number maxAvailable, String message) {
        return new ValidationError(property, "limit", message, Arrays.asList(limit, maxAvailable));
    }

    public static ValidationError invalid(String property, String message) {
        return new ValidationError(property, "invalid", message);
    }

    public static ValidationError range(String property, Number min, Number max, String message) {
        return new ValidationError(property, "range", message, Arrays.asList(min, max));
    }

    public static ValidationError min(String property, Number min, String message){
        return new ValidationError(property, "min", message, Arrays.asList(min));
    }

    public static ValidationError max(String property, Number max, String message){
        return new ValidationError(property, "max", message, Arrays.asList(max));
    }

    public static ValidationError rangeDate(String property, long min, long max, String message) {
        return new ValidationError(property, "rangeDate", message, Arrays.asList(min, max));
    }

    public static ValidationError minDate(String property, long min, String message){
        return new ValidationError(property, "minDate", message, Arrays.asList(min));
    }

    public static ValidationError maxDate(String property, long max, String message){
        return new ValidationError(property, "maxDate", message, Arrays.asList(max));
    }
}
