/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.common.rest.validation.constraints.rules;

import artwork.common.rest.validation.constraints.pattern.PatternCode;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

/**
 * Created by Egor Petushkov
 * on 16.12.16.
 */
@NotBlank
@Length(max = 50)
@PatternCode.List({
        @PatternCode(regexp = "^[A-Za-z.,'\\-]+$", message = "Name can contain only letters and punctuation marks", code = "invalid_name"),
})
@Constraint(validatedBy = {})
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface NameRule {

    String code() default "name_must_be_valid";

    String message() default "Name doesn't match validation constraints";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}