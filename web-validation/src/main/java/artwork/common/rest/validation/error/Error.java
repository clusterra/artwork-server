/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.common.rest.validation.error;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by Egor Petushkov
 * on 14.12.16.
 */
public class Error implements Comparable<Error> {

    @JsonProperty
    final String code;

    @JsonProperty
    final String message;

    @JsonProperty
    final List<Object> arguments;

    public Error(String code, String message, List<Object> arguments) {
        this.code = code;
        this.message = message;
        this.arguments = arguments;
    }

    @Override
    public int compareTo(Error o) {
        return this.code.compareTo(o.code);
    }
}
