/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.common.rest.validation.error;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collections;
import java.util.List;

/**
 * Created by Denis Kuchugurov
 * on 01/12/2016.
 */
public class ErrorsResponse {

    @JsonProperty
    final List<? extends Error> errors;

    @JsonProperty
    final String message;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty
    final Object exampleBody;

    public ErrorsResponse(Error error) {
        this(error, null);
    }

    public ErrorsResponse(Error error, Object exampleBody) {
        this(Collections.singletonList(error), exampleBody);
    }

    public ErrorsResponse(List<? extends Error> errors, String message, Object exampleBody) {
        this.message = message;
        this.errors = errors;
        this.exampleBody = exampleBody;
    }

    public ErrorsResponse(List<? extends Error> errors, Object exampleBody) {
        this(errors, String.format("validation failed with %s error(s)", errors.size()), exampleBody);
    }

}
