/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.likeable.domain;


import artwork.likeable.domain.dislike.Dislike;
import artwork.likeable.domain.dislike.Disliker;
import artwork.likeable.domain.dislike.DislikerId;
import artwork.likeable.domain.like.Like;
import artwork.likeable.domain.like.Liker;
import artwork.likeable.domain.like.LikerId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by Denis Kuchugurov
 * on 14/03/16 00:20.
 */
public interface Likeable {

    boolean like(Liker liker);

    boolean unlike(Liker liker);

    boolean dislike(Disliker disliker);

    boolean undislike(Disliker disliker);

    boolean alreadyLiked(LikerId likerId);

    boolean alreadyDisliked(DislikerId dislikerId);

    Page<Like> findLikes(Pageable pageable);

    Page<Dislike> findDislikes(Pageable pageable);

    List<Liker> getLast10Likes();

    List<Disliker> getLast10Dislikes();

    int getLikesCount();

    int getDislikesCount();
}
