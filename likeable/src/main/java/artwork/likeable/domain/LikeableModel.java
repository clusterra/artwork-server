/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.likeable.domain;


import artwork.likeable.domain.dislike.Dislike;
import artwork.likeable.domain.dislike.DislikeId;
import artwork.likeable.domain.dislike.DislikeModel;
import artwork.likeable.domain.dislike.Disliker;
import artwork.likeable.domain.dislike.DislikerId;
import artwork.likeable.domain.like.Like;
import artwork.likeable.domain.like.LikeId;
import artwork.likeable.domain.like.LikeModel;
import artwork.likeable.domain.like.Liker;
import artwork.likeable.domain.like.LikerId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Created by Denis Kuchugurov
 * on 14/03/16 19:49.
 */
public class LikeableModel implements Likeable {

    @Id
    private LikeableId likeableId;

    private int likesCount;

    private int dislikesCount;

    @NotNull
    @Valid
    private List<Liker> likedLast10;

    @NotNull
    @Valid
    private List<Disliker> dislikedLast10;


    {
        Objects.requireNonNull(LikeableDependencies.dislikeModelRepository, "check if Spring context is initialized properly");
        Objects.requireNonNull(LikeableDependencies.likeModelRepository, "check if Spring context is initialized properly");
        Objects.requireNonNull(LikeableDependencies.eventPublisher, "check if Spring context is initialized properly");
    }

    @PersistenceConstructor
    LikeableModel() {

    }

    public LikeableModel(LikeableId likeableId, List<Liker> likedLast10, List<Disliker> dislikedLast10) {
        this.likeableId = likeableId;
        this.likedLast10 = likedLast10;
        this.dislikedLast10 = dislikedLast10;
    }

    @Override
    public boolean like(Liker liker) {
        Objects.requireNonNull(liker);

        undislike(Disliker.from(liker));

        boolean exists = LikeableDependencies.likeModelRepository.exists(new LikeId(likeableId.getUuid(), liker.getUuid()));

        if (!exists) {
            LikeableDependencies.likeModelRepository.save(new LikeModel(likeableId, liker));
            likesCount++;

            if (likedLast10.size() >= 10) {
                likedLast10.remove(likedLast10.size() - 1);
            }
            likedLast10.add(0, liker);
            return true;
        }
        return false;
    }

    @Override
    public boolean unlike(Liker liker) {
        Objects.requireNonNull(liker);
        LikeModel liked = LikeableDependencies.likeModelRepository.findOne(new LikeId(likeableId.getUuid(), liker.getUuid()));
        if (liked != null) {
            LikeableDependencies.likeModelRepository.delete(liked);
            likesCount--;
            likedLast10.remove(liker);
            return true;
        }
        return false;
    }

    @Override
    public boolean dislike(Disliker disliker) {
        Objects.requireNonNull(disliker);

        unlike(Liker.from(disliker));

        boolean exists = LikeableDependencies.dislikeModelRepository.exists(new DislikeId(likeableId.getUuid(), disliker.getUuid()));

        if (!exists) {
            LikeableDependencies.dislikeModelRepository.save(new DislikeModel(likeableId, disliker));
            dislikesCount++;
            if (dislikedLast10.size() >= 10) {
                dislikedLast10.remove(dislikedLast10.size() - 1);
            }
            dislikedLast10.add(0, disliker);
            return true;
        }

        return false;
    }

    @Override
    public boolean undislike(Disliker disliker) {
        Objects.requireNonNull(disliker);
        DislikeModel disliked = LikeableDependencies.dislikeModelRepository.findOne(new DislikeId(likeableId.getUuid(), disliker.getUuid()));
        if (disliked != null) {
            LikeableDependencies.dislikeModelRepository.delete(disliked);
            dislikesCount--;
            dislikedLast10.remove(disliker);
            return true;
        }
        return false;
    }

    @Override
    public boolean alreadyLiked(LikerId likerId) {
        return LikeableDependencies.likeModelRepository.exists(new LikeId(likeableId.getUuid(), likerId.getUuid()));
    }

    @Override
    public boolean alreadyDisliked(DislikerId dislikerId) {
        return LikeableDependencies.dislikeModelRepository.exists(new DislikeId(likeableId.getUuid(), dislikerId.getUuid()));
    }

    @Override
    public Page<Like> findLikes(Pageable pageable) {
        return LikeableDependencies.likeModelRepository.findByIdLikeableUuid(likeableId.getUuid(), pageable);
    }

    @Override
    public Page<Dislike> findDislikes(Pageable pageable) {
        return LikeableDependencies.dislikeModelRepository.findByIdLikeableUuid(likeableId.getUuid(), pageable);
    }

    @Override
    public int getLikesCount() {
        return likesCount;
    }

    @Override
    public int getDislikesCount() {
        return dislikesCount;
    }

    @Override
    public List<Liker> getLast10Likes() {
        return Collections.unmodifiableList(likedLast10);
    }

    @Override
    public List<Disliker> getLast10Dislikes() {
        return Collections.unmodifiableList(dislikedLast10);
    }

}
