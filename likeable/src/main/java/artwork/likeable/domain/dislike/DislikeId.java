/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.likeable.domain.dislike;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;

import javax.validation.Valid;
import java.io.Serializable;

/**
 * Created on 04/03/2017.
 *
 * @author kepkap
 */
public class DislikeId implements Serializable {

    @Valid
    @NotEmpty
    @Indexed(direction = IndexDirection.DESCENDING)
    private final String likeableUuid;

    @Valid
    @NotEmpty
    private final String dislikerUuid;

    @PersistenceConstructor
    public DislikeId(String likeableUuid, String dislikerUuid) {
        this.likeableUuid = likeableUuid;
        this.dislikerUuid = dislikerUuid;
    }

    public String getLikeableUuid() {
        return likeableUuid;
    }

    public String getDislikerUuid() {
        return dislikerUuid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DislikeId dislikeId = (DislikeId) o;

        if (!likeableUuid.equals(dislikeId.likeableUuid)) return false;
        return dislikerUuid.equals(dislikeId.dislikerUuid);
    }

    @Override
    public int hashCode() {
        int result = likeableUuid.hashCode();
        result = 31 * result + dislikerUuid.hashCode();
        return result;
    }
}
