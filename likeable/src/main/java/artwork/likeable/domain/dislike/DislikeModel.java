/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.likeable.domain.dislike;

import artwork.likeable.domain.LikeableId;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Objects;

/**
 * Created on 12/01/2017.
 *
 * @author kepkap
 */
@Document(collection = "likeable_dislike")
public class DislikeModel implements Dislike {

    @CreatedDate
    @Indexed(direction = IndexDirection.DESCENDING)
    private Date createdDate;

    @Version
    private Long version;

    @Id
    @Valid
    private DislikeId id;

    @NotNull
    @Valid
    private Disliker disliker;

    @PersistenceConstructor
    DislikeModel() {
    }

    public DislikeModel(LikeableId likeableId, Disliker disliker) {
        Objects.requireNonNull(likeableId);
        Objects.requireNonNull(disliker);
        this.id = new DislikeId(likeableId.getUuid(), disliker.getUuid());
        this.disliker = disliker;
    }

    public DislikeId getId() {
        return id;
    }

    @Override
    public Disliker getDisliker() {
        return disliker;
    }

    @Override
    public Date getCreatedDate() {
        return createdDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DislikeModel that = (DislikeModel) o;

        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
