/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.likeable.domain;

import artwork.likeable.domain.dislike.DislikeModelRepository;
import artwork.likeable.domain.like.LikeModelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

/**
 * Created on 12/01/2017.
 *
 * @author kepkap
 */
@Component
class LikeableDependencies {

    static DislikeModelRepository dislikeModelRepository;
    static LikeModelRepository likeModelRepository;
    static ApplicationEventPublisher eventPublisher;

    @Autowired
    public LikeableDependencies(DislikeModelRepository dislikeModelRepository,
                                LikeModelRepository likeModelRepository,
                                ApplicationEventPublisher eventPublisher) {
        LikeableDependencies.dislikeModelRepository = dislikeModelRepository;
        LikeableDependencies.likeModelRepository = likeModelRepository;
        LikeableDependencies.eventPublisher = eventPublisher;
    }
}
