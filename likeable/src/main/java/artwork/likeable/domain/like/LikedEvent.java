/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.likeable.domain.like;


import artwork.likeable.domain.LikeableId;
import org.springframework.context.ApplicationEvent;

/**
 * Created by Denis Kuchugurov
 * on 15.03.2016.
 */
public class LikedEvent extends ApplicationEvent {

    private final LikeableId likeableId;
    private final Liker liker;

    public LikedEvent(LikeableId likeableId, Liker liker) {
        super(likeableId);
        this.likeableId = likeableId;
        this.liker = liker;
    }

    public LikeableId getLikeableId() {
        return likeableId;
    }

    public Liker getLiker() {
        return liker;
    }
}
