/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.likeable.domain.like;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;

import javax.validation.Valid;
import java.io.Serializable;

/**
 * Created on 04/03/2017.
 *
 * @author kepkap
 */
public class LikeId implements Serializable {

    @Valid
    @NotEmpty
    @Indexed(direction = IndexDirection.DESCENDING)
    private final String likeableUuid;

    @Valid
    @NotEmpty
    private final String likerUuid;

    @PersistenceConstructor
    public LikeId(String likeableUuid, String likerUuid) {
        this.likeableUuid = likeableUuid;
        this.likerUuid = likerUuid;
    }

    public String getLikeableUuid() {
        return likeableUuid;
    }

    public String getLikerUuid() {
        return likerUuid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LikeId dislikeId = (LikeId) o;

        if (!likeableUuid.equals(dislikeId.likeableUuid)) return false;
        return likerUuid.equals(dislikeId.likerUuid);
    }

    @Override
    public int hashCode() {
        int result = likeableUuid.hashCode();
        result = 31 * result + likerUuid.hashCode();
        return result;
    }
}
