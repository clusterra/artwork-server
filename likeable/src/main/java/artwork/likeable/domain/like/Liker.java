/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.likeable.domain.like;

import artwork.likeable.domain.dislike.Disliker;
import artwork.web.security.domain.UserProfile;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.URL;

/**
 * Created by Denis Kuchugurov
 * on 10/03/16 23:20.
 */
public class Liker implements UserProfile {

    @NotEmpty
    private String uuid;

    @NotEmpty
    private String displayName;

    @URL
    private String imageUrl;

    @URL
    private String profileUrl;

    public Liker() {
    }

    public Liker(String uuid, String displayName, String imageUrl, String profileUrl) {
        this.uuid = uuid;
        this.displayName = displayName;
        this.imageUrl = imageUrl;
        this.profileUrl = profileUrl;
    }


    public static Liker from(Disliker disliker) {
        return new Liker(disliker.getUuid(), disliker.getDisplayName(), disliker.getImageUrl(), disliker.getProfileUrl());
    }

    public String getUuid() {
        return uuid;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Liker liker = (Liker) o;

        return uuid.equals(liker.uuid);
    }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }


    @Override
    public String toString() {
        return "Liker{" +
                "uuid='" + uuid + '\'' +
                '}';
    }
}
