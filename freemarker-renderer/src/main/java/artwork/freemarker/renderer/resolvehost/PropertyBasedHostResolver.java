/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.freemarker.renderer.resolvehost;

import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Denis Kuchugurov
 * on 31/07/15 22:51.
 */
public class PropertyBasedHostResolver implements HostResolver {

    private static final Logger LOGGER = LoggerFactory.getLogger(PropertyBasedHostResolver.class);


    private String host;

    public void setHost(String host) {
        this.host = host;
    }

    @Override
    public String resolveHost() {
        Validate.notEmpty(host);
        LOGGER.debug("resolved host from property: {}", host);

        return host;
    }
}
