package artwork.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.social.SocialWebAutoConfiguration;
import org.springframework.context.annotation.ImportResource;


/**
 * @author Alexander Fyodorov
 */


@SpringBootApplication(exclude = {
    SocialWebAutoConfiguration.class,
})
@ImportResource("classpath*:META-INF/spring/*.xml")
public class Main {

    public static void main(String[] args) {

        EnvironmentLogger.log();

        SpringApplication.run(Main.class, args);
    }

}