/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created on 03/01/2017.
 *
 * @author kepkap
 */
public class EnvironmentLogger implements ApplicationListener<ContextRefreshedEvent> {

    private static final Logger log = LoggerFactory.getLogger(EnvironmentLogger.class);


    static void log() {

        Stream<Map.Entry<String, String>> stream = System.getenv().entrySet().stream().filter(
            e ->
                e.getKey().startsWith("AW_") ||
                    e.getKey().startsWith("JAVA_"));

        log.info("*==* ENVIRONMENT CONFIGURATION *==* ");
        List<Map.Entry<String, String>> entries = stream.sorted(Comparator.comparing(Map.Entry::getKey)).collect(Collectors.toList());
        for (Map.Entry<String, String> entry : entries) {
            String key = entry.getKey();
            boolean hideCompletely =
                key.toLowerCase().contains("pass") || key.toLowerCase().contains("mongodb://") || key.toLowerCase().contains("pwd");
            log.info("{}={}", entry.getKey(), MaskUtil.mask(entry.getValue(), hideCompletely));
        }

        log.debug("debug level enabled");
        log.trace("trace level enabled");
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        log();
    }
}
