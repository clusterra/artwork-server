/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.email.confirmation.domain;

import artwork.email.confirmation.TestConfirmationConfig;
import artwork.email.confirmation.domain.model.Token;
import artwork.iam.user.model.User;
import artwork.iam.user.model.UserId;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

/**
 * Created by Egor Petushkov
 * on 24.07.17.
 */
@SpringBootTest(classes={TestConfirmationConfig.class})
public class ConfirmationServiceTest extends AbstractTestNGSpringContextTests {

    @Test
    public void changeEmail() throws Exception {
        User user = User.create(
                UserId.generateId(),
                randomAlphabetic(20),
                "http://artwork.pro/user/profile",
                "http://artwork.pro/user/profile/image.png",
                "user@artwork.pro"
        );
        Token token = Token.create(user.getUserId(), randomAlphabetic(5) + "@somewhere.su");
        ConfirmationService.confirmEmailUpdate(user.getUserId(), token.getId());
        user = User.findBy(user.getUserId());
        Assert.assertEquals(user.getEmail(), token.getEmail());
    }
}
