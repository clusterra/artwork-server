/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.email.confirmation.domain.model;

import artwork.email.confirmation.TestConfirmationConfig;
import artwork.iam.user.model.UserId;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;

/**
 * Created by Egor Petushkov
 * on 24.07.17.
 */
@SpringBootTest(classes={TestConfirmationConfig.class})
public class TokenTest extends AbstractTestNGSpringContextTests {

    private UserId userId;

    @BeforeMethod
    public void before() {
        userId = new UserId(randomAlphanumeric(8));
    }

    @Test
    public void createToken() throws Exception {
        String email = randomAlphanumeric(8) + "@somewhere.su";
        Token token = Token.create(userId, email);
        Assert.assertEquals(token.getUserId(), userId.getId());
        Assert.assertEquals(token.getEmail(), email);

        token = Token.findById(token.getId());
        Assert.assertEquals(token.getUserId(), userId.getId());
        Assert.assertEquals(token.getEmail(), email);
    }

    @Test
    public void createSecondToken() throws Exception {
        String email = randomAlphanumeric(8) + "@somewhere.su";
        String email2 = randomAlphanumeric(8) + "@somewhere.su";
        Token token = Token.create(userId, email);
        token = Token.create(userId, email2);
        token = Token.findById(token.getId());
        Assert.assertEquals(token.getEmail(), email2);
    }

    @Test(expectedExceptions = TokenNotFoundException.class)
    public void deleteToken() throws Exception {
        String email = randomAlphanumeric(8) + "@somewhere.su";
        Token token = Token.create(userId, email);
        token.delete();
        Token.findById(token.getId());
    }
}
