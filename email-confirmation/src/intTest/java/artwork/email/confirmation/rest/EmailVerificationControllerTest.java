/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.email.confirmation.rest;

import artwork.email.confirmation.TestConfirmationConfig;
import artwork.iam.user.model.User;
import artwork.iam.user.model.UserId;
import artwork.web.security.social.UserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Egor Petushkov
 * on 24.07.17.
 */
@SpringBootTest(classes={TestConfirmationConfig.class})
public class EmailVerificationControllerTest extends AbstractTestNGSpringContextTests {
    @Autowired
    private WebApplicationContext context;

    protected MockMvc mvc;

    protected Authentication authentication;

    @BeforeMethod
    public void before() throws Exception {

        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

        User user = User.create(UserId.generateId(), "displayName", "http://localhost/image_url", "http://localhost/profile_url", "email@email.com");


        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_SOCIAL_USER");
        authentication = new TestingAuthenticationToken(UserDetails.from(user.getUuid()), "", authorities);
        authentication.setAuthenticated(true);
    }

    @Test
    public void requestChangeEmail() throws Exception {
        mvc.perform(post("/api/emails")
                .content(
                        String.format(
                                "{\"email\":\"%s\", \"frontendUrl\":\"%s\"}",
                                randomAlphabetic(5) + "@somewhere.su",
                                "http://localhost/token?t=")
                )
                .contentType(MediaType.APPLICATION_JSON)
                .with(authentication(authentication)))
                .andDo(print())
                .andExpect(status().isAccepted());
    }
}
