/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.email.confirmation.domain;

import artwork.email.confirmation.domain.model.Token;
import artwork.email.confirmation.domain.model.TokenNotFoundException;
import artwork.iam.user.model.User;
import artwork.iam.user.model.UserId;
import artwork.iam.user.model.UserNotFoundException;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Objects;

import static artwork.email.confirmation.domain.ConfirmationDependencies.confirmEmailBody;
import static artwork.email.confirmation.domain.ConfirmationDependencies.emailSender;
import static artwork.email.confirmation.domain.ConfirmationDependencies.messageSource;


/**
 * Created by Egor Petushkov
 * on 20.07.17.
 */
public class ConfirmationService {

    public static boolean requestEmailUpdate(UserId userId, String email, String baseUrl) throws UserNotFoundException {
        User user = User.findBy(userId);
        if (user.getEmail().equals(email)) {
            return false;
        }
        Token token = Token.create(userId, email);

        // Render baseUrl and token.getId() it somewhere????
        String messageSubject = messageSource.getMessage("message.email.invite.subject", null, "Welcome to Artwork", LocaleContextHolder.getLocale());
        emailSender.send(email, messageSubject, confirmEmailBody);
        return true;
    }

    public static void confirmEmailUpdate(UserId userId, String tokenId) throws UserNotFoundException, TokenNotFoundException, InvalidTokenException {
        Objects.requireNonNull(userId);
        Token token = Token.findById(tokenId);
        if (!token.getUserId().equals(userId.getId())) {
            throw new InvalidTokenException();
        }

        User user = User.findBy(userId);
        user.updateEmail(token.getEmail());
        token.delete();
    }
}
