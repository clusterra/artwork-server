/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.email.confirmation.domain.model;

import artwork.iam.user.model.UserId;
import org.hibernate.validator.constraints.Email;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.TimeToLive;
import org.springframework.data.redis.core.index.Indexed;

import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.UUID;

import static artwork.email.confirmation.domain.model.TokenDependencies.repository;

/**
 * Created by Egor Petushkov
 * on 22.11.16.
 */
@RedisHash("email_tokens")
public class Token {
    private final static long TOKEN_TTL = 60 * 60 * 24 * 4;

    @Id
    private String id;

    @Indexed
    private String userId;

    @NotNull
    @Email
    private String email;

    @TimeToLive
    private long expiration;

    @PersistenceConstructor
    Token() {
    }

    {
        Objects.requireNonNull(repository, "check if Spring context is initialized properly");
    }

    private Token(String id, UserId userId, String email, long expiration) {
        Objects.requireNonNull(userId);
        this.id = id;
        this.userId = userId.getId();
        this.email = email;
        this.expiration = expiration;
    }

    public String getId() {
        return id;
    }

    public String getUserId() {
        return userId;
    }

    public String getEmail() {
        return email;
    }

    public long getExpiration() {
        return expiration;
    }

    public void delete() {
        repository.delete(this);
    }

    public static Token create(UserId userId, String email) {
        Objects.requireNonNull(userId);
        Token oldToken = repository.findByUserId(userId.getId());
        if (oldToken != null) {
            oldToken.delete();
        }

        return repository.save(new Token(UUID.randomUUID().toString(), userId, email, TOKEN_TTL));
    }

    public static Token findById(String id) throws TokenNotFoundException {
        Token token = repository.findOne(id);
        if (token == null) {
            throw new TokenNotFoundException(id);
        }
        return token;
    }
}
