/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.email.confirmation.domain;

import artwork.email.confirmation.domain.model.TokenDependencies;
import artwork.mail.sender.EmailSender;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import java.io.IOException;

/**
 * Created by Egor Petushkov
 * on 20.07.17.
 */
@Configuration
public class ConfirmationDependencies {
    static String confirmEmailBody;
    static EmailSender emailSender;
    static MessageSource messageSource;

    @Autowired
    ConfirmationDependencies(
            @Value("classpath:WEB-INF/templates/index.html") Resource confirmEmailBody,
            EmailSender emailSender,
            MessageSource messageSource
    ) throws IOException {
        ConfirmationDependencies.confirmEmailBody = IOUtils.toString(confirmEmailBody.getInputStream(), "UTF-8");
        ConfirmationDependencies.emailSender = emailSender;
        ConfirmationDependencies.messageSource = messageSource;
    }
}
