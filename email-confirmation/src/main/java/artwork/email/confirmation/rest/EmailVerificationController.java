/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.email.confirmation.rest;

import artwork.email.confirmation.domain.ConfirmationService;
import artwork.email.confirmation.domain.InvalidTokenException;
import artwork.email.confirmation.domain.model.TokenNotFoundException;
import artwork.iam.user.model.UserId;
import artwork.iam.user.model.UserNotFoundException;
import artwork.web.security.tracking.CurrentUser;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.URL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by Egor Petushkov
 * on 20.07.17.
 */
@RestController
@RequestMapping(value = "/api/emails", produces = MediaTypes.HAL_JSON_VALUE)
public class EmailVerificationController {
    private static final Logger log = LoggerFactory.getLogger(EmailVerificationController.class);

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity updateEmail(@Valid @RequestBody ChangeEmailRequest request) throws UserNotFoundException {
        UserId userId = new UserId(CurrentUser.requireNotAnonymous().getUuid());
        if (ConfirmationService.requestEmailUpdate(userId, request.email, request.frontendUrl)) {
            return new ResponseEntity(HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity(HttpStatus.OK);
        }
    }

    static class ChangeEmailRequest {
        @NotNull
        @NotBlank
        @Email
        @JsonProperty
        String email;

        @NotNull
        @NotBlank
        @URL
        @JsonProperty
        String frontendUrl;

        @JsonCreator
        public ChangeEmailRequest(
            @JsonProperty("email") String email,
            @JsonProperty("frontendUrl") String frontendUrl) {
            this.email = email;
            this.frontendUrl = frontendUrl;
        }
    }

    @RequestMapping(value = "/{token}", method = RequestMethod.POST)
    public ResponseEntity confirmEmail(@PathVariable("token") String token) throws UserNotFoundException, TokenNotFoundException, InvalidTokenException {
        UserId userId = new UserId(CurrentUser.requireNotAnonymous().getUuid());
        ConfirmationService.confirmEmailUpdate(userId, token);
        return new ResponseEntity(HttpStatus.ACCEPTED);
    }
}
