/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.email.confirmation;

import artwork.email.confirmation.domain.model.TokenManagerPkg;
import artwork.iam.IamConfig;
import artwork.mail.EmailSenderConfig;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.hateoas.config.EnableHypermediaSupport.HypermediaType;

/**
 * Created by Egor Petushkov
 * on 22.11.16.
 */
@Configuration
@EnableWebMvc
@EnableHypermediaSupport(type = {HypermediaType.HAL})
@EnableSpringDataWebSupport
@ComponentScan
@Import({
        RedisAutoConfiguration.class,
        EmailSenderConfig.class,
        IamConfig.class,
})
@EnableRedisRepositories(basePackageClasses = TokenManagerPkg.class)
public class EmailConfirmationConfig {
    @Bean
    public MessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.addBasenames("WEB-INF.i18n.messages4invite");
        return messageSource;
    }
}
