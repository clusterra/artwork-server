/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.domain.owner;

import artwork.core.TestCoreConfig;
import artwork.core.domain.TestUser;
import artwork.core.domain.images.ImageMeta;
import artwork.core.domain.work.WorkId;
import artwork.core.domain.work.WorkModel;
import artwork.core.domain.work.WorkOwner;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.util.MimeTypeUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * Created by kepkap
 * on 26/06/16.
 */
@SpringBootTest(classes = {TestCoreConfig.class})
public class OwnershipAwareTest extends AbstractTestNGSpringContextTests {


    private WorkOwner workOwner = TestUser.workOwner();
    private WorkOwner notWorkOwner = TestUser.workOwner();

    private WorkModel work;

    @BeforeMethod
    public void before() throws Exception {

        SecurityContextHolder.getContext().setAuthentication(new TestingAuthenticationToken(workOwner.getUuid(), ""));

        work = new WorkModel(WorkId.generateId(), workOwner, new ImageMeta(RandomStringUtils.randomAlphanumeric(10), "image/png"), "description");
    }

    @Test
    public void test_owner() {
        assertThat(work.isMine(workOwner.getWorkOwnerId())).isTrue();
        assertThat(work.isMine(notWorkOwner.getWorkOwnerId())).isFalse();
    }
}
