/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.domain.suggestion;

import artwork.commentary.domain.comment.Comment;
import artwork.commentary.domain.comment.CommentAuthor;
import artwork.commentary.domain.commentable.Commentable;
import artwork.core.TestCoreConfig;
import artwork.core.domain.TestUser;
import artwork.core.domain.images.ImageMeta;
import artwork.core.domain.work.Work;
import artwork.core.domain.work.WorkId;
import artwork.core.domain.work.WorkModel;
import artwork.core.domain.work.WorkOwner;
import artwork.core.domain.work.WorkOwnerId;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.util.MimeTypeUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Denis Kuchugurov
 * on 24.02.2016.
 */
@SpringBootTest(classes = TestCoreConfig.class)
public class SuggestionTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private SuggestionAddedEventInterceptor suggestionAddedEventInterceptor;

    @Autowired
    private SuggestionRemovedEventInterceptor suggestionRemovedEventInterceptor;

    private WorkOwner workOwner = TestUser.workOwner("work_test_user_1");
    private SuggestionOwner suggestionOwner1 = TestUser.suggestionOwner();
    private SuggestionOwner suggestionOwner2 = TestUser.suggestionOwner();
    private SuggestionOwner suggestionOwner3 = TestUser.suggestionOwner();

    private CommentAuthor commentableUser1 = TestUser.commentAuthor();
    private CommentAuthor commentableUser2 = TestUser.commentAuthor();

    private Work work;

    @BeforeMethod
    public void before() throws Exception {

        SecurityContextHolder.getContext().setAuthentication(new TestingAuthenticationToken(workOwner.getUuid(), ""));

        work = new WorkModel(WorkId.generateId(), workOwner, new ImageMeta(RandomStringUtils.randomAlphanumeric(10), "image/png"), "description");
        Commentable.findOne(work.getWorkId().toCommentableId()).addComment(commentableUser1, "text comment work");
    }

    @AfterMethod
    public void after() {
        SecurityContextHolder.clearContext();
    }

    @Test
    public void test_mark_read() throws Exception {
        Suggestion suggestion = work.addSuggestion("test_description", suggestionOwner1, new Point(10, 10));

        assertThat(suggestion.isRead()).isFalse();

        suggestion.markRead(workOwner.getWorkOwnerId());
        assertThat(suggestion.isRead()).isTrue();

    }

    @Test(expectedExceptions = IllegalWorkOwnerForMarkReadOperationException.class)
    public void test_mark_read_illegal_owner() throws Exception {
        Suggestion suggestion = work.addSuggestion("test_description", suggestionOwner1, new Point(10, 10));

        assertThat(suggestion.isRead()).isFalse();

        suggestion.markRead(new WorkOwnerId("wrong"));
    }

    @Test
    public void test_work_suggestions_count() throws Exception {

        assertThat(work.getSuggestionsCount()).isEqualTo(0);

        work.addSuggestion("test_description", suggestionOwner1, new Point(10, 10));
        work.addSuggestion("test_description", suggestionOwner1, new Point(10, 10));

        assertThat(Work.findOne(work.getWorkId()).getSuggestionsCount()).isEqualTo(2);
    }

    @Test
    public void test_suggestion_group_has_new() throws Exception {
        Suggestion suggestion = work.addSuggestion("test_description", suggestionOwner1, new Point(10, 10));
        assertThat(suggestion.isRead()).isFalse();


        Page<SuggestionGroup> page = Suggestion.findByWorkIdGroupedByOwners(work.getWorkId(), new PageRequest(0, 100));


        assertThat(page.getContent()).hasSize(1);
        SuggestionGroup group = page.getContent().get(0);
        assertThat(group.getSuggestions().get(0)).isEqualTo(suggestion);

        assertThat(group.hasNotRead()).isTrue();

        suggestion.markRead(workOwner.getWorkOwnerId());

        page = Suggestion.findByWorkIdGroupedByOwners(work.getWorkId(), new PageRequest(0, 100));

        assertThat(page.getContent().get(0).hasNotRead()).isFalse();
    }

    @Test
    public void test_comment_suggestion() throws Exception {
        String description = "test_description";
        Suggestion suggestion = work.addSuggestion(description, suggestionOwner1, new Point(10, 10));


        Commentable workCommentable = Commentable.findOne(work.getWorkId().toCommentableId());
        Page<Comment> page = workCommentable.findComments(new PageRequest(0, 100));
        assertThat(page.getContent()).hasSize(2);
        assertThat(page.getContent().get(1).getText()).contains(description);


        Commentable commentable = Commentable.findOne(suggestion.getSuggestionId().toCommentableId());
        Comment comment_1 = commentable.addComment(commentableUser1, "suggestion text");
        Comment comment_2 = commentable.addComment(commentableUser1, "suggestion text");
        Comment comment_3 = commentable.addComment(commentableUser1, "suggestion text");

        page = commentable.findComments(new PageRequest(0, 100));

        assertThat(page.getContent()).containsExactly(comment_1, comment_2, comment_3);
        assertThat(Commentable.findOne(commentable.getCommentableId()).getCommentsCount()).isEqualTo(3);

        comment_1.deleteText(commentableUser1);
        page = commentable.findComments(new PageRequest(0, 100));
        assertThat(page.getContent()).containsExactly(comment_1, comment_2, comment_3);
        assertThat(Commentable.findOne(commentable.getCommentableId()).getCommentsCount()).isEqualTo(3);
    }

    @Test
    public void test_find_suggestions_grouped_by_owners() throws Exception {
        Page<SuggestionGroup> page = work.findSuggestionsGroupedByOwners(new PageRequest(0, 10));
        assertThat(page.getContent()).isEmpty();

        Suggestion suggestion_11 = work.addSuggestion("new suggestion 1-1", suggestionOwner1, new Point(1, 0));
        Suggestion suggestion_12 = work.addSuggestion("new suggestion 1-2", suggestionOwner1, new Point(1, 0));
        Suggestion suggestion_13 = work.addSuggestion("new suggestion 1-3", suggestionOwner1, new Point(1, 0));


        Suggestion suggestion_21 = work.addSuggestion("new suggestion 2-1", suggestionOwner2, new Point(1, 0));
        Suggestion suggestion_22 = work.addSuggestion("new suggestion 2-2", suggestionOwner2, new Point(1, 0));

        Suggestion suggestion_31 = work.addSuggestion("new suggestion 3-1", suggestionOwner3, new Point(1, 0));
        Suggestion suggestion_32 = work.addSuggestion("new suggestion 3-2", suggestionOwner3, new Point(1, 0));
        Suggestion suggestion_33 = work.addSuggestion("new suggestion 3-3", suggestionOwner3, new Point(1, 0));

        page = work.findSuggestionsGroupedByOwners(new PageRequest(0, 2));

        assertThat(page.getContent()).hasSize(2);
        assertThat(page.getTotalElements()).isEqualTo(3);
        assertThat(page.getContent().get(1).getSuggestions()).containsExactly(suggestion_13, suggestion_12, suggestion_11);
        assertThat(page.getContent().get(1).getSuggestionOwner()).isEqualTo(suggestion_11.getSuggestionOwner());
        assertThat(page.getContent().get(1).getWorkId()).isEqualTo(suggestion_11.getWorkId());
        assertThat(page.getContent().get(0).getSuggestions()).containsExactly(suggestion_22, suggestion_21);
        assertThat(page.getContent().get(0).getSuggestionOwner()).isEqualTo(suggestion_21.getSuggestionOwner());
        assertThat(page.getContent().get(0).getWorkId()).isEqualTo(suggestion_21.getWorkId());

        page = work.findSuggestionsGroupedByOwners(page.nextPageable());

        assertThat(page.getContent()).hasSize(1);
        assertThat(page.getTotalElements()).isEqualTo(3);
        assertThat(page.getContent().get(0).getSuggestions()).containsExactly(suggestion_33, suggestion_32, suggestion_31);
        assertThat(page.getContent().get(0).getSuggestionOwner()).isEqualTo(suggestion_31.getSuggestionOwner());
        assertThat(page.getContent().get(0).getWorkId()).isEqualTo(suggestion_31.getWorkId());

    }

    @Test
    public void test_find_suggestions_grouped_by_owners_read_order() throws Exception {
        Page<SuggestionGroup> page = work.findSuggestionsGroupedByOwners(new PageRequest(0, 10));
        assertThat(page.getContent()).isEmpty();

        Suggestion suggestion_11 = work.addSuggestion("new suggestion 1-1", suggestionOwner1, new Point(1, 0));
        Suggestion suggestion_12 = work.addSuggestion("new suggestion 1-2", suggestionOwner1, new Point(1, 0));
        Suggestion suggestion_13 = work.addSuggestion("new suggestion 1-3", suggestionOwner1, new Point(1, 0));
        suggestion_12.markRead(workOwner.getWorkOwnerId());
        suggestion_11.markRead(workOwner.getWorkOwnerId());

        Suggestion suggestion_21 = work.addSuggestion("new suggestion 2-1", suggestionOwner2, new Point(1, 0));
        Suggestion suggestion_22 = work.addSuggestion("new suggestion 2-2", suggestionOwner2, new Point(1, 0));

        Suggestion suggestion_31 = work.addSuggestion("new suggestion 3-1", suggestionOwner3, new Point(1, 0));
        Suggestion suggestion_32 = work.addSuggestion("new suggestion 3-2", suggestionOwner3, new Point(1, 0));
        Suggestion suggestion_33 = work.addSuggestion("new suggestion 3-3", suggestionOwner3, new Point(1, 0));

        page = work.findSuggestionsGroupedByOwners(new PageRequest(0, 2));

        assertThat(page.getContent()).hasSize(2);
        assertThat(page.getTotalElements()).isEqualTo(3);
        assertThat(page.getContent().get(1).getSuggestions()).containsExactly(suggestion_13, suggestion_12, suggestion_11);
        assertThat(page.getContent().get(1).getSuggestionOwner()).isEqualTo(suggestion_11.getSuggestionOwner());
        assertThat(page.getContent().get(1).getWorkId()).isEqualTo(suggestion_11.getWorkId());
        assertThat(page.getContent().get(0).getSuggestions()).containsExactly(suggestion_22, suggestion_21);
        assertThat(page.getContent().get(0).getSuggestionOwner()).isEqualTo(suggestion_21.getSuggestionOwner());
        assertThat(page.getContent().get(0).getWorkId()).isEqualTo(suggestion_21.getWorkId());

        page = work.findSuggestionsGroupedByOwners(page.nextPageable());

        assertThat(page.getContent()).hasSize(1);
        assertThat(page.getTotalElements()).isEqualTo(3);
        assertThat(page.getContent().get(0).getSuggestions()).containsExactly(suggestion_33, suggestion_32, suggestion_31);
        assertThat(page.getContent().get(0).getSuggestionOwner()).isEqualTo(suggestion_31.getSuggestionOwner());
        assertThat(page.getContent().get(0).getWorkId()).isEqualTo(suggestion_31.getWorkId());

    }

    @Test
    public void test_find_suggestions_grouped_by_owners_order_by() throws Exception {

        Suggestion suggestion_11 = work.addSuggestion("new suggestion 1-1", suggestionOwner1, new Point(1, 0));
        Suggestion suggestion_12 = work.addSuggestion("new suggestion 1-2", suggestionOwner1, new Point(1, 0));
        Suggestion suggestion_13 = work.addSuggestion("new suggestion 1-3", suggestionOwner1, new Point(1, 0));

        Suggestion suggestion_21 = work.addSuggestion("new suggestion 2-1", suggestionOwner2, new Point(1, 0));
        Suggestion suggestion_22 = work.addSuggestion("new suggestion 2-2", suggestionOwner2, new Point(1, 0));

        Suggestion suggestion_31 = work.addSuggestion("new suggestion 3-1", suggestionOwner3, new Point(1, 0));
        Suggestion suggestion_32 = work.addSuggestion("new suggestion 3-2", suggestionOwner3, new Point(1, 0));
        Suggestion suggestion_33 = work.addSuggestion("new suggestion 3-3", suggestionOwner3, new Point(1, 0));

        Page<SuggestionGroup> page = work.findSuggestionsGroupedByOwners(new PageRequest(0, 10));

        assertThat(page.getContent()).hasSize(3);
        assertThat(page.getTotalElements()).isEqualTo(3);
        assertThat(page.getContent().get(2).getSuggestions()).containsExactly(suggestion_13, suggestion_12, suggestion_11);
        assertThat(page.getContent().get(2).getSuggestions().get(0)).isEqualTo(suggestion_13);
        assertThat(page.getContent().get(2).getLastCreatedDate()).isEqualTo(suggestion_13.getCreatedDate());
        assertThat(page.getContent().get(2).getSuggestionOwner()).isEqualTo(suggestion_11.getSuggestionOwner());
        assertThat(page.getContent().get(2).getWorkId()).isEqualTo(suggestion_11.getWorkId());

        assertThat(page.getContent().get(1).getSuggestions()).containsExactly(suggestion_22, suggestion_21);
        assertThat(page.getContent().get(1).getSuggestionOwner()).isEqualTo(suggestion_21.getSuggestionOwner());
        assertThat(page.getContent().get(1).getWorkId()).isEqualTo(suggestion_21.getWorkId());

        assertThat(page.getContent().get(0).getSuggestions()).containsExactly(suggestion_33, suggestion_32, suggestion_31);
        assertThat(page.getContent().get(0).getSuggestionOwner()).isEqualTo(suggestion_31.getSuggestionOwner());
        assertThat(page.getContent().get(0).getWorkId()).isEqualTo(suggestion_31.getWorkId());

    }

    @Test
    public void test_add_suggestion() throws Exception {

        Page<Suggestion> page = work.findSuggestions(new PageRequest(0, 10));
        assertThat(page.getContent()).isEmpty();

        Suggestion suggestion_1 = work.addSuggestion("new suggestion 1", suggestionOwner1, new Point(1, 0));
        assertThat(suggestionAddedEventInterceptor.getLastEvent().getSuggestion()).isEqualTo(suggestion_1);

        Commentable.findOne(suggestion_1.getSuggestionId().toCommentableId()).addComment(commentableUser1, "test_comment_1");
        Commentable.findOne(suggestion_1.getSuggestionId().toCommentableId()).addComment(commentableUser2, "test_comment_2");

        page = work.findSuggestions(new PageRequest(0, 10));
        assertThat(page.getContent()).containsExactly(suggestion_1);
    }

    @Test
    public void test_add_suggestion_does_not_increment_work_comments_count() throws Exception {
        assertThat(Work.findOne(work.getWorkId()).getCommentsCount()).isEqualTo(1);

        work.addSuggestion("new suggestion 1", suggestionOwner1, new Point(1, 0));
        work.addSuggestion("new suggestion 2", suggestionOwner1, new Point(1, 0));
        work.addSuggestion("new suggestion 3", suggestionOwner1, new Point(1, 0));

        assertThat(Work.findOne(work.getWorkId()).getCommentsCount()).isEqualTo(1);

        Commentable commentable = Commentable.findOne(work.getWorkId().toCommentableId());
        commentable.addComment(TestUser.commentAuthor(), "new comment1");
        commentable.addComment(TestUser.commentAuthor(), "new comment2");

        assertThat(Work.findOne(work.getWorkId()).getCommentsCount()).isEqualTo(3);
    }


    @Test
    public void test_remove_suggestion() throws Exception {

        Suggestion suggestion_1 = work.addSuggestion("new suggestion 1", suggestionOwner1, new Point(1, 0));

        Page<Suggestion> page = work.findSuggestions(new PageRequest(0, 10));
        assertThat(page.getContent()).containsExactly(suggestion_1);

        Page<Comment> commentPage = Commentable.findOne(work.getWorkId().toCommentableId()).findComments(new PageRequest(0, 10));
        assertThat(commentPage.getContent()).hasSize(2);


        Commentable.findOne(suggestion_1.getSuggestionId().toCommentableId()).addComment(commentableUser1, "test_comment_1");
        Commentable.findOne(suggestion_1.getSuggestionId().toCommentableId()).addComment(commentableUser2, "test_comment_2");

        commentPage = Commentable.findOne(work.getWorkId().toCommentableId()).findComments(new PageRequest(0, 10));
        assertThat(commentPage.getContent()).hasSize(2);

        work.removeMySuggestion(suggestionOwner1, suggestion_1.getSuggestionId());
        assertThat(suggestionRemovedEventInterceptor.getLastEvent().getSuggestion()).isEqualTo(suggestion_1);


        commentPage = Commentable.findOne(work.getWorkId().toCommentableId()).findComments(new PageRequest(0, 10));
        assertThat(commentPage.getContent()).hasSize(2);
        assertThat(commentPage.getContent().get(1).getText()).contains("removed");
        assertThat(commentPage.getContent().get(1).hasRelateTo()).isFalse();


        page = work.findSuggestions(new PageRequest(0, 10));
        assertThat(page.getContent()).isEmpty();
    }

    @Test(expectedExceptions = IllegalSuggestionOwnerOperationException.class)
    public void test_remove_suggestion_not_owner() throws Exception {

        Suggestion suggestion_1 = work.addSuggestion("new suggestion 1", suggestionOwner1, new Point(1, 0));

        work.removeMySuggestion(suggestionOwner2, suggestion_1.getSuggestionId());
    }

    @Test(expectedExceptions = SuggestionNotFoundException.class)
    public void test_remove_suggestion_not_valid_id() throws Exception {

        work.removeMySuggestion(suggestionOwner2, new SuggestionId(randomAlphabetic(20)));
    }


}