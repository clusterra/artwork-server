/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.domain.suggestion;

import artwork.core.TestCoreConfig;
import artwork.core.domain.TestUser;
import artwork.core.domain.images.ImageMeta;
import artwork.core.domain.work.Work;
import artwork.core.domain.work.WorkId;
import artwork.core.domain.work.WorkModel;
import artwork.core.domain.work.WorkOwner;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.validation.ConstraintViolationException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.testng.Assert.fail;

/**
 * Created by kepkap
 * on 10/06/16.
 */
@SpringBootTest(classes = {TestCoreConfig.class})
public class SuggestionValidationTest extends AbstractTestNGSpringContextTests {


    private Work work;
    private WorkOwner workOwner = TestUser.workOwner();
    private SuggestionOwner suggestionOwner = TestUser.suggestionOwner();

    ImageMeta randomImageId() {
        return new ImageMeta(RandomStringUtils.randomAlphanumeric(10), "image/png");
    }

    @BeforeMethod
    public void beforeMethod() throws Exception {


        SecurityContextHolder.getContext().setAuthentication(new TestingAuthenticationToken(workOwner.getUuid(), ""));

        work = new WorkModel(WorkId.generateId(), workOwner, randomImageId(), "description");
    }

    @Test
    public void test_create_validation_point_is_null() throws Exception {

        try {
            work.addSuggestion("test_description", suggestionOwner, null);
            fail();
        } catch (ConstraintViolationException e) {
            assertThat(e.getConstraintViolations()).hasSize(1);
        }
    }

    @Test
    public void test_create_validate_description_empty() throws Exception {
        try {
            work.addSuggestion("", suggestionOwner, new Point(10, 10));
            fail();
        } catch (ConstraintViolationException e) {
            assertThat(e.getConstraintViolations()).hasSize(1);
        }
    }

    @Test
    public void test_create_validate_description_max() throws Exception {
        StringBuilder builder = new StringBuilder("string that will exceed length 2000 ");

        while (builder.length() < 2000) {
            builder.append("more string ");
        }

        try {
            work.addSuggestion(builder.toString(), suggestionOwner, new Point(10, 10));
            fail();
        } catch (ConstraintViolationException e) {
            assertThat(e.getConstraintViolations()).hasSize(1);
        }
    }

    @Test
    public void test_create_author_validation_null() throws Exception {

        try {
            work.addSuggestion("test_description", null, new Point(10, 10));
            fail();
        } catch (ConstraintViolationException e) {
            assertThat(e.getConstraintViolations()).hasSize(1);
        }
    }

}