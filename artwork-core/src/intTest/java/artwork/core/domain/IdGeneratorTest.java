/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.domain;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class IdGeneratorTest {


    @Test
    public void testGenerateId() throws Exception {

        for (int i = 0; i < 100; i++) {
            String actual = IdGenerator.generateId();
            System.out.println(actual);
            assertThat(actual.length()).isGreaterThan(7);
        }


    }

}