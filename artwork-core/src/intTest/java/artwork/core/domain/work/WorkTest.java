/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.domain.work;

import artwork.commentary.domain.comment.Comment;
import artwork.commentary.domain.commentable.Commentable;
import artwork.core.TestCoreConfig;
import artwork.core.domain.TestUser;
import artwork.core.domain.images.ImageMeta;
import artwork.likeable.domain.dislike.Disliker;
import artwork.likeable.domain.like.Liker;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.validation.ConstraintViolationException;
import java.util.Comparator;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.testng.Assert.fail;

/**
 * Created by Denis Kuchugurov
 * on 24.02.2016.
 */
@SpringBootTest(classes = {TestCoreConfig.class})
public class WorkTest extends AbstractTestNGSpringContextTests {

    private WorkOwner workOwner;
    private WorkModel work;

    ImageMeta randomImageId() {
        return new ImageMeta(RandomStringUtils.randomAlphanumeric(10), "image/png");
    }

    @BeforeMethod
    public void before() throws Exception {

        workOwner = TestUser.workOwner();

        SecurityContextHolder.getContext().setAuthentication(new TestingAuthenticationToken(workOwner.getUuid(), ""));

        work = new WorkModel(WorkId.generateId(), workOwner, randomImageId(), "description");
    }

    @AfterMethod
    public void after() {
        SecurityContextHolder.clearContext();
    }

    @Test
    public void test_view_counter() throws Exception {


        assertThat(Work.findOne(work.getWorkId()).getViewsCount()).isEqualTo(0);
        ViewerId viewerId1 = new ViewerId(RandomStringUtils.randomAlphabetic(10));

        work.view(viewerId1);
        work.view(viewerId1);
        work.view(viewerId1);
        assertThat(Work.findOne(work.getWorkId()).getViewsCount()).isEqualTo(1);

        work.view(new ViewerId(RandomStringUtils.randomAlphabetic(10)));
        work.view(new ViewerId(RandomStringUtils.randomAlphabetic(10)));
        work.view(new ViewerId(RandomStringUtils.randomAlphabetic(10)));
        work.view(new ViewerId(RandomStringUtils.randomAlphabetic(10)));
        work.view(new ViewerId(RandomStringUtils.randomAlphabetic(10)));
        work.view(new ViewerId(RandomStringUtils.randomAlphabetic(10)));
        work.view(new ViewerId(RandomStringUtils.randomAlphabetic(10)));
        work.view(new ViewerId(RandomStringUtils.randomAlphabetic(10)));
        work.view(new ViewerId(RandomStringUtils.randomAlphabetic(10)));
        work.view(new ViewerId(RandomStringUtils.randomAlphabetic(10)));
        work.view(new ViewerId(RandomStringUtils.randomAlphabetic(10)));

        assertThat(Work.findOne(work.getWorkId()).getViewsCount()).isEqualTo(12);
        work.view(viewerId1);
        assertThat(Work.findOne(work.getWorkId()).getViewsCount()).isEqualTo(13);
        work.view(viewerId1);
        assertThat(Work.findOne(work.getWorkId()).getViewsCount()).isEqualTo(13);
    }

    @Test
    public void test_view_one() throws Exception {


        assertThat(Work.findOne(work.getWorkId()).getViewsCount()).isEqualTo(0);

        ViewerId viewerId1 = new ViewerId(RandomStringUtils.randomAlphabetic(10));
        Work.viewOne(work.getWorkId(), viewerId1);

        int attempts = 0;
        while (Work.findOne(work.getWorkId()).getViewsCount() == 0 && attempts < 10) {
            attempts++;
            Thread.sleep(100);
        }

        assertThat(Work.findOne(work.getWorkId()).getViewsCount()).isEqualTo(1);
    }


    @Test
    public void test_auditing_values_are_set() throws Exception {
        assertThat(work.getCreatedDate()).isNotNull();
        Date date = work.getLastModifiedDate();
        assertThat(date).isNotNull();

        work.updateDescription("new description", workOwner.getWorkOwnerId());

        assertThat(date).isBefore(work.getLastModifiedDate());
    }

    @Test
    public void test_auditing_version_gets_incremented() throws Exception {
        assertThat(work.getVersion()).isEqualTo(0);
        work.updateDescription("new value 1", workOwner.getWorkOwnerId());
        assertThat(work.getVersion()).isEqualTo(1);
        work.updateDescription("new value 2", workOwner.getWorkOwnerId());
        assertThat(work.getVersion()).isEqualTo(2);
    }

    @Test
    public void test_rus_encoding() throws Exception {
        String description = "русский текст";
        work = new WorkModel(WorkId.generateId(), workOwner, randomImageId(), description);
        assertThat(work.getDescription()).isEqualTo(description);
    }

    @Test
    public void test_find_by_owner_id() throws Exception {
        Page<WorkModel> all = Work.findAll(workOwner.getWorkOwnerId(), 0, 10);

        assertThat(all.getContent()).isNotEmpty();
    }

    @Test
    public void test_find_by_owner_id_pageable() throws Exception {
        Page<WorkModel> all = Work.findAll(workOwner.getWorkOwnerId(), 0, 10);

        assertThat(all.getContent()).hasSize(1);
        assertThat(all.getContent()).contains(work);
        assertThat(all.hasNext()).isFalse();

        WorkModel work1 = new WorkModel(WorkId.generateId(), workOwner, randomImageId(), "description");
        WorkModel work2 = new WorkModel(WorkId.generateId(), workOwner, randomImageId(), "description");
        WorkModel work3 = new WorkModel(WorkId.generateId(), workOwner, randomImageId(), "description");
        WorkModel work4 = new WorkModel(WorkId.generateId(), workOwner, randomImageId(), "description");

        all = Work.findAll(workOwner.getWorkOwnerId(), 0, 2);
        assertThat(all.getContent()).hasSize(2);
        assertThat(all.getContent()).contains(work4, work3);
        assertThat(all.hasNext()).isTrue();

        all = Work.findAll(workOwner.getWorkOwnerId(), all.nextPageable().getPageNumber(), all.nextPageable().getPageSize());
        assertThat(all.getContent()).hasSize(2);
        assertThat(all.getContent()).contains(work2, work1);
        assertThat(all.hasNext()).isTrue();

        all = Work.findAll(workOwner.getWorkOwnerId(), all.nextPageable().getPageNumber(), all.nextPageable().getPageSize());
        assertThat(all.getContent()).hasSize(1);
        assertThat(all.getContent()).contains(work);
        assertThat(all.hasNext()).isFalse();
    }

    @Test
    public void test_find_popular_pageable() throws Exception {
        Page<WorkModel> all = Work.findAll(0, 1);

        assertThat(all.getContent()).hasSize(1);
        assertThat(all.getContent()).contains(work);

        WorkModel work1 = new WorkModel(WorkId.generateId(), TestUser.workOwner("test_user_1"), randomImageId(), "description");
        WorkModel work2 = new WorkModel(WorkId.generateId(), TestUser.workOwner("test_user_2"), randomImageId(), "description");
        WorkModel work3 = new WorkModel(WorkId.generateId(), TestUser.workOwner("test_user_3"), randomImageId(), "description");
        WorkModel work4 = new WorkModel(WorkId.generateId(), TestUser.workOwner("test_user_4"), randomImageId(), "description");

        all = Work.findAll(0, 3);
        assertThat(all.getContent()).hasSize(3);
        assertThat(all.getContent()).contains(work2, work3, work4);
        assertThat(all.hasNext()).isTrue();

        all = Work.findAll(all.nextPageable().getPageNumber(), all.nextPageable().getPageSize());
        assertThat(all.getContent()).contains(work, work1);
        assertThat(all.hasNext()).isTrue();

        all = Work.findAll(all.nextPageable().getPageNumber(), all.nextPageable().getPageSize());
        assertThat(all.getContent().size()).isGreaterThanOrEqualTo(1);
    }

    @Test
    public void test_find_popular() throws Exception {

        WorkModel work1 = new WorkModel(WorkId.generateId(), TestUser.workOwner("test_user_1"), randomImageId(), "description");
        WorkModel work2 = new WorkModel(WorkId.generateId(), TestUser.workOwner("test_user_2"), randomImageId(), "description");
        WorkModel work3 = new WorkModel(WorkId.generateId(), TestUser.workOwner("test_user_3"), randomImageId(), "description");
        WorkModel work4 = new WorkModel(WorkId.generateId(), TestUser.workOwner("test_user_4"), randomImageId(), "description");


        work1.view(new ViewerId(RandomStringUtils.randomAlphabetic(10)));

        work2.view(new ViewerId(RandomStringUtils.randomAlphabetic(10)));
        work2.view(new ViewerId(RandomStringUtils.randomAlphabetic(10)));
        work2.view(new ViewerId(RandomStringUtils.randomAlphabetic(10)));

        work3.view(new ViewerId(RandomStringUtils.randomAlphabetic(10)));
        work3.view(new ViewerId(RandomStringUtils.randomAlphabetic(10)));
        work3.view(new ViewerId(RandomStringUtils.randomAlphabetic(10)));
        work3.view(new ViewerId(RandomStringUtils.randomAlphabetic(10)));

        Page<WorkModel> all = Work.findMostPopularThisMonth(0, 4);

        assertThat(all.getContent()).hasSize(4);
        assertThat(all.getContent()).isSortedAccordingTo(new Comparator<WorkModel>() {
            @Override
            public int compare(WorkModel o1, WorkModel o2) {
                return o1.getViewsCount() < o2.getViewsCount() ? 1 : -1;
            }
        });
        assertThat(all.hasNext()).isTrue();

    }

    @Test
    public void test_find_rated() throws Exception {

        WorkModel work1 = new WorkModel(WorkId.generateId(), TestUser.workOwner("test_user_1"), randomImageId(), "description");
        WorkModel work2 = new WorkModel(WorkId.generateId(), TestUser.workOwner("test_user_2"), randomImageId(), "description");
        WorkModel work3 = new WorkModel(WorkId.generateId(), TestUser.workOwner("test_user_3"), randomImageId(), "description");
        WorkModel work4 = new WorkModel(WorkId.generateId(), TestUser.workOwner("test_user_4"), randomImageId(), "description");


        work1.like(TestUser.liker());

        work2.like(TestUser.liker());
        work2.like(TestUser.liker());
        work2.like(TestUser.liker());

        work3.like(TestUser.liker());
        work3.like(TestUser.liker());
        work3.like(TestUser.liker());
        work3.like(TestUser.liker());

        Page<WorkModel> all = Work.findMostRatedThisMonth(0, 4);

        assertThat(all.getContent()).hasSize(4);
        assertThat(all.getContent()).isSortedAccordingTo(new Comparator<WorkModel>() {
            @Override
            public int compare(WorkModel o1, WorkModel o2) {
                return o1.getLikesCount() < o2.getLikesCount() ? 1 : -1;
            }
        });
        assertThat(all.hasNext()).isTrue();

    }

    @Test
    public void test_find_rated_by_owner() throws Exception {

        WorkOwner test_user_1 = TestUser.workOwner("test_user_1");
        WorkModel work1 = new WorkModel(WorkId.generateId(), test_user_1, randomImageId(), "description");
        WorkModel work2 = new WorkModel(WorkId.generateId(), test_user_1, randomImageId(), "description");
        WorkModel work3 = new WorkModel(WorkId.generateId(), test_user_1, randomImageId(), "description");
        WorkModel work4 = new WorkModel(WorkId.generateId(), test_user_1, randomImageId(), "description");


        work1.like(TestUser.liker());

        work2.like(TestUser.liker());
        work2.like(TestUser.liker());
        work2.like(TestUser.liker());

        work3.like(TestUser.liker());
        work3.like(TestUser.liker());
        work3.like(TestUser.liker());
        work3.like(TestUser.liker());

        Page<WorkModel> all = Work.findMostRatedThisMonth(test_user_1.getWorkOwnerId(), 0, 4);

        assertThat(all.getContent()).hasSize(4);
        assertThat(all.getContent()).isSortedAccordingTo(new Comparator<WorkModel>() {
            @Override
            public int compare(WorkModel o1, WorkModel o2) {
                return o1.getLikesCount() < o2.getLikesCount() ? 1 : -1;
            }
        });
        assertThat(all.hasNext()).isTrue();

    }

    @Test
    public void test_find_popular_by_owner() throws Exception {

        WorkOwner test_user_1 = TestUser.workOwner("test_user_1");
        WorkModel work1 = new WorkModel(WorkId.generateId(), test_user_1, randomImageId(), "description");
        WorkModel work2 = new WorkModel(WorkId.generateId(), test_user_1, randomImageId(), "description");
        WorkModel work3 = new WorkModel(WorkId.generateId(), test_user_1, randomImageId(), "description");
        WorkModel work4 = new WorkModel(WorkId.generateId(), test_user_1, randomImageId(), "description");


        work1.view(new ViewerId(RandomStringUtils.randomAlphabetic(10)));

        work2.view(new ViewerId(RandomStringUtils.randomAlphabetic(10)));
        work2.view(new ViewerId(RandomStringUtils.randomAlphabetic(10)));
        work2.view(new ViewerId(RandomStringUtils.randomAlphabetic(10)));

        work3.view(new ViewerId(RandomStringUtils.randomAlphabetic(10)));
        work3.view(new ViewerId(RandomStringUtils.randomAlphabetic(10)));
        work3.view(new ViewerId(RandomStringUtils.randomAlphabetic(10)));
        work3.view(new ViewerId(RandomStringUtils.randomAlphabetic(10)));

        Page<WorkModel> all = Work.findMostPopularThisMonth(test_user_1.getWorkOwnerId(), 0, 4);

        assertThat(all.getContent()).hasSize(4);
        assertThat(all.getContent()).isSortedAccordingTo(new Comparator<WorkModel>() {
            @Override
            public int compare(WorkModel o1, WorkModel o2) {
                return o1.getViewsCount() < o2.getViewsCount() ? 1 : -1;
            }
        });
        assertThat(all.hasNext()).isTrue();

    }

    //    @Test
    public void test_popularization() throws Exception {

        WorkModel work1 = new WorkModel(WorkId.generateId(), TestUser.workOwner("test_user_1"), randomImageId(), "description");
        WorkModel work2 = new WorkModel(WorkId.generateId(), TestUser.workOwner("test_user_2"), randomImageId(), "description");
        Page<WorkModel> all = Work.findAll(0, 3);

        assertThat(all.getContent()).hasSize(3);
        assertThat(all.getContent()).contains(work, work1, work2);
        assertThat(all.hasNext()).isTrue();

//        work1.unpopularize();

        all = Work.findAll(0, 3);
        assertThat(all.getContent()).doesNotContain(work1);
        assertThat(all.getContent()).contains(work, work2);
        assertThat(all.hasNext()).isTrue();
    }

    @Test
    public void test_update_description() throws Exception {
        String old = work.getDescription();
        work.updateDescription("new description", workOwner.getWorkOwnerId());

        assertThat(old).isNotEqualTo(Work.findOne(work.getWorkId()));
    }

    @Test
    public void test_update_description_validation_min() throws Exception {
        try {
            work.updateDescription("", workOwner.getWorkOwnerId());
            fail();
        } catch (ConstraintViolationException e) {
            assertThat(e.getConstraintViolations()).hasSize(1);
        }
    }

    @Test
    public void test_update_description_validation_max() throws Exception {
        StringBuilder builder = new StringBuilder("string that will exceed length 500 ");

        while (builder.length() < 2000) {
            builder.append("more string ");
        }

        try {
            work.updateDescription(builder.toString(), workOwner.getWorkOwnerId());
            fail();
        } catch (ConstraintViolationException e) {
            assertThat(e.getConstraintViolations()).hasSize(1);
        }
    }

    @Test
    public void test_add_comment() throws Exception {

        Commentable commentable = Commentable.findOne(work.getWorkId().toCommentableId());
        Page<Comment> workCommentPage = commentable.findComments(new PageRequest(0, 10));
        assertThat(workCommentPage.getContent()).isEmpty();
        Comment newComment1 = commentable.addComment(TestUser.commentAuthor(), "new comment1");
        Comment newComment2 = commentable.addComment(TestUser.commentAuthor(), "new comment2");

        workCommentPage = commentable.findComments(new PageRequest(0, 10));
        assertThat(workCommentPage.getContent()).contains(newComment1, newComment2);
    }

    @Test
    public void test_work_like() throws Exception {
        assertThat(work.getLikesCount()).isEqualTo(0);
        assertThat(work.getDislikesCount()).isEqualTo(0);

        Liker author1 = TestUser.liker();
        Liker author2 = TestUser.liker();
        Liker author3 = TestUser.liker();


//        work.like(new AuthorId(authorId.getWorkId()));
//        assertThat(work.likesCount()).isEqualTo(0);


        work.like(author1);
        assertThat(work.getLikesCount()).isEqualTo(1);

        work.like(author1);
        assertThat(work.getLikesCount()).isEqualTo(1);

        work.like(author2);
        work.like(author3);
        assertThat(work.getLikesCount()).isEqualTo(3);

        work.unlike(author3);
        assertThat(work.getLikesCount()).isEqualTo(2);

        work.dislike(Disliker.from(author1));
        assertThat(work.getLikesCount()).isEqualTo(1);
        assertThat(work.getDislikesCount()).isEqualTo(1);
    }

    @Test
    public void test_work_dislike() throws Exception {
        assertThat(work.getLikesCount()).isEqualTo(0);
        assertThat(work.getDislikesCount()).isEqualTo(0);

        Disliker author0 = TestUser.disliker();
        Disliker author1 = TestUser.disliker();
        Disliker author2 = TestUser.disliker();
        Disliker author3 = TestUser.disliker();


        work.dislike(author0);
        assertThat(work.getDislikesCount()).isEqualTo(1);


        work.dislike(author1);
        assertThat(work.getDislikesCount()).isEqualTo(2);

        work.dislike(author1);
        assertThat(work.getDislikesCount()).isEqualTo(2);

        work.dislike(author2);
        work.dislike(author3);
        assertThat(work.getDislikesCount()).isEqualTo(4);

        work.undislike(author3);
        assertThat(work.getDislikesCount()).isEqualTo(3);


        work.like(Liker.from(author1));
        assertThat(work.getLikesCount()).isEqualTo(1);
        assertThat(work.getDislikesCount()).isEqualTo(2);

    }

    @Test
    public void test_work_view_count() throws Exception {
        assertThat(work.getViewsCount()).isEqualTo(0);

        work.view(new ViewerId("viewer_1"));
        work.view(new ViewerId("viewer_2"));

        assertThat(Work.findOne(work.getWorkId()).getViewsCount()).isEqualTo(2);
    }

}