/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.domain.images;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ImageMetaTest {


    @Test
    public void testIsGif() throws Exception {
        assertThat(new ImageMeta("123", "image/gif").isGif()).isTrue();
        assertThat(new ImageMeta("123", "image/png").isGif()).isFalse();
        assertThat(new ImageMeta("123", "image/jpg").isGif()).isFalse();
        assertThat(new ImageMeta("123", "image/jpeg").isGif()).isFalse();
        assertThat(new ImageMeta("123", "asffg").isGif()).isFalse();
        assertThat(new ImageMeta("123", null).isGif()).isFalse();
        assertThat(new ImageMeta("123", "").isGif()).isFalse();
    }

}