/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.domain.tag;

import artwork.core.TestCoreConfig;
import artwork.core.domain.IdGenerator;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created on 29/01/2017.
 *
 * @author kepkap
 */
@SpringBootTest(classes = {TestCoreConfig.class})

public class TaggedWorkTest extends AbstractTestNGSpringContextTests {

    //    @Test
    public void test_query_pageable() throws Exception {

        String workUuid = IdGenerator.generateId();
        String ownerUuid = IdGenerator.generateId();
        TaggedWork taggedWork1 = new TaggedWork("none", "dummy", ownerUuid);
        TaggedWork taggedWork2 = new TaggedWork("tag1", workUuid, ownerUuid);
        TaggedWork taggedWork3 = new TaggedWork("tag2", workUuid, ownerUuid);
        TaggedWork taggedWork4 = new TaggedWork("tag", workUuid, ownerUuid);


        Page<TaggedWork> result = TaggedWork.findAll(new PageRequest(0, 2));

        assertThat(result.getContent()).hasSize(2);
        assertThat(result.hasNext()).isTrue();

        result = TaggedWork.findAll(result.nextPageable());
        assertThat(result.getContent()).hasSize(2);
        assertThat(result.getContent()).contains(taggedWork2, taggedWork3);
        assertThat(result.hasNext()).isTrue();
    }

}