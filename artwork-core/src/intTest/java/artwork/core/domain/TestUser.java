/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.domain;

import artwork.commentary.domain.comment.CommentAuthor;
import artwork.core.domain.suggestion.SuggestionOwner;
import artwork.core.domain.work.WorkOwner;
import artwork.likeable.domain.dislike.Disliker;
import artwork.likeable.domain.like.Liker;

/**
 * Created on 26/01/2017.
 *
 * @author kepkap
 */
public class TestUser {


    public static Liker liker() {
        return new Liker(IdGenerator.generateId(), "test user", "http://artwork.pro/user/profile", "https://randomuser.me/api/portraits/thumb/lego/9.jpg");
    }

    public static Disliker disliker() {
        return new Disliker(IdGenerator.generateId(), "test user", "http://artwork.pro/user/profile", "http://artwork.pro/user/profile/image.png");
    }

    public static WorkOwner workOwner() {
        return new WorkOwner(IdGenerator.generateId(), "test user", "http://artwork.pro/user/profile", "http://artwork.pro/user/profile/image.png");
    }

    public static WorkOwner workOwner(String uuid) {
        return new WorkOwner(uuid, "test user", "http://artwork.pro/user/profile", "http://artwork.pro/user/profile/image.png");
    }

    public static SuggestionOwner suggestionOwner() {
        return new SuggestionOwner(IdGenerator.generateId(), "test user", "http://artwork.pro/user/profile", "http://artwork.pro/user/profile/image.png");
    }

    public static CommentAuthor commentAuthor() {
        return new CommentAuthor(IdGenerator.generateId(), "test user", "http://artwork.pro/user/profile", "http://artwork.pro/user/profile/image.png");
    }
}
