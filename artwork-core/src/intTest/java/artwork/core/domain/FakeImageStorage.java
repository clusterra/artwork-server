/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.domain;

import artwork.core.domain.images.ImageMeta;
import artwork.core.domain.images.ImageStorage;
import artwork.core.domain.images.ImageTooSmallException;
import artwork.core.domain.images.ImageWeightTooMuchException;

/**
 * Created by Egor Petushkov
 * on 16.06.17.
 */
public class FakeImageStorage implements ImageStorage {
    @Override
    public ImageMeta save(String imageId) throws ImageTooSmallException, ImageWeightTooMuchException {
        return new ImageMeta(imageId, "image/png");
    }

    @Override
    public void delete(ImageMeta imageMeta) {
    }
}
