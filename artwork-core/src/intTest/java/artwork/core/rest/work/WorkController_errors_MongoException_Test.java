/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.rest.work;

import artwork.core.rest.AbstractControllerTest;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.MimeTypeUtils;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created on 18/04/2017.
 *
 * @author kepkap
 */
public class WorkController_errors_MongoException_Test extends AbstractControllerTest {

    private static final String worksBaseUri = "/api/works";


    //    @Test
    public void test_mongo_exception() throws Exception {
        super.before();

        byte[] bytes = IOUtils.toByteArray(new ClassPathResource("img/test.png").getInputStream());

        MockMultipartFile file = new MockMultipartFile("file", "test_file_name", MimeTypeUtils.IMAGE_PNG_VALUE, bytes);

        String descriptionJson = "{\"description\":\"test description\"}";

        MockMultipartFile description = new MockMultipartFile("descriptionRequest", "", "application/json", descriptionJson.getBytes());

        MvcResult workResult = mvc.perform(fileUpload(worksBaseUri)
                .file(file)
                .file(description)
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0].code").value("data_storage_error"))
                .andExpect(jsonPath("$.errors[0].property").value("file"))
                .andExpect(jsonPath("$.errors[0].message").isNotEmpty())
                .andReturn();
    }


}
