/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.rest.work;

import artwork.core.rest.AbstractWorkControllerTest;
import com.jayway.jsonpath.JsonPath;
import org.omg.CORBA.Object;
import org.testng.annotations.Test;

import java.util.List;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Denis Kuchugurov
 * on 18.02.2016.
 */

public class WorkController_4_query_Test extends AbstractWorkControllerTest {

    @Test
    public void test_find_all() throws Exception {

        String jsonBody = mvc.perform(get(worksBaseUri)
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/hal+json;charset=UTF-8"))
                .andReturn().getResponse().getContentAsString();

        assertThat(jsonBody).isNotEmpty();

        List<Object> result = JsonPath.read(jsonBody, "_embedded.workResourceList");
        assertThat(result.size()).isGreaterThan(1);
    }

    @Test
    public void test_find_by_author() throws Exception {


        String jsonBody = mvc.perform(get(worksBaseUri)
                .param("authorId", authentication_1.getName())
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/hal+json;charset=UTF-8"))
                .andReturn().getResponse().getContentAsString();


        List<Object> result = JsonPath.read(jsonBody, "_embedded.workResourceList");
        assertThat(result.size()).isGreaterThanOrEqualTo(1);

        mvc.perform(get(worksBaseUri)
                .param("ownerId", randomAlphabetic(20))
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/hal+json;charset=UTF-8"))
                .andExpect(jsonPath("$._embedded").doesNotExist());

    }

    @Test
    public void test_find_no_order() throws Exception {

        mvc.perform(get(worksBaseUri)
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/hal+json;charset=UTF-8"))
                .andExpect(jsonPath("$._embedded.workResourceList").isNotEmpty());

    }

    @Test
    public void test_find_order_by_view_count() throws Exception {

        mvc.perform(get(worksBaseUri)
                .param("sort", "date")
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/hal+json;charset=UTF-8"))
                .andExpect(jsonPath("$._embedded.workResourceList").isNotEmpty());

    }


    @Test
    public void test_find_sort_by_rating() throws Exception {

        mvc.perform(get(worksBaseUri)
                .param("sort", "rating")
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/hal+json;charset=UTF-8"))
                .andExpect(jsonPath("$._embedded.workResourceList").isNotEmpty());

    }

    @Test
    public void test_find_sort_by_popularity() throws Exception {

        mvc.perform(get(worksBaseUri)
                .param("sort", "popularity")
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/hal+json;charset=UTF-8"))
                .andExpect(jsonPath("$._embedded.workResourceList").isNotEmpty());

    }

    @Test
    public void test_find_bad_requests() throws Exception {

        mvc.perform(get(worksBaseUri)
                .param("sort", "not_valid")
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0].code").value("illegal_sort_parameter"))
                .andExpect(jsonPath("$.errors[0].property").doesNotExist())
                .andExpect(jsonPath("$.errors[0].message").value(containsString("illegal sort parameter 'not_valid'")));

    }
}