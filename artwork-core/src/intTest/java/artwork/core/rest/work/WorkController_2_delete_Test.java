/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.rest.work;

import artwork.core.rest.AbstractWorkControllerTest;
import org.testng.annotations.Test;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Denis Kuchugurov
 * on 18.02.2016.
 */
public class WorkController_2_delete_Test extends AbstractWorkControllerTest {

    @Test
    public void test_get() throws Exception {

        mvc.perform(get(work_links_self_href)
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("workId").exists());
    }

    @Test
    public void test_get_anonymous() throws Exception {

        mvc.perform(get(work_links_self_href)
                .with(authentication(anonymousAuthentication)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("workId").exists());
    }


    @Test
    public void test_delete() throws Exception {

        mvc.perform(delete(work_links_self_href)
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isNoContent());


        mvc.perform(get(work_links_self_href)
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isBadRequest());

    }

}