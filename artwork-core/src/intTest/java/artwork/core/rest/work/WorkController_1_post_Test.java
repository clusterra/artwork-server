/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.rest.work;

import artwork.core.rest.AbstractControllerTest;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.util.MimeTypeUtils;
import org.testng.annotations.Test;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Denis Kuchugurov
 * on 18.02.2016.
 */

public class WorkController_1_post_Test extends AbstractControllerTest {

    public static final String worksBaseUri = "/api/works";

    // @Test
    public void test_post_description_constraint_violation() throws Exception {

        byte[] bytes = IOUtils.toByteArray(new ClassPathResource("img/test.png").getInputStream());

        MockMultipartFile file = new MockMultipartFile("file", "test_file_name", MimeTypeUtils.IMAGE_PNG_VALUE, bytes);
        String emptyDescriptionJson = "{\"description\":\"\"}";

        MockMultipartFile description = new MockMultipartFile(
                "descriptionRequest",
                "",
                "application/json",
                emptyDescriptionJson.getBytes()
        );

        mvc.perform(fileUpload(worksBaseUri)
                .file(file)
                .file(description)
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.errors[0].property").value("description"))
                .andExpect(jsonPath("$.errors[0].code").value("NotEmpty"));
    }

    // @Test
    public void test_post_image_exceeding_size_limit() throws Exception {

        byte[] bytes = IOUtils.toByteArray(new ClassPathResource("img/Tulips.jpg").getInputStream());
        MockMultipartFile file = new MockMultipartFile("file", "test_file_name", MimeTypeUtils.IMAGE_PNG_VALUE, bytes);
        String desc = "{\"description\":\"test description\"}";
        MockMultipartFile description = new MockMultipartFile("descriptionRequest", "", "application/json", desc.getBytes());

        mvc.perform(fileUpload(worksBaseUri)
                .file(file)
                .file(description)
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isCreated());
    }

    // @Test
    public void test_post_2_files_ignores_except_parameter_named_file() throws Exception {

        byte[] bytes = IOUtils.toByteArray(new ClassPathResource("img/Tulips.jpg").getInputStream());

        MockMultipartFile file = new MockMultipartFile("file", "test_file_name", MimeTypeUtils.IMAGE_PNG_VALUE, bytes);
        MockMultipartFile ignored = new MockMultipartFile("ignored", "test_file_name_2", MimeTypeUtils.IMAGE_PNG_VALUE, bytes);
        String desc = "{\"description\":\"test description\"}";
        MockMultipartFile description = new MockMultipartFile("descriptionRequest", "", "application/json", desc.getBytes());

        mvc.perform(fileUpload(worksBaseUri)
                .file(file)
                .file(ignored)
                .file(description)
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isCreated());
    }

    // @Test
    public void test_post_auditor_forbidden_for_anonymous() throws Exception {

        byte[] bytes = IOUtils.toByteArray(new ClassPathResource("img/test.png").getInputStream());

        MockMultipartFile file = new MockMultipartFile("file", "test_file_name", MimeTypeUtils.IMAGE_PNG_VALUE, bytes);
        String desc = "{\"description\":\"test description\"}";
        MockMultipartFile description = new MockMultipartFile("descriptionRequest", "", "application/json", desc.getBytes());

        mvc.perform(fileUpload(worksBaseUri)
                .file(file)
                .file(description)
                .with(authentication(anonymousAuthentication)))
                .andDo(print())
                .andExpect(status().isUnauthorized());

    }

    // @Test
    public void test_post_image_4MB() throws Exception {

        byte[] bytes = IOUtils.toByteArray(new ClassPathResource("img/img-almost-5mb.png").getInputStream());
        MockMultipartFile file = new MockMultipartFile("file", "test_file_name", MimeTypeUtils.IMAGE_PNG_VALUE, bytes);
        String desc = "{\"description\":\"test description\"}";
        MockMultipartFile description = new MockMultipartFile("descriptionRequest", "", "application/json", desc.getBytes());

        mvc.perform(fileUpload(worksBaseUri)
                .file(file)
                .file(description)
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isCreated());
    }
}