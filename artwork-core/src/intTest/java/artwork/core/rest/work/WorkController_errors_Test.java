/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.rest.work;

import artwork.core.rest.AbstractControllerTest;
import com.jayway.jsonpath.JsonPath;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.MimeTypeUtils;
import org.testng.annotations.Test;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created on 18/04/2017.
 *
 * @author kepkap
 */
public class WorkController_errors_Test extends AbstractControllerTest {

    private static final String worksBaseUri = "/api/works";

    // @Test
    public void test_image_width_too_small() throws Exception {
        super.before();

        byte[] bytes = IOUtils.toByteArray(new ClassPathResource("img/test.png").getInputStream());

        MockMultipartFile file = new MockMultipartFile("file", "test_file_name", MimeTypeUtils.IMAGE_PNG_VALUE, bytes);

        String descriptionJson = "{\"description\":\"test description\"}";

        MockMultipartFile description = new MockMultipartFile("descriptionRequest", "", "application/json", descriptionJson.getBytes());

        MvcResult workResult = mvc.perform(fileUpload(worksBaseUri)
                .file(file)
                .file(description)
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0].code").value("image_width_too_small"))
                .andExpect(jsonPath("$.errors[0].property").value("file"))
                .andExpect(jsonPath("$.errors[0].message").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].arguments[0]").value(500))
                .andExpect(jsonPath("$.errors[0].arguments[1]").value(400))
                .andExpect(jsonPath("$.message").value(containsString("validation failed with 1 error(s)")))

                .andReturn();
    }

    //    @Test
    public void test_image_file_size_too_big() throws Exception {
        super.before();

        ClassPathResource resource = new ClassPathResource("img/img-almost-5mb.png");
        byte[] bytes = IOUtils.toByteArray(resource.getInputStream());

        MockMultipartFile file = new MockMultipartFile("file", resource.getFilename(), MimeTypeUtils.IMAGE_PNG_VALUE, bytes);

        String descriptionJson = "{\"description\":\"test description\"}";

        MockMultipartFile description = new MockMultipartFile("descriptionRequest", "", "application/json", descriptionJson.getBytes());

        MvcResult workResult = mvc.perform(fileUpload(worksBaseUri)
                .file(file)
                .file(description)
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0].code").value("image_file_size_too_big"))
                .andExpect(jsonPath("$.errors[0].property").value("file"))
                .andExpect(jsonPath("$.errors[0].message").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].arguments[0]").value(500))
                .andExpect(jsonPath("$.errors[0].arguments[1]").value(400))
                .andExpect(jsonPath("$.message").value(containsString("validation failed with 1 error(s)")))

                .andReturn();
    }

    // @Test
    public void test_description_empty() throws Exception {
        super.before();

        byte[] bytes = IOUtils.toByteArray(new ClassPathResource("img/test.png").getInputStream());

        MockMultipartFile file = new MockMultipartFile("file", "test_file_name", MimeTypeUtils.IMAGE_PNG_VALUE, bytes);

        String descriptionJson = "{\"description\":\"\"}";

        MockMultipartFile description = new MockMultipartFile("descriptionRequest", "", "application/json", descriptionJson.getBytes());

        MvcResult workResult = mvc.perform(fileUpload(worksBaseUri)
                .file(file)
                .file(description)
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0].code").value("NotEmpty"))
                .andExpect(jsonPath("$.errors[0].property").value("description"))
                .andExpect(jsonPath("$.errors[0].message").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].arguments").isEmpty())
                .andReturn();
    }

    // @Test
    public void test_description_size() throws Exception {

        byte[] bytes = IOUtils.toByteArray(new ClassPathResource("img/Tulips.jpg").getInputStream());

        MockMultipartFile file = new MockMultipartFile("file", "test_file_name", MimeTypeUtils.IMAGE_PNG_VALUE, bytes);


        StringBuilder descValue = new StringBuilder();
        while (descValue.length() < 1000) {
            descValue.append(" more string ");
        }

        String descriptionJson = "{\"description\":\"" + descValue.toString() + "\"}";

        MockMultipartFile description = new MockMultipartFile("descriptionRequest", "", "application/json", descriptionJson.getBytes());

        MvcResult workResult = mvc.perform(fileUpload(worksBaseUri)
                .file(file)
                .file(description)
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0].code").value("Size"))
                .andExpect(jsonPath("$.errors[0].property").value("description"))
                .andExpect(jsonPath("$.errors[0].message").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].arguments[0]").value(1000))
                .andExpect(jsonPath("$.errors[0].arguments[1]").value(0))
                .andReturn();
    }

    @Test
    public void test_work_not_found() throws Exception {

        MvcResult workResult = mvc.perform(get(worksBaseUri + "/asdfg")
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0].code").value("work_not_found"))
                .andExpect(jsonPath("$.errors[0].property").doesNotExist())
                .andExpect(jsonPath("$.errors[0].message").isNotEmpty())
                .andReturn();
    }

    // @Test
    public void test_update_work_not_modified_description() throws Exception {

        byte[] bytes = IOUtils.toByteArray(new ClassPathResource("img/Tulips.jpg").getInputStream());

        MockMultipartFile file = new MockMultipartFile("file", "test_file_name", MimeTypeUtils.IMAGE_PNG_VALUE, bytes);

        String descriptionJson = "{\"description\":\"test description\"}";

        MockMultipartFile description = new MockMultipartFile("descriptionRequest", "", "application/json", descriptionJson.getBytes());

        MvcResult workResult = mvc.perform(fileUpload(worksBaseUri)
                .file(file)
                .file(description)
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("workId").exists())
                .andReturn();


        String jsonBody = workResult.getResponse().getContentAsString();

        String work_links_self_href = JsonPath.read(jsonBody, "_links.self.href");

        mvc.perform(put(work_links_self_href)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"description\":\"new description\"}")
                .with(authentication(authentication_2)))
                .andDo(print())
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.errors[0].code").value("illegal_work_owner_operation"))
                .andExpect(jsonPath("$.errors[0].property").doesNotExist())
                .andExpect(jsonPath("$.errors[0].message").value(containsString("operation is not allowed on not owning entity")));
    }


}
