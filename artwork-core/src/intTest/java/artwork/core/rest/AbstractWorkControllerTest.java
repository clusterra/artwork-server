/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.rest;

import com.jayway.jsonpath.JsonPath;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.MimeTypeUtils;
import org.testng.annotations.BeforeMethod;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Denis Kuchugurov
 * on 18.02.2016.
 */
public abstract class AbstractWorkControllerTest extends AbstractControllerTest {

    protected static final String worksBaseUri = "/api/works";

    protected String work_links_self_href;

    @BeforeMethod
    public void before() throws Exception {
        super.before();

        MvcResult workResult = mvc.perform(post(worksBaseUri)
                .content("{\"description\":\"test description\", \"imageId\":\"fake id\"}")
                .contentType(MediaType.APPLICATION_JSON)
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("workId").isNotEmpty())
                .andExpect(jsonPath("owner").isNotEmpty())
                .andExpect(jsonPath("owner.uuid").isNotEmpty())
                .andExpect(jsonPath("owner.displayName").isNotEmpty())
                .andExpect(jsonPath("owner.imageUrl").isNotEmpty())
                .andExpect(jsonPath("owner.profileUrl").isNotEmpty())
                .andReturn();


        String jsonBody = workResult.getResponse().getContentAsString();

        work_links_self_href = JsonPath.read(jsonBody, "_links.self.href");
    }
}