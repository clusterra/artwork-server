/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.rest.suggestion;

import artwork.core.rest.AbstractWorkControllerTest;
import com.jayway.jsonpath.JsonPath;
import org.springframework.http.MediaType;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created on 06/02/2017.
 *
 * @author kepkap
 */
public class SuggestionControllerTest extends AbstractWorkControllerTest {

    private String suggestion_self_href;

    @BeforeMethod
    public void before() throws Exception {
        super.before();
        String content = mvc.perform(post(work_links_self_href + "/suggestions")
                .content("{\"text\":\"some_text\", \"pointX\":\"10.33\", \"pointY\":\"10.77\"}")
                .contentType(MediaType.APPLICATION_JSON)
                .with(authentication(authentication_2)))

                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("suggestionId").isNotEmpty())
                .andExpect(jsonPath("author").isNotEmpty())
                .andExpect(jsonPath("author.uuid").isNotEmpty())
                .andExpect(jsonPath("author.displayName").isNotEmpty())
                .andExpect(jsonPath("author.imageUrl").isNotEmpty())
                .andExpect(jsonPath("author.profileUrl").isNotEmpty())

                .andReturn().getResponse().getContentAsString();

        suggestion_self_href = JsonPath.read(content, "_links.self.href");

        assertThat(suggestion_self_href).isNotEmpty();

    }

    @Test
    public void test_decimal_points() throws Exception {
        mvc.perform(get(suggestion_self_href)
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("pointX", equalTo(10.33)))
                .andExpect(jsonPath("pointY", equalTo(10.77)))
        ;

    }

    @Test
    public void test_pageable() throws Exception {

        mvc.perform(post(work_links_self_href + "/suggestions")
                .content("{\"text\":\"some_text\", \"pointX\":\"10\", \"pointY\":\"10\"}")
                .contentType(MediaType.APPLICATION_JSON)
                .with(authentication(authentication_1)))
                .andExpect(status().isCreated());

        mvc.perform(post(work_links_self_href + "/suggestions")
                .content("{\"text\":\"some_text\", \"pointX\":\"10\", \"pointY\":\"10\"}")
                .contentType(MediaType.APPLICATION_JSON)
                .with(authentication(authentication_1)))
                .andExpect(status().isCreated());

        mvc.perform(post(work_links_self_href + "/suggestions")
                .content("{\"text\":\"some_text\", \"pointX\":\"10\", \"pointY\":\"10\"}")
                .contentType(MediaType.APPLICATION_JSON)
                .with(authentication(authentication_1)))
                .andExpect(status().isCreated());

        String content = mvc.perform(
                get(work_links_self_href + "/suggestions")
                        .param("size", "2")
                        .with(authentication(authentication_1)))

                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("_links.next.href").exists())
                .andExpect(jsonPath("page.size").isNumber())
                .andExpect(jsonPath("page.size", equalTo(2)))
                .andExpect(jsonPath("_embedded.suggestionResourceList").isArray())

                .andReturn().getResponse().getContentAsString();

        List<Object> resourceList = JsonPath.read(content, "_embedded.suggestionResourceList");
        assertThat(resourceList).hasSize(2);

        String suggestion_self_href = JsonPath.read(content, "_links.self.href");
        assertThat(suggestion_self_href).isNotEmpty();

        String next_href = JsonPath.read(content, "_links.next.href");


        content = mvc.perform(
                get(next_href)
                        .with(authentication(authentication_1)))

                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("_links.next.href").doesNotExist())
                .andExpect(jsonPath("page.size").isNumber())
                .andExpect(jsonPath("page.size", equalTo(2)))
                .andExpect(jsonPath("_embedded.suggestionResourceList").isArray())
                .andReturn().getResponse().getContentAsString();

        resourceList = JsonPath.read(content, "_embedded.suggestionResourceList");
        assertThat(resourceList).hasSize(2);

    }

    @Test
    public void test_mark_read() throws Exception {

        mvc.perform(get(suggestion_self_href)
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("read", equalTo(false)))
        ;

        mvc.perform(put(suggestion_self_href).param("read", "true")
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("read", equalTo(true)))
        ;
    }

    @Test
    public void test_is_read_always_false_for_not_owner() throws Exception {

        mvc.perform(get(suggestion_self_href)
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("read", equalTo(false)))
        ;

        mvc.perform(put(suggestion_self_href).param("read", "true")
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("read", equalTo(true)))
        ;

        mvc.perform(get(suggestion_self_href)
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("read", equalTo(true)))
        ;

        mvc.perform(get(suggestion_self_href)
                .with(authentication(authentication_2)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("read", equalTo(false)))
        ;

    }


}