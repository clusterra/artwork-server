/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.rest.suggestion;

import artwork.core.rest.AbstractWorkControllerTest;
import com.jayway.jsonpath.JsonPath;
import org.hamcrest.core.IsEqual;
import org.springframework.http.MediaType;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created on 06/02/2017.
 *
 * @author kepkap
 */
public class SuggestionController_errors_Test extends AbstractWorkControllerTest {

    private String suggestion_self_href;

    @BeforeMethod
    public void before() throws Exception {
        super.before();
        String content = mvc.perform(post(work_links_self_href + "/suggestions")
                .content("{\"text\":\"some_text\", \"pointX\":\"10\", \"pointY\":\"10\"}")
                .contentType(MediaType.APPLICATION_JSON)
                .with(authentication(authentication_1)))

                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("suggestionId").isNotEmpty())
                .andReturn().getResponse().getContentAsString();

        suggestion_self_href = JsonPath.read(content, "_links.self.href");

        assertThat(suggestion_self_href).isNotEmpty();

    }

    @Test
    public void test_error_empty_text() throws Exception {

        mvc.perform(post(work_links_self_href + "/suggestions")
                .content("{\"text\":\"\", \"pointX\":\"10\", \"pointY\":\"10\"}")
                .contentType(MediaType.APPLICATION_JSON)
                .with(authentication(authentication_1)))

                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0].code").value("NotEmpty"))
                .andExpect(jsonPath("$.errors[0].property").value("text"))
                .andExpect(jsonPath("$.errors[0].message").value(containsString("may not be empty")));


    }

    @Test
    public void test_error_null_point() throws Exception {


        mvc.perform(post(work_links_self_href + "/suggestions")
                .content("{\"text\":\"\", \"pointX\":null, \"pointY\":\"10\"}")
                .contentType(MediaType.APPLICATION_JSON)
                .with(authentication(authentication_1)))

                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0].code").value("NotEmpty"))
                .andExpect(jsonPath("$.errors[0].property").value("text"))
                .andExpect(jsonPath("$.errors[0].message").value(containsString("may not be empty")))
                .andExpect(jsonPath("$.errors[1].code").value("NotNull"))
                .andExpect(jsonPath("$.errors[1].property").value("pointX"))
                .andExpect(jsonPath("$.errors[1].message").value(containsString("may not be null")))
                .andExpect(jsonPath("$.message").value(containsString("validation failed with 2 error(s)")));


    }

    @Test
    public void test_error_negative_point() throws Exception {


        mvc.perform(post(work_links_self_href + "/suggestions")
                .content("{\"text\":\"text\", \"pointX\":\"1000000000\", \"pointY\":\"-100\"}")
                .contentType(MediaType.APPLICATION_JSON)
                .with(authentication(authentication_1)))

                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0].code").value("Max"))
                .andExpect(jsonPath("$.errors[0].property").value("pointX"))
                .andExpect(jsonPath("$.errors[0].message").value(containsString("must be less than or equal to")))
                .andExpect(jsonPath("$.errors[1].code").value("Min"))
                .andExpect(jsonPath("$.errors[1].property").value("pointY"))
                .andExpect(jsonPath("$.errors[1].message").value(containsString("must be greater than or equal to")))
                .andExpect(jsonPath("$.message").value(containsString("validation failed with 2 error(s)")));


    }

    @Test
    public void test_error_mark_read_by_not_work_owner_not_allowed() throws Exception {

        mvc.perform(get(suggestion_self_href)
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("read", equalTo(false)))
        ;

        mvc.perform(put(suggestion_self_href).param("read", "true")
                .with(authentication(authentication_2)))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("message", notNullValue()))
                .andExpect(jsonPath("errors", notNullValue()))
                .andExpect(jsonPath("errors[0].code", IsEqual.equalTo("illegal_work_owner_for_mark_suggestion_read_operation")))
                .andExpect(jsonPath("$.errors[0].property").doesNotExist())
                .andExpect(jsonPath("$.errors[0].message").value(containsString("operation is not allowed on not owning entity")));
    }

}