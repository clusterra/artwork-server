/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.rest;

import artwork.core.TestCoreConfig;
import artwork.web.security.social.UserDetails;
import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeMethod;

import java.util.List;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

/**
 * Created by Denis Kuchugurov
 * on 18.02.2016.
 */
@SpringBootTest(classes = {TestCoreConfig.class})
public abstract class AbstractControllerTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private WebApplicationContext context;

    protected MockMvc mvc;

    protected AnonymousAuthenticationToken anonymousAuthentication = new AnonymousAuthenticationToken("TEST_ANONYMOUS_KEY", "anonymousUser", AuthorityUtils.createAuthorityList("ROLE_ANONYMOUS"));

    protected Authentication authentication_1;
    protected Authentication authentication_2;

    @BeforeMethod
    public void before() throws Exception {

        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_SOCIAL_USER");
        authentication_1 = new TestingAuthenticationToken(UserDetails.from("user_1"), "", authorities);
        authentication_2 = new TestingAuthenticationToken(UserDetails.from("user_2"), "", authorities);
        authentication_1.setAuthenticated(true);
        authentication_2.setAuthenticated(true);
    }

}