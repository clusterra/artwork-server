/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core;

import artwork.common.rest.validation.ValidationConfig;
import artwork.core.domain.FakeImageStorage;
import artwork.core.domain.images.ImageStorage;
import artwork.core.domain.suggestion.SuggestionAddedEventInterceptor;
import artwork.core.domain.suggestion.SuggestionRemovedEventInterceptor;
import artwork.web.security.WebSecurityConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.context.PropertyPlaceholderAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Created by Denis Kuchugurov
 * on 19/12/2016.
 */
@Configuration
@Import({
        PropertyPlaceholderAutoConfiguration.class,
        CoreConfig.class,
        WebSecurityConfig.class,
        ValidationConfig.class,
})
public class TestCoreConfig {

    @Bean
    SuggestionAddedEventInterceptor getSuggestionAddedEventInterceptor() {
        return new SuggestionAddedEventInterceptor();
    }

    @Bean
    SuggestionRemovedEventInterceptor getSuggestionRemovedEventInterceptor() {
        return new SuggestionRemovedEventInterceptor();
    }

    @Bean
    public ImageStorage getImageStorage() {
        return new FakeImageStorage();
    }
}
