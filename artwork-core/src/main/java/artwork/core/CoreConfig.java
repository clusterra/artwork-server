/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core;

import artwork.commentary.CommentaryConfig;
import artwork.core.domain.popularity.Popularity;
import artwork.core.domain.suggestion.Suggestion;
import artwork.core.domain.tag.TaggedWork;
import artwork.core.domain.work.Work;
import artwork.iam.IamConfig;
import artwork.iam.MongoConfig;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.web.MultipartAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.hateoas.config.EnableHypermediaSupport.HypermediaType;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created by Denis Kuchugurov
 * on 24.02.2016.
 */
@Configuration
@EnableWebMvc
@EnableHypermediaSupport(type = {HypermediaType.HAL})
@EnableSpringDataWebSupport
@ComponentScan
@EntityScan(basePackageClasses = {Work.class, TaggedWork.class, Popularity.class, Suggestion.class})
@Import({
        MultipartAutoConfiguration.class,
        MongoConfig.class,
        CommentaryConfig.class,
        IamConfig.class})
@EnableMongoRepositories(basePackageClasses = {Work.class, Suggestion.class, Popularity.class})
public class CoreConfig {


}
