/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.migration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.set;
import static com.mongodb.client.model.Updates.unset;

@ChangeLog
public class CoreChangeLog {

    private static final Logger log = LoggerFactory.getLogger(CoreChangeLog.class);

    private static final String changeSet_v101 = "v1.0.1_work_image_id_to_image_meta_with_mime_type";

    @ChangeSet(order = changeSet_v101, id = changeSet_v101, author = "system")
    public void v1_0_1(MongoDatabase mongoDatabase) throws JsonProcessingException {

        MongoCollection<Document> collection = mongoDatabase.getCollection("work");

        FindIterable<Document> documents = collection.find();

        for (Document document : documents) {
            Document documentId = (Document) document.get("_id");
            Document imageId = (Document) document.get("imageId");

            if (imageId == null) {
                log.warn("has migration {} already been run on work {} ?", changeSet_v101, documentId);
                continue;
            }
            log.info("executing migration {} on work {}", changeSet_v101, documentId);
            String uuid = (String) imageId.get("uuid");
            Document imageMeta = new Document().append("uuid", uuid).append("mimeType", "");
            collection.updateOne(
                eq("_id", documentId),
                combine(
                    set("imageMeta", imageMeta),
                    unset("imageId")
                )
            );
        }

    }


}
