/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.domain.images;

import com.uploadcare.api.Client;
import com.uploadcare.api.File;

import java.util.Objects;

/**
 * Created by Egor Petushkov
 * on 13.06.17.
 */
public class UploadcareImageStorage implements ImageStorage {

    private final Client client;
    private final long maxWeight = 1024 * 1024 * 10; // 10 MB

    public UploadcareImageStorage(Client client) {
        this.client = client;
    }

    public ImageMeta save(String imageId) throws ImageTooSmallException, ImageWeightTooMuchException {
        File file = client.getFile(imageId);
        if (file.getSize() > maxWeight) {
            throw new ImageWeightTooMuchException(maxWeight, file.getSize());
        }
        return new ImageMeta(file.save().getFileId(), file.getMimeType());
    }

    public void delete(ImageMeta imageMeta) {
        Objects.requireNonNull(imageMeta);
        File file = client.getFile(imageMeta.getUuid());
        file.delete();
    }
}
