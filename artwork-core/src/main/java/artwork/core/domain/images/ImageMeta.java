/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.domain.images;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by Egor Petushkov
 * on 13.06.17.
 */
public class ImageMeta implements Serializable {

    @NotEmpty
    private final String uuid;

    @NotNull
    private final String mimeType;


    public ImageMeta(String uuid, String mimeType) {
        this.uuid = uuid;
        this.mimeType = mimeType;
    }

    public String getUuid() {
        return uuid;
    }

    public String getMimeType() {
        return mimeType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ImageMeta)) return false;

        ImageMeta imageMeta = (ImageMeta) o;

        return uuid.equals(imageMeta.uuid);
    }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }

    public boolean isGif() {
        return "image/gif".equals(mimeType);
    }
}
