/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.domain.images;

/**
 * Created by Egor Petushkov
 * on 29.06.17.
 */
public class ImageWeightTooMuchException extends Exception {
    private final long maxWeight;
    private final long imageWeight;

    public ImageWeightTooMuchException(long maxWeight, long imageWeight) {
        this.maxWeight = maxWeight;
        this.imageWeight = imageWeight;
    }

    public long getMaxWeight() {
        return maxWeight;
    }

    public long getImageWeight() {
        return imageWeight;
    }
}
