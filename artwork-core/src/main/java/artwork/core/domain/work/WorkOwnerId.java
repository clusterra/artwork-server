/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.domain.work;

import artwork.iam.user.model.UserId;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.index.Indexed;

import java.io.Serializable;
import java.util.Objects;

/**
 * Created by Denis Kuchugurov
 * on 24.04.2017.
 */
public class WorkOwnerId implements Serializable {

    @NotEmpty
    @Indexed
    private final String uuid;

    public WorkOwnerId(String uuid) {
        Objects.requireNonNull(uuid);
        this.uuid = uuid;
    }


    public String getUuid() {
        return uuid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WorkOwnerId ownerId = (WorkOwnerId) o;

        return uuid.equals(ownerId.uuid);
    }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }

    @Override
    public String toString() {
        return uuid;
    }

    public UserId toUserId() {
        return new UserId(uuid);
    }
}
