/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.domain.work;

import artwork.commentary.domain.commentable.CommentableId;
import artwork.core.domain.IdGenerator;
import artwork.likeable.domain.LikeableId;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.PersistenceConstructor;

import java.io.Serializable;

/**
 * Created by Denis Kuchugurov
 * on 25.02.2016.
 */
public class WorkId implements Serializable {

    @NotEmpty
    private final String uuid;

    @PersistenceConstructor
    public WorkId(String uuid) {
        this.uuid = uuid;
    }

    public LikeableId toLikeableId() {
        return new LikeableId(uuid);
    }

    public CommentableId toCommentableId() {
        return new CommentableId(uuid);
    }

    public static WorkId generateId() {
        return new WorkId(IdGenerator.generateId());
    }

    public static WorkId from(CommentableId commentableId) {
        return new WorkId(commentableId.getUuid());
    }

    public String getUuid() {
        return uuid;
    }

    @Override
    public String toString() {
        return uuid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WorkId workId = (WorkId) o;

        return uuid.equals(workId.uuid);
    }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }
}
