/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.domain.work;

import org.springframework.data.annotation.PersistenceConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by kepkap
 * on 05/05/2017.
 */
public class ViewableModel implements Viewable {

    private int viewsCount;

    @NotNull
    @Valid
    private ViewerId ownerViewerId;

    @NotNull
    @Valid
    private List<ViewerId> viewerIdsLast10;


    @PersistenceConstructor
    ViewableModel() {
    }

    public ViewableModel(ViewerId ownerViewerId, List<ViewerId> viewerIdsLast10) {
        this.ownerViewerId = ownerViewerId;
        this.viewerIdsLast10 = viewerIdsLast10;
    }

    @Override
    public int getViewsCount() {
        return viewsCount;
    }

    @Override
    public boolean view(ViewerId viewerId) {

        if (this.ownerViewerId.equals(viewerId)) {
            return false;
        }
        
        if (viewerIdsLast10.contains(viewerId)) {
            return false;
        }

        viewsCount++;

        if (viewerIdsLast10.size() >= 10) {
            viewerIdsLast10.remove(viewerIdsLast10.size() - 1);
        }
        viewerIdsLast10.add(0, viewerId);
        return true;
    }
}
