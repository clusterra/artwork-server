/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.domain.work;

import artwork.core.domain.images.ImageMeta;
import artwork.core.domain.suggestion.IllegalSuggestionOwnerOperationException;
import artwork.core.domain.suggestion.Point;
import artwork.core.domain.suggestion.Suggestion;
import artwork.core.domain.suggestion.SuggestionGroup;
import artwork.core.domain.suggestion.SuggestionId;
import artwork.core.domain.suggestion.SuggestionModel;
import artwork.core.domain.suggestion.SuggestionNotFoundException;
import artwork.core.domain.suggestion.SuggestionOwner;
import artwork.likeable.domain.LikeableModel;
import artwork.likeable.domain.dislike.Dislike;
import artwork.likeable.domain.dislike.Disliker;
import artwork.likeable.domain.dislike.DislikerId;
import artwork.likeable.domain.like.Like;
import artwork.likeable.domain.like.Liker;
import artwork.likeable.domain.like.LikerId;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.annotation.Version;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static artwork.core.domain.work.WorkModelDependencies.eventPublisher;
import static artwork.core.domain.work.WorkModelDependencies.imageStorage;
import static artwork.core.domain.work.WorkModelDependencies.workModelRepository;

/**
 * Created by Denis Kuchugurov
 * on 24.02.2016.
 */
@Document(collection = "work")
@CompoundIndexes({
        @CompoundIndex(def = "{ 'workOwner.uuid' : 1, 'createdDate' : -1 }"),
        @CompoundIndex(def = "{ 'workOwner.uuid' : 1, 'lastModifiedDate' : -1, 'likeable.likesCount' : -1 }"),
        @CompoundIndex(def = "{ 'workOwner.uuid' : 1, 'lastModifiedDate' : -1, 'viewable.viewsCount' : -1 }"),
        @CompoundIndex(def = "{ 'lastModifiedDate' : -1, 'likeable.likesCount' : -1 }"),
        @CompoundIndex(def = "{ 'lastModifiedDate' : -1, 'viewable.viewsCount' : -1 }"),
})
public class WorkModel implements Work {

    @CreatedDate
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date createdDate;

    @LastModifiedDate
    private Date lastModifiedDate;

    @Version
    private Long version;

    @Id
    private WorkId workId;

    @NotNull
    @Valid
    private WorkOwner workOwner;

    @NotNull
    @Valid
    private ImageMeta imageMeta;

    @NotEmpty
    @Size(max = 1000)
    private String description;

    private Set<String> tags;

    @NotNull
    @Valid
    private LikeableModel likeable;

    @NotNull
    @Valid
    private Viewable viewable;

    private int suggestionsCount;

    private int commentsCount;

    {
        Objects.requireNonNull(workModelRepository, "check if Spring context is initialized properly");
        Objects.requireNonNull(imageStorage, "check if Spring context is initialized properly");
        Objects.requireNonNull(eventPublisher, "check if Spring context is initialized properly");
    }


    @PersistenceConstructor
    WorkModel() {

    }

    public WorkModel(WorkId workId, WorkOwner workOwner, ImageMeta imageMeta, String description) {
        Objects.requireNonNull(workId);
        Objects.requireNonNull(workOwner);
        this.workId = workId;
        this.workOwner = workOwner;
        this.description = description;
        this.imageMeta = imageMeta;
        this.likeable = new LikeableModel(workId.toLikeableId(), new ArrayList<>(10), new ArrayList<>(10));
        this.viewable = new ViewableModel(new ViewerId(workOwner.getUuid()), new ArrayList<>(10));
        workModelRepository.save(this);
        eventPublisher.publishEvent(new WorkCreatedEvent(this));
    }

    private void touch() {
        this.lastModifiedDate = new Date();
    }

    public void incrementCommentsCount() {
        commentsCount++;
        workModelRepository.save(this);
    }

    public void incrementSuggestionsCount() {
        suggestionsCount++;
        workModelRepository.save(this);
    }

    @Override
    public int getCommentsCount() {
        return commentsCount;
    }

    public WorkOwner getWorkOwner() {
        return workOwner;
    }

    public ImageMeta getImageMeta() {
        return imageMeta;
    }

    @Override
    public Set<String> getTags() {
        return tags;
    }

    @Override
    public void addTag(String tag) {
        Objects.requireNonNull(tag, "tag is null");

        if (tags == null) {
            tags = new HashSet<>();
        }

        tags.add(tag);
        workModelRepository.save(this);
        eventPublisher.publishEvent(new WorkTagAddedEvent(this, tag));
    }

    @Override
    public void removeTag(String tag) {
        Objects.requireNonNull(tag, "tag is null");
        tags.remove(tag);
        workModelRepository.save(this);
        eventPublisher.publishEvent(new WorkTagRemovedEvent(this, tag));
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public boolean updateDescription(String description, WorkOwnerId updateById) throws IllegalWorkOwnerOperationException {

        if (!(workOwner.getWorkOwnerId().equals(updateById))) {
            throw new IllegalWorkOwnerOperationException(workId);
        }

        if (this.description.equals(description)) {
            return false;
        }

        this.description = description;
        touch();
        workModelRepository.save(this);
        eventPublisher.publishEvent(new WorkDescriptionUpdatedEvent(this, description));
        return true;
    }

    @Override
    public int getLikesCount() {
        return likeable.getLikesCount();
    }

    @Override
    public int getDislikesCount() {
        return likeable.getDislikesCount();
    }

    @Override
    public boolean like(Liker liker) {
        boolean added = likeable.like(liker);
        workModelRepository.save(this);
        if (added) {
            eventPublisher.publishEvent(new WorkLikedEvent(this, liker));
        }
        return added;
    }

    @Override
    public boolean unlike(Liker liker) {
        boolean removed = likeable.unlike(liker);
        workModelRepository.save(this);
        if (removed) {
            eventPublisher.publishEvent(new WorkUnlikedEvent(this, liker));
        }
        return removed;
    }

    @Override
    public boolean dislike(Disliker disliker) {
        boolean added = likeable.dislike(disliker);
        workModelRepository.save(this);
        if (added) {
            eventPublisher.publishEvent(new WorkDislikedEvent(this, disliker));
        }
        return added;
    }

    @Override
    public boolean undislike(Disliker disliker) {
        boolean removed = likeable.undislike(disliker);
        workModelRepository.save(this);
        if (removed) {
            eventPublisher.publishEvent(new WorkUndislikedEvent(this, disliker));
        }
        return removed;
    }

    @Override
    public Date getCreatedDate() {
        return createdDate;
    }

    @Override
    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    @Override
    public Long getVersion() {
        return version;
    }


    @Override
    public Page<Like> findLikes(Pageable pageable) {
        return likeable.findLikes(pageable);
    }

    @Override
    public Page<Dislike> findDislikes(Pageable pageable) {
        return likeable.findDislikes(pageable);
    }

    @Override
    public List<Liker> getLast10Likes() {
        return likeable.getLast10Likes();
    }

    @Override
    public List<Disliker> getLast10Dislikes() {
        return likeable.getLast10Dislikes();
    }

    @Override
    public boolean alreadyLiked(LikerId likerId) {
        return likeable.alreadyLiked(likerId);
    }

    @Override
    public boolean alreadyDisliked(DislikerId dislikerId) {
        return likeable.alreadyDisliked(dislikerId);
    }


    public boolean view(ViewerId viewerId) {

        boolean viewed = viewable.view(viewerId);

        if (viewed) {
            workModelRepository.save(this);
        }
        return viewed;
    }

    @Override
    public int getViewsCount() {

        if (viewable == null) {
            viewable = new ViewableModel(new ViewerId(workOwner.getUuid()), new ArrayList<>(10));
            workModelRepository.save(this);
        }

        return viewable.getViewsCount();
    }

    @Override
    public int getSuggestionsCount() {
        return suggestionsCount;
    }

    @Override
    public Suggestion addSuggestion(String description, SuggestionOwner suggestionOwner, Point point) {
        return new SuggestionModel(SuggestionId.generateId(), getWorkId(), getWorkOwner(), suggestionOwner, point, description);
    }

    @Override
    public void removeMySuggestion(SuggestionOwner suggestionOwner, SuggestionId suggestionId) throws SuggestionNotFoundException, IllegalSuggestionOwnerOperationException {
        Objects.requireNonNull(suggestionOwner, "suggestionOwner is null");
        Objects.requireNonNull(suggestionId, "suggestionId is null");


        Suggestion suggestion = Suggestion.findBy(suggestionId);

        suggestion.remove(suggestionOwner.getSuggestionOwnerId());

    }

    @Override
    public Page<Suggestion> findSuggestions(Pageable pageable) {
        return Suggestion.findByWorkId(getWorkId(), pageable);
    }

    @Override
    public Page<SuggestionGroup> findSuggestionsGroupedByOwners(Pageable pageable) {
        return Suggestion.findByWorkIdGroupedByOwners(getWorkId(), pageable);
    }

    @Override
    public WorkId getWorkId() {
        return workId;
    }

    @Override
    public boolean isMine(WorkOwnerId workOwnerId) {
        return this.workOwner.getWorkOwnerId().equals(workOwnerId);
    }

    @Override
    public void deleteMine(WorkOwnerId workOwnerId) throws IllegalWorkOwnerOperationException {

        if (!(workOwner.getWorkOwnerId().equals(workOwnerId))) {
            throw new IllegalWorkOwnerOperationException(getWorkId());
        }
        imageStorage.delete(imageMeta);

        workModelRepository.delete(this);
        eventPublisher.publishEvent(new WorkDeletedEvent(this));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WorkModel work = (WorkModel) o;

        return workId.equals(work.workId);
    }

    @Override
    public int hashCode() {
        return workId.hashCode();
    }

    @Override
    public String toString() {
        return "WorkModel{" +
                "createdDate=" + createdDate +
                ", workId='" + workId + '\'' +
                ", ownerId='" + workOwner.getUuid() + '\'' +
                '}';
    }
}

