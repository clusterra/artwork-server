/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.domain.work;

import artwork.core.domain.images.ImageMeta;
import artwork.core.domain.suggestion.IllegalSuggestionOwnerOperationException;
import artwork.core.domain.suggestion.Point;
import artwork.core.domain.suggestion.Suggestion;
import artwork.core.domain.suggestion.SuggestionGroup;
import artwork.core.domain.suggestion.SuggestionId;
import artwork.core.domain.suggestion.SuggestionNotFoundException;
import artwork.core.domain.suggestion.SuggestionOwner;
import artwork.likeable.domain.Likeable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

/**
 * Created on 26/01/2017.
 *
 * @author kepkap
 */
public interface Work extends Likeable, Viewable {

    static WorkModel viewOne(WorkId workId, ViewerId viewerId) throws WorkNotFoundException {
        WorkModel work = findOne(workId);

        WorkModelDependencies.eventPublisher.publishEvent(new WorkViewedEvent(work, viewerId));

        return work;
    }

    static WorkModel findOne(WorkId workId) throws WorkNotFoundException {
        Objects.requireNonNull(workId, "workId is null");

        WorkModel work = WorkModelDependencies.workModelRepository.findOne(workId);

        if (work == null) {
            throw new WorkNotFoundException(workId);
        }

        return work;
    }

    static Page<WorkModel> findAll(int page, int size) {
        return WorkModelDependencies.workModelRepository.findAllByOrderByCreatedDateDesc(new PageRequest(page, size));
    }

    static Page<WorkModel> findAll(WorkOwnerId workOwnerId, int page, int size) {
        return WorkModelDependencies.workModelRepository.findByWorkOwnerUuidOrderByLastModifiedDateDesc(workOwnerId.getUuid(), new PageRequest(page, size));
    }

    static Page<WorkModel> findMostPopularThisMonth(int page, int size) {
        return WorkModelDependencies.workModelRepository.findByLastModifiedDateGreaterThanOrderByViewableViewsCountDesc(
                Date.from(LocalDate.now().minusMonths(1).atStartOfDay().toInstant(ZoneOffset.UTC)),
                new PageRequest(page, size));
    }

    static Page<WorkModel> findMostPopularThisMonth(WorkOwnerId workOwnerId, int page, int size) {
        return WorkModelDependencies.workModelRepository.findByWorkOwnerUuidAndLastModifiedDateGreaterThanOrderByViewableViewsCountDesc(
                workOwnerId.getUuid(),
                Date.from(LocalDate.now().minusMonths(1).atStartOfDay().toInstant(ZoneOffset.UTC)),
                new PageRequest(page, size));

    }

    static Page<WorkModel> findMostRatedThisMonth(int page, int size) {
        return WorkModelDependencies.workModelRepository.findByLastModifiedDateGreaterThanOrderByLikeableLikesCountDesc(
                Date.from(LocalDate.now().minusMonths(1).atStartOfDay().toInstant(ZoneOffset.UTC)),
                new PageRequest(page, size));

    }

    static Page<WorkModel> findMostRatedThisMonth(WorkOwnerId workOwnerId, int page, int size) {
        return WorkModelDependencies.workModelRepository.findByWorkOwnerUuidAndLastModifiedDateGreaterThanOrderByLikeableLikesCountDesc(
                workOwnerId.getUuid(),
                Date.from(LocalDate.now().minusMonths(1).atStartOfDay().toInstant(ZoneOffset.UTC)),
                new PageRequest(page, size));

    }

    int getCommentsCount();

    int getSuggestionsCount();

    WorkOwner getWorkOwner();

    ImageMeta getImageMeta();

    Set<String> getTags();

    void addTag(String tag);

    void removeTag(String tag);

    String getDescription();

    boolean updateDescription(String description, WorkOwnerId updateById) throws IllegalWorkOwnerOperationException;

    Date getCreatedDate();

    Date getLastModifiedDate();

    Long getVersion();

    Suggestion addSuggestion(String description, SuggestionOwner suggestionOwner, Point point);

    void removeMySuggestion(SuggestionOwner suggestionOwner, SuggestionId suggestionId) throws SuggestionNotFoundException, IllegalSuggestionOwnerOperationException;

    Page<Suggestion> findSuggestions(Pageable pageable);

    Page<SuggestionGroup> findSuggestionsGroupedByOwners(Pageable pageable);

    WorkId getWorkId();

    boolean isMine(WorkOwnerId workOwnerId);

    void deleteMine(WorkOwnerId workOwnerId) throws IllegalWorkOwnerOperationException;

    void incrementCommentsCount();

    void incrementSuggestionsCount();
}
