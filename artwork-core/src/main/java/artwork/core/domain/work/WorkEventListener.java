/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.domain.work;

import artwork.commentary.domain.comment.Comment;
import artwork.commentary.domain.comment.CommentCreatedEvent;
import artwork.commentary.domain.commentable.CommentableId;
import artwork.commentary.domain.commentable.CommentableModel;
import artwork.commentary.domain.commentable.CommentableOwnerId;
import artwork.core.domain.suggestion.Suggestion;
import artwork.core.domain.suggestion.SuggestionCreatedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * Created by kepkap
 * on 07/09/16.
 */
@Component
class WorkEventListener {

    private final WorkModelRepository workModelRepository;

    WorkEventListener(WorkModelRepository workModelRepository) {
        this.workModelRepository = workModelRepository;
    }


    @EventListener
    void createCommentable(WorkCreatedEvent event) {
        Work work = event.getWork();
        WorkOwner workOwner = work.getWorkOwner();
        new CommentableModel(work.getWorkId().toCommentableId(), new CommentableOwnerId(workOwner.getUuid()));
    }

    @EventListener
    void incrementCommentsCount(CommentCreatedEvent event) throws WorkNotFoundException {
        Comment comment = event.getComment();
        CommentableId commentableId = comment.getCommentableId();

        Work work = workModelRepository.findOne(new WorkId(commentableId.getUuid()));
        if (work != null && !comment.hasRelateTo()) {
            work.incrementCommentsCount();
        }
    }

    @EventListener
    void incrementSuggestionsCount(SuggestionCreatedEvent event) throws WorkNotFoundException {
        Suggestion suggestion = event.getSuggestion();

        Work work = workModelRepository.findOne(suggestion.getWorkId());
        if (work != null) {
            work.incrementSuggestionsCount();
        }
    }

    @Async
    @EventListener
    void handle(WorkViewedEvent event) throws WorkNotFoundException {
        Work work = event.getWork();
        work.view(event.getViewerId());
    }


}
