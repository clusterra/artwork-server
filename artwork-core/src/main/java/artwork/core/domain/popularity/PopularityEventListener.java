/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.domain.popularity;

import artwork.commentary.domain.comment.CommentCreatedEvent;
import artwork.commentary.domain.commentable.Commentable;
import artwork.core.domain.work.Work;
import artwork.core.domain.work.WorkDislikedEvent;
import artwork.core.domain.work.WorkLikedEvent;
import artwork.core.domain.work.WorkNotFoundException;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * Created by kepkap
 * on 07/09/16.
 */
@Component
class PopularityEventListener {


    @EventListener
    void handle(WorkLikedEvent event) {
        Work work = event.getWork();
        Popularity popularity = Popularity.find();

        int likesCount = work.getLikesCount();

        popularity.updateMaxLikesCountIfRequired(likesCount);
    }

    @EventListener
    void handle(WorkDislikedEvent event) throws WorkNotFoundException {
        Work work = event.getWork();
        Popularity popularity = Popularity.find();

        int dislikesCount = work.getDislikesCount();

        popularity.updateMaxDislikesCountIfRequired(dislikesCount);
    }

    @EventListener
    void handle(CommentCreatedEvent event) throws WorkNotFoundException {
        Commentable commentable = null;

        if (commentable == null) {
            return;
        }

        Popularity popularity = Popularity.find();

        int commentsCount = commentable.getCommentsCount();

        popularity.updateMaxCommentsCountIfRequired(commentsCount);
    }


}
