/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.domain.popularity;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static artwork.core.domain.popularity.PopularityDependencies.popularityRepository;

/**
 * Created on 29/01/2017.
 *
 * @author kepkap
 */
public class Popularity {

    static final String POPULARITY_HASH_KEY = "POPULARITY_HASH_KEY";

    @NotEmpty
    private String uuid;

    @NotNull
    private Map<String, Integer> tagCounts;

    @NotNull
    private Integer maxViewsCount;

    @NotNull
    private Integer maxLikesCount;

    @NotNull
    private Integer maxDislikesCount;

    @NotNull
    private Integer maxCommentsCount;


    {
        Objects.requireNonNull(popularityRepository, "check if Spring context is initialized properly");
    }


    public Popularity() {
    }

    private Popularity(String uuid) {
        this.uuid = uuid;
        this.tagCounts = new HashMap<>();
        this.maxViewsCount = 0;
        this.maxLikesCount = 0;
        this.maxDislikesCount = 0;
        this.maxCommentsCount = 0;
        popularityRepository.save(this);
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Map<String, Integer> getTagCounts() {
        return tagCounts;
    }

    public void setTagCounts(Map<String, Integer> tagCounts) {
        this.tagCounts = tagCounts;
    }

    public Integer getMaxViewsCount() {
        return maxViewsCount;
    }

    public void setMaxViewsCount(Integer maxViewsCount) {
        this.maxViewsCount = maxViewsCount;
    }

    public Integer getMaxLikesCount() {
        return maxLikesCount;
    }

    public void setMaxLikesCount(Integer maxLikesCount) {
        this.maxLikesCount = maxLikesCount;
    }

    public Integer getMaxDislikesCount() {
        return maxDislikesCount;
    }

    public void setMaxDislikesCount(Integer maxDislikesCount) {
        this.maxDislikesCount = maxDislikesCount;
    }

    public Integer getMaxCommentsCount() {
        return maxCommentsCount;
    }

    public void setMaxCommentsCount(Integer maxCommentsCount) {
        this.maxCommentsCount = maxCommentsCount;
    }

    public static Popularity find() {
        Popularity popularity = popularityRepository.findOne(POPULARITY_HASH_KEY);

        if (popularity == null) {
            return new Popularity(POPULARITY_HASH_KEY);
        }

        return popularity;
    }

    public void updateMaxLikesCountIfRequired(int likesCount) {
        if (maxLikesCount < likesCount) {
            maxLikesCount = likesCount;
            popularityRepository.save(this);
        }
    }

    public void updateMaxDislikesCountIfRequired(int dislikesCount) {
        if (maxDislikesCount < dislikesCount) {
            maxDislikesCount = dislikesCount;
            popularityRepository.save(this);
        }
    }

    public void updateMaxCommentsCountIfRequired(int commentsCount) {
        if (maxCommentsCount < commentsCount) {
            maxCommentsCount = commentsCount;
            popularityRepository.save(this);
        }
    }
}
