/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.domain.suggestion;

import artwork.core.domain.work.WorkId;
import artwork.core.domain.work.WorkOwner;
import artwork.core.domain.work.WorkOwnerId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;

import java.util.Date;

import static artwork.core.domain.suggestion.SuggestionDependencies.mongoTemplate;
import static artwork.core.domain.suggestion.SuggestionDependencies.suggestionModelRepository;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.limit;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.skip;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;

/**
 * Created on 31/01/2017.
 *
 * @author kepkap
 */
public interface Suggestion {

    static Suggestion findBy(SuggestionId suggestionId) throws SuggestionNotFoundException {
        Suggestion suggestion = suggestionModelRepository.findOne(suggestionId);
        if (suggestion == null) {
            throw new SuggestionNotFoundException(suggestionId);
        }
        return suggestion;
    }

    static Page<Suggestion> findByWorkId(WorkId workId, Pageable pageable) {
        return suggestionModelRepository.findByWorkId(workId, pageable);
    }

    static Page<SuggestionGroup> findByWorkIdGroupedByOwners(WorkId workId, Pageable pageable) {

        AggregationResults<Total> totalAggregationResults = mongoTemplate.aggregate(
                newAggregation(
                        match(Criteria.where("workId").is(workId)),
                        group("suggestionOwner").count().as("total")
                ),
                SuggestionModel.class,
                Total.class);

        long total = totalAggregationResults.getMappedResults().size();

        AggregationResults<SuggestionGroup> results = mongoTemplate.aggregate(
                newAggregation(
                        match(Criteria.where("workId").is(workId)),
                        sort(new Sort(Sort.Direction.DESC, "createdDate")),
                        group("suggestionOwner").push("$$ROOT").as("suggestions").first("createdDate").as("createdDate"),
                        skip((long) pageable.getOffset()),
                        limit((long) pageable.getPageSize()),
                        sort(new Sort(Sort.Direction.DESC, "createdDate"))
                ),
                SuggestionModel.class,
                SuggestionGroup.class);
        return new PageImpl<>(results.getMappedResults(), pageable, total);
    }

    boolean isRead();

    boolean isNotRead();

    void markRead(WorkOwnerId workOwnerId) throws IllegalWorkOwnerForMarkReadOperationException;

    String getDescription();

    String updateDescription(SuggestionOwnerId suggestionOwnerId, String description) throws IllegalSuggestionOwnerOperationException;

    SuggestionOwner getSuggestionOwner();

    WorkOwner getWorkOwner();

    Point getPoint();

    void remove(SuggestionOwnerId suggestionOwnerId) throws IllegalSuggestionOwnerOperationException;

    SuggestionId getSuggestionId();

    WorkId getWorkId();

    Date getCreatedDate();

    int getCommentsCount();

    void incrementCommentsCount();

    boolean isMine(SuggestionOwnerId suggestionOwnerId);
}
