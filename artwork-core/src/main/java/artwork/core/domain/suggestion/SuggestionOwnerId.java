/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.domain.suggestion;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.index.Indexed;

import java.io.Serializable;
import java.util.Objects;

/**
 * A class representing acting user identity.
 * To be used for tracking authenticated individuals.
 * <p>
 * <p>
 * <p>
 * Created by Denis Kuchugurov
 * on 25.02.2016.
 */
public class SuggestionOwnerId implements Serializable {

    @NotEmpty
    @Indexed
    private final String uuid;

    public SuggestionOwnerId(String uuid) {
        Objects.requireNonNull(uuid);
        this.uuid = uuid;
    }


    public String getUuid() {
        return uuid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SuggestionOwnerId suggestionOwnerId = (SuggestionOwnerId) o;

        return uuid.equals(suggestionOwnerId.uuid);
    }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }

    @Override
    public String toString() {
        return uuid;
    }
}
