/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.domain.suggestion;

import artwork.commentary.domain.commentable.CommentableId;
import artwork.commentary.domain.commentable.RelateToId;
import artwork.core.domain.IdGenerator;

import java.io.Serializable;

/**
 * Created by kepkap
 * on 07/06/16.
 */
public class SuggestionId implements Serializable {

    private final String uuid;

    public SuggestionId(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

    @Override
    public String toString() {
        return uuid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SuggestionId that = (SuggestionId) o;

        return uuid.equals(that.uuid);
    }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }

    public RelateToId toRelatedToId() {
        return new RelateToId(getUuid());
    }

    public CommentableId toCommentableId() {
        return new CommentableId(getUuid());
    }

    public static SuggestionId generateId() {
        return new SuggestionId(IdGenerator.generateId());
    }
}
