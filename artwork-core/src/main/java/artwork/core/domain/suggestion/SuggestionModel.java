/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.domain.suggestion;

import artwork.core.domain.work.WorkId;
import artwork.core.domain.work.WorkOwner;
import artwork.core.domain.work.WorkOwnerId;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.Objects;

import static artwork.core.domain.suggestion.SuggestionDependencies.eventPublisher;
import static artwork.core.domain.suggestion.SuggestionDependencies.suggestionModelRepository;

/**
 * Created by kepkap
 * on 07/06/16.
 */
@Document(collection = "suggestion")
@CompoundIndexes({
        @CompoundIndex(name = "suggestionOwner.ownerId.uuid.lastModifiedDate", def = "{ 'suggestionOwner.ownerId.uuid' : 1, 'lastModifiedDate' : -1 }"),
        @CompoundIndex(name = "lastModifiedDate", def = "{ 'lastModifiedDate' : -1 }"),
})
public class SuggestionModel implements Suggestion {

    @CreatedDate
    private Date createdDate;

    @LastModifiedDate
    private Date lastModifiedDate;

    @Version
    private Long version;

    @Id
    private SuggestionId suggestionId;

    @NotNull
    @Valid
    private WorkId workId;

    @NotNull
    @Valid
    private WorkOwner workOwner;

    @NotNull
    @Valid
    private SuggestionOwner suggestionOwner;

    @NotNull
    private Point point;

    @NotEmpty
    @Size(max = 2000)
    private String description;

    private int commentsCount;

    private boolean read;

    {
        Objects.requireNonNull(suggestionModelRepository, "check if Spring context is initialized properly");
        Objects.requireNonNull(eventPublisher, "check if Spring context is initialized properly");
    }

    public SuggestionModel() {
    }

    public SuggestionModel(SuggestionId suggestionId, WorkId workId, WorkOwner workOwner, SuggestionOwner suggestionOwner, Point point, String description) {
        Objects.requireNonNull(suggestionId);
        Objects.requireNonNull(workId);
        this.suggestionId = suggestionId;
        this.workId = workId;
        this.workOwner = workOwner;
        this.suggestionOwner = suggestionOwner;
        this.point = point;
        this.description = description;
        suggestionModelRepository.save(this);
        eventPublisher.publishEvent(new SuggestionCreatedEvent(this));
    }

    public void incrementCommentsCount() {
        commentsCount++;
        suggestionModelRepository.save(this);
    }

    @Override
    public boolean isRead() {
        return read;
    }

    @Override
    public boolean isNotRead() {
        return !read;
    }

    @Override
    public void markRead(WorkOwnerId workOwnerId) throws IllegalWorkOwnerForMarkReadOperationException {
        assertWorkOwner(workOwnerId);
        this.read = true;
        suggestionModelRepository.save(this);
    }

    void assertWorkOwner(WorkOwnerId workOwnerId) throws IllegalWorkOwnerForMarkReadOperationException {
        if (!this.workOwner.getWorkOwnerId().equals(workOwnerId)) {
            throw new IllegalWorkOwnerForMarkReadOperationException(workId);
        }
    }

    void assertSuggestionOwner(SuggestionOwnerId suggestionOwnerId) throws IllegalSuggestionOwnerOperationException {
        if (!this.suggestionOwner.getSuggestionOwnerId().equals(suggestionOwnerId)) {
            throw new IllegalSuggestionOwnerOperationException(suggestionId);
        }
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String updateDescription(SuggestionOwnerId suggestionOwnerId, String description) throws IllegalSuggestionOwnerOperationException {
        assertSuggestionOwner(suggestionOwnerId);
        if (this.description.equals(description)) {
            return this.description;
        }
        this.description = description;
        suggestionModelRepository.save(this);
        return this.description;
    }


    @Override
    public SuggestionOwner getSuggestionOwner() {
        return suggestionOwner;
    }

    @Override
    public WorkOwner getWorkOwner() {
        return workOwner;
    }

    @Override
    public Point getPoint() {
        return point;
    }

    @Override
    public void remove(SuggestionOwnerId suggestionOwnerId) throws IllegalSuggestionOwnerOperationException {
        assertSuggestionOwner(suggestionOwnerId);
        suggestionModelRepository.delete(this);
        eventPublisher.publishEvent(new SuggestionRemovedEvent(this, this));
    }

    @Override
    public SuggestionId getSuggestionId() {
        return suggestionId;
    }

    @Override
    public WorkId getWorkId() {
        return workId;
    }

    @Override
    public boolean isMine(SuggestionOwnerId suggestionOwnerId) {
        return this.suggestionOwner.getSuggestionOwnerId().equals(suggestionOwnerId);
    }

    @Override
    public Date getCreatedDate() {
        return createdDate;
    }

    @Override
    public int getCommentsCount() {
        return commentsCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SuggestionModel that = (SuggestionModel) o;

        return suggestionId.equals(that.suggestionId);
    }

    @Override
    public int hashCode() {
        return suggestionId.hashCode();
    }

    @Override
    public String toString() {
        return "SuggestionModel{" +
                "suggestionId=" + suggestionId +
                ", workId=" + workId +
                ", workOwner=" + workOwner +
                ", suggestionOwner=" + suggestionOwner +
                '}';
    }
}
