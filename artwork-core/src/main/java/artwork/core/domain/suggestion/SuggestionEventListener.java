/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.domain.suggestion;

import artwork.commentary.domain.comment.Comment;
import artwork.commentary.domain.comment.CommentAuthor;
import artwork.commentary.domain.comment.CommentCreatedEvent;
import artwork.commentary.domain.comment.CommentNotFoundException;
import artwork.commentary.domain.comment.IllegalCommentAuthorOperationException;
import artwork.commentary.domain.commentable.Commentable;
import artwork.commentary.domain.commentable.CommentableId;
import artwork.commentary.domain.commentable.CommentableModel;
import artwork.commentary.domain.commentable.CommentableNotFoundException;
import artwork.commentary.domain.commentable.CommentableOwnerId;
import artwork.commentary.domain.commentable.RelateTo;
import artwork.commentary.domain.commentable.RelateToId;
import artwork.commentary.domain.commentable.RelateToOwnerId;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import static artwork.core.domain.suggestion.SuggestionDependencies.suggestionModelRepository;

/**
 * Created by kepkap
 * on 07/09/16.
 */
@Component
class SuggestionEventListener {

    private static final String SUGGESTED_TEXT_WORKAROUND_FIXME = "Recommended Suggestion";

    @EventListener
    void addCommentToWork(SuggestionCreatedEvent event) throws CommentableNotFoundException {

        Suggestion suggestion = event.getSuggestion();

        SuggestionOwner suggestionOwner = suggestion.getSuggestionOwner();

        CommentAuthor commentAuthor = suggestionOwner.toAuthor();
        RelateToId relateToId = suggestion.getWorkId().toCommentableId().toRelateToId();
        RelateToOwnerId relateToOwnerId = new RelateToOwnerId(suggestion.getWorkOwner().getUuid());
        new CommentableModel(suggestion.getSuggestionId().toCommentableId(), new RelateTo(relateToId, relateToOwnerId), new CommentableOwnerId(suggestionOwner.getUuid()));

        Commentable commentable = Commentable.findOne(suggestion.getWorkId().toCommentableId());

        commentable.addComment(commentAuthor, String.format("%s: %s", SUGGESTED_TEXT_WORKAROUND_FIXME, suggestion.getDescription()), suggestion.getSuggestionId().toRelatedToId());
    }

    @EventListener
    void removeRelateTo(SuggestionRemovedEvent event) throws IllegalCommentAuthorOperationException, CommentableNotFoundException, CommentNotFoundException {
        Suggestion suggestion = event.getSuggestion();
//        Comment.deleteAll(suggestion.getWorkId().toCommentableId());

        Commentable commentable = Commentable.findOne(suggestion.getWorkId().toCommentableId());

        Comment comment = commentable.findOne(suggestion.getSuggestionId().toRelatedToId());
        comment.removeRelateTo();
    }

    @EventListener
    void incrementCommentsCount(CommentCreatedEvent event) {
        Comment comment = event.getComment();
        Commentable commentable = event.getCommentable();
        CommentableId commentableId = commentable.getCommentableId();

        RelateTo relateTo = commentable.getRelateTo();
        if (relateTo == null) {
            return;
        }
        Suggestion suggestion = suggestionModelRepository.findOne(new SuggestionId(relateTo.getRelateToId().getUuid()));
        if (suggestion != null) {
            suggestion.incrementCommentsCount();
        }
    }
}
