/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.domain.suggestion;

import artwork.commentary.domain.comment.CommentAuthor;
import artwork.web.security.domain.UserProfile;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotNull;

/**
 * Created by Denis Kuchugurov
 * on 10/03/16 23:20.
 */
public class SuggestionOwner implements UserProfile {

    @NotEmpty
    private String uuid;

    @NotEmpty
    private String displayName;

    @NotNull
    @URL
    private String imageUrl;

    @NotNull
    @URL
    private String profileUrl;

    public SuggestionOwner() {
    }

    public SuggestionOwner(String uuid, String displayName, String imageUrl, String profileUrl) {
        this.uuid = uuid;
        this.displayName = displayName;
        this.imageUrl = imageUrl;
        this.profileUrl = profileUrl;
    }

    public String getUuid() {
        return uuid;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public SuggestionOwnerId getSuggestionOwnerId() {
        return new SuggestionOwnerId(uuid);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SuggestionOwner owner = (SuggestionOwner) o;

        return uuid.equals(owner.uuid);
    }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }

    @Override
    public String toString() {
        return "SuggestionOwner{" +
                "uuid='" + uuid + '\'' +
                '}';
    }

    public CommentAuthor toAuthor() {
        return new CommentAuthor(uuid, displayName, imageUrl, profileUrl);
    }
}
