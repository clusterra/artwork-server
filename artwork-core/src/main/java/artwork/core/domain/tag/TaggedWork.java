/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.domain.tag;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Version;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.Objects;

/**
 * Created on 29/01/2017.
 *
 * @author kepkap
 */
public class TaggedWork {

    @CreatedDate
    private Date createdDate;

    @Version
    private Long version;

    @NotEmpty
    private String tag;

    @NotEmpty
    private String workUuid;

    @NotEmpty
    private String ownerUuid;


    {
        Objects.requireNonNull(TaggedWorkDependencies.taggedWorkRepository, "check if Spring context is initialized properly");
    }


    public TaggedWork() {
    }

    public TaggedWork(String tag, String workUuid, String ownerUuid) {
        this.workUuid = workUuid;
        this.ownerUuid = ownerUuid;
        this.tag = tag;
        TaggedWorkDependencies.taggedWorkRepository.save(this);
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getWorkUuid() {
        return workUuid;
    }

    public void setWorkUuid(String workUuid) {
        this.workUuid = workUuid;
    }

    public String getOwnerUuid() {
        return ownerUuid;
    }

    public void setOwnerUuid(String ownerUuid) {
        this.ownerUuid = ownerUuid;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public static Page<TaggedWork> findAll(Pageable pageable) {
        return TaggedWorkDependencies.taggedWorkRepository.findAll(pageable);
    }
}
