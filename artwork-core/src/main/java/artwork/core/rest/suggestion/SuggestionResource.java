/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.rest.suggestion;

import artwork.core.rest.owner.OwnerJson;
import org.springframework.hateoas.ResourceSupport;

import java.util.Date;

/**
 * Created by Denis Kuchugurov
 * on 18.02.2016.
 */
class SuggestionResource extends ResourceSupport {

    public final String suggestionId;

    public final OwnerJson author;

    public final String description;

    public final double pointX;

    public final double pointY;

    public final int commentsCount;

    public final Date createdDate;

    public final boolean isMine;

    public final boolean read;


    public SuggestionResource(String suggestionId,
                              OwnerJson author,
                              String description,
                              double pointX,
                              double pointY,
                              int commentsCount,
                              Date createdDate,
                              boolean isMine,
                              boolean read) {
        this.suggestionId = suggestionId;
        this.author = author;
        this.description = description;
        this.pointX = pointX;
        this.pointY = pointY;
        this.commentsCount = commentsCount;
        this.createdDate = createdDate;
        this.isMine = isMine;
        this.read = read;
    }

}
