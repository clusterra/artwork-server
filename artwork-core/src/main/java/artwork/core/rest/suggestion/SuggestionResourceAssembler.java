/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.rest.suggestion;

import artwork.commentary.rest.CommentController;
import artwork.core.domain.suggestion.Suggestion;
import artwork.core.domain.suggestion.SuggestionOwner;
import artwork.core.rest.owner.OwnerJson;
import artwork.core.rest.work.WorkController;
import org.springframework.data.domain.PageRequest;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by Denis Kuchugurov
 * on 18.02.2016.
 */
@Component
class SuggestionResourceAssembler extends ResourceAssemblerSupport<Suggestion, SuggestionResource> {

    public SuggestionResourceAssembler() {
        super(SuggestionController.class, SuggestionResource.class);
    }

    @Override
    public SuggestionResource toResource(Suggestion entity) {

        try {

            boolean read = entity.getWorkOwner().getWorkOwnerId().equals(CurrentUser.asWorkOwnerId()) && entity.isRead();
            SuggestionResource resource = new SuggestionResource(
                    entity.getSuggestionId().getUuid(),
                    toOwnerJson(entity.getSuggestionOwner()),
                    entity.getDescription(),
                    entity.getPoint().getX(),
                    entity.getPoint().getY(),
                    entity.getCommentsCount(),
                    entity.getCreatedDate(),
                    entity.isMine(CurrentUser.asSuggestionOwnerId()),
                    read);

            resource.add(linkTo(methodOn(SuggestionController.class).get(entity.getWorkId(), entity.getSuggestionId())).withSelfRel());
            resource.add(linkTo(methodOn(CommentController.class).getAll(entity.getSuggestionId().toCommentableId(), new PageRequest(0, 20), null)).withRel("comments"));
            resource.add(linkTo(methodOn(WorkController.class).get(entity.getWorkId())).withRel("work"));
            return resource;
        } catch (Exception e) {
            throw new IllegalStateException("should never happen...", e);
        }
    }

    private OwnerJson toOwnerJson(SuggestionOwner owner) {
        return new OwnerJson(owner.getUuid(), owner.getDisplayName(), owner.getImageUrl(), owner.getProfileUrl());
    }
}
