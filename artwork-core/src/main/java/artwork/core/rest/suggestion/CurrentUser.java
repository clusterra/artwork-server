/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.rest.suggestion;

import artwork.core.domain.suggestion.SuggestionOwner;
import artwork.core.domain.suggestion.SuggestionOwnerId;
import artwork.core.domain.work.WorkOwnerId;

/**
 * Created on 26/01/2017.
 *
 * @author kepkap
 */
class CurrentUser {


    static SuggestionOwner asSuggestionOwner() {
        artwork.web.security.tracking.CurrentUser currentUser = artwork.web.security.tracking.CurrentUser.requireNotAnonymous();
        return new SuggestionOwner(currentUser.getUuid(), currentUser.getDisplayName(), currentUser.getImageUrl(), currentUser.getProfileUrl());
    }

    static WorkOwnerId asWorkOwnerId() {
        artwork.web.security.tracking.CurrentUser any = artwork.web.security.tracking.CurrentUser.requireAny();
        return new WorkOwnerId(any.getUuid());
    }


    static SuggestionOwnerId asSuggestionOwnerId() {
        artwork.web.security.tracking.CurrentUser any = artwork.web.security.tracking.CurrentUser.requireAny();
        return new SuggestionOwnerId(any.getUuid());
    }
}
