/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.rest.suggestion;

import artwork.common.rest.validation.error.Error;
import artwork.common.rest.validation.error.ErrorsResponse;
import artwork.common.rest.validation.error.ModelError;
import artwork.core.domain.suggestion.SuggestionGroup;
import artwork.core.domain.work.Work;
import artwork.core.domain.work.WorkId;
import artwork.core.domain.work.WorkNotFoundException;
import com.mongodb.MongoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;

/**
 * Created by kepkap
 * on 12/06/16.
 */
@RestController
@RequestMapping(value = "/api/works/{workId}/suggestion-groups", produces = MediaTypes.HAL_JSON_VALUE)
public class SuggestionGroupController {

    private static final Logger log = LoggerFactory.getLogger(SuggestionGroupController.class);

    private final SuggestionGroupResourceAssembler suggestionGroupResourceAssembler;


    public SuggestionGroupController(SuggestionGroupResourceAssembler suggestionGroupResourceAssembler) {
        this.suggestionGroupResourceAssembler = suggestionGroupResourceAssembler;
    }


    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<PagedResources<SuggestionGroupResource>> getAll(
            @PathVariable("workId") WorkId workId,
            @PageableDefault Pageable pageable,
            PagedResourcesAssembler<SuggestionGroup> pagedResourcesAssembler) throws WorkNotFoundException {

        Work work = Work.findOne(workId);

        Page<SuggestionGroup> page = work.findSuggestionsGroupedByOwners(pageable);

        PagedResources<SuggestionGroupResource> pagedResources = pagedResourcesAssembler.toResource(page, suggestionGroupResourceAssembler);

        return new ResponseEntity<>(pagedResources, HttpStatus.OK);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorsResponse handle(WorkNotFoundException e) {
        log.trace(e.getMessage(), e);
        return new ErrorsResponse(new ModelError("work_not_found", e.getMessage()));
    }


    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorsResponse handle(MongoException e) {
        log.trace(e.getMessage(), e);
        return new ErrorsResponse(new Error("data_storage_error", e.getMessage(), Collections.emptyList()));
    }
}
