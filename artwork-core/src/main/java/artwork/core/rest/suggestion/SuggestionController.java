/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.rest.suggestion;

import artwork.common.rest.validation.error.Error;
import artwork.common.rest.validation.error.ErrorsResponse;
import artwork.common.rest.validation.error.ModelError;
import artwork.core.domain.suggestion.IllegalSuggestionOwnerOperationException;
import artwork.core.domain.suggestion.IllegalWorkOwnerForMarkReadOperationException;
import artwork.core.domain.suggestion.Point;
import artwork.core.domain.suggestion.Suggestion;
import artwork.core.domain.suggestion.SuggestionId;
import artwork.core.domain.suggestion.SuggestionNotFoundException;
import artwork.core.domain.work.Work;
import artwork.core.domain.work.WorkId;
import artwork.core.domain.work.WorkNotFoundException;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mongodb.MongoException;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Collections;

/**
 * Created by kepkap
 * on 12/06/16.
 */
@RestController
@RequestMapping(value = "/api/works/{workId}/suggestions", produces = MediaTypes.HAL_JSON_VALUE)
public class SuggestionController {

    private static final Logger log = LoggerFactory.getLogger(SuggestionController.class);

    private final SuggestionResourceAssembler suggestionResourceAssembler;


    public SuggestionController(SuggestionResourceAssembler suggestionResourceAssembler) {
        this.suggestionResourceAssembler = suggestionResourceAssembler;
    }


    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<SuggestionResource> create(@PathVariable("workId") WorkId workId,
                                                     @Valid @RequestBody SuggestionRequest suggestionRequest) throws WorkNotFoundException {

        Work work = Work.findOne(workId);

        Suggestion suggestion = work.addSuggestion(suggestionRequest.text, CurrentUser.asSuggestionOwner(), new Point(suggestionRequest.pointX, suggestionRequest.pointY));

        return new ResponseEntity<>(suggestionResourceAssembler.toResource(suggestion), HttpStatus.CREATED);
    }

    private static class SuggestionRequest {

        @NotEmpty
        @JsonProperty
        String text;

        @NotNull
        @Min(0)
        @Max(1280 * 2)
        @JsonProperty
        Double pointX;

        @NotNull
        @Min(0)
        @Max(1280 * 2)
        @JsonProperty
        Double pointY;

        SuggestionRequest() {
        }

        public SuggestionRequest(String text, Double pointX, Double pointY) {
            this.text = text;
            this.pointX = pointX;
            this.pointY = pointY;
        }

    }

    @RequestMapping(value = "/{suggestionId}", method = RequestMethod.DELETE)
    public ResponseEntity delete(
            @PathVariable("workId") WorkId workId,
            @PathVariable("suggestionId") SuggestionId suggestionId) throws WorkNotFoundException, SuggestionNotFoundException {

        Work work = Work.findOne(workId);

        try {
            work.removeMySuggestion(CurrentUser.asSuggestionOwner(), suggestionId);
        } catch (IllegalSuggestionOwnerOperationException e) {
            e.printStackTrace();
        }

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/{suggestionId}", method = RequestMethod.PUT)
    public ResponseEntity<SuggestionResource> update(
            @PathVariable("workId") WorkId workId,
            @PathVariable("suggestionId") SuggestionId suggestionId,
            @RequestParam(required = false) String description,
            @RequestParam(required = false) Boolean read
    ) throws WorkNotFoundException, SuggestionNotFoundException, IllegalSuggestionOwnerOperationException, IllegalWorkOwnerForMarkReadOperationException {

        Work work = Work.findOne(workId);

        Suggestion suggestion = Suggestion.findBy(suggestionId);

        if (description != null) {
            suggestion.updateDescription(CurrentUser.asSuggestionOwnerId(), description);
        }

        if (read != null) {
            if (read) {
                suggestion.markRead(CurrentUser.asWorkOwnerId());
            }
        }

        return new ResponseEntity<>(suggestionResourceAssembler.toResource(suggestion), HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<PagedResources<SuggestionResource>> getAll(
            @PathVariable("workId") WorkId workId,
            @PageableDefault Pageable pageable,
            PagedResourcesAssembler<Suggestion> pagedResourcesAssembler) throws WorkNotFoundException {

        Work work = Work.findOne(workId);

        Page<Suggestion> page = work.findSuggestions(pageable);

        PagedResources<SuggestionResource> pagedResources = pagedResourcesAssembler.toResource(page, suggestionResourceAssembler);

        return new ResponseEntity<>(pagedResources, HttpStatus.OK);
    }

    @RequestMapping(value = "/{suggestionId}", method = RequestMethod.GET)
    public ResponseEntity<SuggestionResource> get(
            @PathVariable("workId") WorkId workId,
            @PathVariable("suggestionId") SuggestionId suggestionId) throws WorkNotFoundException, SuggestionNotFoundException {

        Work work = Work.findOne(workId);

        Suggestion suggestion = Suggestion.findBy(suggestionId);

        return new ResponseEntity<>(suggestionResourceAssembler.toResource(suggestion), HttpStatus.OK);
    }

    public static Object getExampleBody(Class target) {
        if (target == SuggestionRequest.class) {
            return new SuggestionRequest("I'd rather use circles here", 20.33, 20.66);
        }
        return null;
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorsResponse handle(WorkNotFoundException e) {
        log.trace(e.getMessage(), e);
        return new ErrorsResponse(new ModelError("work_not_found", e.getMessage()));
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorsResponse handle(IllegalSuggestionOwnerOperationException e) {
        log.trace(e.getMessage(), e);
        return new ErrorsResponse(new ModelError("illegal_suggestion_owner_operation", e.getMessage()));
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorsResponse handle(IllegalWorkOwnerForMarkReadOperationException e) {
        log.trace(e.getMessage(), e);
        return new ErrorsResponse(new ModelError("illegal_work_owner_for_mark_suggestion_read_operation", e.getMessage()));
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorsResponse handle(SuggestionNotFoundException e) {
        log.trace(e.getMessage(), e);
        return new ErrorsResponse(new ModelError("suggestion_not_found", e.getMessage()));
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorsResponse handle(MongoException e) {
        log.trace(e.getMessage(), e);
        return new ErrorsResponse(new Error("data_storage_error", e.getMessage(), Collections.emptyList()));
    }
}
