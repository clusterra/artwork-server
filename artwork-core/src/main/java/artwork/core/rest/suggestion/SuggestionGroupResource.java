/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.rest.suggestion;

import artwork.core.rest.owner.OwnerJson;
import org.springframework.hateoas.ResourceSupport;

import java.util.Date;
import java.util.List;

/**
 * Created by Denis Kuchugurov
 * on 18.02.2016.
 */
class SuggestionGroupResource extends ResourceSupport {


    public final OwnerJson author;

    public final int suggestionsCount;

    public final boolean hasNotRead;

    public final Date lastUpdate;

    public final List<SuggestionResource> suggestions;


    SuggestionGroupResource(OwnerJson author, List<SuggestionResource> suggestions, boolean hasNotRead, Date lastUpdate) {
        this.author = author;
        this.hasNotRead = hasNotRead;
        this.suggestions = suggestions;
        this.suggestionsCount = suggestions.size();
        this.lastUpdate = lastUpdate;
    }
}
