/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.rest.suggestion;

import artwork.core.domain.suggestion.SuggestionGroup;
import artwork.core.domain.suggestion.SuggestionOwner;
import artwork.core.rest.owner.OwnerJson;
import artwork.core.rest.work.WorkController;
import org.springframework.data.domain.PageRequest;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by Denis Kuchugurov
 * on 18.02.2016.
 */
@Component
class SuggestionGroupResourceAssembler extends ResourceAssemblerSupport<SuggestionGroup, SuggestionGroupResource> {

    private final SuggestionResourceAssembler suggestionResourceAssembler;

    public SuggestionGroupResourceAssembler(SuggestionResourceAssembler suggestionResourceAssembler) {
        super(SuggestionController.class, SuggestionGroupResource.class);
        this.suggestionResourceAssembler = suggestionResourceAssembler;
    }

    @Override
    public SuggestionGroupResource toResource(SuggestionGroup entity) {

        try {

            OwnerJson ownerJson = toOwnerJson(entity.getSuggestionOwner());
            List<SuggestionResource> suggestions = entity.getSuggestions().stream().map(suggestionResourceAssembler::toResource).collect(Collectors.toList());
            SuggestionGroupResource resource = new SuggestionGroupResource(ownerJson, suggestions, entity.hasNotRead(), entity.getLastCreatedDate());

            resource.add(linkTo(methodOn(SuggestionGroupController.class).getAll(entity.getWorkId(), new PageRequest(0, 10), null)).withSelfRel());
            resource.add(linkTo(methodOn(WorkController.class).get(entity.getWorkId())).withRel("work"));
            resource.add(linkTo(methodOn(SuggestionController.class).getAll(entity.getWorkId(), new PageRequest(0, 10), null)).withRel("suggestions"));
            return resource;
        } catch (Exception e) {
            throw new IllegalStateException("should never happen...", e);
        }
    }

    private OwnerJson toOwnerJson(SuggestionOwner owner) {
        return new OwnerJson(owner.getUuid(), owner.getDisplayName(), owner.getImageUrl(), owner.getProfileUrl());
    }
}
