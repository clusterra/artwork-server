/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.rest.work;

import artwork.commentary.rest.CommentController;
import artwork.core.domain.images.ImageMeta;
import artwork.core.domain.work.WorkModel;
import artwork.core.domain.work.WorkOwner;
import artwork.core.rest.owner.OwnerJson;
import artwork.core.rest.suggestion.SuggestionGroupController;
import artwork.likeable.domain.like.LikerId;
import org.springframework.data.domain.PageRequest;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by Denis Kuchugurov
 * on 18.02.2016.
 */
@Component
class WorkResourceAssembler extends ResourceAssemblerSupport<WorkModel, WorkResource> {

    public WorkResourceAssembler() {
        super(WorkController.class, WorkResource.class);
    }

    @Override
    public WorkResource toResource(WorkModel entity) {

        try {

            LikerId likerId = CurrentUser.asLikerId();
            ImageMeta imageMeta = entity.getImageMeta();
            WorkResource resource = new WorkResource(
                    entity.getWorkId().getUuid(),
                    toOwnerJson(entity.getWorkOwner()),
                new ImageMetaJson(imageMeta.getUuid(), imageMeta.isGif()),
                    entity.getDescription(),
                    entity.getViewsCount(),
                    entity.getSuggestionsCount(),
                    entity.getLikesCount(),
                    entity.alreadyLiked(likerId),
                    entity.getCommentsCount(),
                    entity.getTags(),
                    entity.getCreatedDate(),
                    entity.isMine(CurrentUser.asWorkOwnerId()));

            resource.add(linkTo(methodOn(WorkController.class).get(entity.getWorkId())).withSelfRel());
            resource.add(linkTo(methodOn(WorkController.class).getAll(entity.getWorkOwner().getWorkOwnerId(), 0, 10, null, null)).withRel("works-by-owner"));

            if (!artwork.web.security.tracking.CurrentUser.isAnonymous() && !entity.alreadyLiked(likerId)) {
                resource.add(linkTo(methodOn(WorkController.class).update(entity.getWorkId(), true, null, null)).withRel("like"));
            }
            if (!artwork.web.security.tracking.CurrentUser.isAnonymous() && entity.alreadyLiked(likerId)) {
                resource.add(linkTo(methodOn(WorkController.class).update(entity.getWorkId(), false, null, null)).withRel("unlike"));
            }
            resource.add(linkTo(methodOn(CommentController.class).getAll(entity.getWorkId().toCommentableId(), new PageRequest(0, 20), null)).withRel("comments"));
            resource.add(linkTo(methodOn(SuggestionGroupController.class).getAll(entity.getWorkId(), new PageRequest(0, 20), null)).withRel("suggestion-groups"));
            return resource;

        } catch (Exception e) {
            throw new IllegalStateException("should never happen...", e);
        }

    }

    private OwnerJson toOwnerJson(WorkOwner owner) {
        return new OwnerJson(owner.getUuid(), owner.getDisplayName(), owner.getImageUrl(), owner.getProfileUrl());
    }
}
