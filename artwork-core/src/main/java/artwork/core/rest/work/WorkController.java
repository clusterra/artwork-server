/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.rest.work;

import artwork.common.rest.validation.error.Error;
import artwork.common.rest.validation.error.ErrorsResponse;
import artwork.common.rest.validation.error.MalformedJsonError;
import artwork.common.rest.validation.error.ModelError;
import artwork.common.rest.validation.error.ValidationError;
import artwork.core.domain.images.ImageMeta;
import artwork.core.domain.images.ImageStorage;
import artwork.core.domain.images.ImageTooSmallException;
import artwork.core.domain.images.ImageWeightTooMuchException;
import artwork.core.domain.work.IllegalWorkOwnerOperationException;
import artwork.core.domain.work.Work;
import artwork.core.domain.work.WorkId;
import artwork.core.domain.work.WorkModel;
import artwork.core.domain.work.WorkNotFoundException;
import artwork.core.domain.work.WorkOwner;
import artwork.core.domain.work.WorkOwnerId;
import artwork.likeable.domain.dislike.Disliker;
import artwork.likeable.domain.like.Liker;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mongodb.MongoException;
import org.apache.tomcat.util.http.fileupload.FileUploadBase;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartException;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Denis Kuchugurov
 * on 15.02.2016.
 */
@RestController
@RequestMapping(value = "/api/works", produces = MediaTypes.HAL_JSON_VALUE)
public class WorkController {

    private static final Logger log = LoggerFactory.getLogger(WorkController.class);


    private final WorkResourceAssembler workResourceAssembler;

    private final ImageStorage imageStorage;

    public WorkController(WorkResourceAssembler workResourceAssembler, ImageStorage imageStorage) {
        this.workResourceAssembler = workResourceAssembler;
        this.imageStorage = imageStorage;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<WorkResource> create(@Valid @RequestBody CreateRequest createRequest) throws IOException, ImageTooSmallException, ImageWeightTooMuchException {
        WorkOwner workOwner = CurrentUser.asWorkOwner();
        ImageMeta imageMeta = imageStorage.save(createRequest.imageId);
        WorkId workId = WorkId.generateId();
        try {
            WorkModel work = new WorkModel(workId, workOwner, imageMeta, createRequest.description);
            return new ResponseEntity<>(workResourceAssembler.toResource(work), HttpStatus.CREATED);
        } catch(Exception ex) {
            imageStorage.delete(imageMeta);
            throw ex;
        }
    }

    static class CreateRequest {
        @NotEmpty
        private final String imageId;

        @NotEmpty
        @Size(max = 1000)
        @JsonProperty("description")
        private final String description;

        @JsonCreator
        CreateRequest(
                @JsonProperty("imageId") String imageId,
                @JsonProperty("description") String description) {
            this.imageId = imageId;
            this.description = description;
        }

        public String getDescription() {
            return description;
        }
    }

    @RequestMapping(value = "/{workId}", method = RequestMethod.PUT)
    public ResponseEntity<WorkResource> update(
            @PathVariable("workId") WorkId workId,
            @RequestParam(required = false) Boolean like,
            @RequestParam(required = false) Boolean dislike,
            @Valid @RequestBody(required = false) DescriptionRequest descriptionRequest) throws WorkNotFoundException, IllegalWorkOwnerOperationException {
        if (like == null && dislike == null && descriptionRequest == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        WorkModel work = Work.findOne(workId);

        boolean modified = false;
        if (descriptionRequest != null) {
            modified = work.updateDescription(descriptionRequest.getDescription(), CurrentUser.asWorkOwnerId());
        }

        if (like != null) {
            Liker liker = CurrentUser.asLiker();
            if (like) {
                modified |= work.like(liker);
            } else {
                modified |= work.unlike(liker);
            }
        }

        if (dislike != null) {
            Disliker disliker = CurrentUser.asDisliker();
            if (dislike) {
                modified |= work.dislike(disliker);
            } else {
                modified |= work.undislike(disliker);
            }
        }

        if (modified) {
            return new ResponseEntity<>(workResourceAssembler.toResource(work), HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<>(workResourceAssembler.toResource(work), HttpStatus.OK);
        }
    }

    static class DescriptionRequest {

        @NotEmpty
        @Size(max = 1000)
        @JsonProperty("description")
        private final String description;

        @JsonCreator
        DescriptionRequest(
                @JsonProperty("description") String description) {
            this.description = description;
        }

        public String getDescription() {
            return description;
        }
    }

    @RequestMapping(value = "/{workId}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable("workId") WorkId workId) throws IllegalWorkOwnerOperationException, WorkNotFoundException {
        Work work = Work.findOne(workId);
        work.deleteMine(CurrentUser.asWorkOwnerId());
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/{workId}", method = RequestMethod.GET)
    public ResponseEntity<WorkResource> get(@PathVariable("workId") WorkId workId) throws WorkNotFoundException {
        WorkModel work = Work.viewOne(workId, CurrentUser.asViewerId());
        return new ResponseEntity<>(workResourceAssembler.toResource(work), HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<PagedResources<WorkResource>> getAll(
            @RequestParam(required = false) WorkOwnerId ownerId,
            @RequestParam(required = false, defaultValue = "0") Integer page,
            @RequestParam(required = false, defaultValue = "10") Integer size,
            @RequestParam(required = false, defaultValue = "date") String sort,
            PagedResourcesAssembler<WorkModel> assembler
    ) throws IllegalSortParameterException {

        Sort srt;
        try {
            srt = Sort.valueOf(sort);
        } catch (IllegalArgumentException e) {

            List<String> expected = Arrays.stream(Sort.values()).map(Enum::toString).collect(Collectors.toList());
            throw new IllegalSortParameterException(sort, expected, e);
        }

        Page<WorkModel> works = ownerId == null ? srt.query(page, size) : srt.query(ownerId, page, size);
        return new ResponseEntity<>(assembler.toResource(works, workResourceAssembler), HttpStatus.OK);
    }

    private enum Sort {
        date {
            Page<WorkModel> query(int page, int size) {
                return Work.findAll(page, size);
            }

            Page<WorkModel> query(WorkOwnerId ownerId, int page, int size) {
                return Work.findAll(ownerId, page, size);
            }
        },
        rating {
            Page<WorkModel> query(int page, int size) {
                return Work.findMostRatedThisMonth(page, size);
            }

            Page<WorkModel> query(WorkOwnerId ownerId, int page, int size) {
                return Work.findMostRatedThisMonth(ownerId, page, size);
            }
        },
        popularity {
            Page<WorkModel> query(int page, int size) {
                return Work.findMostPopularThisMonth(page, size);
            }

            Page<WorkModel> query(WorkOwnerId ownerId, int page, int size) {
                return Work.findMostPopularThisMonth(ownerId, page, size);
            }
        };

        abstract Page<WorkModel> query(int page, int size);

        abstract Page<WorkModel> query(WorkOwnerId ownerId, int page, int size);
    }

    @SuppressWarnings("unused")
    public static Object getExampleBody(Class target) {
        if (target == DescriptionRequest.class) {
            return new DescriptionRequest("It's my first work");
        }
        return null;
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorsResponse handle(ImageTooSmallException e) {
        log.trace(e.getMessage(), e);
        return new ErrorsResponse(
                new ValidationError(
                        "file", "image_too_small", e.getMessage(),
                        Arrays.asList(e.getMinWidth(), e.getMinHeight(), e.getWidth(), e.getHeight())
                )
        );
    }


    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorsResponse handle(ImageWeightTooMuchException e) {
        log.trace(e.getMessage(), e);
        return new ErrorsResponse(
                new ValidationError(
                        "file", "image_weight_too_much", e.getMessage(),
                        Arrays.asList(e.getMaxWeight(), e.getImageWeight())
                )
        );
    }

    /*
    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorsResponse handle(StorageException e) {
        log.trace(e.getMessage(), e);
        return new ErrorsResponse(new ModelError("storage_error", e.getMessage()));
    }*/


    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorsResponse handle(WorkNotFoundException e) {
        log.trace(e.getMessage(), e);
        return new ErrorsResponse(new ModelError("work_not_found", e.getMessage()));
    }


    @ExceptionHandler
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ResponseBody
    public ErrorsResponse handle(IllegalWorkOwnerOperationException e) {
        log.trace(e.getMessage(), e);
        return new ErrorsResponse(new ModelError("illegal_work_owner_operation", e.getMessage()));
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorsResponse handle(MongoException e) {
        log.trace(e.getMessage(), e);
        return new ErrorsResponse(new Error("data_storage_error", e.getMessage(), Collections.emptyList()));
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorsResponse handle(IllegalSortParameterException e) {
        log.trace(e.getMessage(), e);
        return new ErrorsResponse(new Error("illegal_sort_parameter", e.getMessage(), Collections.emptyList()));
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorsResponse handle(FileUploadBase.InvalidContentTypeException e) {
        log.trace(e.getMessage(), e);
        return new ErrorsResponse(new MalformedJsonError(e.getMessage()));
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorsResponse handle(MultipartException e) {
        log.debug(e.getMessage(), e);

        if (e.getCause() != null && e.getCause().getCause() instanceof FileUploadBase.FileSizeLimitExceededException) {
            FileUploadBase.FileSizeLimitExceededException cause = (FileUploadBase.FileSizeLimitExceededException) e.getCause().getCause();
            long actualSize = cause.getActualSize();
            String fieldName = cause.getFieldName();
            return new ErrorsResponse(new MalformedJsonError(e.getCause().getCause().getMessage()));
        } else if (e.getCause() != null && e.getCause().getCause() instanceof FileUploadBase.InvalidContentTypeException) {
            return new ErrorsResponse(new MalformedJsonError(e.getCause().getCause().getMessage()));
        } else {
            throw e;
        }
    }
}
