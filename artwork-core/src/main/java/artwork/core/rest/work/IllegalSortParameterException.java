/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.rest.work;

import java.util.List;

/**
 * Created by kepkap
 * on 05/05/2017.
 */
public class IllegalSortParameterException extends Exception {

    private final String actual;

    private final List<String> expected;

    IllegalSortParameterException(String actual, List<String> expected, Throwable cause) {
        super(String.format("illegal sort parameter '%s', supported one of %s", actual, expected), cause);
        this.actual = actual;
        this.expected = expected;
    }

    public String getActual() {
        return actual;
    }

    public List<String> getExpected() {
        return expected;
    }
}
