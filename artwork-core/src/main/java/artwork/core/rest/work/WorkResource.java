/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.rest.work;

import artwork.core.rest.owner.OwnerJson;
import org.springframework.hateoas.ResourceSupport;

import java.util.Date;
import java.util.Set;

/**
 * Created by Denis Kuchugurov
 * on 18.02.2016.
 */
class WorkResource extends ResourceSupport {

    public final String workId;

    public final OwnerJson owner;

    public final ImageMetaJson imageMeta;

    public final String description;

    public final int viewsCount;

    public final int suggestionsCount;

    public final int likesCount;

    public final boolean alreadyLiked;

    public final int commentsCount;

    public final Set<String> tags;

    public final Date createdDate;

    public final boolean isMine;


    public WorkResource(String workId, OwnerJson owner, ImageMetaJson imageMeta, String description, int viewsCount, int suggestionsCount, int likesCount, boolean alreadyLiked, int commentsCount, Set<String> tags, Date createdDate, boolean isMine) {
        this.workId = workId;
        this.owner = owner;
        this.imageMeta = imageMeta;
        this.description = description;
        this.viewsCount = viewsCount;
        this.suggestionsCount = suggestionsCount;
        this.likesCount = likesCount;
        this.alreadyLiked = alreadyLiked;
        this.commentsCount = commentsCount;
        this.tags = tags;
        this.createdDate = createdDate;
        this.isMine = isMine;
    }

}