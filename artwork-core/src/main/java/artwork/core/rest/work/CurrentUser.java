/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.core.rest.work;

import artwork.core.domain.work.ViewerId;
import artwork.core.domain.work.WorkOwner;
import artwork.core.domain.work.WorkOwnerId;
import artwork.likeable.domain.dislike.Disliker;
import artwork.likeable.domain.like.Liker;
import artwork.likeable.domain.like.LikerId;

/**
 * Created on 26/01/2017.
 *
 * @author kepkap
 */
class CurrentUser {

    static WorkOwner asWorkOwner() {
        artwork.web.security.tracking.CurrentUser currentUser = artwork.web.security.tracking.CurrentUser.requireNotAnonymous();
        return new WorkOwner(currentUser.getUuid(), currentUser.getDisplayName(), currentUser.getImageUrl(), currentUser.getProfileUrl());
    }

    static Liker asLiker() {
        artwork.web.security.tracking.CurrentUser currentUser = artwork.web.security.tracking.CurrentUser.requireNotAnonymous();
        return new Liker(currentUser.getUuid(), currentUser.getDisplayName(), currentUser.getImageUrl(), currentUser.getProfileUrl());
    }

    static Disliker asDisliker() {
        artwork.web.security.tracking.CurrentUser currentUser = artwork.web.security.tracking.CurrentUser.requireNotAnonymous();
        return new Disliker(currentUser.getUuid(), currentUser.getDisplayName(), currentUser.getImageUrl(), currentUser.getProfileUrl());
    }

    static WorkOwnerId asWorkOwnerId() {
        artwork.web.security.tracking.CurrentUser any = artwork.web.security.tracking.CurrentUser.requireAny();
        return new WorkOwnerId(any.getUuid());
    }

    static ViewerId asViewerId() {
        artwork.web.security.tracking.CurrentUser any = artwork.web.security.tracking.CurrentUser.requireAny();
        return new ViewerId(any.getUuid());
    }

    static LikerId asLikerId() {
        artwork.web.security.tracking.CurrentUser any = artwork.web.security.tracking.CurrentUser.requireAny();
        return new LikerId(any.getUuid());
    }
}
