/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.notification.domain;

import artwork.commentary.domain.comment.CommentAuthorId;
import artwork.commentary.domain.commentable.CommentableOwnerId;
import artwork.core.domain.suggestion.SuggestionOwnerId;
import artwork.core.domain.work.WorkOwnerId;
import org.hibernate.validator.constraints.NotEmpty;

import java.io.Serializable;

/**
 * A class representing acting user identity.
 * To be used for tracking authenticated individuals.
 * <p>
 * <p>
 * <p>
 * Created by Denis Kuchugurov
 * on 25.02.2016.
 */
public class RecipientId implements Serializable {

    @NotEmpty
    private final String uuid;

    private RecipientId(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

    @Override
    public String toString() {
        return uuid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RecipientId that = (RecipientId) o;

        return uuid.equals(that.uuid);
    }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }


    public static RecipientId from(SuggestionOwnerId suggestionOwnerId) {
        return new RecipientId(suggestionOwnerId.getUuid());
    }

    public static RecipientId from(WorkOwnerId workOwnerId) {
        return new RecipientId(workOwnerId.getUuid());
    }

    public static RecipientId from(String localUserId) {
        return new RecipientId(localUserId);
    }

    public static RecipientId from(CommentAuthorId commentAuthorId) {
        return new RecipientId(commentAuthorId.getUuid());
    }

    public static RecipientId from(CommentableOwnerId commentableOwnerId) {
        return new RecipientId(commentableOwnerId.getUuid());
    }
}
