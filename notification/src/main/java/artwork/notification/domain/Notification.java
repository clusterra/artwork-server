/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.notification.domain;

import artwork.commentary.domain.comment.CommentId;
import artwork.core.domain.suggestion.SuggestionId;
import artwork.core.domain.work.WorkId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Date;

/**
 * Created on 19/03/2017.
 *
 * @author kepkap
 */
public interface Notification {

    boolean isRead();

    void markRead(RecipientId recipientId) throws IllegalNotificationOwnerOperationException;

    NotificationId getId();

    RecipientId getRecipientId();

    Date getCreatedDate();

    Initiator getInitiator();

    String getSubject();

    String getText();

    Type getType();

    WorkId getWorkId();

    SuggestionId getSuggestionId();

    CommentId getCommentId();

    void delete(RecipientId recipientId) throws IllegalNotificationOwnerOperationException;

    static NotificationModel findBy(RecipientId recipientId, NotificationId notificationId) throws NotificationNotFoundException, IllegalNotificationOwnerOperationException {
        NotificationModel notificationModel = NotificationDependencies.notificationModelRepository.findOne(notificationId);
        if (notificationModel == null) {
            throw new NotificationNotFoundException(notificationId);
        }
        notificationModel.assertOwner(recipientId);
        return notificationModel;
    }

    static Page<Notification> findBy(RecipientId recipientId, int page, int size) {
        return NotificationDependencies.notificationModelRepository.findByRecipientIdOrderByCreatedDateDesc(recipientId, new PageRequest(page, size));
    }

    static Page<Notification> findBy(RecipientId recipientId, Boolean read, int page, int size) {
        return NotificationDependencies.notificationModelRepository.findByRecipientIdAndReadOrderByCreatedDateDesc(
                recipientId, read, new PageRequest(page, size));
    }
}
