/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.notification.domain;

import artwork.commentary.domain.comment.CommentAuthor;
import artwork.core.domain.suggestion.SuggestionOwner;
import artwork.core.domain.work.WorkOwner;
import artwork.likeable.domain.like.Liker;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.URL;
import org.springframework.data.annotation.PersistenceConstructor;

import javax.validation.constraints.NotNull;

/**
 * Created by Denis Kuchugurov
 * on 10/03/16 23:20.
 */
public class Initiator {

    @NotNull
    @NotEmpty
    private final String uuid;

    @NotEmpty
    private final String displayName;

    @NotNull
    @URL
    private final String imageUrl;

    @NotNull
    @URL
    private final String profileUrl;

    @PersistenceConstructor
    public Initiator(String uuid, String displayName, String imageUrl, String profileUrl) {
        this.uuid = uuid;
        this.displayName = displayName;
        this.imageUrl = imageUrl;
        this.profileUrl = profileUrl;
    }

    public String getUuid() {
        return uuid;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Initiator owner = (Initiator) o;

        return uuid.equals(owner.uuid);
    }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }

    public static Initiator from(WorkOwner workOwner) {
        return new Initiator(workOwner.getUuid(), workOwner.getDisplayName(), workOwner.getImageUrl(), workOwner.getProfileUrl());
    }

    public static Initiator from(SuggestionOwner suggestionOwner) {
        return new Initiator(suggestionOwner.getUuid(), suggestionOwner.getDisplayName(), suggestionOwner.getImageUrl(), suggestionOwner.getProfileUrl());
    }

    public static Initiator from(CommentAuthor commentAuthor) {
        return new Initiator(commentAuthor.getUuid(), commentAuthor.getDisplayName(), commentAuthor.getImageUrl(), commentAuthor.getProfileUrl());
    }

    public static Initiator from(Liker liker) {
        return new Initiator(liker.getUuid(), liker.getDisplayName(), liker.getImageUrl(), liker.getProfileUrl());
    }
}
