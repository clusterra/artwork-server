/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.notification.domain;

import artwork.commentary.domain.comment.CommentId;
import artwork.core.domain.suggestion.SuggestionId;
import artwork.core.domain.work.WorkId;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Objects;

import static artwork.notification.domain.NotificationDependencies.eventPublisher;
import static artwork.notification.domain.NotificationDependencies.notificationModelRepository;


/**
 * Created by Denis Kuchugurov
 * on 31.07.2016.
 */
@Document(collection = "notification")
@CompoundIndexes({
        @CompoundIndex(def = "{ 'recipientId.uuid' : 1, 'createdDate' : -1 }"),
})
public class NotificationModel implements Notification {

    @Id
    private NotificationId id;

    @NotNull
    @Valid
    private RecipientId recipientId;

    @NotNull
    @Valid
    private Initiator initiator;

    @NotNull
    private Date createdDate;

    @NotEmpty
    private String subject;

    @NotEmpty
    private String text;

    @NotNull
    private Type type;

    @Valid
    @NotNull
    private WorkId workId;

    @Valid
    private SuggestionId suggestionId;

    @Valid
    private CommentId commentId;

    private boolean read;

    {
        Objects.requireNonNull(notificationModelRepository, "check if Spring context is initialized properly");
        Objects.requireNonNull(eventPublisher, "check if Spring context is initialized properly");
    }

    @PersistenceConstructor
    NotificationModel() {
    }

    public NotificationModel(RecipientId recipientId, Initiator initiator, String subject, String text, Type type, WorkId workId, SuggestionId suggestionId, CommentId commentId) {
        this.id = NotificationId.generateId();
        this.recipientId = recipientId;
        this.initiator = initiator;
        this.createdDate = new Date();
        this.subject = subject;
        this.text = text;
        this.type = type;
        this.workId = workId;
        this.suggestionId = suggestionId;
        this.commentId = commentId;
        this.read = false;
        notificationModelRepository.save(this);
        eventPublisher.publishEvent(new NotificationCreatedEvent(this));
    }

    @Override
    public boolean isRead() {
        return read;
    }

    @Override
    public void markRead(RecipientId recipientId) throws IllegalNotificationOwnerOperationException {
        assertOwner(recipientId);
        this.read = true;
        notificationModelRepository.save(this);
    }

    @Override
    public NotificationId getId() {
        return id;
    }

    @Override
    public RecipientId getRecipientId() {
        return recipientId;
    }

    void assertOwner(RecipientId recipientId) throws IllegalNotificationOwnerOperationException {
        if (!this.recipientId.equals(recipientId)) {
            throw new IllegalNotificationOwnerOperationException(getId());
        }
    }

    @Override
    public Initiator getInitiator() {
        return initiator;
    }

    @Override
    public String getSubject() {
        return subject;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public Date getCreatedDate() {
        return createdDate;
    }

    @Override
    public Type getType() {
        return type;
    }

    @Override
    public WorkId getWorkId() {
        return workId;
    }

    @Override
    public SuggestionId getSuggestionId() {
        return suggestionId;
    }

    @Override
    public CommentId getCommentId() {
        return commentId;
    }

    @Override
    public void delete(RecipientId recipientId) throws IllegalNotificationOwnerOperationException {
        assertOwner(recipientId);
        notificationModelRepository.delete(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NotificationModel that = (NotificationModel) o;

        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return "NotificationModel{" +
                "id=" + id +
                ", recipientId=" + recipientId +
                ", initiator=" + initiator +
                ", createdDate=" + createdDate +
                ", subject=[HIDDEN]" +
                ", message=[HIDDEN]" +
                ", type=" + type +
                ", workId=" + workId +
                ", suggestionId=" + suggestionId +
                ", commentId=" + commentId +
                '}';
    }
}
