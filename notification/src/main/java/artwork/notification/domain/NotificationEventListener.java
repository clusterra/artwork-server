/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.notification.domain;

import artwork.commentary.domain.comment.Comment;
import artwork.commentary.domain.comment.CommentCreatedEvent;
import artwork.commentary.domain.comment.CommentLikedEvent;
import artwork.core.domain.suggestion.Suggestion;
import artwork.core.domain.suggestion.SuggestionCreatedEvent;
import artwork.core.domain.work.Work;
import artwork.core.domain.work.WorkId;
import artwork.core.domain.work.WorkLikedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * Created by Denis Kuchugurov
 * on 06.08.2016.
 */
@Component
class NotificationEventListener {


    @EventListener(SuggestionCreatedEvent.class)
    public void handle(SuggestionCreatedEvent event) {
        Suggestion suggestion = event.getSuggestion();
        Initiator initiator = Initiator.from(suggestion.getSuggestionOwner());
        String subject = "suggestion added";
        String text = suggestion.getDescription();
        new NotificationModel(RecipientId.from(suggestion.getWorkOwner().getUuid()), initiator, subject, text, Type.SUGGESTION_CREATED, suggestion.getWorkId(), suggestion.getSuggestionId(), null);
    }

    @EventListener(CommentCreatedEvent.class)
    public void handle(CommentCreatedEvent event) {
        Comment comment = event.getComment();

        if (comment.hasRelateTo()) {
            //this means comment reflects suggestion, which has already caused notification
            //from SuggestionCreatedEvent
            return;
        }

        Initiator initiator = Initiator.from(comment.getCommentAuthor());
        String subject = "comment added";
        String message = comment.getText();

        new NotificationModel(RecipientId.from(event.getCommentable().getCommentableOwnerId()), initiator, subject, message, Type.WORK_COMMENTED, WorkId.from(comment.getCommentableId()), null, comment.getCommentId());
    }

    @EventListener(WorkLikedEvent.class)
    public void handle(WorkLikedEvent event) {


        Initiator initiator = Initiator.from(event.getLiker());
        String subject = "work liked";
        String message = "work liked";

        Work work = event.getWork();
        new NotificationModel(RecipientId.from(work.getWorkOwner().getWorkOwnerId()), initiator, subject, message, Type.WORK_LIKED, work.getWorkId(), null, null);
    }

    @EventListener(CommentLikedEvent.class)
    public void handle(CommentLikedEvent event) {


        Initiator initiator = Initiator.from(event.getLiker());
        String subject = "comment liked";
        String message = "comment liked";

        Comment comment = event.getComment();
        new NotificationModel(RecipientId.from(comment.getCommentAuthor().getCommentAuthorId()), initiator, subject, message, Type.COMMENT_LIKED, WorkId.from(comment.getCommentableId()), null, comment.getCommentId());
    }

}
