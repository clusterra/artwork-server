/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.notification.rest;

import artwork.common.rest.validation.error.ErrorsResponse;
import artwork.common.rest.validation.error.ModelError;
import artwork.notification.domain.IllegalNotificationOwnerOperationException;
import artwork.notification.domain.Notification;
import artwork.notification.domain.NotificationId;
import artwork.notification.domain.NotificationModel;
import artwork.notification.domain.NotificationNotFoundException;
import artwork.notification.domain.RecipientId;
import artwork.notification.rest.resource.NotificationResource;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by Denis Kuchugurov
 * on 05.07.2016.
 */
@Controller
@RequestMapping(value = "/api/notifications", produces = MediaTypes.HAL_JSON_VALUE)
class NotificationController {

    private static final Logger log = LoggerFactory.getLogger(NotificationController.class);

    private final NotificationResourceAssembler notificationResourceAssembler;

    public NotificationController(NotificationResourceAssembler notificationResourceAssembler) {
        this.notificationResourceAssembler = notificationResourceAssembler;
    }


    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<PagedResources<NotificationResource>> get(@RequestParam(required = false, defaultValue = "0") Integer page,
                                                                    @RequestParam(required = false, defaultValue = "10") Integer size,
                                                                    @RequestParam(required = false) Boolean read,
                                                                    PagedResourcesAssembler<Notification> assembler) {
        Page<Notification> notifications;
        if (read == null)
            notifications = Notification.findBy(CurrentNotificationUser.asRecipientId(), page, size);
        else
            notifications = Notification.findBy(CurrentNotificationUser.asRecipientId(), read, page, size);
        return new ResponseEntity<>(assembler.toResource(notifications, notificationResourceAssembler), HttpStatus.OK);
    }

    @RequestMapping(value = "/{notificationId}", method = RequestMethod.GET)
    public ResponseEntity<NotificationResource> get(@PathVariable("notificationId") NotificationId notificationId) throws NotificationNotFoundException, IllegalNotificationOwnerOperationException {
        NotificationModel notification = Notification.findBy(CurrentNotificationUser.asRecipientId(), notificationId);
        return new ResponseEntity<>(notificationResourceAssembler.toResource(notification), HttpStatus.OK);
    }

    @RequestMapping(value = "/{notificationId}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable("notificationId") NotificationId notificationId) throws NotificationNotFoundException, IllegalNotificationOwnerOperationException {
        Notification notification = Notification.findBy(CurrentNotificationUser.asRecipientId(), notificationId);
        notification.delete(CurrentNotificationUser.asRecipientId());
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/{notificationId}", method = RequestMethod.PUT)
    public ResponseEntity<NotificationResource> update(@PathVariable("notificationId") NotificationId notificationId,
                                                       @RequestParam(required = false) Boolean read) throws IllegalNotificationOwnerOperationException, NotificationNotFoundException {
        RecipientId recipientId = CurrentNotificationUser.asRecipientId();
        NotificationModel notification = Notification.findBy(recipientId, notificationId);

        if (read != null) {
            if (read) {
                notification.markRead(recipientId);
                return new ResponseEntity<>(notificationResourceAssembler.toResource(notification), HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(notificationResourceAssembler.toResource(notification), HttpStatus.NOT_MODIFIED);
    }

    @RequestMapping(value = "/read", method = RequestMethod.POST)
    public ResponseEntity<?> massRead(@Valid @RequestBody MassReadRequest readRequest) throws IllegalNotificationOwnerOperationException, NotificationNotFoundException {
        RecipientId recipientId = CurrentNotificationUser.asRecipientId();
        NotificationResource[] notifications = new NotificationResource[readRequest.ids.size()];
        int i = 0;
        for(NotificationId id : readRequest.ids) {
            Notification n = Notification.findBy(recipientId, id);
            n.markRead(recipientId);
            notifications[i] = notificationResourceAssembler.toResource(n);
            i++;
        }
        return new ResponseEntity<>(notifications, HttpStatus.ACCEPTED);
    }

    static class MassReadRequest {
        @JsonProperty("ids")
        @NotNull
        @NotEmpty
        final List<NotificationId> ids;

        @JsonCreator
        public MassReadRequest(@JsonProperty("ids") List<NotificationId> ids) {
            this.ids = ids;
        }
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorsResponse handle(IllegalNotificationOwnerOperationException e) {
        log.trace(e.getMessage(), e);
        return new ErrorsResponse(new ModelError("illegal_notification_owner_operation", e.getMessage()));
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorsResponse handle(NotificationNotFoundException e) {
        log.trace(e.getMessage(), e);
        return new ErrorsResponse(new ModelError("notification_not_found", e.getMessage()));
    }

}
