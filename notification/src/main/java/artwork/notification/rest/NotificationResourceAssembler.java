/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.notification.rest;

import artwork.commentary.rest.CommentController;
import artwork.core.rest.suggestion.SuggestionController;
import artwork.notification.domain.Initiator;
import artwork.notification.domain.Notification;
import artwork.notification.domain.Type;
import artwork.notification.rest.resource.InitiatorResource;
import artwork.notification.rest.resource.NotificationResource;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import java.util.Objects;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by Denis Kuchugurov
 * on 03.08.2016.
 */
@Component
class NotificationResourceAssembler extends ResourceAssemblerSupport<Notification, NotificationResource> {


    public NotificationResourceAssembler() {
        super(NotificationController.class, NotificationResource.class);
    }

    @Override
    public NotificationResource toResource(Notification entity) {

        Objects.requireNonNull(entity);

        Initiator initiator = entity.getInitiator();
        NotificationResource resource = new NotificationResource(
                entity.getId().getUuid(),
                entity.getRecipientId().getUuid(),
                entity.getText(),
                entity.getType(),
                new InitiatorResource(initiator.getUuid(), initiator.getDisplayName(), initiator.getImageUrl(), initiator.getProfileUrl()),
                entity.isRead(),
                entity.getCreatedDate(),
                entity.getWorkId().getUuid(),
                entity.getSuggestionId() != null ? entity.getSuggestionId().getUuid() : null,
                entity.getCommentId() != null ? entity.getCommentId().getUuid() : null);

        try {
            resource.add(linkTo(methodOn(NotificationController.class).get(entity.getId())).withSelfRel());
            resource.add(linkTo(methodOn(NotificationController.class).update(entity.getId(), true)).withRel("markRead"));

            if (entity.getType().equals(Type.WORK_COMMENTED)) {
                resource.add(linkTo(methodOn(CommentController.class).get(entity.getWorkId().toCommentableId(), entity.getCommentId())).withRel("comment"));
            }
            if (entity.getType().equals(Type.SUGGESTION_CREATED)) {
                resource.add(linkTo(methodOn(SuggestionController.class).get(entity.getWorkId(), entity.getSuggestionId())).withRel("suggestion"));
            }
        } catch (Exception e) {
            throw new IllegalStateException("exception adding links to resource, actually should not ever happen", e);
        }

        return resource;
    }
}
