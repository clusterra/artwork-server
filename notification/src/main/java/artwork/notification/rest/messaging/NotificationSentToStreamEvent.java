/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.notification.rest.messaging;

import artwork.notification.domain.RecipientId;
import artwork.notification.rest.resource.NotificationResource;
import org.springframework.context.ApplicationEvent;

/**
 * Created on 27/05/2017.
 *
 * @author kepkap
 */
public class NotificationSentToStreamEvent extends ApplicationEvent {

    private final RecipientId recipientId;
    private final NotificationResource notificationResource;

    public NotificationSentToStreamEvent(Object source, RecipientId recipientId, NotificationResource notificationResource) {
        super(source);
        this.recipientId = recipientId;
        this.notificationResource = notificationResource;
    }

    public RecipientId getRecipientId() {
        return recipientId;
    }

    public NotificationResource getNotificationResource() {
        return notificationResource;
    }
}
