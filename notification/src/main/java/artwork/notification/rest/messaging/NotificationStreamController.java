/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.notification.rest.messaging;

import artwork.commentary.domain.comment.CommentId;
import artwork.core.domain.work.WorkId;
import artwork.notification.domain.Initiator;
import artwork.notification.domain.NotificationModel;
import artwork.notification.domain.RecipientId;
import artwork.notification.domain.Type;
import artwork.notification.rest.resource.InitiatorResource;
import artwork.notification.rest.resource.NotificationResource;
import artwork.web.security.tracking.CurrentUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Denis Kuchugurov
 * on 05.07.2016.
 */
@Controller
@RequestMapping(value = "/api/notifications/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
class NotificationStreamController {

    private static final Logger log = LoggerFactory.getLogger(NotificationStreamController.class);

    private final Map<RecipientId, SseEmitter> emitters = new ConcurrentHashMap<>();

    private final ApplicationEventPublisher eventPublisher;

    @Autowired
    NotificationStreamController(ApplicationEventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public SseEmitter stream(@RequestHeader(name = "Last-Event-ID", required = false) String lastId) {
        RecipientId recipientId = currentRecipientId();

        SseEmitter existing = emitters.get(recipientId);
        if (existing != null) {
            return existing;
        }

        SseEmitter emitter = new SseEmitter(180_000L);
        emitter.onCompletion(() -> {
            log.trace("removing completed emitter for recipientId:{}", recipientId.getUuid());
            emitters.remove(recipientId);
        });
        emitter.onTimeout(() -> {
            log.trace("removing timed out emitter for recipientId:{}", recipientId.getUuid());
            emitters.remove(recipientId);
        });

        emitters.put(recipientId, emitter);

        return emitter;
    }

    @RequestMapping(value = "/trigger", method = RequestMethod.GET)
    public ResponseEntity<String> trigger() {

        Initiator initiator = new Initiator("test_uuid", "test initiator", "http://localhost/image", "http://localhost/profile");
        new NotificationModel(currentRecipientId(), initiator, "test subj", "message from " + new Date(), Type.WORK_COMMENTED, WorkId.generateId(), null, CommentId.generateId());
        return new ResponseEntity<>("triggered", HttpStatus.OK);
    }

    @RabbitListener(queues = "#{notificationsQueue.name}")
    public void sendNotificationStream(@Payload NotificationPayload payload) {

        if (emitters.isEmpty()) {
            return;
        }


        RecipientId recipientId = RecipientId.from(payload.recipientId);

        if (emitters.containsKey(recipientId)) {
            SseEmitter emitter = emitters.get(recipientId);
            try {
                NotificationResource resource = toResource(payload);
                SseEmitter.SseEventBuilder message = SseEmitter
                        .event()
                        .data(resource)
                        .reconnectTime(10_000L)
                        .name("message");
                emitter.send(message);
                log.trace("notification sent to emitter for recipientId:{} (total emitters:{})", payload.recipientId, emitters.size());
                eventPublisher.publishEvent(new NotificationSentToStreamEvent(this, recipientId, resource));
            } catch (Exception e) {
                emitter.completeWithError(e);
                log.debug("removing emitter for recipientId:{} due to error: {}", recipientId.getUuid(), e.getMessage());
                emitters.remove(recipientId);
            }
        }
    }

    private NotificationResource toResource(NotificationPayload payload) {
        return new NotificationResource(
                payload.notificationId,
                payload.recipientId,
                payload.text,
                payload.type,
                new InitiatorResource(payload.initiator.uuid, payload.initiator.displayName, payload.initiator.imageUrl, payload.initiator.profileUrl),
                payload.read,
                payload.createdDate,
                payload.workId,
                payload.suggestionId,
                payload.commentId);
    }

    private static RecipientId currentRecipientId() {
        CurrentUser currentUser = CurrentUser.requireNotAnonymous();
        return RecipientId.from(currentUser.getUuid());
    }
}
