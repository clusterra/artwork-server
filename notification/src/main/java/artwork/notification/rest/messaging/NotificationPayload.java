/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.notification.rest.messaging;

import artwork.notification.domain.Type;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Denis Kuchugurov
 * on 03.08.2016.
 */
class NotificationPayload implements Serializable {

    public final String notificationId;

    public final String recipientId;

    public final String text;

    public final Type type;

    public final InitiatorPayload initiator;

    public final boolean read;

    public final Date createdDate;

    public final String workId;

    public final String suggestionId;

    public final String commentId;


    public NotificationPayload(String notificationId, String recipientId, String text, Type type, InitiatorPayload initiator, boolean read, Date createdDate, String workId, String suggestionId, String commentId) {
        this.notificationId = notificationId;
        this.recipientId = recipientId;
        this.text = text;
        this.type = type;
        this.initiator = initiator;
        this.read = read;
        this.createdDate = createdDate;
        this.workId = workId;
        this.suggestionId = suggestionId;
        this.commentId = commentId;
    }

}
