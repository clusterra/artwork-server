/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.notification.rest.messaging;

import artwork.notification.domain.Initiator;
import artwork.notification.domain.Notification;
import artwork.notification.domain.NotificationCreatedEvent;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * Created on 25/05/2017.
 *
 * @author kepkap
 */
@Component
class NotificationCreatedEventListener {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private FanoutExchange fanoutExchange;


    @EventListener
    public void sendAsMessage(NotificationCreatedEvent event) {
        Notification notification = event.getNotification();

        Initiator initiator = notification.getInitiator();

        NotificationPayload payload = new NotificationPayload(
                notification.getId().getUuid(),
                notification.getRecipientId().getUuid(),
                notification.getText(),
                notification.getType(),
                new InitiatorPayload(initiator.getUuid(), initiator.getDisplayName(), initiator.getImageUrl(), initiator.getProfileUrl()),
                notification.isRead(),
                notification.getCreatedDate(),
                notification.getWorkId().getUuid(),
                notification.getSuggestionId() != null ? notification.getSuggestionId().getUuid() : null,
                notification.getCommentId() != null ? notification.getCommentId().getUuid() : null);

        rabbitTemplate.convertAndSend(fanoutExchange.getName(), "", payload);
    }
}
