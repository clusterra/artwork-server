/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.notification.domain;

import artwork.commentary.domain.comment.CommentId;
import artwork.core.domain.work.WorkId;
import artwork.notification.TestNotificationTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Denis Kuchugurov
 * on 22.03.2017.
 */
@SpringBootTest(classes = TestNotificationTest.class)
public class NotificationTest extends AbstractTestNGSpringContextTests {

    private RecipientId recipientId;

    private Notification notification;

    @BeforeMethod
    public void before() throws Exception {
        recipientId = RecipientId.from(randomAlphabetic(20));
        notification = new NotificationModel(recipientId, createInitiator(), "subject", "message", Type.WORK_COMMENTED, WorkId.generateId(), null, CommentId.generateId());
    }

    private Initiator createInitiator() {
        return new Initiator(randomAlphabetic(20), "test user", "http://artwork.pro/user/profile", "http://artwork.pro/user/profile/image.png");
    }

    @Test
    public void test_delete() throws Exception {
        new NotificationModel(recipientId, createInitiator(), "subject", "message", Type.WORK_COMMENTED, WorkId.generateId(), null, CommentId.generateId());

        Page<Notification> page = Notification.findBy(recipientId, 0, 100);
        List<Notification> content = page.getContent();
        assertThat(content).hasSize(2);

        for (Notification notification : content) {
            notification.delete(recipientId);
        }

        page = Notification.findBy(recipientId, 0, 100);
        content = page.getContent();
        assertThat(content).isEmpty();
    }

    @Test(expectedExceptions = IllegalNotificationOwnerOperationException.class)
    public void test_delete_illegal_for_wrong_recipient() throws Exception {
        Page<Notification> page = Notification.findBy(recipientId, 0, 100);
        List<Notification> content = page.getContent();
        assertThat(content).hasSize(1);

        for (Notification notification : content) {
            notification.delete(RecipientId.from(randomAlphabetic(20)));
        }
    }

    @Test
    public void test_mark_read() throws Exception {
        assertThat(notification.isRead()).isFalse();

        notification.markRead(recipientId);
        assertThat(notification.isRead()).isTrue();
        assertThat(Notification.findBy(recipientId, notification.getId()).isRead()).isTrue();

    }

    @Test(expectedExceptions = IllegalNotificationOwnerOperationException.class)
    public void test_mark_read_throws_illegal_owner_exception() throws Exception {
        notification.markRead(RecipientId.from(randomAlphabetic(20)));
    }

    @Test
    public void get_unread_notificatins() throws Exception {
        notification.markRead(recipientId);
        new NotificationModel(recipientId, createInitiator(), "subject", "message", Type.WORK_COMMENTED, WorkId.generateId(), null, CommentId.generateId());
        Page<Notification> page = Notification.findBy(recipientId, false, 0, 10);
        assertThat(page.getTotalElements()).isPositive();
        for(Notification n : page.getContent())
            assertThat(n.isRead()).isFalse();
    }

}