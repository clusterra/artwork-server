/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.notification.domain;

import artwork.commentary.domain.IdGenerator;
import artwork.commentary.domain.comment.Comment;
import artwork.commentary.domain.comment.CommentAuthor;
import artwork.commentary.domain.commentable.Commentable;
import artwork.commentary.domain.commentable.CommentableId;
import artwork.commentary.domain.commentable.CommentableModel;
import artwork.likeable.domain.like.Liker;
import artwork.notification.NotificationCreatedEventInterceptor;
import artwork.notification.TestNotificationTest;
import artwork.notification.TestUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Denis Kuchugurov
 * on 03.08.2016.
 */
@SpringBootTest(classes = TestNotificationTest.class)
public class Notification_comment_Test extends AbstractTestNGSpringContextTests {

    @Autowired
    private NotificationCreatedEventInterceptor interceptor;

    private RecipientId recipientId;

    private CommentableId commentableId;
    private Commentable commentable;
    private Comment comment;

    @BeforeMethod
    public void before() throws Exception {
        commentableId = new CommentableId(IdGenerator.generateId());
        commentable = new CommentableModel(commentableId, TestUser.commentableOwnerId());
        CommentAuthor commentAuthor = TestUser.commentAuthor();
        comment = commentable.addComment(commentAuthor, "comment 0");
        recipientId = RecipientId.from(commentAuthor.getCommentAuthorId());
    }


    @Test
    public void test_comment_like_notification() throws Exception {

        comment.like(TestUser.liker());

        Notification notificationFromEvent = interceptor.getLastEvent().getNotification();


        Page<Notification> notifications = Notification.findBy(recipientId, 0, 100);
        assertThat(notifications.getContent().size()).isEqualTo(1);
        assertThat(notifications.getContent()).containsExactly(notificationFromEvent);

        Notification notification = notifications.getContent().get(0);

        assertThat(notification.getType()).isEqualTo(Type.COMMENT_LIKED);
    }

    @Test
    public void test_comment_unlike_does_not_produce_notification() throws Exception {

        Liker liker = TestUser.liker();

        comment.like(liker);

        Page<Notification> notifications = Notification.findBy(recipientId, 0, 100);
        assertThat(notifications.getContent().size()).isEqualTo(1);

        Notification notificationFromEvent = interceptor.getLastEvent().getNotification();
        assertThat(notifications.getContent()).containsExactly(notificationFromEvent);

        comment.unlike(liker);

        notifications = Notification.findBy(recipientId, 0, 100);
        assertThat(notifications.getContent().size()).isEqualTo(1);

        Notification notification = notifications.getContent().get(0);

        assertThat(notification.getType()).isEqualTo(Type.COMMENT_LIKED);
    }

    @Test
    public void test_comment_like_notification_is_idempotent() throws Exception {

        Liker liker = TestUser.liker();
        comment.like(liker);

        Notification notificationFromEvent = interceptor.getLastEvent().getNotification();


        Page<Notification> notifications = Notification.findBy(recipientId, 0, 100);
        assertThat(notifications.getContent().size()).isEqualTo(1);
        assertThat(notifications.getContent()).containsExactly(notificationFromEvent);

        Notification notification = notifications.getContent().get(0);

        assertThat(notification.getType()).isEqualTo(Type.COMMENT_LIKED);
        comment.like(liker);

        notifications = Notification.findBy(recipientId, 0, 100);
        assertThat(notifications.getContent().size()).isEqualTo(1);
        assertThat(notifications.getContent()).containsExactly(notificationFromEvent);

        notification = notifications.getContent().get(0);

        assertThat(notification.getType()).isEqualTo(Type.COMMENT_LIKED);
    }

}