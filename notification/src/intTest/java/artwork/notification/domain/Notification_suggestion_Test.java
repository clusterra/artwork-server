/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.notification.domain;

import artwork.core.domain.images.ImageMeta;
import artwork.core.domain.suggestion.Point;
import artwork.core.domain.suggestion.Suggestion;
import artwork.core.domain.suggestion.SuggestionOwner;
import artwork.core.domain.work.Work;
import artwork.core.domain.work.WorkId;
import artwork.core.domain.work.WorkModel;
import artwork.core.domain.work.WorkOwner;
import artwork.notification.NotificationCreatedEventInterceptor;
import artwork.notification.TestNotificationTest;
import artwork.notification.TestUser;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Denis Kuchugurov
 * on 03.08.2016.
 */
@SpringBootTest(classes = TestNotificationTest.class)
public class Notification_suggestion_Test extends AbstractTestNGSpringContextTests {

    @Autowired
    private NotificationCreatedEventInterceptor interceptor;

    private Notification notification;
    private RecipientId recipientId;
    private Suggestion suggestion;

    @BeforeMethod
    public void before() throws Exception {

        WorkOwner workOwner = TestUser.workOwner();
        Work work = new WorkModel(WorkId.generateId(), workOwner, new ImageMeta(RandomStringUtils.randomAlphanumeric(10), "image/png"), "description");

        SuggestionOwner suggestionOwner = TestUser.suggestionOwner();
        suggestion = work.addSuggestion("this is test suggestion", suggestionOwner, new Point(10, 10));

        recipientId = RecipientId.from(workOwner.getUuid());

        notification = interceptor.getLastEvent().getNotification();
    }

    @Test
    public void test_suggestion_notification() throws Exception {
        Page<Notification> notifications = Notification.findBy(recipientId, 0, 100);
        assertThat(notifications.getContent().size()).isEqualTo(1);
        assertThat(notifications.getContent()).containsExactly(notification);

        Notification notification = notifications.getContent().get(0);

        assertThat(notification.getType()).isEqualTo(Type.SUGGESTION_CREATED);
        assertThat(notification.getWorkId().getUuid()).isEqualTo(suggestion.getWorkId().getUuid());
        assertThat(notification.getSuggestionId().getUuid()).isEqualTo(suggestion.getSuggestionId().getUuid());
    }

}