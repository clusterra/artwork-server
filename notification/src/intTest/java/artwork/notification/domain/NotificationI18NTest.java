/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.notification.domain;

import artwork.core.domain.images.ImageMeta;
import artwork.core.domain.suggestion.Point;
import artwork.core.domain.suggestion.Suggestion;
import artwork.core.domain.suggestion.SuggestionOwner;
import artwork.core.domain.work.Work;
import artwork.core.domain.work.WorkId;
import artwork.core.domain.work.WorkModel;
import artwork.core.domain.work.WorkOwner;
import artwork.notification.NotificationCreatedEventInterceptor;
import artwork.notification.TestNotificationTest;
import artwork.notification.TestUser;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;

import java.util.Locale;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Denis Kuchugurov
 * on 03.08.2016.
 */
@SpringBootTest(classes = TestNotificationTest.class)
public class NotificationI18NTest extends AbstractTestNGSpringContextTests {

    private NotificationId notificationId;
    private WorkOwner workOwner;
    private SuggestionOwner suggestionOwner;

    @Autowired
    private NotificationCreatedEventInterceptor notificationCreatedEventInterceptor;
    private RecipientId recipientId;

    @BeforeMethod
    public void before() throws Exception {
        LocaleContextHolder.setLocale(new Locale("ru", "RU"));

        workOwner = TestUser.workOwner();
        suggestionOwner = TestUser.suggestionOwner();

        SecurityContextHolder.getContext().setAuthentication(new TestingAuthenticationToken(workOwner.getUuid(), ""));

        Work work = new WorkModel(WorkId.generateId(), workOwner, new ImageMeta(RandomStringUtils.randomAlphanumeric(10), "image/png"), "description");

        Suggestion suggestion = work.addSuggestion("test_description", suggestionOwner, new Point(0, 1));

        Notification notification = notificationCreatedEventInterceptor.getLastEvent().getNotification();
        notificationId = notification.getId();
        recipientId = notification.getRecipientId();
    }

    //    @Test
    public void test_created() throws Exception {
        Notification notification = Notification.findBy(recipientId, notificationId);

        assertThat(notification).isNotNull();
        assertThat(notification.getText()).isNotEmpty();
        assertThat(notification.getText()).contains(suggestionOwner.getDisplayName());
    }

}