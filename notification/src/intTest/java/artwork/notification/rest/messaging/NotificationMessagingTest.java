/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.notification.rest.messaging;

import artwork.commentary.domain.comment.CommentId;
import artwork.core.domain.work.WorkId;
import artwork.core.domain.work.WorkOwner;
import artwork.notification.TestNotificationTest;
import artwork.notification.TestUser;
import artwork.notification.domain.Initiator;
import artwork.notification.domain.Notification;
import artwork.notification.domain.NotificationModel;
import artwork.notification.domain.RecipientId;
import artwork.notification.domain.Type;
import artwork.web.security.social.UserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created on 25/05/2017.
 *
 * @author kepkap
 */
@SpringBootTest(classes = TestNotificationTest.class)
public class NotificationMessagingTest extends AbstractTestNGSpringContextTests {

    private RecipientId recipientId;

    private Notification notification;

    private Authentication authentication_1;

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;


    @Autowired
    private NotificationMessageInterceptor messageInterceptor;

    @Autowired
    private NotificationSentToStreamEventInterceptor eventInterceptor;

    @BeforeMethod
    public void before() throws Exception {

        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();


        WorkOwner workOwner = TestUser.workOwner();

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_SOCIAL_USER");
        authentication_1 = new TestingAuthenticationToken(UserDetails.from(workOwner.getUuid()), "", authorities);
        authentication_1.setAuthenticated(true);

        mvc.perform(get("/api/notifications/stream")
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isOk());

        recipientId = RecipientId.from(workOwner.getWorkOwnerId());
        notification = new NotificationModel(recipientId, createInitiator(), "subject", "message", Type.WORK_COMMENTED, WorkId.generateId(), null, CommentId.generateId());

    }

    private Initiator createInitiator() {
        return new Initiator(randomAlphabetic(20), "test user", "http://artwork.pro/user/profile", "http://artwork.pro/user/profile/image.png");
    }

    @Test
    public void test_message_produced() throws Exception {

        eventInterceptor.getLatch().await(20, TimeUnit.SECONDS);

        assertThat(eventInterceptor.getLastEvent()).isNotNull();
        assertThat(eventInterceptor.getLastEvent().getNotificationResource().notificationId).isEqualTo(notification.getId().getUuid());

        messageInterceptor.getLatch().await(20, TimeUnit.SECONDS);

        assertThat(messageInterceptor.getPayload()).isNotNull();

    }
}
