/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.notification.rest.messaging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AnonymousQueue;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.concurrent.CountDownLatch;

/**
 * Created on 25/05/2017.
 *
 * @author kepkap
 */
@Component
public class NotificationMessageInterceptor {

    private static final Logger log = LoggerFactory.getLogger(NotificationMessageInterceptor.class);

    private CountDownLatch latch = new CountDownLatch(1);

    private Object payload;


    @RabbitListener(queues = "#{autoDeleteQueue2.name}")
    public void receive(@Payload NotificationPayload payload) {
        this.payload = payload;

        log.info("***** received payload {}", payload);

        latch.countDown();

    }

    @Bean
    public Queue autoDeleteQueue2() {
        return new AnonymousQueue();
    }

    @Bean
    Binding binding2(Queue autoDeleteQueue2, FanoutExchange fanoutExchange) {
        return BindingBuilder.bind(autoDeleteQueue2).to(fanoutExchange);
    }

    public CountDownLatch getLatch() {
        return latch;
    }

    public Object getPayload() {
        return payload;
    }
}
