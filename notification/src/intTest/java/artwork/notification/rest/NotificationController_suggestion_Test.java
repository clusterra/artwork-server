/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.notification.rest;

import artwork.core.domain.images.ImageMeta;
import artwork.core.domain.suggestion.Point;
import artwork.core.domain.suggestion.SuggestionOwner;
import artwork.core.domain.work.Work;
import artwork.core.domain.work.WorkId;
import artwork.core.domain.work.WorkModel;
import artwork.core.domain.work.WorkOwner;
import artwork.notification.NotificationCreatedEventInterceptor;
import artwork.notification.TestNotificationTest;
import artwork.notification.TestUser;
import artwork.notification.domain.Notification;
import artwork.web.security.social.UserDetails;
import com.jayway.jsonpath.JsonPath;
import org.apache.commons.lang3.RandomStringUtils;
import org.omg.CORBA.Object;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/**
 * Created by Denis Kuchugurov
 * on 11.08.2016.
 */
@SpringBootTest(classes = TestNotificationTest.class)
public class NotificationController_suggestion_Test extends AbstractTestNGSpringContextTests {

    public static final String notificationsBaseUri = "/api/notifications";

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private NotificationCreatedEventInterceptor interceptor;

    private MockMvc mvc;

    private Authentication authentication_1;
    private Authentication authentication_2;
    private Authentication anonymousAuthentication = new AnonymousAuthenticationToken("TEST_ANONYMOUS_KEY", "anonymousUser", AuthorityUtils.createAuthorityList("ROLE_ANONYMOUS"));

    private Notification notification;


    @BeforeMethod
    public void before() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

        WorkOwner workOwner = TestUser.workOwner();
        WorkOwner workOwner2 = TestUser.workOwner();

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_SOCIAL_USER");
        authentication_1 = new TestingAuthenticationToken(UserDetails.from(workOwner.getUuid()), "", authorities);
        authentication_2 = new TestingAuthenticationToken(UserDetails.from(workOwner2.getUuid()), "", authorities);
        authentication_1.setAuthenticated(true);
        authentication_2.setAuthenticated(true);

        Work work = new WorkModel(WorkId.generateId(), workOwner, new ImageMeta(RandomStringUtils.randomAlphanumeric(10), "image/png"), "description");

        SuggestionOwner suggestionOwner = TestUser.suggestionOwner();
        work.addSuggestion("this is test suggestion", suggestionOwner, new Point(10, 10));

        notification = interceptor.getLastEvent().getNotification();
    }

    @Test
    public void test_get_by_and_not_owner() throws Exception {

        String content = mvc.perform(get(notificationsBaseUri)
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/hal+json;charset=UTF-8"))
                .andExpect(jsonPath("_embedded").exists())
                .andExpect(jsonPath("_links.self.href").exists())
                .andReturn().getResponse().getContentAsString();

        List<Object> result = JsonPath.read(content, "_embedded.notificationResourceList");
        assertThat(result.size()).isEqualTo(1);

        String pageable_links_self_href = JsonPath.read(content, "_links.self.href");
        String notification_links_self_href = JsonPath.read(content, "_embedded.notificationResourceList[0]._links.self.href");

        mvc.perform(get(notification_links_self_href)
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("_links.self.href").exists())
                .andExpect(jsonPath("_links.suggestion.href").exists())
                .andExpect(jsonPath("notificationId", equalTo(notification.getId().getUuid())))
                .andExpect(jsonPath("initiator").exists())
                .andExpect(jsonPath("initiator.uuid").exists())
                .andExpect(jsonPath("initiator.displayName").exists())
                .andExpect(jsonPath("initiator.imageUrl").exists())
                .andExpect(jsonPath("initiator.profileUrl").exists())
                ;

        mvc.perform(get(notification_links_self_href)
                .with(authentication(authentication_2)))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("errors[0].code", equalTo("illegal_notification_owner_operation")))
        ;

    }

    @Test
    public void test_get_by_anonymous() throws Exception {

        String content = mvc.perform(get(notificationsBaseUri)
                .with(authentication(anonymousAuthentication)))
                .andDo(print())
                .andExpect(status().isUnauthorized())
//                .andExpect(jsonPath("errors[0].code", equalTo("access_denied")))
//                .andExpect(jsonPath("errors[0].message", equalTo("Full authentication is required to access this resource")))
                .andReturn().getResponse().getContentAsString();
    }

    @Test
    public void test_get_or_delete_not_existing_id() throws Exception {

        String id = randomAlphabetic(10);
        mvc.perform(get(notificationsBaseUri + "/{id}", id)
                .with(authentication(authentication_2)))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("errors[0].code", equalTo("notification_not_found")))
        ;
        mvc.perform(delete(notificationsBaseUri + "/{id}", id)
                .with(authentication(authentication_2)))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("errors[0].code", equalTo("notification_not_found")))
        ;

    }

    @Test
    public void test_delete_by_owner() throws Exception {

        String uuid = notification.getId().getUuid();
        mvc.perform(delete(notificationsBaseUri + "/{id}", uuid)
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isOk());

        mvc.perform(get(notificationsBaseUri + "/{id}", uuid)
                .with(authentication(authentication_2)))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("errors[0].code", equalTo("notification_not_found")))
        ;
    }

    @Test
    public void test_delete_by_not_owner() throws Exception {

        mvc.perform(delete(notificationsBaseUri + "/{id}", notification.getId().getUuid())
                .with(authentication(authentication_2)))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("errors[0].code", equalTo("illegal_notification_owner_operation")))
        ;

    }

    @Test
    public void test_mark_read() throws Exception {

        mvc.perform(get(notificationsBaseUri + "/{id}", notification.getId().getUuid())
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("read", equalTo(false)))
        ;

        mvc.perform(put(notificationsBaseUri + "/{id}", notification.getId().getUuid()).param("read", "true")
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("read", equalTo(true)))
        ;
    }

    @Test
    public void test_mark_mass_read() throws Exception {

        mvc.perform(post(notificationsBaseUri + "/read")
                .content(String.format("{\"ids\": [\"%s\"]}", notification.getId().getUuid()))
                .contentType(MediaType.APPLICATION_JSON)
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(jsonPath("[0].read", equalTo(true)))
        ;
    }

}