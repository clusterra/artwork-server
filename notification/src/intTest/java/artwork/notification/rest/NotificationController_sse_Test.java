/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.notification.rest;

import artwork.core.domain.images.ImageMeta;
import artwork.core.domain.work.Work;
import artwork.core.domain.work.WorkId;
import artwork.core.domain.work.WorkModel;
import artwork.core.domain.work.WorkOwner;
import artwork.notification.NotificationCreatedEventInterceptor;
import artwork.notification.TestNotificationTest;
import artwork.notification.TestUser;
import artwork.notification.domain.Notification;
import artwork.web.security.social.UserDetails;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/**
 * Created by Denis Kuchugurov
 * on 11.08.2016.
 */
@SpringBootTest(classes = TestNotificationTest.class)
public class NotificationController_sse_Test extends AbstractTestNGSpringContextTests {

    public static final String notificationsBaseUri = "/api/notifications";

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private NotificationCreatedEventInterceptor interceptor;

    private MockMvc mvc;

    private Authentication authentication_1;
    private Notification workCommentNotification;
    private Work work;

    @BeforeMethod
    public void before() throws Exception {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

        WorkOwner workOwner = TestUser.workOwner();

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_SOCIAL_USER");
        authentication_1 = new TestingAuthenticationToken(UserDetails.from(workOwner.getUuid()), "", authorities);
        authentication_1.setAuthenticated(true);

        work = new WorkModel(WorkId.generateId(), workOwner, new ImageMeta(RandomStringUtils.randomAlphanumeric(10), "image/png"), "description");
    }


    @Test
    public void test_sse() throws Exception {

        /*ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

        executor.schedule(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                Author commenter = createCommenter();
                Commentable commentableWork = Commentable.findOne(work.getWorkId().toCommentableId());
                commentableWork.addComment(commenter, "test comment");
                workCommentNotification = interceptor.getLastEvent().getNotification();
                return null;
            }
        }, 2, TimeUnit.SECONDS);*/

        String body = mvc.perform(get(notificationsBaseUri + "/stream")
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(request().asyncStarted())
//                .andExpect(request().asyncResult(nullValue()))
                .andExpect(header().string("Content-Type", "text/event-stream;charset=UTF-8"))
//                .andExpect(content().string("asd"))
                .andReturn().getResponse().getContentAsString();

        assertThat(body).isEqualTo("");

    }
}