/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.notification.rest;

import artwork.commentary.domain.comment.CommentAuthor;
import artwork.commentary.domain.commentable.Commentable;
import artwork.core.domain.images.ImageMeta;
import artwork.core.domain.suggestion.Point;
import artwork.core.domain.suggestion.SuggestionOwner;
import artwork.core.domain.work.Work;
import artwork.core.domain.work.WorkId;
import artwork.core.domain.work.WorkModel;
import artwork.core.domain.work.WorkOwner;
import artwork.notification.NotificationCreatedEventInterceptor;
import artwork.notification.TestNotificationTest;
import artwork.notification.TestUser;
import artwork.notification.domain.Notification;
import artwork.web.security.social.UserDetails;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/**
 * Created by Denis Kuchugurov
 * on 11.08.2016.
 */
@SpringBootTest(classes = TestNotificationTest.class)
public class NotificationController_work_comment_Test extends AbstractTestNGSpringContextTests {

    public static final String notificationsBaseUri = "/api/notifications";

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private NotificationCreatedEventInterceptor interceptor;

    private MockMvc mvc;

    private Authentication authentication_1;
    private Notification suggestionNotification;
    private Notification workCommentNotification;

    @BeforeMethod
    public void before() throws Exception {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

        WorkOwner workOwner = TestUser.workOwner();

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_SOCIAL_USER");
        authentication_1 = new TestingAuthenticationToken(UserDetails.from(workOwner.getUuid()), "", authorities);
        authentication_1.setAuthenticated(true);

        Work work = new WorkModel(WorkId.generateId(), workOwner, new ImageMeta(RandomStringUtils.randomAlphanumeric(10), "image/png"), "description");

        SuggestionOwner suggestionOwner = TestUser.suggestionOwner();
        work.addSuggestion("this is test suggestion", suggestionOwner, new Point(10, 10));

        suggestionNotification = interceptor.getLastEvent().getNotification();

        CommentAuthor commentAuthor = TestUser.commentAuthor();

        Commentable commentableWork = Commentable.findOne(work.getWorkId().toCommentableId());
        commentableWork.addComment(commentAuthor, "test comment");
        workCommentNotification = interceptor.getLastEvent().getNotification();
    }


    @Test
    public void test_get_by_and_not_owner() throws Exception {

        String content = mvc.perform(get(notificationsBaseUri + "/{id}", suggestionNotification.getId())
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/hal+json;charset=UTF-8"))
                .andExpect(jsonPath("_links.self.href").exists())
                .andExpect(jsonPath("_links.suggestion.href").exists())
                .andExpect(jsonPath("_links.suggestion.href").value(containsString(suggestionNotification.getSuggestionId().getUuid())))
                .andReturn().getResponse().getContentAsString();

        mvc.perform(get(notificationsBaseUri + "/{id}", workCommentNotification.getId())
                .with(authentication(authentication_1)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("_links.self.href").exists())
                .andExpect(jsonPath("_links.comment.href").exists())
                .andExpect(jsonPath("_links.comment.href").value(containsString(workCommentNotification.getCommentId().getUuid())));

    }
}