/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.mail.sender;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

/**
 * Created by Denis Kuchugurov
 * on 27/03/16 22:57.
 */
@SpringBootTest(classes = {TestEmailSenderConfig.class})
public class EmailSenderTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private EmailSender emailSender;

    @Value("${artwork.mail.from}")
    private String toEmail;

    @Test(enabled = false)
    public void testSend() throws Exception {
        emailSender.send(toEmail, "test email from me", "just some body");

        Thread.sleep(10000);
    }
}