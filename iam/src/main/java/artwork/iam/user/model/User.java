/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.iam.user.model;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.URL;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import static artwork.iam.user.model.UserModelDependencies.eventPublisher;
import static artwork.iam.user.model.UserModelDependencies.userModelRepository;

/**
 * Created by Denis Kuchugurov
 * on 09.03.2016.
 */
@Document(collection = "user")
public class User {

    @CreatedDate
    private Date createdDate;

    @LastModifiedDate
    private Date lastModifiedDate;

    @CreatedBy
    private String createdBy;

    @LastModifiedBy
    private String lastModifiedBy;

    @Version
    private Long version;

    @Id
    private String uuid;

    @NotEmpty
    private String displayName;

    @URL
    private String profileUrl;

    @URL
    private String imageUrl;

    @Email
    private String email;

    {
        Objects.requireNonNull(userModelRepository, "check if Spring context is initialized properly");
        Objects.requireNonNull(eventPublisher, "check if Spring context is initialized properly");
    }

    public User() {
    }

    public User(UserId userId, String displayName, String profileUrl, String imageUrl, String email) {
        this.uuid = userId.getId();
        this.displayName = displayName;
        this.profileUrl = profileUrl;
        this.imageUrl = imageUrl;
        this.email = email;
    }

    public UserId getUserId() {
        return new UserId(uuid);
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public Long getVersion() {
        return version;
    }

    public boolean hasEmail() {
        return email != null;
    }

    public String getEmail() {
        return email;
    }

    public void delete() {
        userModelRepository.delete(this);
        eventPublisher.publishEvent(new UserDeletedEvent(this, this));
    }

    public void updateDisplayName(String displayName) {
        this.displayName = displayName;
        userModelRepository.save(this);
    }

    public void updateEmail(String email){
        this.email = email;
        userModelRepository.save(this);
        eventPublisher.publishEvent(new UserEmailUpdatedEvent(this));
    }

    public String getUuid() {
        return uuid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return getUserId().equals(user.getUserId());

    }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }

    public static User create(UserId userId, String displayName, String profileUrl, String imageUrl, String email) {
        User user = new User(userId, displayName, profileUrl, imageUrl, email);
        userModelRepository.save(user);
        eventPublisher.publishEvent(new UserCreatedEvent(user));

        return user;
    }

    public static User findBy(UserId userId) throws UserNotFoundException {
        User user = userModelRepository.findOne(userId.getId());
        if (user == null) {
            throw new UserNotFoundException(userId);
        }
        return user;
    }

    public static List<User> findByDisplayName(String displayName) {
        return userModelRepository.findByDisplayName(displayName);
    }
}
