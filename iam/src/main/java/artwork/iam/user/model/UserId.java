/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.iam.user.model;

import org.apache.commons.lang3.Validate;

import java.io.Serializable;
import java.util.Objects;

/**
 * Created by Denis Kuchugurov
 * on 09.03.2016.
 */
public class UserId implements Serializable {

    private final String id;

    public UserId(String id) {
        Objects.requireNonNull(id, "id is null");
        Validate.notEmpty(id, "id is empty");
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public static UserId generateId() {
        return new UserId(IdGenerator.generateId());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserId that = (UserId) o;

        return id.equals(that.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return id;
    }
}
