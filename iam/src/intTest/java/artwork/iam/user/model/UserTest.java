/*
 * Copyright (c) 2016 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.iam.user.model;

import artwork.iam.IamConfig;
import org.springframework.boot.autoconfigure.context.PropertyPlaceholderAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.validation.ConstraintViolationException;
import java.util.List;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

/**
 * Created by kepkap
 * on 22/07/16.
 */
@SpringBootTest(classes = {PropertyPlaceholderAutoConfiguration.class, IamConfig.class})
public class UserTest extends AbstractTestNGSpringContextTests {

    @BeforeMethod
    public void beforeMethod() {
        SecurityContextHolder.getContext().setAuthentication(new TestingAuthenticationToken("test", "test"));
    }


    @Test
    public void test_create() throws Exception {
        User user = User.create(UserId.generateId(), randomAlphabetic(20), "http://artwork.pro/user/profile", "http://artwork.pro/user/profile/image.png", "user@artwork.pro");

        user = User.findBy(user.getUserId());

        assertThat(user.getDisplayName()).isNotEmpty();
        assertThat(user.getEmail()).isNotEmpty();
        assertThat(user.getImageUrl()).isNotEmpty();
        assertThat(user.getProfileUrl()).isNotEmpty();
        assertThat(user.getCreatedDate()).isNotNull();
        assertThat(user.getLastModifiedDate()).isNotNull();

        assertThat(user.getVersion()).isGreaterThanOrEqualTo(0);
    }

    @Test
    public void test_auditing() throws Exception {
        User user = User.create(UserId.generateId(), randomAlphabetic(20), "http://artwork.pro/user/profile", "http://artwork.pro/user/profile/image.png", "user@artwork.pro");

        user = User.findBy(user.getUserId());

        assertThat(user.getCreatedBy()).isEqualTo("test");
        assertThat(user.getVersion()).isEqualTo(0);

        SecurityContextHolder.getContext().setAuthentication(new TestingAuthenticationToken("test2", "test2"));

        user.updateDisplayName(randomAlphabetic(20));
        user = User.findBy(user.getUserId());

        assertThat(user.getCreatedBy()).isEqualTo("test");
        assertThat(user.getLastModifiedBy()).isEqualTo("test2");
        assertThat(user.getVersion()).isEqualTo(1);
    }

    @Test
    public void test_delete() throws Exception {
        User user = User.create(UserId.generateId(), randomAlphabetic(20), "http://artwork.pro/user/profile", "http://artwork.pro/user/profile/image.png", "user@artwork.pro");

        UserId userId = user.getUserId();
        user = User.findBy(userId);

        assertThat(user).isNotNull();

        user.delete();
        try {
            User.findBy(userId);
            failBecauseExceptionWasNotThrown(UserNotFoundException.class);
        } catch (UserNotFoundException e) {

        }
    }

    @Test
    public void test_create_invalid_display_name() throws Exception {
        try {
            User.create(UserId.generateId(), "", "http://artwork.pro/user/profile", "http://artwork.pro/user/profile/image.png", "user@artwork.pro");
            fail("expecting exception");
        } catch (ConstraintViolationException e) {
            e.printStackTrace();
            assertThat(e.getConstraintViolations()).hasSize(1);
            assertThat(e.getConstraintViolations().iterator().next().getMessage()).contains("may not be empty");
        }
    }

    @Test
    public void test_create_invalid_display_name_and_email() throws Exception {
        try {
            User.create(UserId.generateId(), "", "http://artwork.pro/user/profile", "http://artwork.pro/user/profile/image.png", "invalid4email");
            fail("expecting exception");
        } catch (ConstraintViolationException e) {
            e.printStackTrace();
            assertThat(e.getConstraintViolations()).hasSize(2);
        }
    }

    @Test
    public void test_create_invalid_all_fields() throws Exception {
        try {
            User.create(UserId.generateId(), "", "invalid_url", "invalid_url", "invalid@email");
            fail("expecting exception");
        } catch (ConstraintViolationException e) {
            e.printStackTrace();
            assertThat(e.getConstraintViolations()).hasSize(3);
        }
    }

    @Test
    public void test_update_invalid_display_name() throws Exception {
        User user = User.create(UserId.generateId(), randomAlphabetic(20), "http://artwork.pro/user/profile", "http://artwork.pro/user/profile/image.png", "user@artwork.pro");

        try {
            user.updateDisplayName("");
            fail("expecting exception");
        } catch (ConstraintViolationException e) {
            e.printStackTrace();
            assertThat(e.getConstraintViolations()).hasSize(1);
            assertThat(e.getConstraintViolations().iterator().next().getMessage()).contains("may not be empty");
        }
    }

    @Test
    public void test_update_display_name() throws Exception {
        String displayName = randomAlphabetic(20);
        User user = User.create(UserId.generateId(), displayName, "http://artwork.pro/user/profile", "http://artwork.pro/user/profile/image.png", "user@artwork.pro");

        user = User.findBy(user.getUserId());

        assertThat(user.getDisplayName()).isEqualTo(displayName);
        Long version = user.getVersion();
        assertThat(version).isGreaterThanOrEqualTo(0);

        user.updateDisplayName(randomAlphabetic(20));

        assertThat(user.getDisplayName()).isNotEqualTo(displayName);
        assertThat(user.getLastModifiedDate()).isAfter(user.getCreatedDate());

        assertThat(user.getVersion()).isGreaterThan(version);
    }

    @Test
    public void test_update_email() throws Exception {
        String displayName = randomAlphabetic(20);
        User user = User.create(UserId.generateId(), displayName, "http://artwork.pro/user/profile", "http://artwork.pro/user/profile/image.png", null);

        user = User.findBy(user.getUserId());

        assertThat(user.hasEmail()).isFalse();
        assertThat(user.getEmail()).isNull();
        Long version = user.getVersion();
        assertThat(version).isGreaterThanOrEqualTo(0);

        user.updateEmail(randomAlphabetic(20) + "@something.com");

        assertThat(user.hasEmail()).isTrue();
        assertThat(user.getEmail()).isNotEqualTo(displayName);
        assertThat(user.getLastModifiedDate()).isAfter(user.getCreatedDate());

        assertThat(user.getVersion()).isGreaterThan(version);
    }

    @Test(expectedExceptions = UserNotFoundException.class)
    public void test_find_by_throws_exception() throws Exception {
        UserId userId = new UserId(randomAlphabetic(20));
        User.findBy(userId);
    }

    @Test
    public void test_find_by_display_name() {
        String displayName = randomAlphabetic(20);
        User dave = User.create(UserId.generateId(), displayName, "http://artwork.pro/user/profile", "http://artwork.pro/user/profile/image.png", "user@artwork.pro");

        List<User> result = User.findByDisplayName(displayName);
        assertThat(result.size()).isEqualTo(1);
        assertThat(result).contains(dave);
    }

}