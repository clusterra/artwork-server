# Errors

## Common Validations
code | message 
--- | --- 
Size | todo
NotEmpty | may not be empty
NotNull | may not be null


## Works

code | message | arguments
--- | --- | ---
image_format_error | image must be in png or jpg format |
image_too_small | image is too small | minWidth, minHeight, width, height
work_not_found | work not found by id '%id' 
illegal_work_owner_operation | operation is not allowed on not owning entity id '%id' 

## Suggestions
code | message 
--- | --- 
illegal_suggestion_owner_operation | operation is not allowed on not owning entity id '%id'
illegal_work_owner_for_mark_suggestion_read_operation | operation is not allowed on not owning entity id '%id'
suggestion_not_found | suggestion not found by '%id'


## Commentable/Comments

code | message 
--- | --- 
commentable_not_found | commentable not found by '%id'
comment_not_found | comment not found by comment id '%id'
comment_not_found | comment not found by related to id '%id'
illegal_comment_owner_operation | operation is not allowed on not owning entity id '%id




