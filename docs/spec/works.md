# /works
## Common object
### Work
```
workId: string
workOwner:
  uuid: string
  displayName: string
  imageUrl: string
  profileUrl: string

imageResource:
  filename: string
  originalUri: string
  thumbnailUri: string
  mimeType: string

description: string
viewsCount: integer
likesCount: integer
commentsCount: integer
tags: array of string
createdDate: timestamp
createdByUserId: string
isMine: boolean
[alreadyLiked: boolean]
  true - like
  false - dislike
  null - user doesn't like nor dislike the work
```

## Create work
`POST /`
### Request
Body (multipart)
description:
```
description: string
```
file: jpg/png image up to 5MB
### Response
Code: 201
Body: **Work**
### Errors
to do

## Like/unlike work
`PUT /{id}`
### Request
Parameters:
```
like: boolean
```
### Response
Code: 200 (work state was modified) or 304 (work state is unchanged)
Body: **Work**
### Errors
to do

## Dislike/Undislike work
`PUT /{id}`
### Request
Parameters:
```
dislike: boolean
```
### Response
Code: 200 (work state was modified) or 304 (work state is unchanged)
Body: **Work**
### Errors
to do

## Update work
`PUT /{id}`
### Request
Body:
```
descriptionUpdateJson:
  description: string
```
### Response
Code: 200 (work state was modified) or 304 (work state is unchanged)
Body: **Work**
### Errors
to do

## Delete work
`DELETE /{id}`
### Response
Code: 204
### Errors
to do

## Get work
`GET /{id}`
### Response
Code: 200
Body: **Work**
### Errors
to do

## Get works page
`GET /`
### Request
Parameters:
```
[searchBy: string]
[authorId: string]
TODO
```
### Response
Code: 200
Body: 
```
TODO
```
### Errors
to do

