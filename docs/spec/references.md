# /url
## Common object
### Something
```
field: type
[optionalField: type]
nestedObject:
  field: type
  field2: type
  [optionalField: type]

whatIsIt: type
  description
  blah blah blah
[optional: type]
  description
  blah blah blah
[optionalNestedObject:]
  field: type
  field: type
```

## Action
`METHOD /url/{path_var}`
### Request
Parameters:
```
param: type
[optionalParam: type]
```
Body: **common object**
### Response
Code: xxx

Body: **common object**
### Errors
4xx error_code: description 

4xx another_error_code: description

```
Special error body...
```

## Action
`METHOD /url/{path_var}`
### Request
Parameters:
```
param: type
[optionalParam: type]
```
Body:
```
field: type
field2: type2
```
### Response
Code: xxx

Body: **common object**
### Errors
4xx error_code: description 
