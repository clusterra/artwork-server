# /commentable/{commentableId}/comments
## Common parameters
```
commentableId: string
```
## Common objects
### Comment
```
commentId: string
author: User object
text: string
likesCount: integer
createdDate: timestamp
isMine: boolean
isAuthorial: boolean
suggestionCommentsCount: integer
relatesToSuggestion: boolean
```
### User
```
uuid: string
displayName: string
imageUrl: string
profileUrl: string
```

## Create comment
`POST /`
### Request
Body:
```
text: string
```
### Response
Code: 201

Body: **Comment**

Errors:

**400 commentable_not_found** Commentable id is invalid

## Delete comment
Delete text of the comment

`DELETE /{commentId}`

### Response
Code: 200

Body: **Comment**

Errors:

**400 commentable_not_found** Commentable id is invalid

**400 comment_not_found_not_found** Comment id is invalid

## Like/unlike comment
`PUT /{commentId}`

### Request
Parameters:
```
like: boolean
```
### Response
Code: 200

Body: **Comment**

Errors:

**400 commentable_not_found** Commentable id is invalid

**400 comment_not_found_not_found** Comment id is invalid

## Update comment
`PUT /{commentId}`

### Request
Parameters:
```
text: string
```
### Response
Code: 200

Body: **Comment**

Errors:
**400 commentable_not_found** Commentable id is invalid

**400 comment_not_found_not_found** Comment id is invalid

**400 illegal_comment_owner_operation** You must own the comment to edit it

## Get comment
`GET /{commentId}`

### Response
Code: 200

Body: **Comment**

Errors:

**400 commentable_not_found** Commentable id is invalid

**400 comment_not_found_not_found** Comment id is invalid

## Get comments
`GET /`

### Response
Code: 200

Body: array of **Comment**

Errors:

**400 commentable_not_found** Commentable id is invalid

## Get liked users
`GET /{commentId}/liked`

### Response
Code: 200

Body: array of **User**

Errors:

**400 commentable_not_found** Commentable id is invalid

**400 comment_not_found_not_found** Comment id is invalid