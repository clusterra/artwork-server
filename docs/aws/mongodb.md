# Mongodb set-up (docker)

```batch
mkdir data/db

sudo docker pull mongo

sudo docker run --name artwork-mongo -v /home/ec2-user/data/db:/data/db -p 27017:27017 -d mongo

docker exec -it artwork-mongo mongo admin

db.createUser({ user: 'ADMIN', pwd: 'SECRET', roles: [ { role: "userAdminAnyDatabase", db: "admin" } ] });


use DATABASE

db.createUser({user:"USER", pwd:"SECRET", roles:['dbOwner']})

sudo docker run --name artwork-mongo -v /home/ec2-user/data/db:/data/db -p 27017:27017 -d mongo --auth
```


# Cluster set-up

## 1. install docker

```bash
    sudo yum install -y docker && sudo service docker start
```

## 2. openssl key

NB! copy key to the same location

```bash
    mkdir mongo/data/key
    openssl rand -base64 741 > mongodb-keyfile
    sudo chmod 600 mongodb-keyfile
    sudo chown 999 mongodb-keyfile
    mv mongodb-keyfile mongo/data/key
```

## 3. on each node of replica set

### 3.1 start mongo with no authentication 

```bash
    sudo docker run --name artwork-mongo \
    -v /home/ec2-user/mongo/data/db:/data/db \
    -v /home/ec2-user/mongo/data/key:/data/key \
    -p 27017:27017 \
    -d mongo --smallfiles
```

### 3.2 set-up authentication

```bash
    sudo docker exec -it artwork-mongo /bin/bash
    mongo
```

```javascript

    use admin
    
    db.createUser({ 
        user: 'admin', 
        pwd: 'password', 
        roles: [ { role: "userAdminAnyDatabase", db: "admin" } ] 
    });

    db.createUser({ 
        user: "rootAdmin", 
        pwd: "password", 
        roles: [ { role: "root", db: "admin" } ] 
    });
```

>exit exit

### 3.3 start mongo with replica set 

```bash
    sudo docker run --name artwork-mongo \
    -v /home/ec2-user/mongo/data/db:/data/db \
    -v /home/ec2-user/mongo/data/key:/data/key \
    -p 27017:27017 \
    -d mongo --smallfiles --keyFile /data/key/mongodb-keyfile --replSet "rs0"
```

## 4. connect to a PRIMARY node of replica set and configure replica set

```bash
    sudo docker exec -it artwork-mongo /bin/bash
    mongo
```

```javascript
    
    use admin
    db.auth("rootAdmin", "password");

    rs.initiate();
    rs.conf();
```

```javascript

//need to set host from uuid to real ip or resolvable dns
cfg = rs.conf() 
cfg.members[0].host = "xx.yy.zz.126:27017" 
cfg.members[1].host = "xx.yy.zz.171:27017" 
cfg.members[2].host = "xx.yy.zz.101:27017" 
rs.reconfig(cfg)

rs0:PRIMARY> rs.add("172.0.0.1") //
rs0:PRIMARY> rs.add("172.0.0.2")
```


# 5. Helper commands

## 5.1. view container logs
```bash
sudo docker logs -ft artwork-mongo
```

## 5.2. mongo connection url
```bash
mongodb://host1,host2,host3:27017/?replicaSet=rs0
```
