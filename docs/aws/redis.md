# AWS Redis set-up

## Redis instance (docker)
1. sudo docker pull redis
2. sudo docker run --name artwork-redis -p 6379:6379 --restart=unless-stopped -d redis


check the redis is running
```batch
    sudo yum install gcc
    wget http://download.redis.io/redis-stable.tar.gz
    tar xvzf redis-stable.tar.gz
    cd redis-stable
    make
    src/redis-cli -h host -p 6379 ping
    src/redis-cli -c -h host -p 6379
```
    
For Spring session to work the following parameter must be set. 
In aws can only be done through custom 'Parameter Group'.
```batch
    src/redis-cli config set notify-keyspace-events Egx
```
