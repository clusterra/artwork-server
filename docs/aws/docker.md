# Docker

## 1. Install docker

```batch
sudo yum install -y docker
sudo service docker start
```

## Copy logs from aws

```batch
    scp -i "key.pem" ec2-user@xyz.amazonaws.com:/home/ec2-user/log/artwork-server.log ~/artwork-server/log/
```

## Maintaining docker images    

Command | For 
--- | --- 
```    docker rm $(docker ps -aq)``` | Remove non-running containers
```    docker rmi $(docker images -q)``` | Remove unused images
```    docker rmi $(docker images -f 'dangling=true' -q)```  | Remove all dangling images
```    docker rm $(docker ps -a -q)```  | Remove all stopped containers    
 
 
## Common tools

check that port is open (no firewalled)
```batch
    nc -zv portquiz.net 5432
    install telnet: yum install telnet
```

