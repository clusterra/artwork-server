# RabbitMQ set-up (docker)

```batch

docker pull rabbitmq:3-management

docker run -d \
        -p 15672:15672 \
        -p 5672:5672 \
        -e RABBITMQ_DEFAULT_USER=user \
        -e RABBITMQ_DEFAULT_PASS=password \
        --hostname rabbitmq-artwork-pro \
        --restart=unless-stopped \
        rabbitmq:3-management
        
docker logs some-rabbit-container-name
```

## Admin console

use http://localhost:15672/ for admin console
