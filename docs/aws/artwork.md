# ARTWORK server set-up (docker)


## 1. Pull latest docker image

```batch
sudo docker pull kepkap/artwork-server
```

## 2. Run container

```batch
sudo docker run -p 8080:8080 --restart=unless-stopped \
-v /home/ec2-user/log/:/log \
-e AW_MAIL_HOST=localhost \
-e AW_MAIL_PORT=465 \
-e AW_MAIL_USERNAME=user \
-e AW_MAIL_PASSWORD=password \
-e AW_MAIL_FROM=no-reply@artwork.pro \
-e AW_APPLICATION_URL=https://www.artwork.pro \
-e AW_ALLOWED_ORIGINS=https://www.artwork.pro \
-e AW_AUTH_FACEBOOK_APP_KEY= \
-e AW_AUTH_FACEBOOK_APP_SECRET= \
-e AW_AUTH_VKONTAKTE_CLIENT_ID= \
-e AW_AUTH_VKONTAKTE_CLIENT_SECRET= \
-e AW_MONGODB_URI=localhost \
-e AW_REDIS_HOST=localhost \
-e AW_REDIS_PASSWORD= \
-e AW_REDIS_PORT=6379 \
-e AW_PROFILE=beta \
-e AW_RABBITMQ_USERNAME=guest \
-e AW_RABBITMQ_PASSWORD=guest \
-e AW_RABBITMQ_HOST=localhost \
-e AW_RABBITMQ_PORT=5672 \
-d kepkap/artwork-server
```

2. https://dev-server.artwork.pro:8080/health

 
## Building docker image
 
```batch
    docker build -t artwork-server .    
    docker push kepkap/artwork-server
``` 
