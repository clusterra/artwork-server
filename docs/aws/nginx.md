# AWS nginx set-up

## Nginx load balancer
1. sudo yum install -y nginx
2. sudo nano /etc/nginx/sites-available/default

3. cd /etc/nginx/conf.d
4. sudo nano artwork-upstream.conf

```
upstream backend  {
  server backend1.example.com;
  server backend2.example.com;
  server backend3.example.com;
}
```


5. sudo nano nginx.conf

```
server {
    listen      80;
    return      301 https://$host$request_uri;
}

server {

    server_name local.artwork.pro;

    listen 443 ssl http2 default_server;
    listen [::]:443 ssl http2 default_server;
    
    ssl                 on;
    ssl_certificate     /etc/nginx/artwork.pem;
    ssl_certificate_key /etc/nginx/artwork.key;
    ssl_protocols       TLSv1 TLSv1.1 TLSv1.2;
    
    ssl_session_timeout 5m;
    ssl_session_cache shared:SSL:50m;
    
    location / {
        add_header Cache-Control no-store;
        add_header Pragma no-cache;
        add_header X-Frame-Options SAMEORIGIN;

        proxy_set_header Connection '';
        proxy_http_version 1.1;

        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header   X-Forwarded-Proto $scheme;
        proxy_set_header Host $http_host;
        proxy_redirect off;
        proxy_connect_timeout 120s;
        proxy_read_timeout 240s;

        proxy_pass https://backend;
    }


    location /api/notifications/stream {
        proxy_buffering off;
        proxy_cache off;
        proxy_set_header Connection '';
        proxy_http_version 1.1;
        chunked_transfer_encoding off;

        proxy_pass https://backend;
    }

}

```


6. sudo service nginx restart 
