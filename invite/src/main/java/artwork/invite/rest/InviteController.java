/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.invite.rest;

import artwork.invite.domain.Invite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created by Egor Petushkov
 * on 10.07.17.
 */
@Controller
@RequestMapping(value = "/admin/invites", produces = MediaTypes.HAL_JSON_VALUE)
class InviteController {
    private static final Logger log = LoggerFactory.getLogger(InviteController.class);

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<List<Invite>> getInvites() {
        return new ResponseEntity<>(Invite.findAll(), HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<?> sendInvites() {
        List<Invite> invites = Invite.findUnInvited();
        for (Invite invite : invites) {
            invite.inviteIfNotInvited();
        }

        return new ResponseEntity<>(invites, HttpStatus.ACCEPTED);
    }
}
