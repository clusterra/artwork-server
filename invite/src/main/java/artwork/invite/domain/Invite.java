/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.invite.domain;

import org.hibernate.validator.constraints.Email;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.Objects;

/**
 * Created by Egor Petushkov
 * on 10.07.17.
 */
@Document(collection = "invite")
public class Invite {

    @Id
    private String id;

    @Email
    @Indexed(unique = true)
    private String email;

    private boolean invited;

    {
        Objects.requireNonNull(InviteDependencies.inviteModelRepository, "check if Spring context is initialized properly");
        Objects.requireNonNull(InviteDependencies.emailSender, "check if Spring context is initialized properly");
        Objects.requireNonNull(InviteDependencies.messageSource, "check if Spring context is initialized properly");
    }

    @PersistenceConstructor
    Invite() {
    }

    Invite(String email) {
        this.email = email;
        this.invited = false;
        InviteDependencies.inviteModelRepository.save(this);
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public boolean isInvited() {
        return invited;
    }

    public void setUninvited() {
        if (!invited) {
            return;
        }
        invited = false;
        InviteDependencies.inviteModelRepository.save(this);
    }

    public boolean inviteIfNotInvited() {
        if (invited) {
            return false;
        }

        String messageSubject = InviteDependencies.messageSource.getMessage("message.email.invite.subject", null, "Welcome to Artwork", LocaleContextHolder.getLocale());
        InviteDependencies.emailSender.send(email, messageSubject, InviteDependencies.inviteEmailBody);

        invited = true;
        InviteDependencies.inviteModelRepository.save(this);
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Invite)) return false;

        Invite invite = (Invite) o;

        return email.equals(invite.email);
    }

    @Override
    public int hashCode() {
        return email.hashCode();
    }

    public static List<Invite> findUnInvited() {
        return InviteDependencies.inviteModelRepository.findByInvitedFalseOrInvitedIsNullOrderByEmail();
    }

    public static List<Invite> findAll() {
        return InviteDependencies.inviteModelRepository.findAll(new Sort("email"));
    }

    public static Invite findOrCreate(String email) {
        Invite invite = InviteDependencies.inviteModelRepository.findByEmail(email);
        return invite != null ? invite : new Invite(email);
    }
}
