/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.invite.domain;

import artwork.invite.TestInviteConfig;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Egor Petushkov
 * on 10.07.17.
 */
@SpringBootTest(classes = TestInviteConfig.class)
public class InviteManualTest extends AbstractTestNGSpringContextTests {

    @Test(enabled = false)
    public void sendEmail() throws InterruptedException {
        Invite invite = Invite.findOrCreate("some@email");
        invite.setUninvited();
        assertThat(invite.inviteIfNotInvited()).isTrue();
        assertThat(invite.inviteIfNotInvited()).isFalse();
        Thread.sleep(30000);
    }
}
