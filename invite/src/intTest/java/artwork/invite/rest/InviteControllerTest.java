/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.invite.rest;

import artwork.invite.TestInviteConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Egor Petushkov
 * on 10.07.17.
 */
@SpringBootTest(classes = TestInviteConfig.class)
public class InviteControllerTest extends AbstractTestNGSpringContextTests {
    @Autowired
    private WebApplicationContext context;

    @Value("${management.user.name}")
    private String user;

    @Value("${management.user.password}")
    private String password;

    private MockMvc mvc;

    @BeforeMethod
    public void before() throws Exception {
        mvc = MockMvcBuilders
            .webAppContextSetup(context)
            .apply(springSecurity())
            .build();
    }

    @Test
    public void testGet() throws Exception {
        mvc.perform(get("/admin/invites"))
            .andDo(print())
            .andExpect(status().isUnauthorized());
    }

    @Test
    public void testPost() throws Exception {
        mvc.perform(post("/admin/invites"))
            .andDo(print())
            .andExpect(status().isUnauthorized());
    }

    @Test
    public void testGet_authorized() throws Exception {
        mvc.perform(get("/admin/invites")
            .with(httpBasic(user, password)))
            .andDo(print())
            .andExpect(status().isOk());
    }
}
