#!/bin/sh
export AW_PROFILE=dev


export AW_MAIL_HOST=localhost
export AW_MAIL_PORT=465
export AW_MAIL_USERNAME=user
export AW_MAIL_PASSWORD=password
export AW_MAIL_FROM=no-reply@artwork.pro

export AW_APPLICATION_URL=https://www.artwork.pro
export AW_ALLOWED_ORIGINS=https://www.artwork.pro

export AW_AUTH_FACEBOOK_APP_KEY=
export AW_AUTH_FACEBOOK_APP_SECRET=

export AW_AUTH_VKONTAKTE_CLIENT_ID=
export AW_AUTH_VKONTAKTE_CLIENT_SECRET=

export AW_MONGODB_URI=mongodb://user:secret@mongo1.example.com:12345,mongo2.example.com:23456/test

export AW_RABBITMQ_USERNAME=guest
export AW_RABBITMQ_PASSWORD=guest
export AW_RABBITMQ_HOST=localhost
export AW_RABBITMQ_PORT=5672

export AW_MANAGEMENT_USERNAME=user
export AW_MANAGEMENT_PASSWORD=secret

export AW_REDIS_HOST=localhost
export AW_REDIS_PASSWORD=
export AW_REDIS_PORT=6379

export AW_UPLOADCARE_PUBLIC=demopublickey
export AW_UPLOADCARE_SECRET=demoprivatekey

env | grep AW_ | sort
env | grep JAVA_ | sort
