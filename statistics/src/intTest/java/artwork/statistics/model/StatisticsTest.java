/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.statistics.model;

import artwork.commentary.domain.commentable.CommentableId;
import artwork.commentary.domain.commentable.CommentableModel;
import artwork.commentary.domain.commentable.CommentableOwnerId;
import artwork.core.domain.IdGenerator;
import artwork.core.domain.images.ImageMeta;
import artwork.core.domain.work.Work;
import artwork.core.domain.work.WorkId;
import artwork.core.domain.work.WorkModel;
import artwork.iam.user.model.UserId;
import artwork.iam.user.model.User;
import artwork.statistics.TestStatisticsConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by
 * Denis Kuchugurov on 17/06/2017.
 */
@SpringBootTest(classes = TestStatisticsConfig.class)
public class StatisticsTest extends AbstractTestNGSpringContextTests {


    @Autowired
    private ApplicationEventPublisher publisher;

    private StatisticsId statisticsId;
    private Statistics statistics;
    private User user;

    @BeforeMethod
    public void before() throws Exception {
        statisticsId = new StatisticsId(IdGenerator.generateId());
        user = User.create(new UserId(statisticsId.getUuid()), "displayName", "http://localhost/image_url", "http://localhost/profile_url", "email@email.com");
        statistics = Statistics.findOrCreate(statisticsId);
    }

    @Test(expectedExceptions = UnableToCreateStatisticsForNotExistingUser.class)
    public void test_unable_to_create_statistics_for_not_exisitng_user() throws Exception {
        Statistics.findOrCreate(new StatisticsId(IdGenerator.generateId()));
    }

    @Test
    public void test_auditing_values_are_set() throws Exception {

        assertThat(statistics.getCreatedDate()).isNotNull();
        Date date = statistics.getLastModifiedDate();
        assertThat(date).isNotNull();

        assertThat(statistics.getVersion()).isEqualTo(0);
        statistics.incrementCommentsCount();

        assertThat(date).isBefore(statistics.getLastModifiedDate());
        assertThat(statistics.getVersion()).isEqualTo(1);
    }

    @Test
    public void test_statistics_owner() throws Exception {

        assertThat(statistics.getStatisticsOwner()).isNotNull();
        assertThat(statistics.getStatisticsOwner().getDisplayName()).isEqualTo(user.getDisplayName());
        assertThat(statistics.getStatisticsOwner().getImageUrl()).isEqualTo(user.getImageUrl());
        assertThat(statistics.getStatisticsOwner().getProfileUrl()).isEqualTo(user.getProfileUrl());
        assertThat(statistics.getStatisticsOwner().getUuid()).isEqualTo(user.getUserId().getId());
    }

    @Test
    public void test_comments_count() throws Exception {

        CommentableId commentableId = CommentableId.generateId();
        CommentableOwnerId commentableOwnerId = new CommentableOwnerId(statisticsId.getUuid());
        CommentableModel commentable = new CommentableModel(commentableId, commentableOwnerId);

        assertThat(Statistics.findOrCreate(statisticsId).getCommentsCount()).isEqualTo(0);

        commentable.addComment(new TestUser("commenter").toCommentAuthor(), "text");

        assertThat(Statistics.findOrCreate(statisticsId).getCommentsCount()).isEqualTo(1);
    }

    @Test
    public void test_likes_count() throws Exception {


        ImageMeta imageMeta = new ImageMeta(IdGenerator.generateId(), "image/png");
        Work work = new WorkModel(WorkId.generateId(), new TestUser(statisticsId.getUuid()).toWorkOwner(), imageMeta, "description");


        assertThat(Statistics.findOrCreate(statisticsId).getLikesCount()).isEqualTo(0);

        work.like(new TestUser("liker").toLiker());

        assertThat(Statistics.findOrCreate(statisticsId).getLikesCount()).isEqualTo(1);

    }

    @Test
    public void test_works_count() throws Exception {

        assertThat(Statistics.findOrCreate(statisticsId).getWorksCount()).isEqualTo(0);

        ImageMeta imageMeta = new ImageMeta(IdGenerator.generateId(), "image/png");
        Work work = new WorkModel(WorkId.generateId(), new TestUser(statisticsId.getUuid()).toWorkOwner(), imageMeta, "description");
        Thread.sleep(100);

        assertThat(Statistics.findOrCreate(statisticsId).getWorksCount()).isEqualTo(1);

    }

}