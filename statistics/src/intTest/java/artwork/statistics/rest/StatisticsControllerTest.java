/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.statistics.rest;

import artwork.core.domain.IdGenerator;
import artwork.iam.user.model.UserId;
import artwork.iam.user.model.User;
import artwork.statistics.TestStatisticsConfig;
import artwork.statistics.model.Statistics;
import artwork.statistics.model.StatisticsId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.Test;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Egor Petushkov
 * on 19.06.17.
 */
@SpringBootTest(classes = TestStatisticsConfig.class)
public class StatisticsControllerTest extends AbstractTestNGSpringContextTests {
    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    @Test
    public void getStatistics() throws Exception {
        User user = User.create(new UserId(IdGenerator.generateId()), "displayName", "http://localhost/image_url", "http://localhost/profile_url", "email@email.com");

        StatisticsId statisticsId = new StatisticsId(user.getUserId().getId());
        Statistics statistics = Statistics.findOrCreate(statisticsId);

        statistics.incrementCommentsCount();
        statistics.incrementLikesCount();
        statistics.incrementViewsCount();
        statistics.incrementWorksCount();

        mvc = MockMvcBuilders
            .webAppContextSetup(context)
            .apply(springSecurity())
            .build();
        mvc.perform(get("/api/users/{id}/statistics", statisticsId.getUuid()))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(jsonPath("statisticsId", equalTo(statisticsId.getUuid())))
            .andExpect(jsonPath("uuid", equalTo(statisticsId.getUuid())))
            .andExpect(jsonPath("displayName").value(notNullValue()))
            .andExpect(jsonPath("imageUrl").value(notNullValue()))
            .andExpect(jsonPath("profileUrl").value(notNullValue()))
            .andExpect(jsonPath("commentsCount", equalTo(1)))
            .andExpect(jsonPath("likesCount", equalTo(1)))
            .andExpect(jsonPath("worksCount", equalTo(1)))
            .andExpect(jsonPath("viewsCount", equalTo(1)));

    }
}
