/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.statistics.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Denis Kuchugurov
 * on 17.06.2016.
 */
@Configuration
class StatisticsModelDependencies {

    static StatisticsModelRepository statisticsModelRepository;
    static ApplicationEventPublisher eventPublisher;

    @Autowired
    public StatisticsModelDependencies(StatisticsModelRepository statisticsModelRepository,
                                       ApplicationEventPublisher eventPublisher) {
        StatisticsModelDependencies.statisticsModelRepository = statisticsModelRepository;
        StatisticsModelDependencies.eventPublisher = eventPublisher;
    }

}
