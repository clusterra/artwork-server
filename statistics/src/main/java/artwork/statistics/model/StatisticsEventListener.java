/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.statistics.model;

import artwork.commentary.domain.comment.CommentCreatedEvent;
import artwork.commentary.domain.commentable.CommentableOwnerId;
import artwork.core.domain.work.Work;
import artwork.core.domain.work.WorkCreatedEvent;
import artwork.core.domain.work.WorkLikedEvent;
import artwork.core.domain.work.WorkOwner;
import artwork.core.domain.work.WorkViewedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * Created by kepkap
 * on 10/09/16.
 */
@Component
class StatisticsEventListener {

    @Async
    @EventListener
    void handle(WorkViewedEvent event) throws UnableToCreateStatisticsForNotExistingUser {
        Work work = event.getWork();
        WorkOwner workOwner = work.getWorkOwner();

        StatisticsId statisticsId = StatisticsId.from(workOwner.getWorkOwnerId());
        Statistics statistics = Statistics.findOrCreate(statisticsId);
        statistics.incrementViewsCount();
    }

    @Async
    @EventListener
    void handle(WorkCreatedEvent event) throws UnableToCreateStatisticsForNotExistingUser {

        Work work = event.getWork();
        WorkOwner workOwner = work.getWorkOwner();

        StatisticsId statisticsId = StatisticsId.from(workOwner.getWorkOwnerId());

        Statistics statistics = Statistics.findOrCreate(statisticsId);
        statistics.incrementWorksCount();
    }

    @Async
    @EventListener
    void handle(CommentCreatedEvent event) throws StatisticsNotFoundException {

        CommentableOwnerId commentableOwnerId = event.getCommentable().getCommentableOwnerId();
        StatisticsId statisticsId = StatisticsId.from(commentableOwnerId);


        Statistics statistics = Statistics.findOne(statisticsId);
        statistics.incrementCommentsCount();
    }

    @Async
    @EventListener
    void handle(WorkLikedEvent event) throws UnableToCreateStatisticsForNotExistingUser {

        Work work = event.getWork();
        WorkOwner workOwner = work.getWorkOwner();

        StatisticsId statisticsId = StatisticsId.from(workOwner.getWorkOwnerId());

        Statistics statistics = Statistics.findOrCreate(statisticsId);
        statistics.incrementLikesCount();
    }

}
