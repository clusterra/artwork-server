/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.statistics.model;

import artwork.iam.user.model.UserId;
import artwork.iam.user.model.User;
import artwork.iam.user.model.UserNotFoundException;

import java.util.Date;

import static artwork.statistics.model.StatisticsModelDependencies.statisticsModelRepository;

/**
 * Created by
 * Denis Kuchugurov on 17/06/2017.
 */
public interface Statistics {

    static Statistics findOrCreate(StatisticsId statisticsId) throws UnableToCreateStatisticsForNotExistingUser {

        StatisticsModel model = statisticsModelRepository.findOne(statisticsId);

        if (model == null) {
            try {
                User user = User.findBy(new UserId(statisticsId.getUuid()));
                return new StatisticsModel(statisticsId, StatisticsOwner.from(user));
            } catch (UserNotFoundException e) {
                throw new UnableToCreateStatisticsForNotExistingUser(statisticsId, e);
            }

        }
        return model;
    }

    static Statistics findOne(StatisticsId statisticsId) throws StatisticsNotFoundException {
        StatisticsModel model = statisticsModelRepository.findOne(statisticsId);

        if (model == null) {
            throw new StatisticsNotFoundException(statisticsId);
        }
        return model;
    }

    StatisticsId getStatisticsId();

    StatisticsOwner getStatisticsOwner();

    long getCommentsCount();

    long getLikesCount();

    long getWorksCount();

    long getViewsCount();

    void incrementCommentsCount();

    void incrementLikesCount();

    void incrementWorksCount();

    void incrementViewsCount();

    Date getCreatedDate();

    Date getLastModifiedDate();

    Long getVersion();
}
