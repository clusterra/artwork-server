/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.statistics.model;

import artwork.iam.user.model.User;
import artwork.web.security.domain.UserProfile;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotNull;

/**
 * Created by Denis Kuchugurov
 * on 10/03/16 23:20.
 */
public class StatisticsOwner implements UserProfile {

    @NotEmpty
    private String uuid;

    @NotEmpty
    private String displayName;

    @NotNull
    @URL
    private String imageUrl;

    @NotNull
    @URL
    private String profileUrl;

    public StatisticsOwner() {
    }

    public StatisticsOwner(String uuid, String displayName, String imageUrl, String profileUrl) {
        this.uuid = uuid;
        this.displayName = displayName;
        this.imageUrl = imageUrl;
        this.profileUrl = profileUrl;
    }

    public String getUuid() {
        return uuid;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public StatisticsId getWorkOwnerId() {
        return new StatisticsId(uuid);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StatisticsOwner statisticsOwner = (StatisticsOwner) o;

        return uuid.equals(statisticsOwner.uuid);
    }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }

    @Override
    public String toString() {
        return "StatisticsOwner{" +
                "uuid=" + uuid +
                '}';
    }

    public static StatisticsOwner from(User user) {
        return new StatisticsOwner(user.getUserId().getId(), user.getDisplayName(), user.getImageUrl(), user.getProfileUrl());
    }
}
