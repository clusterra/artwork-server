/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.statistics.model;

import org.springframework.data.annotation.*;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Objects;

import static artwork.statistics.model.StatisticsModelDependencies.statisticsModelRepository;

/**
 * Created by
 * Denis Kuchugurov on 17/06/2017.
 */
@Document(collection = "statistics")
public class StatisticsModel implements Statistics {

    @CreatedDate
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date createdDate;

    @LastModifiedDate
    private Date lastModifiedDate;

    @Version
    private Long version;

    @Id
    private StatisticsId statisticsId;

    @NotNull
    @Valid
    private StatisticsOwner statisticsOwner;

    private long commentsCount;

    private long likesCount;

    private long worksCount;

    private long viewsCount;

    {
        Objects.requireNonNull(statisticsModelRepository, "check if Spring context is initialized properly");
    }

    @PersistenceConstructor
    StatisticsModel() {

    }


    public StatisticsModel(StatisticsId statisticsId, StatisticsOwner statisticsOwner) {
        this.statisticsId = statisticsId;
        this.statisticsOwner = statisticsOwner;
        statisticsModelRepository.save(this);
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public Long getVersion() {
        return version;
    }

    @Override
    public StatisticsId getStatisticsId() {
        return statisticsId;
    }

    @Override
    public StatisticsOwner getStatisticsOwner() {
        return statisticsOwner;
    }

    @Override
    public long getCommentsCount() {
        return commentsCount;
    }

    @Override
    public long getLikesCount() {
        return likesCount;
    }

    @Override
    public long getWorksCount() {
        return worksCount;
    }

    @Override
    public long getViewsCount() {
        return viewsCount;
    }

    @Override
    public void incrementLikesCount() {
        likesCount++;
        statisticsModelRepository.save(this);
    }

    @Override
    public void incrementWorksCount() {
        worksCount++;
        statisticsModelRepository.save(this);
    }

    @Override
    public void incrementViewsCount() {
        viewsCount++;
        statisticsModelRepository.save(this);
    }

    @Override
    public void incrementCommentsCount() {
        commentsCount++;
        statisticsModelRepository.save(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StatisticsModel that = (StatisticsModel) o;

        return statisticsId != null ? statisticsId.equals(that.statisticsId) : that.statisticsId == null;
    }

    @Override
    public int hashCode() {
        return statisticsId != null ? statisticsId.hashCode() : 0;
    }
}
