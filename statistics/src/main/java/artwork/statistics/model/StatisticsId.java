/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.statistics.model;

import artwork.commentary.domain.commentable.CommentableOwnerId;
import artwork.core.domain.IdGenerator;
import artwork.core.domain.work.WorkOwnerId;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.index.Indexed;

import java.io.Serializable;
import java.util.Objects;

/**
 * Created by Denis Kuchugurov
 * on 24.04.2017.
 */
public class StatisticsId implements Serializable {

    @NotEmpty
    @Indexed
    private final String uuid;

    public StatisticsId(String uuid) {
        Objects.requireNonNull(uuid);
        this.uuid = uuid;
    }


    public String getUuid() {
        return uuid;
    }

    public static StatisticsId from(WorkOwnerId workOwnerId) {
        return new StatisticsId(workOwnerId.getUuid());
    }

    public WorkOwnerId toWorkOwnerId() {
        return new WorkOwnerId(uuid);
    }

    public static StatisticsId from(CommentableOwnerId commentableOwnerId) {
        return new StatisticsId(commentableOwnerId.getUuid());
    }

    public static StatisticsId generateId() {
        return new StatisticsId(IdGenerator.generateId());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StatisticsId ownerId = (StatisticsId) o;

        return uuid.equals(ownerId.uuid);
    }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }

    @Override
    public String toString() {
        return uuid;
    }
}
