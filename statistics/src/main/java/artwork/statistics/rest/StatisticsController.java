/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.statistics.rest;

import artwork.common.rest.validation.error.ErrorsResponse;
import artwork.common.rest.validation.error.ModelError;
import artwork.statistics.model.Statistics;
import artwork.statistics.model.StatisticsId;
import artwork.statistics.model.UnableToCreateStatisticsForNotExistingUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Egor Petushkov
 * on 19.06.17.
 */
@RestController
@RequestMapping(value = "/api/users/{id}/statistics", produces = MediaTypes.HAL_JSON_VALUE)
public class StatisticsController {
    private static final Logger log = LoggerFactory.getLogger(StatisticsController.class);

    private final StatisticsResourceAssembler assembler;

    public StatisticsController(StatisticsResourceAssembler assembler) {
        this.assembler = assembler;
    }


    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<StatisticsResource> get(@PathVariable("id") StatisticsId statisticsId) throws UnableToCreateStatisticsForNotExistingUser {
        Statistics statistics = Statistics.findOrCreate(statisticsId);
        return new ResponseEntity<>(assembler.toResource(statistics), HttpStatus.OK);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorsResponse handle(UnableToCreateStatisticsForNotExistingUser e) {
        log.trace(e.getMessage(), e);
        return new ErrorsResponse(new ModelError("user_not_exists", e.getMessage()));
    }
}
