/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.statistics.rest;

import artwork.core.rest.work.WorkController;
import artwork.statistics.model.Statistics;
import artwork.statistics.model.StatisticsOwner;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by Egor Petushkov
 * on 19.06.17.
 */
@Component
public class StatisticsResourceAssembler extends ResourceAssemblerSupport<Statistics, StatisticsResource> {

    public StatisticsResourceAssembler() {
        super(StatisticsController.class, StatisticsResource.class);
    }

    @Override
    public StatisticsResource toResource(Statistics entity) {
        StatisticsOwner owner = entity.getStatisticsOwner();
        StatisticsResource resource = new StatisticsResource(
            entity.getStatisticsId().getUuid(),
            entity.getStatisticsId().getUuid(),
            owner.getDisplayName(),
            owner.getImageUrl(),
            owner.getProfileUrl(),
            entity.getCommentsCount(),
            entity.getLikesCount(),
            entity.getWorksCount(),
            entity.getViewsCount()
        );

        try {
            resource.add(linkTo(methodOn(StatisticsController.class).get(entity.getStatisticsId())).withSelfRel());
            resource.add(linkTo(methodOn(WorkController.class).getAll(entity.getStatisticsId().toWorkOwnerId(), 0, 20, "date", null)).withRel("works"));
        } catch (Exception e) {
            throw new IllegalStateException("should never happen...", e);
        }

        return resource;
    }
}
