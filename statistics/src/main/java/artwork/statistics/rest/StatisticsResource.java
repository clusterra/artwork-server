/*
 * Copyright (c) 2017 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package artwork.statistics.rest;

import org.springframework.hateoas.ResourceSupport;

/**
 * Created by Egor Petushkov
 * on 19.06.17.
 */
public class StatisticsResource extends ResourceSupport {

    public final String statisticsId;

    public final String uuid;

    public final String displayName;

    public final String imageUrl;

    public final String profileUrl;

    public final long commentsCount;

    public final long likesCount;

    public final long worksCount;

    public final long viewsCount;

    StatisticsResource(String statisticsId, String uuid, String displayName, String imageUrl, String profileUrl, long commentsCount,
                       long likesCount,
                       long worksCount,
                       long viewsCount) {
        this.statisticsId = statisticsId;
        this.uuid = uuid;
        this.displayName = displayName;
        this.imageUrl = imageUrl;
        this.profileUrl = profileUrl;
        this.commentsCount = commentsCount;
        this.likesCount = likesCount;
        this.worksCount = worksCount;
        this.viewsCount = viewsCount;
    }
}
